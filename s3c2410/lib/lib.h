#ifndef LIB_H
#define LIB_H

#include <sys/types.h>
#include <stdarg.h>

void *memset(void *, int, unsigned long);
int   isdigit(char c);
int   isxdigit(char c);
int   islower(char c);
int   isupper(char c);
int   isalpha(char c);
int   isalnum(char c);
int   isspace(char c);
char toupper(char c);
char tolower(char c);
size_t strnlen(const char *str, size_t count);

/*
#define memset(b, c, len) __builtin_memset((b), (c), (len))
#define isdigit(n)        __builtin_isdigit((n))
#define isxdigit(n)       __builtin_isxdigit((n))
#define isspace(n)        __builtin_isspace((n))
#define islower(n)        __builtin_islower((n))
#define toupper(n)        __builtin_toupper((n))
#define strcpy(d, s)      __builtin_strcpy((d), (s))
short cksum(const void *, unsigned long);
short htons(short);
short ntohs(short);
long  htonl(long);
long  ntohl(long);
int atoi(const char *p);
int htoi(char *ptr);
long htol(char *ptr);
*/
int clz(unsigned long val);

int putchar(int c);
int puts(const char *s);
int sprintf(char * buf, const char *fmt, ...);
int vsprintf(char *buf, const char *fmt, va_list args);
int printf(const char *fmt,...);

#endif /* LIB_H */
