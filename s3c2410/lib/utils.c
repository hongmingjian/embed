#include "config.h"
#include "../drivers/drivers.h"

#include <sys/types.h>
#include <stdarg.h>

#ifdef CONFIG_UART
extern void sio_putc(char c);
void sio_puts(char *s)
{
  while(*s != 0) {
    sio_putc(*s); 
    s++;
  }
}
#endif

int
strncmp(const char *s1, const char *s2, unsigned int n)
{
	unsigned char u1, u2;

	while (n-- > 0)
	{
		u1 = (unsigned char) *s1++;
		u2 = (unsigned char) *s2++;
		if (u1 != u2)
			return u1 - u2;
		if (u1 == '\0')
			return 0;
	}
	return 0;
}

void *memcpy(void *dest, void *src, unsigned long len)
{
	unsigned long i;
	char *char_dest = (char *)dest;
	char *char_src  = (char *)src;
	
	for(i = 0; i < len; i++)
		*char_dest++ = *char_src++;
		
	return dest;
}

void *
memset(void *b, int c, unsigned long len)
{
	char *p;
	p = (char *)b;

	while(len--) {
		*p++ = c;
	}
	return b;
}

#define	ISDIGIT(_c) \
	((_c) >= '0' && (_c) <= '9')

#define	ISXDIGIT(_c) \
	(ISDIGIT(_c) || \
	((_c) >= 'a' && (_c) <= 'f') || \
	((_c) >= 'A' && (_c) <= 'F'))

#define	ISLOWER(_c) \
	((_c) >= 'a' && (_c) <= 'z')

#define	ISUPPER(_c) \
	((_c) >= 'A' && (_c) <= 'Z')

#define	ISALPHA(_c) \
	(ISUPPER(_c) || \
	ISLOWER(_c))

#define	ISALNUM(_c) \
	(ISALPHA(_c) || \
	ISDIGIT(_c))

#define	ISSPACE(_c) \
	((_c) == ' ' || \
	(_c) == '\t' || \
	(_c) == '\r' || \
	(_c) == '\n')

inline int
isdigit(char c)
{
	return (ISDIGIT(c));
}


inline int
isxdigit(char c)
{
	return (ISXDIGIT(c));
}


inline int
islower(char c)
{
	return (ISLOWER(c));
}


inline int
isupper(char c)
{
	return (ISUPPER(c));
}


inline int
isalpha(char c)
{
	return (ISALPHA(c));
}


inline int
isalnum(char c)
{
	return (ISALNUM(c));
}

inline int
isspace(char c)
{
	return (ISSPACE(c));
}

size_t
strnlen(const char *str, size_t count)
{
  const char *s;

  if (str == 0)
    return 0;
  for (s = str; *s && count; ++s, count--);
  return s-str;
}

char
toupper(char c)
{
	return ((c >= 'a' && c <= 'z') ? c - 32: c);
}

char
tolower(char c)
{
	return ((c >= 'A' && c <= 'Z') ? c + 32: c);
}

int
putchar(int c)
{
    if (c == '\n')
		sio_putc('\r');
    sio_putc(c);
	return 0;
}

int
puts(const char *s)
{
	while(*s)
		putchar(*s++);
	putchar('\n');
	return 1;
}

#if 0
int
printf(const char *fmt,...)
{
	char buf[1024];
	va_list args;
	int i, j;

	va_start(args, fmt);
	i=vsprintf(buf,fmt,args);
	va_end(args);

	for(j = 0; j < i; j++)
		putchar(buf[j]);
		
	return i;
}
#else
static unsigned int
div(unsigned int x, unsigned int y, unsigned int *r)
{
	unsigned int q;
	
	for(q = 0; x >= y; x -= y, q++)
		;

	if(r)
		*r = x;
	
	return q;
}

int
printf(const char *fmt,...)
{
    static const char digits[16] = "0123456789abcdef";
	va_list ap;
	char buf[10];
	char *s;
	int c;
	unsigned int b, u;
	unsigned int q, r;

	va_start(ap, fmt);
	while ((c = *fmt++)) {
		if (c == '%') {
			c = *fmt++;
			switch (c) {
			case 'c':
				putchar(va_arg(ap, int));
				continue;
			case 's':
				for (s = va_arg(ap, char *); *s; s++)
					putchar(*s);
				continue;
			case 'u':
			case 'x':
				b = (c == 'x') ? 16U : 10U;
				u = va_arg(ap, unsigned);
				s = buf;
				do {
					q = div(u, b, &r);
					*s++ = digits[r];
				} while (u = q);
				while (--s >= buf)
					putchar(*s);
				continue;
			case 'd':
				b = 10U;
				u = va_arg(ap, int);
				s = buf;
				do {
					q = div(u, b, &r);
					*s++ = digits[r];
				} while (u = q);
				while (--s >= buf)
					putchar(*s);
				continue;
			}
		}
		putchar(c);
	}
	va_end(ap);
	return 0;
}
#endif

int clz(unsigned long x)
{
	int res = 31;
	while(!(x & (1 << res)))
	--res;
	return res;
}

#if 0
unsigned short 
eth_pkt_len(struct eth_pkt *p)
{
	unsigned short length;

	switch(p->type){
		case	FRM_IP:
					length = *(unsigned short *)&(p->data[2]) + EHT_HEADER_LENGTH;/*IP Length:Without Header Length*/	
					break;
		case 	FRM_ARP:
					length = ARP_SIZE;
					break;
		default:
					length = 0;
			#ifdef _DEBUG
					printf("CS8900:Bad Send Frame Type: %d!\n",p->type);
			#endif
					break;
		}
	return length;	
}
#endif

