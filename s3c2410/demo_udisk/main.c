/*
********************************************************************************
*
* Project ....: USB Disk
* File .......: main.c
* Authors ....: ApolloHan
* Mail........: apollohan@gmail.com
* Date .......: 08.03.07
*
********************************************************************************
*/
#include "config.h"
#include "../drivers/drivers.h"

#include <stdio.h>


/*
********************************************************************************
*                                       
* Description: The main function for project
*              The C entrance for assemble codes   
*
* Arguments  : void
*             
* Returns    : void     
*                        
********************************************************************************
*/

int main(void)
{
	init_uart(); 
	printf("\nUART initialized successfully!\n");	
	
	init_nf();
	printf("The nand flash initialized successfully!\n");
	
	init_udisk();
	printf("The USB initialized successfully!\n");        
		
    while(1);
	
	return 0;	
}


