
#include "config.h"
#include <stdio.h>

#define      INT_COUNT           52
#define     EINT_OFFSET          32

int         g_irq_nesting = -1;		     /* -1�����ж� 0����Ƕ��,>0��Ƕ��   */

void default_irq_handler(unsigned int irq)
{
    printf("WARNING: %2dth IRQ triggered without setting ISR.\n", irq);
}

static void (*ivt[INT_COUNT])(unsigned int) = {
    default_irq_handler, /*0*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*4*/ /*cascade*/
    default_irq_handler,       /*cascade*/
    default_irq_handler,       /*reserved*/
    default_irq_handler,
    default_irq_handler, /*8*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*12*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*16*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*20*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*24*//*reserved*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*28*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*32*//*reserved*/
    default_irq_handler,       /*reserved*/
    default_irq_handler,       /*reserved*/
    default_irq_handler,       /*reserved*/
    default_irq_handler, /*36*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*40*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*44*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,
    default_irq_handler, /*48*/
    default_irq_handler,
    default_irq_handler,
    default_irq_handler,   
};

void handle_irq(void)
{
	unsigned int irq_nbr = INTOFFSET;
	
	g_irq_nesting++;

	if(irq_nbr == EINT4_7 || irq_nbr == EINT8_23) {
	    unsigned int eirq_nbr = 4, eintpnd;
	    eintpnd = EINTPEND;
	    eintpnd &= 0x00fffff0;
	    if(eintpnd) {
    	    eintpnd >>= 4;
    	    
            while(!(eintpnd & 1)) {
                eintpnd >>= 1;
                eirq_nbr++;
            }
            
    		EINT_DISABLE(eirq_nbr);
    
    		((void (*)(unsigned int))(ivt[eirq_nbr + EINT_OFFSET]))(eirq_nbr + EINT_OFFSET);
    
    		EINT_ENABLE(eirq_nbr);
    		EINT_CLEAR_PENDING(eirq_nbr);
	    }
	} else {
		INT_DISABLE(irq_nbr);

		((void (*)(unsigned int))(ivt[irq_nbr]))(irq_nbr);

		INT_ENABLE(irq_nbr);
	}

    INT_CLEAR_PENDING(irq_nbr); /*XXX*/

	g_irq_nesting--;
}

void (*intr_setisr(unsigned int irq_nbr, void (*isr)(unsigned int irq_nbr)))(unsigned int)
{
    void (*old)(unsigned int) = NULL;
    
    if(irq_nbr>=INT_COUNT)
		return old;

	if(irq_nbr>=EINT_OFFSET) {
        EINT_ENABLE(irq_nbr - EINT_OFFSET);
  		EINT_CLEAR_PENDING(irq_nbr - EINT_OFFSET);
    } else {
		INT_ENABLE(irq_nbr);
        INT_CLEAR_PENDING(irq_nbr);
	}

    old = ivt[irq_nbr];
	ivt[irq_nbr] = isr;
	
	return old;
}
