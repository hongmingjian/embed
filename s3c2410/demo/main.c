
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "../drivers/drivers.h"

int main()
{
	char *p;

	/* Test UART */
	init_uart();
	printf("\nUART initialised successfully!\n");
		
	/* Test malloc */
	p = (char *)malloc(10*1024*1024);
	printf("malloc(10M) returns 0x%p\n", p);
	free(p); 

	/* Test IRQ */
	init_timer();
			
	while(1)
        ;
		
	return 0;
}
