#ifndef CONFIG_H
#define CONFIG_H

#include "board.h"

/*
********************************************************************************
* Constants
********************************************************************************
*/

/*
 * Constants for stack
 */
#define STACK_SIZE		0x1000

#define STACK_FIQ		(SDRAM_BASE+SDRAM_SIZE)
#define STACK_IRQ		(STACK_FIQ-STACK_SIZE)
#define STACK_SVC		(STACK_IRQ-STACK_SIZE)
#define STACK_ABT		(STACK_SVC-STACK_SIZE)
#define STACK_UND		(STACK_ABT-STACK_SIZE)
#define STACK_SYS		(STACK_UND-STACK_SIZE)

#define CONFIG_UART


#ifdef CONFIG_UART
#define CONSOLE_PORT	0
#define CONSOLE_SPEED	115200
#endif


#endif /*CONFIG_H*/

