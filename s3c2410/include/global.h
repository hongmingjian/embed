#ifndef GLOBAL_H
#define GLOBAL_H

#ifndef __ASSEMBLY__
#define	REGL(base, offset)	( *(volatile unsigned int *)(base + offset))
#define	REGW(base, offset)	( *(volatile unsigned short *)(base + offset))
#define	REGB(base, offset)	( *(volatile unsigned char *)(base + offset))
#else
#define	REGL(base, offset)	(base + offset)
#define	REGW(base, offset)	(base + offset)
#define	REGB(base, offset)	(base + offset)
#endif /* __ASSEMBLY__ */

#define outportb(p,d)		( ( *(volatile char *)(p) ) = (d) )
#define outportw(p,d)		( ( *(volatile unsigned short *)(p) ) = (d) )
#define outport(p,d)		( ( *(volatile unsigned long *)(p) ) = (d) )

#define inportb(p)			( *(volatile char *)(p) )
#define inportw(p)			( *(volatile unsigned short *)(p) )
#define inport(p)			  ( *(volatile unsigned long *)(p) )

#define __roundoff(x)		( (int)(x + 0.5) )
#define __countof(x)		(sizeof(x) / sizeof( (x)[0] ) )

/*
 * Copyright (c) 1991, 1993
 *      The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Berkeley Software Design, Inc.
 *
 */
#define __CONCAT1(x,y)		      x ## y
#define __CONCAT(x,y)		        __CONCAT1(x,y)
#define __STRING(x)             #x              /* stringify without expanding x */
#define __XSTRING(x)            __STRING(x)     /* expand x, then stringify */
#define __offsetof(type, field) ( (size_t)( &( (type *) 0) -> field) )

#define min(x,y)			  ( ((x) > (y) ) ? (y) : (x) )
#define max(x,y)			  ( ((x) < (y) ) ? (y) : (x) )

#define BCD2HEX(n)			( ( (n) >> 4) * 10 + ( (n) & 0x0f) )

#define TRUE    1   
#define FALSE 	0

#endif /* GLOBAL_H */
