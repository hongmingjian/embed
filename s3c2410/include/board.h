/*
 * board.h: Board specific defines
 */

#ifndef BOARD_H
#define BOARD_H

#include "s3c2410.h"

#define	XTAL		12000000
#define	PCLK		50000000
#define	HCLK		(PCLK << 1)
#define	FCLK		(HCLK << 1)

#define P_MDIV		0x5c
#define P_PDIV		0x4
#define P_SDIV		0x0

#define U_MDIV		0x48/*28 48 78*/
#define U_PDIV		0x3 /* 4  3  2*/
#define U_SDIV		0x2 /* 1  2  3*/ 

#define LED1 			(1<<4)
#define LED2 			(1<<7)

#define DW8       0x0
#define DW16      0x1
#define DW32      0x2

/*Bank 0 parameter*/
#define B0_DW	  0x0/*Nand-boot*/

#define B0_Tacs 0x0
#define B0_Tcos 0x0
#define B0_Tacc	0x7
#define B0_Tcoh	0x0
#define B0_Tcah	0x0
#define B0_Tacp	0x0
#define B0_PMC	0x0

/*Bank 1 parameter*/
#define B1_DW	DW16
#define B1_WS	0x0
#define B1_ST	0x0

#define B1_Tacs	0x0
#define B1_Tcos	0x0
#define B1_Tacc	0x7
#define B1_Tcoh	0x0
#define B1_Tcah	0x0
#define B1_Tacp	0x0
#define B1_PMC	0x0

/*Bank 2 parameter*/
#define B2_DW	DW16
#define B2_WS	0x0
#define B2_ST	0x0

#define B2_Tacs	0x0
#define B2_Tcos	0x0
#define B2_Tacc	0x7
#define B2_Tcoh	0x0
#define B2_Tcah	0x0
#define B2_Tacp	0x0
#define B2_PMC	0x0

/*Bank 3 parameter*/
#define B3_DW	DW16
#define B3_WS	0x1
#define B3_ST	0x1

#define B3_Tacs	0x0
#define B3_Tcos	0x3
#define B3_Tacc	0x7
#define B3_Tcoh	0x1
#define B3_Tcah	0x0
#define B3_Tacp	0x3
#define B3_PMC	0x0

/*Bank 4 parameter*/
#define B4_DW	DW16
#define B4_WS	0x0
#define B4_ST	0x0

#define B4_Tacs	0x0
#define B4_Tcos	0x0
#define B4_Tacc	0x7
#define B4_Tcoh	0x0
#define B4_Tcah	0x0
#define B4_Tacp	0x0
#define B4_PMC	0x0

/*Bank 5 parameter*/
#define B5_DW	DW16
#define B5_WS	0x0
#define B5_ST	0x0

#define B5_Tacs	0x0
#define B5_Tcos	0x0
#define B5_Tacc	0x7
#define B5_Tcoh	0x0
#define B5_Tcah	0x0
#define B5_Tacp	0x0
#define B5_PMC	0x0

/*Bank 6 parameter*/
#define B6_DW	DW32
#define B6_WS	0x0
#define B6_ST	0x0
#define B6_MT MT_SDRAM

#if B6_MT==MT_SDRAM
#define B6_Trcd	0x1
#define B6_SCAN	0x1
#endif /*B6_MT*/

#define B6_WBL 0x0
#define B6_TM	 0x0
#define B6_CL	 0x3
#define B6_BT	 0x0
#define B6_BL	 0x0

/*Bank 7 parameter*/
#define B7_DW	DW32
#define B7_WS	0x0
#define B7_ST	0x0
#define B7_MT MT_SDRAM

#if B7_MT==MT_SDRAM
#define B7_Trcd	0x1
#define B7_SCAN	0x1
#endif /*B7_MT*/

#define B7_WBL 0x0
#define B7_TM	 0x0
#define B7_CL	 0x3
#define B7_BT	 0x0
#define B7_BL	 0x0

/*REFRESH parameter*/
#define REFEN   0x1
#define TREFMD  0x0
#define Trp		  0x0
#define Tsrc    0x3
#define REFCNT  1268 /*Refresh period = (2048-REFCNT+1)/(HCLK/1M) (us)*/

/*BANKSIZE parameter*/
#define BURST_EN 0x1
#define SCKE_EN  0x1
#define SCLK_EN  0x1
#define BK76MAP  0x2

/*SDRAM parameter*/
#define SDRAM_BASE 0x30000000
#define SDRAM_SIZE 0x04000000

/*CS8900A parameter*/
#define CS8900_BASE 0x19000300 /*nGCS3*/
#define CS8900_BUS  B3_DW
#define CS8900_IRQ	9

/*NAND FLASH parameter*/
#define NF_TOTAL_BLOCKS     4096
#define NF_PAGES_PER_BLOCK  32
#define NF_BYTES_PER_PAGE   512
#define NF_BYTES_PER_BLOCK  (NF_PAGES_PER_BLOCK * NF_BYTES_PER_PAGE)
#define NF_TOTAL_PAGES      (NF_TOTAL_BLOCKS * NF_PAGES_PER_BLOCK)
#define NF_TOTAL_BYTES      (NF_TOTAL_PAGES  * NF_BYTES_PER_PAGE)

#endif /*BOARD_H*/
