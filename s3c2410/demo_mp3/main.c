#include <stdlib.h>
#include <stdio.h>
#include "mad.h"
#include "config.h"
#include "../drivers/drivers.h"


int fat_read_sect512(unsigned long logic_page, char *page_buf)
{
	return nf_read_page(logic_page + NF_PAGES_RESERVED, page_buf);
}

/*
 * This is a private message structure. A generic pointer to this structure
 * is passed to each of the callback functions. Put here any data you need
 * to access from within the callbacks.
 */

struct cb_data {
    int fd;

    /*2281 comes from here(http://www.mars.org/pipermail/mad-dev/2002-January/000425.html)*/
    unsigned char buf[2281];
    unsigned char dummy[MAD_BUFFER_GUARD];
};

/*
 * This is the input callback. The purpose of this callback is to (re)fill
 * the stream buffer which is to be decoded. In this example, an entire file
 * has been mapped into memory, so we just call mad_stream_buffer() with the
 * address and length of the mapping. When this callback is called a second
 * time, we are finished decoding.
 */

static
enum mad_flow input(void *data, struct mad_stream *stream)
{
    struct cb_data *p = data;
    unsigned int unconsumedBytes = 0;
    int read;

    if(stream->next_frame) {
        unconsumedBytes = (p->buf + sizeof(p->buf)) - stream->next_frame;
        memmove(p->buf, stream->next_frame, unconsumedBytes);
    }

    read = sizeof(p->buf)-unconsumedBytes;

    if(read <= 0)
        /*This means the size of p->buf is too small. Is it possible?*/
        return MAD_FLOW_BREAK;

    read = fat_read(p->fd, p->buf + unconsumedBytes, read);

    if(read <= 0)
        return MAD_FLOW_STOP;

    mad_stream_buffer(stream, p->buf, read + unconsumedBytes);

    return MAD_FLOW_CONTINUE;
}


struct audio_stats {
  unsigned long clipped_samples;
  mad_fixed_t peak_clipping;
  mad_fixed_t peak_sample;
};

struct audio_dither {
  mad_fixed_t error[3];
  mad_fixed_t random;
};

/*
 * NAME:	clip()
 * DESCRIPTION:	gather signal statistics while clipping
 */
static inline
void clip(mad_fixed_t *sample, struct audio_stats *stats)
{
  enum {
    MIN = -MAD_F_ONE,
    MAX =  MAD_F_ONE - 1
  };

  if (*sample >= stats->peak_sample) {
    if (*sample > MAX) {
      ++stats->clipped_samples;
      if (*sample - MAX > stats->peak_clipping)
	stats->peak_clipping = *sample - MAX;

      *sample = MAX;
    }
    stats->peak_sample = *sample;
  }
  else if (*sample < -stats->peak_sample) {
    if (*sample < MIN) {
      ++stats->clipped_samples;
      if (MIN - *sample > stats->peak_clipping)
	stats->peak_clipping = MIN - *sample;

      *sample = MIN;
    }
    stats->peak_sample = -*sample;
  }
}

/*
 * NAME:	audio_linear_round()
 * DESCRIPTION:	generic linear sample quantize routine
 */
# if defined(_MSC_VER)
extern  /* needed to satisfy bizarre MSVC++ interaction with inline */
# endif
static inline
signed long audio_linear_round(unsigned int bits, mad_fixed_t sample,
			       struct audio_stats *stats)
{
  /* round */
  sample += (1L << (MAD_F_FRACBITS - bits));

  /* clip */
  clip(&sample, stats);

  /* quantize and scale */
  return sample >> (MAD_F_FRACBITS + 1 - bits);
}

/*
 * NAME:	prng()
 * DESCRIPTION:	32-bit pseudo-random number generator
 */
static inline
unsigned long prng(unsigned long state)
{
  return (state * 0x0019660dL + 0x3c6ef35fL) & 0xffffffffL;
}

/*
 * NAME:	audio_linear_dither()
 * DESCRIPTION:	generic linear sample quantize and dither routine
 */
# if defined(_MSC_VER)
extern  /* needed to satisfy bizarre MSVC++ interaction with inline */
# endif
static inline
signed long audio_linear_dither(unsigned int bits, mad_fixed_t sample,
				struct audio_dither *dither,
				struct audio_stats *stats)
{
  unsigned int scalebits;
  mad_fixed_t output, mask, random;

  enum {
    MIN = -MAD_F_ONE,
    MAX =  MAD_F_ONE - 1
  };

  /* noise shape */
  sample += dither->error[0] - dither->error[1] + dither->error[2];

  dither->error[2] = dither->error[1];
  dither->error[1] = dither->error[0] / 2;

  /* bias */
  output = sample + (1L << (MAD_F_FRACBITS + 1 - bits - 1));

  scalebits = MAD_F_FRACBITS + 1 - bits;
  mask = (1L << scalebits) - 1;

  /* dither */
  random  = prng(dither->random);
  output += (random & mask) - (dither->random & mask);

  dither->random = random;

  /* clip */
  if (output >= stats->peak_sample) {
    if (output > MAX) {
      ++stats->clipped_samples;
      if (output - MAX > stats->peak_clipping)
	stats->peak_clipping = output - MAX;

      output = MAX;

      if (sample > MAX)
	sample = MAX;
    }
    stats->peak_sample = output;
  }
  else if (output < -stats->peak_sample) {
    if (output < MIN) {
      ++stats->clipped_samples;
      if (MIN - output > stats->peak_clipping)
	stats->peak_clipping = MIN - output;

      output = MIN;

      if (sample < MIN)
	sample = MIN;
    }
    stats->peak_sample = -output;
  }

  /* quantize */
  output &= ~mask;

  /* error feedback */
  dither->error[0] = sample - output;

  /* scale */
  return output >> scalebits;
}

static inline
signed int scale(mad_fixed_t sample)
{
  /* round */
  sample += (1L << (MAD_F_FRACBITS - 16));

  /* clip */
  if (sample >= MAD_F_ONE)
    sample = MAD_F_ONE - 1;
  else if (sample < -MAD_F_ONE)
    sample = -MAD_F_ONE;

  /* quantize */
  return sample >> (MAD_F_FRACBITS + 1 - 16);
}

static volatile int bufcnt;

static void write_cb(void *p, int size, int count)
{
    long cpsr;
	save_flag_cli(cpsr);
	bufcnt++;
	restore_flag(cpsr);
}

/*
 * This is the output callback function. It is called after each frame of
 * MPEG audio data has been completely decoded. The purpose of this callback
 * is to output (or play) the decoded PCM audio.
 */
static
enum mad_flow output(void *data,
		     struct mad_header const *header,
		     struct mad_pcm *pcm)
{
#define MAX_RESAMPLEFACTOR	6
#define MAX_NSAMPLES		(1152 * MAX_RESAMPLEFACTOR)
#define NBUF                8/*XXX - should be enough*/

	static struct audio_stats  Audio_stats;
	static struct audio_dither Audio_dither;

	static int first = 1;
    static char *bufs[NBUF];
    static int bufid;

    long cpsr;
	char *pout;
	int i;

	if(first) {
    	dma_device_t *device;

        device = dma_get_device(2, 1);
        if(device == NULL)
            return MAD_FLOW_BREAK;
        device->callback = write_cb;

        for(i = 0 ;i < NBUF; i++)
            bufs[i] = (char *)malloc(MAX_NSAMPLES * 4 * 2);

        bufcnt = NBUF;

        first = 0;
	}

    led_on (LED2);

    /*XXX - busy waiting */
    while(bufcnt == 0)
        ;

    led_off (LED2);

    pout = bufs[bufid];
	for(i = 0; i < pcm->length; i++) {
		signed int tmp;
/*		tmp = scale(pcm->samples[0][i]);
		tmp = audio_linear_round (16, pcm->samples[0][i],&Audio_stats);*/
        tmp = audio_linear_dither(16, pcm->samples[0][i],&Audio_dither, &Audio_stats);

		*(pout++) = tmp; *(pout++) = (tmp >> 8);

		if(pcm->channels == 2) {
/*    		tmp = scale(pcm->samples[1][i]);
			tmp = audio_linear_round (16, pcm->samples[1][i],&Audio_stats); */
      	    tmp = audio_linear_dither(16, pcm->samples[1][i],&Audio_dither, &Audio_stats);
			*(pout++) = tmp; *(pout++) = (tmp >> 8);
		}
	}

	iis_play(pcm->length, pcm->samplerate, pcm->channels, 16, bufs[bufid]);

	bufid++;
	if(bufid == NBUF)
	    bufid = 0;

	save_flag_cli(cpsr);
	bufcnt--;
	restore_flag(cpsr);

    return MAD_FLOW_CONTINUE;
}

/*
 * This is the error callback function. It is called whenever a decoding
 * error occurs. The error is indicated by stream->error; the list of
 * possible MAD_ERROR_* errors can be found in the mad.h (or stream.h)
 * header file.
 */
static
enum mad_flow error(void *data,
		    struct mad_stream *stream,
		    struct mad_frame *frame)
{
/*
    struct cb_data *p = data;
    printf("decoding error 0x%04x (%s) at byte offset %u\n",
        stream->error, mad_stream_errorstr(stream), stream->this_frame - p->buf);
*/
    return MAD_FLOW_CONTINUE;
}

/*
********************************************************************************
*
* Description: The main function for project
*              The C entrance for assemble codes
*
* Arguments  : void
*
* Returns    : void
*
********************************************************************************
*/

int main(void)
{
    struct eth_addr localmac = {{0x00,0x05,0x5d,0xe3,0x2c,0xfc}};
    struct mad_decoder decoder;
    struct cb_data cd;
	int type, length;
	char file[260];
	char *pdot;
	char *exts[] = {"wav", "mp3"};

	init_led();

	init_uart();
    printf("\n\nWelcome to EPMP3 ("__DATE__" "__TIME__")\n\n");

    printf("Calibrating delay loop....");
    init_delay();
    printf("%lu.%02lu BogoMIPS.\n",
                loops_per_tick/(500000/HZ),
                loops_per_tick/(5000/HZ) % 100);

    printf("Initialising DMA .........");
    init_dma();
    printf("Done.\n");

    printf("Initialising NAND flash...");
	init_nf();
    printf("Done.\n");

    printf("Initialising UDA1341 .....");
    init_uda1341ts(0);
    printf("Done.\n");

    printf("Initialising IIS .........");
    if(init_iis(0) < 0)
        printf("Failed.\n");
    else
        printf("Done.\n");

    printf("Initialising UDISK .......");
	init_udisk();
    printf("Done.\n");

    printf("Initialising FAT .........");
	init_fat();
    printf("Done.\n");

    printf("Initialising CS8900 ......");
    if(init_cs8900(&localmac, 0) < 0)
        printf("Failed.\n");
    else
        printf("Done.\n");

	init_timer(); /*heartbeat*/

    printf("\n");

    mad_decoder_init(&decoder, &cd,
		   input, 0 /* header */, 0 /* filter */, output,
		   error, 0 /* message */);

again:
    printf("Please input a filename: ");
    scanf("%s", file);

    pdot = strrchr(file, '.');

    if(pdot == NULL)
        goto again;

    pdot++;

    for(type = 0; type < __countof(exts); type++)
        if(0 == strcasecmp(pdot, exts[type]))
            break;
    if(type == __countof(exts))
        goto again;

	cd.fd = fat_open(file, O_RDONLY);

    if(cd.fd < 0)
        goto again;

    length = fat_getlen(cd.fd);
    printf("Playing `%s'(%d bytes)....\b \b", file, length);
    fflush(stdout);
    switch(type) {
    	case 0:
    	    {
        	    char *where = malloc(length);
        	    if(where != NULL) {
            		struct WAVE {
            			int ChunkID;
            			int ChunkSize;
            			int Format;
            			int Subchunk1ID;
            			int Subchunk1Size;
            			short AudioFormat;
            			short NumChannels;
            			int SampleRate;
            			int ByteRate;
            			short BlockAlign;
            			short BitsPerSample;
            			int Subchunk2ID;
            			int Subchunk2Size;
            		} __attribute__((packed)) *hdr = (void *)where;

        	        if(length == fat_read(cd.fd, where, length)) {
                		iis_play(hdr->Subchunk2Size/hdr->NumChannels/(hdr->BitsPerSample/8),
                				 hdr->SampleRate,
                				 hdr->NumChannels,
                				 hdr->BitsPerSample,
                				 where+sizeof(struct WAVE));
                	}
                    free(where);
                }
            }
            break;
    	case 1:
            mad_decoder_run(&decoder, MAD_DECODER_MODE_SYNC);
    	    break;
    }

    printf("Done.\n");

	fat_close(cd.fd);

	goto again;
    mad_decoder_finish(&decoder);

	return 0;
}

