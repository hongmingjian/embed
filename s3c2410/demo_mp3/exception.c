/*
 * exception.c: handler exceptions
 */
#include <stdio.h>

#include "frame.h"

/*
 * Print context frame
 */

static 
void print_context_frame(struct contextframe *cf)
{
    printf("========= Context Frame ==========\n");
    printf("  CPSR: 0x%x\n", cf->cf_spsr);
    printf("  R0  : 0x%x\n", cf->cf_r0);
    printf("  R1  : 0x%x\n", cf->cf_r1);
    printf("  R2  : 0x%x\n", cf->cf_r2);
    printf("  R3  : 0x%x\n", cf->cf_r3);
    printf("  R4  : 0x%x\n", cf->cf_r4);
    printf("  R5  : 0x%x\n", cf->cf_r5);
    printf("  R6  : 0x%x\n", cf->cf_r6);
    printf("  R7  : 0x%x\n", cf->cf_r7);
    printf("  R8  : 0x%x\n", cf->cf_r8);
    printf("  R9  : 0x%x\n", cf->cf_r9);
    printf("  R10 : 0x%x\n", cf->cf_r10);
    printf("  R11 : 0x%x\n", cf->cf_r11);
    printf("  R12 : 0x%x\n", cf->cf_r12);
    printf("  USR_SP  : 0x%x\n", cf->cf_usr_sp);
    printf("  USR_LR  : 0x%x\n", cf->cf_usr_lr);
    printf("  SVC_SP  : 0x%x\n", cf->cf_svc_sp);
    printf("  SVC_LR  : 0x%x\n", cf->cf_svc_lr);
    printf("  PC  : 0x%x\n", cf->cf_pc);
    printf("==================================\n");
}

/*
 * Hanlder unexpected exception
 */
void default_exception_handler(struct contextframe *cf)
{
    /*
     * Print the context frame and die here
     */
    printf("+================================+\n");
    printf("| !UNEXPECTED EXCEPTION OCCURED! |\n");
    printf("+================================+\n");
    print_context_frame(cf);
    printf("System halted.\n");
    while (1);
}

