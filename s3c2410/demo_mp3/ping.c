#include "config.h"
#include "../drivers/drivers.h"

static void memswp(char *p1, char *p2, int len)
{
    int i;
	char c;
	for(i = 0; i < len; i++) {
		c=p1[i];
		p1[i]=p2[i];
		p2[i]=c;
	}
}

struct ip_addr localip={{192,168,0,138}};

void eth_in(struct eth_header *eth, int pktlen)
{
	struct arp_header *arp;
	struct ip_header  *ip;
	struct icmp_header *icmp;

	int datalen;
	
	switch(ntohs(eth->eth_type)) {
	case ETH_TYPE_IP:
		ip = (struct ip_header *)eth->eth_data;
		datalen = ntohs(ip->ip_len);
		switch(ip->ip_proto) {
		case IP_PROTO_ICMP:
		    icmp = ip->ip_data;
		    if(icmp->icmp_type != ICMP_TYPE_ECHO_REQUEST)
		        return;

            icmp->icmp_type = ICMP_TYPE_ECHO_REPLY;
            icmp->icmp_cksum = 0;
            icmp->icmp_cksum = cksum(icmp, datalen-IP_HLEN(ip));

			memswp(ip->ip_shost.addr, ip->ip_dhost.addr, sizeof(ip->ip_shost.addr));
			ip->ip_ttl = IP_DEFAULT_TTL;
			ip->ip_cksum = 0;
			ip->ip_cksum = cksum(ip, IP_HLEN(ip));

			memswp(eth->eth_snode.addr, eth->eth_dnode.addr, sizeof(eth->eth_snode.addr));
			eth_out(eth, max(pktlen, ETH_MIN_PACKET_LENGTH));
			break;
		}
		break;

	case ETH_TYPE_ARP:
		arp = (struct arp_header *)eth->eth_data;
		if(0 != memcmp(arp->arp_dhost.addr, localip.addr, sizeof(arp->arp_dhost.addr)))
			return;
		switch(ntohs(arp->arp_opcode)) {
		case ARP_REQUEST:
            arp->arp_opcode = htons(ARP_REPLY);
			memswp(arp->arp_shost.addr, arp->arp_dhost.addr, sizeof(arp->arp_shost.addr));
			memswp(arp->arp_snode.addr, arp->arp_dnode.addr, sizeof(arp->arp_snode.addr));
			cs8900_get_addr(&arp->arp_snode);
			
			memswp(eth->eth_snode.addr, eth->eth_dnode.addr, sizeof(eth->eth_snode.addr));
			cs8900_get_addr(&eth->eth_snode);
			eth_out(eth, pktlen);
			break;
		}
		break;
	}
}
