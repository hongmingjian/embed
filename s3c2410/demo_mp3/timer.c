/*
 * timer.c: funcitons implement for timer
 */
#include "config.h"
#include "../drivers/drivers.h"

#include <stdio.h>

volatile unsigned ticks;

static void
timer_isr(unsigned int irq)
{
	ticks++;
	if(ticks & 1) {
		led_on (LED1);
	} else {
		led_off(LED1);
	}
}

void
init_timer(void)
{
	TCFG0 = 0xff << 8;
	TCFG1 = 0x3 << 16;
	TCNTB4 = (PCLK / (255+1) / 16) / 1;	
	TCON = 0x6 << 20; /*update TCNTBn*/	
	TCON = 0x5 << 20; /*kick it off*/
	
	intr_setisr(INT_TIMER4, timer_isr);	
}

