#include "config.h"
#include "drivers.h"

#ifdef CONFIG_ENDIAN

short
htons(short x)
{
#ifdef __BIG_ENDIAN__	
	return x;
#else
	unsigned char *s = (unsigned char *)&x;
	return (short)(s[0] << 8 | s[1]);
#endif
}

long
htonl(long x)
{
#ifdef __BIG_ENDIAN__	
	return x;
#else
	unsigned char *s = (unsigned char *)&x;
	return (long)(s[0] << 24 | s[1] << 16 | s[2] << 8 | s[3]);
#endif
}

long
ntohl(long x)
{
#ifdef __BIG_ENDIAN__	
	return x;
#else
	unsigned char *s = (unsigned char *)&x;
	return (long)(s[0] << 24 | s[1] << 16 | s[2] << 8 | s[3]);
#endif
}

short
ntohs(short x)
{
#ifdef __BIG_ENDIAN__	
	return x;
#else
	unsigned char *s = (unsigned char *)&x;
	return (short)(s[0] << 8 | s[1]);
#endif
}

#endif/*CONFIG_ENDIAN*/