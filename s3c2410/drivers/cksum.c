#include "config.h"
#include "drivers.h"

#ifdef CONFIG_CKSUM
short cksum(const void *b, unsigned long len)
{
	unsigned long sum;
	unsigned short *p;

	sum = 0;
	p = (unsigned short *)b;

	while(len > 1) {
		sum += *p++;
		len -= 2;
	}

	if(len)
		sum += *(unsigned char *)p;

	sum = (sum & 0xffff) + (sum >> 16);
	sum += (sum >> 16);

	return ~sum;
}
#endif/*CONFIG_CKSUM*/