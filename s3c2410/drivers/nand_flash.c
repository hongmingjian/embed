#include "config.h"
#include "drivers.h"

#ifdef CONFIG_NF
/*
********************************************************************************
*
* Project ....: NAND Flash
* File .......: nand_flash.c
* Authors ....: ApolloHan
* Mail........: apollohan@gmail.com
* Date .......: 08.03.25
*
********************************************************************************
*/

/*
********************************************************************************
* Constants for NAND Flash K9F1208U0B
********************************************************************************
*/

#define TACLS   0x00
#define TWRPH0  0x03
#define TWRPH1  0x00

/*
********************************************************************************
* Micro Function for NAND Flash K9F1208U0B
********************************************************************************
*/

/*
 * Send command
 */
#define NF_CMD(cmd)      {NFCMD = cmd; }

/*
 * Set address
 */
#define NF_ADDR(addr)    {NFADDR = addr; }

/*
 * NAND Flash memory chip enable
 */
#define NF_nFCE_L()      {NFCONF &= ~(1 << 11); }

/*
 * NAND Flash memory chip disable
 */
#define NF_nFCE_H()      {NFCONF |= (1 << 11); }

/*
 * Initialize ECC
 */
#define NF_INIT_ECC()    {NFCONF |= (1 << 12); }

/*
 * Read data
 */
#define NF_RD_DATA()     (NFDATA)

/*
 * Write data
 */
#define NF_WR_DATA(data) {NFDATA = data; }

/*
 * Delay for NAND Flash ready
 */
#define NF_DELAY()       {while( !(NFSTAT & 1 ) ); }

/*
********************************************************************************
*                                       
* Description: Get the ID of NAND Flash K9F1208U0B  
*
* Arguments  : void
*             
* Returns    : id          is the unique ID     
*                        
********************************************************************************
*/
#if 0
static  short 
nf_get_ID(void)
{
  int i;
   short id;
 
  NF_nFCE_L(); 
  NF_CMD(0x90); 
  NF_ADDR(0x0);
 
  for(i = 0; i < 10; i++);                        /* wait tWB(100ns) */
 
  id = NF_RD_DATA() << 8;                         /* maker code(K9S1208V:  0xec) */
  id |= NF_RD_DATA();                             /* devide code(K9S1208V: 0x76) */
  NF_nFCE_H();  
 
  return id;
}
#endif
/*
********************************************************************************
*                                       
* Description: Reset NAND Flash   
*
* Arguments  : void
*             
* Returns    : void    
*                        
********************************************************************************
*/

static void 
nf_reset(void)
{
  int i;

  NF_nFCE_L();  
  NF_CMD(0xFF);                                   /* reset command */
  
//  for(i = 0; i < 10; i++);                        /* wait tWB(100ns) */
  
  NF_DELAY();                                     /* waiting for NAND Flash ready */
  NF_nFCE_H();  
}

/*
********************************************************************************
*                                       
* Description: Initialize NAND Flash configuration
*
* Arguments  : void
*             
* Returns    : void    
*                        
********************************************************************************
*/

void 
init_nf(void)
{
  NFCONF = (1 << 15) | (1 << 14) | (1 << 13)
           | (1 << 12) | (1 << 11) | (TACLS << 8)
           | (TWRPH0 << 4) | (TWRPH1 << 0); 
 
  nf_reset();
}

/*
********************************************************************************
*                                       
* Description: Read data from NAND Flash 
*
* Arguments  : block    	is the address we start reading
*              page   		is the address we store our reading data
*              page_buf 	is the size of data we want to read
*             
* Returns    : 0			read succeed
* 			   -1			read failed  
*                        
********************************************************************************
*/
#ifdef NF_READ
int nf_read_page(unsigned int page_offset, char *page_buf)
{
  int i;
  
  if(page_offset >= NF_TOTAL_PAGES)
  	return -1;
  
   NF_nFCE_L();     
    
   NF_CMD(0x00);   
    
   NF_ADDR(0x0);                    			/* column address */
   NF_ADDR( (page_offset >> 0)  & 0xFF);           /* page address   */
   NF_ADDR( (page_offset >> 8)  & 0xFF);           /* page address   */
   NF_ADDR( (page_offset >> 16) & 0xFF);           /* page address   */
   
//   for(i = 0; i < 10; i++);                      /* wait tWB(100ns) */
    
   NF_DELAY();                                   /* waiting for NAND Flash ready */
 
   /*
    * Read from main area
    */
   for(i = 0; i < NF_BYTES_PER_PAGE; i++)
   {
     *(page_buf++) = NF_RD_DATA();
   }
    
   NF_nFCE_H();    
  
  return 0;
}
#endif
/*
********************************************************************************
*                                       
* Description: Erase data in NAND Flash  
*
* Arguments  : block       is the address of block we start erasing
*
* Returns    : 0           erase succeed
*              -1          erase fail
*                        
********************************************************************************
*/
#ifdef NF_WRITE
int
nf_erase_blk(unsigned int page_offset) 
{ 
   char status;
  int i;
  
  if(page_offset & 0x1f)
  	return -1;
	
  NF_nFCE_L();
  NF_CMD(0x60);     
	NF_ADDR( (page_offset >> 0)  & 0xFF);  
	NF_ADDR( (page_offset >> 8)  & 0xFF);
	NF_ADDR( (page_offset >> 16) & 0xFF);
	
//  for(i = 0; i < 1; i++);		                      /* wait tWC(10ns) */  
	
	NF_CMD(0xD0);  
//	for(i = 0; i < 1; i++);
	NF_DELAY() 					                    

  NF_CMD(0x70);
//	for(i = 0; i < 1; i++);                                                    
	
	status = NF_RD_DATA();
	if (status & 0x1)                               /* erase error */
	{
	  NF_nFCE_H();
		return -1;
	}
	else 
	{
		NF_nFCE_H();  
		return 0;
	}
}

/*
********************************************************************************
*                                       
* Description: Erase data in NAND Flash  
*
* Arguments  : block       is the address of block we start writing
*              page        is the page address on the block
*              
*
* Returns    : 0           write succeed
*              -1          write fail  
*                        
********************************************************************************
*/

int nf_write_page(unsigned int page_offset, char *page_buf)  
{
	 char status;
	char *source;                          
	int i;
	
	if(page_offset >= NF_TOTAL_PAGES)
		return -1;
		
	source = page_buf;
	
	NF_nFCE_L();
	NF_CMD(0x80);
  NF_ADDR(0x0);
	NF_ADDR( (page_offset >>  0) & 0xFF);
	NF_ADDR( (page_offset >>  8) & 0xFF);
	NF_ADDR( (page_offset >> 16) & 0xFF);
	
//	for(i = 0; i < 10; i++);
	NF_DELAY();
	
	for(i = 0; i < NF_BYTES_PER_PAGE; i++)
	{
	 	NF_WR_DATA( *(source++) );	 	
	}                                               
		
	NF_CMD(0x10);
//	for(i = 0; i < 1; i++);                               
	NF_DELAY();	                                    
  
	NF_CMD(0x70);	
//	for(i = 0; i < 1; i++);
	NF_DELAY();
	
	status = NF_RD_DATA();                             
	if(status & 0x1)		
	{
		NF_nFCE_H();
		return -1;
	}
	else
	{		
	  NF_nFCE_H();
		return 0;
	}
}
#endif
#endif
