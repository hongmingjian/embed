#ifndef DRIVERS_H
#define DRIVERS_H

#ifndef CONFIG_H
#error "You must include config.h first."
#endif

#include <sys/types.h>
#include <stdarg.h>

#include "fat/fat.h"
#include "dma/dma.h"

extern void (*intr_setisr(unsigned int irq_nbr, void (*isr)(unsigned int irq_nbr)))(unsigned int);

#ifdef CONFIG_DELAY
extern unsigned long loops_per_tick;
void init_delay (void);
void __delay(unsigned long);
#endif

#ifdef CONFIG_UART
void init_uart();
int  sio_ischar();
int  sio_oschar();
char sio_getc();
void sio_putc(char c);
#endif

#ifdef CONFIG_XMODEM
int xmodem(char *p);
#endif

#ifdef CONFIG_UUDECODE
int uudecode (char *where);
#endif

#ifdef CONFIG_NF
void init_nf();

#ifdef NF_READ
int nf_read_page(unsigned int page_offset, char *page_buf);
#endif

#ifdef NF_WRITE
int nf_erase_blk(unsigned int page_offset);
int nf_write_page(unsigned int page_offset, char *page_buf);
#endif
#endif

#ifdef CONFIG_LED
void init_led();
void led_on(unsigned int led);
void led_off(unsigned int led);
#endif

#ifdef CONFIG_UDA1341
void init_uda1341ts(int mode);
#endif

#ifdef CONFIG_IIS
#define IIS_CLOCK_FREQ	384
int init_iis(int mode);
void iis_play(int nsamples, int samplerate, int channels, int bpc, void *data);
#endif

#ifdef CONFIG_UDISK
void init_udisk();
#endif

#ifdef CONFIG_ENDIAN
short htons(short x);
long htonl(long x);
long ntohl(long x);
short ntohs(short x);
#endif

#ifdef CONFIG_CKSUM
short cksum(const void *b, unsigned long len);
#endif

#ifdef CONFIG_CS8900
#define ETH_MIN_PACKET_LENGTH	60
#define ETH_MAX_PACKET_LENGTH	1514
#define ETH_ADDR_LENGTH			6
struct eth_addr{char addr[ETH_ADDR_LENGTH];} __attribute__ ((packed));
struct eth_header {
	struct eth_addr eth_dnode;	
	struct eth_addr eth_snode;	
	short    eth_type;	
#define ETH_TYPE_ARP			0x0806
#define ETH_TYPE_IP				0x0800

	char     eth_data[0];
} __attribute__ ((packed));

#define IP_ADDR_LENGTH	4
struct ip_addr{char addr[IP_ADDR_LENGTH];} __attribute__ ((packed));
struct ip_header {
	char       ip_verlen;
#define IP_HLEN(pih)	(((pih)->ip_verlen & 0xf) << 2)

	char       ip_tos;
	short      ip_len;
	short      ip_id;
	short      ip_fragoff;
	char       ip_ttl;
#define IP_DEFAULT_TTL	64

	char       ip_proto;
#define IP_PROTO_ICMP 1
#define IP_PROTO_UDP  17

	short      ip_cksum;
	struct ip_addr    ip_shost;
	struct ip_addr    ip_dhost;
	char       ip_data[0];
} __attribute__ ((packed));

struct arp_header {
	unsigned short arp_htype;
#define ARP_HTYPE_ETH	1

	unsigned short arp_ptype;
#define ARP_PTYPE_IP	0x0800
	
	unsigned char  arp_hlen;
	unsigned char  arp_plen;
	unsigned short arp_opcode;
#define ARP_REQUEST		1
#define ARP_REPLY		2

	struct eth_addr       arp_snode;
	struct ip_addr        arp_shost;
	struct eth_addr       arp_dnode;
	struct ip_addr        arp_dhost;
} __attribute__ ((packed));

struct icmp_header {
	char  icmp_type;
#define ICMP_TYPE_ECHO_REPLY		0
#define ICMP_TYPE_ECHO_REQUEST		8

	char  icmp_code;
	short icmp_cksum;
	char  icmp_data0[4];
	char  icmp_data[0];
} __attribute__ ((packed));

struct udp_header {
	unsigned short	udp_sport;
	unsigned short	udp_dport;
	unsigned short	udp_len;
	unsigned short	udp_cksum;
	char			udp_data[0];
} __attribute__ ((packed));

int init_cs8900(struct eth_addr *, int mode);
void cs8900_get_addr(struct eth_addr *);
void cs8900_set_addr(struct eth_addr *);
       void eth_out(struct eth_header *pkt, int pktlen);
extern void eth_in (struct eth_header *pkt, int pktlen);
#endif

#endif/*DRIVERS_H*/
