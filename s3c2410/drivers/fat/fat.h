/*
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * 本文件定义FAT文件系统的所有接口函数的定义
 */

/*------------------------------------------------------------------------------
 * 项目名 ....: FAT文件系统设计与实现
 * 文件名 ....: fat.h
 * 程序员 ....: 龙云
 * 指导者 ....: 洪明坚
 * 团  队 ....: 嵌入式开发小组
 * 组  织 ....: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * 时  间 ....: 2009/3/22
 */

#include "config.h"

#ifdef CONFIG_FAT

#ifdef FAT_DIR

typedef void DIR; 

struct dirent {
	long offset;
	char type;
	char name[13];
};
#endif

/* FAT文件系统基础模块 */
void init_fat();

/* Flag definition */
#define O_RDONLY		1
#define O_WRONLY		2
#define O_RDWR			(O_RDONLY | O_WRONLY)
int  fat_open (const char *pathname, int flag);

int  fat_close(int fd);
int  fat_getlen(int fd);

/* whence definition */
#define SEEK_SET		0
#define SEEK_CUR		1
#define SEEK_END		2
int  fat_lseek(int fd, unsigned int offset, int whence);

/* FAT文件系统读功能 */
#ifdef FAT_READ
int  fat_read(int fd, void *buf, unsigned int count);
#endif

/* FAT文件系统文件夹操作功能 */
#ifdef FAT_DIR

int  fat_mkdir(const char *pathname);
int  fat_rmdir(const char *pathname);

DIR* opendir(const char *pathname);
int    closedir(DIR *dir_stream);
struct dirent* readdir(DIR *dir_stream);
void  rewinddir(DIR *dir_stream);
void  seekdir(DIR *dir_stream, long offset);
long  telldir(DIR *dir_stream);
#endif

/* FAT文件系统写功能 */
#ifdef FAT_WRITE
int  fat_creat(const char *pathname, int attr);
int  fat_write(int fd, void *buf, unsigned int count);
int  fat_remove(const char *pathname);
int  fat_rename(char *oldname, char *newname)
#endif



#endif
