/*
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * 本文件实现FAT文件系统的所有接口函数，FAT文件系统的内部实现，并定义FAT文件系统
 * 的相关数据结构
 * 
 * ==============================================================================
 * 本fat文件系统依赖如下的函数(需在fat文件系统外实现)：
 * int strlen(char *s);
 * int toupper(int c);
 * void *memset(void *s, char ch, unsigned n);
 * void *memcpy(void *dest, void *src, unsigned int count);
 * int memcmp(void *buf1, void *buf2, unsigned int count);
 * int fat_read_sect512(unsigned long logic_page, char *page_buf);
 * int fat_write_sect512(unsigned long logic_page, char *page_buf);
 * int printf(const char *format, ...);(在调试中使用，即定义了DEBUG_FAT)
 * ==============================================================================
 */

/*------------------------------------------------------------------------------
 * 项目名 ....: FAT文件系统设计与实现
 * 文件名 ....: fat.c
 * 程序员 ....: 龙云
 * 指导者 ....: 洪明坚
 * 团  队 ....: 嵌入式开发小组
 * 组  织 ....: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * 时  间 ....: 2009/3/22
 */
 
#include "fat.h"

#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#ifdef DEBUG_FAT
    #include <stdio.h>
	#define dprintf	printf
#else
	#define dprintf
#endif

#ifdef CONFIG_FAT

/*
================================================================================
  类型声明
================================================================================
*/
#ifndef BYTE
typedef unsigned char BYTE;
typedef unsigned char U8;
#endif

#ifndef CHAR
typedef char CHAR;
#endif

#ifndef WORD
typedef unsigned short WORD;
#endif

#ifndef DWORD
typedef unsigned long DWORD;
#endif

/*
================================================================================
  FAT16文件系统相关结构体
================================================================================
*/

/* Boot Sector，即MBR的数据结构 */
struct BOOTSECT {
    BYTE    BS_jmpBoot[3];               /* jump inst E9xxxx or EBxx90 */
    char    BS_OEMName[8];               /* OEM name and version */
    char    BS_BPB[25];                  /* BIOS parameter block */
    char    BS_EXT[54];                  /* Bootsector Extension */
    char    BS_BootCode[420];            /* pad so structure is 512b */
    BYTE    BS_BootSectSig0;             /* boot sector signature byte 0x55 */
	BYTE    BS_BootSectSig1;             /* boot sector signature byte 0xAA */
#define BOOTSIG0        0x55
#define BOOTSIG1        0xaa
}__attribute__ ((packed));

/* BPB的数据结构 */
#define FAT_BPB_OFFSET		11
struct BPB {
	WORD    BPB_BytsPerSec; 			/* bytes per sector */
    BYTE    BPB_SecPerClus; 			/* sectors per cluster */
    WORD    BPB_RsvdSecCnt;  			/* number of reserved sectors */
    BYTE    BPB_NumFATs;       			/* number of FATs */
    WORD    BPB_RootEntCnt; 			/* number of root directory entries */
    WORD    BPB_TotSec16;     			/* total number of sectors */
    BYTE    BPB_Media;       			/* media descriptor */
    WORD    BPB_FATSz16;     			/* number of sectors per FAT */
    WORD    BPB_SecPerTrk; 				/* sectors per track */
    WORD    BPB_NumHeads;      			/* number of heads */
    DWORD   BPB_HiddSec;	  			/* # of hidden sectors */
    DWORD   BPB_TotSec32;	 			/* # of sectors if bpbSectors == 0 */
}__attribute__ ((packed));

/* FAT16文件系统的EXT部分 */
#define FAT_EXT_OFFSET		36
struct EXT_FAT16 {
    BYTE    BS_DrvNum;             		/* drive number (0x80) */
    BYTE    BS_Reserved1;               /* reserved */
    BYTE    BS_BootSig;			        /* ext. boot signature (0x29) */
#define EXBOOTSIG       0x29
    DWORD   BS_VolID;	                /* volume ID number */
    char    BS_VolLab[11];		        /* volume label */
    char    BS_FilsysType[8];           /* fs type (FAT12 or FAT16) */
}__attribute__ ((packed));


/* FAT32文件系统的EXT部分 */
struct EXT_FAT32 {
	DWORD	BPB_FATSz32;				/* FAT32文件系统的FAT表占用的扇区数 */
	WORD	BPB_ExtFlags;				/* FAT32文件系统的FAT激活标记 */ 
	WORD	BPB_FSVer;					/* FAT32文件系统的版本号 */
	DWORD	BPB_RootClus;				/* FAT32文件系统根目录的起始簇号，通常为2 */
	WORD	BPB_FSInfo;					/* FSInfo结构体的在Reserved Area的扇区号，通常为1 */
	WORD	BPB_BkBootSec;				/* Boot Record在Reserved Area的扇区号，通常为6 */
	char	BPB_Reserved[12];			/* 用于以后版本扩展的12个字节 */
	
	struct EXT_FAT16 ext16;
}__attribute__ ((packed));

/* FAT文件系统文件项数据结构 */
struct DIRENTRY
{
    BYTE        DIR_Name[11];      		/* filename, blank filled */
 #define SLOT_EMPTY      0x00           /* slot has never been used */
 #define SLOT_E5         0x05           /* the real value is 0xe5 */
 #define SLOT_DELETED    0xe5           /* file in this slot deleted */
 #define SLOT_IS_DIR     0x2e           /* this is directory */

    BYTE        DIR_Attr;   	        /* file attributes */
 #define ATTR_READ_ONLY  0x01           /* file is readonly */
 #define ATTR_HIDDEN     0x02           /* file is hidden */
 #define ATTR_SYSTEM     0x04           /* file is a system file */
 #define ATTR_VOLUME_ID  0x08           /* entry is a volume label */
 #define ATTR_DIRECTORY  0x10           /* entry is a directory name */
 #define ATTR_ARCHIVE    0x20           /* file is new or modified */
 #define ATTR_LONG_FILENAME \
                         (ATTR_READ_ONLY|ATTR_HIDDEN|ATTR_SYSTEM|ATTR_VOLUME_ID)       /* this is a long filename entry */

    BYTE        DIR_NTRes;    		    /* NT VFAT lower case flags */
 #define LCASE_BASE      0x08           /* filename base in lower case */
 #define LCASE_EXT       0x10           /* filename extension in lower case */

    BYTE        DIR_CrtTimeTenth;   	/* hundredth of seconds in CTime */
    WORD        DIR_CrtTime;     		/* create time */
    WORD        DIR_CrtDate;     		/* create date */
    WORD        DIR_LstAccDate;     	/* access date */
    WORD        DIR_FstClusHI;    		/* high bytes of cluster number */
    WORD        DIR_WrtTime;     		/* last update time */
    WORD        DIR_WrtDate;     		/* last update date */
    WORD        DIR_FstClusLO; 		    /* starting cluster of file */
    DWORD       DIR_FileSize;     		/* size of file in bytes */
}__attribute__ ((packed));

/* FAT文件系统的组成结构体 */
struct FAT_STRUCT {
	BYTE	FATType;					/* FAT文件系统的类型 */
	DWORD	FirstDataSect;				/* 数据区第一个逻辑扇区 */
	DWORD	FirstRootDirSect;			/* 根目录第一个逻辑扇区 */
	DWORD	RootDirSects;				/* 根目录占用的扇区数 */
	DWORD	RootDirClus;				/* 根目录的起始簇号 */
	DWORD	SectsPerClus;				/* 每个簇的扇区数 */
	DWORD	BytsPerSect;				/* 每个扇区的字节数 */
	DWORD	RsvdSectCnt;				/* 预留的逻辑扇区数 */
};

/* FAT文件系统打开文件描述结构体 */
struct FILE {
	int   valid; 						/* 1 valid, 0 free. */
	
	int   FileFlag;						/* 文件标记，标记只读，只写或者可读写 */
	DWORD DirSect;						/* 文件父目录的扇区号 */
	int   DirIndex;						/* 文件在父目录中的偏移量 */
	DWORD FirstSect;					/* 文件的第一个扇区 */
	DWORD CurrentSect;					/* 文件的当前扇区 */
	DWORD SectOffset;					/* 文件在当前扇区的偏移量 */
	DWORD Offset;						/* 文件的总偏移量 */
	
	struct DIRENTRY Dir;				/* 文件的文件项 */
}__attribute__ ((packed));


/*
================================================================================
  系统相关宏定义
================================================================================
*/
#define FAT_SECTOR_SIZE		512
#define FAT_MAX_FILDES		16

#define FAT_TYPE_FAT12		12
#define FAT_TYPE_FAT16		16
#define FAT_TYPE_FAT32		32

#define FAT12_EOC       0x00000ff8UL
#define FAT16_EOC       0x0000fff8UL
#define FAT32_EOC		0x0ffffff8UL

/*
================================================================================
  全局变量定义
================================================================================
*/
static struct FAT_STRUCT gs_fat_struct;
static struct FILE gs_handles[FAT_MAX_FILDES];

/*
================================================================================
  声明FAT文件系统从存储介质上读写数据的函数
================================================================================
*/
#ifdef FAT_READ
extern int fat_read_sect512(unsigned long logic_sect, char *sect_buf);
#endif

#ifdef FAT_WRITE
extern int fat_write_sect512(unsigned long logic_sect, char *sect_buf);
#endif

/*
================================================================================
  代码区
================================================================================
*/
#define FirstSectOfClus(N) ((((N) - 2) * gs_fat_struct.SectsPerClus) + gs_fat_struct.FirstDataSect)
#define SectNum2ClusNum(N) (((N) - gs_fat_struct.FirstDataSect) / gs_fat_struct.SectsPerClus + 2)

/*
================================================================================
  内部静态函数，将文件名格式化为[8.3]格式文件名
参  数：pathname		文件路径名
返回值：NULL			无效的文件路径名
		char*			[8.3]格式文件名
================================================================================
*/
static
char* get_format83(const char *pathname)
{
	static char format_name[360];
	char pchar, *pname;
	int dir_len_count = 0, i;
	
	if(pathname == NULL || strlen(pathname) > 80 || *pathname != '\\') {
		dprintf("[%s]@%d: invalid pre name\n", __FILE__, __LINE__);
		return NULL;
	}
		
	if(strlen(pathname) > 1 && pathname[strlen(pathname) - 1] == '\\') {
		dprintf("[%s]@%d: invalid post name\n", __FILE__, __LINE__);
		return NULL;
	}
		
	memset(format_name, 0x0, 360);
	pname = format_name;
	
	while(1) {
		switch(pchar = toupper(*pathname++)) {
			case 0x00:		/* 结束符 */
				for(i = dir_len_count; i < 11; i++)
					pname[i] = 0x20;
				return format_name;
			case '.':
				if(dir_len_count > 8) {
					dprintf("[%s]@%d: invalid name length\n", __FILE__, __LINE__);					
					return NULL;
				}
				for(i = dir_len_count; i < 8; i++)
					pname[i] = 0x20;
				dir_len_count = 8;
				break;
			case '\\':
				for(i = dir_len_count; i < 11; i++)
					pname[i] = 0x20;
				pname[11] = '\\';
				pname += 12;				
				dir_len_count = 0;
				break;
			case 0x22: case 0x2a: case 0x2b: case 0x2f:
			case 0x3a: case 0x3b: case 0x3c: case 0x3d:
			case 0x3e: case 0x3f: case 0x5b: case 0x5d:
			case 0x7c:		/* 非法字符 */
				dprintf("[%s]@%d: invalid char\n", __FILE__, __LINE__);				
				return NULL;
				
			default:
				if(pchar <= 0x20 && dir_len_count == 0) {
					dprintf("[%s]@%d: invalid char\n", __FILE__, __LINE__);
					return NULL;
				}
				
				pname[dir_len_count++] = pchar;
				
				if(dir_len_count > 11) {
					dprintf("[%s]@%d: over length\n", __FILE__, __LINE__);
					return NULL;
				}
		}
	}
}

/*
================================================================================
  获取下一个簇的簇号
参  数：Cluster				当前簇号
返回值：DWORD				下一个簇的簇号
		EOC					表明当前簇为最后一个簇
================================================================================
*/
static
DWORD get_next_clus(DWORD Cluster)
{
	DWORD RsvdSecCnt, BytesPerSector, FATType;
    char tmpbuf[FAT_SECTOR_SIZE + FAT_SECTOR_SIZE];
    DWORD FATOffset, ThisFATSecNum, ThisFATEntOffset;

	RsvdSecCnt     = gs_fat_struct.RsvdSectCnt;
	BytesPerSector = gs_fat_struct.BytsPerSect;
	FATType        = gs_fat_struct.FATType;

    if(FATType == FAT_TYPE_FAT12)
        FATOffset = Cluster + (Cluster >> 1);
    else if (FATType == FAT_TYPE_FAT16)
        FATOffset = Cluster << 1;
    else/* if (FATType == FAT_TYPE_FAT32)*/
        FATOffset = Cluster << 2;

    ThisFATSecNum = RsvdSecCnt + (FATOffset / BytesPerSector);
    ThisFATEntOffset = FATOffset % BytesPerSector;

    fat_read_sect512(ThisFATSecNum, tmpbuf);

    if(FATType == FAT_TYPE_FAT12) {
        DWORD N = Cluster;
        if(ThisFATEntOffset == (BytesPerSector - 1)) {
            /* This cluster access spans a sector boundary in the FAT      */
            /* There are a number of strategies to handling this. The      */
            /* easiest is to always load FAT sectors into memory           */
            /* in pairs if the volume is FAT12 (if you want to load        */
            /* FAT sector N, you also load FAT sector N+1 immediately      */
            /* following it in memory unless sector N is the last FAT      */
            /* sector). It is assumed that this is the strategy used here  */
            /* which makes this if test for a sector boundary span         */
            /* unnecessary.                                                */
            fat_read_sect512(ThisFATSecNum+1, tmpbuf+BytesPerSector);
        }

       Cluster = *((WORD *) &tmpbuf[ThisFATEntOffset]);
       if(N & 0x0001)
          Cluster = Cluster >> 4;	/* Cluster number is ODD */
       else
          Cluster = Cluster & 0x0FFF;	/* Cluster number is EVEN */
    } else if(FATType == FAT_TYPE_FAT16)
        Cluster = *((WORD *) &tmpbuf[ThisFATEntOffset]);
    else/* if (FATType == FAT_TYPE_FAT32)*/
        Cluster = (*((DWORD *) &tmpbuf[ThisFATEntOffset])) & 0x0FFFFFFF;
        
    return Cluster;
}

/*
================================================================================
  内部静态函数，在指定的扇区内寻找与filename名称相同的项目
参  数：Sector			起始扇区号
		filename		要查找的文件名
		file			输出参数，用以返回文件的描述信息
返回值：DWORD			定位文件的第一个扇区号
		<0				没有找到指定的文件
================================================================================
*/
static
long sector_search(DWORD sector, const char filename[11], struct FILE *file)
{
	int i;
	char buf[FAT_SECTOR_SIZE];
	struct DIRENTRY* dir;
	long start_cluster;
	int dir_count;
	
	/* 获得指定扇区的数据 */	
	fat_read_sect512(sector, buf);
	
	/* 开始扇区搜索 */
	dir_count = (gs_fat_struct.BytsPerSect) / sizeof(struct DIRENTRY);
	for(i = 0, dir = (struct DIRENTRY*)buf; i < dir_count; i++, dir++) {
		if ((strncmp((dir->DIR_Name), filename, 11) == 0) /*&& (!(dir->DIR_Attr & ATTR_VOLUME_ID))*/) {
			/* 如果目录名/文件名相同，且不是卷标文件 */
			if (file != NULL) {
				memset(file, 0, sizeof(struct FILE));
				file->DirSect = sector;
				file->DirIndex = i;
				memcpy(&(file->Dir), (void*)dir, sizeof(struct DIRENTRY));
			}
			
			if(gs_fat_struct.FATType != FAT_TYPE_FAT32)
				start_cluster = dir->DIR_FstClusLO;
			else
				start_cluster = (dir->DIR_FstClusHI << 16) + dir->DIR_FstClusLO;

			return FirstSectOfClus(start_cluster);
		}
	}
	
	return -1;
}

/*
================================================================================
  内部静态函数，在文件系统中寻找是否有与filename名称相同的项目
参  数：sector			起始扇区号
		filename		要查找的文件名
		file			输出参数，用以返回文件的描述信息
返回值：DWORD			定位文件的第一个扇区号
		<0				没有找到指定的文件
================================================================================
*/
static
long fat_search(DWORD sector, const char filename[11], struct FILE *file)
{
	int i;
	long first_sector;
	
	if((gs_fat_struct.FATType != FAT_TYPE_FAT32) && (sector == gs_fat_struct.FirstRootDirSect)) {
		/* 起始扇区为根目录，并且不是FAT32文件系统 */
		for(i = 0; i < (gs_fat_struct.RootDirSects); i++) {
			first_sector = sector_search(sector++, filename, file);
			if (first_sector > 0)
				return first_sector;
		}
	} else {	/* 不从根目录开始搜索或者是FAT32文件系统 */
		DWORD clus_num;
		clus_num = SectNum2ClusNum(sector);
		
		while (1) {	/* 不等于最后一簇 */
			for (i = 0; i < gs_fat_struct.SectsPerClus; i++) {
				first_sector = sector_search(sector++, filename, file);
				if(first_sector > 0)
					return first_sector;
			}
			
			clus_num = get_next_clus(clus_num);
			if ( ((gs_fat_struct.FATType == FAT_TYPE_FAT12) && (clus_num >= FAT12_EOC)) ||
				 ((gs_fat_struct.FATType == FAT_TYPE_FAT16) && (clus_num >= FAT16_EOC)) ||
				 ((gs_fat_struct.FATType == FAT_TYPE_FAT32) && (clus_num >= FAT32_EOC)) )
				break;
			else
				sector = FirstSectOfClus(clus_num);				
		}
	}
	
	return -1;
}

/*
================================================================================
  内部静态函数，用以定位文件在文件系统的位置
参  数：pathname_f83	8.3格式文件路径名
		file			输出参数，用以返回文件的描述信息
返回值：DWORD			定位文件的第一个扇区号
		<0				没有找到指定的文件
================================================================================
*/
static
int fat_locate(const char *pathname_f83, struct FILE *file)
{
	int i;
	DWORD sector = gs_fat_struct.FirstRootDirSect;
	
	/* 开始定位根目录下的第一个文件夹 */
	pathname_f83 += 12;		/* 略过第一个'\\' */
	
	for(i = 0; ; i++) {
		if (*pathname_f83 == 0x0 || *pathname_f83 == 0x20) {
			if(i == 0) {	/* pathname指向根目录 */
				if(file != NULL) {
					if(gs_fat_struct.FATType != FAT_TYPE_FAT32) {
						file->Dir.DIR_FstClusHI = 0;
						file->Dir.DIR_FstClusLO = 2;					
					} else {
						file->Dir.DIR_FstClusHI = (gs_fat_struct.RootDirClus >> 16) & 0xffff;
						file->Dir.DIR_FstClusLO = (gs_fat_struct.RootDirClus) & 0xffff;
					}
					file->FirstSect = FirstSectOfClus(file->Dir.DIR_FstClusLO);
				}
			}
			
			return sector;
		}
			
		sector = fat_search(sector, pathname_f83, file);	/* 从指定扇区开始寻找与path同名的目录/文件项 */
		if (sector == -1)
			return -1;
			
		pathname_f83 += 12;
	}
}

/*
================================================================================
  从存储介质中获得FAT文件系统的信息
参  数：void
返回值：void
================================================================================
*/
void init_fat()
{
	DWORD FATSz;
	DWORD TotSec, CountofClusters;
	struct BPB *pbpb;
	char buf[FAT_SECTOR_SIZE];
	
	/* 读取MBR扇区 */
	fat_read_sect512(0, buf);
	
	pbpb = (struct BPB*)&buf[FAT_BPB_OFFSET];
	gs_fat_struct.SectsPerClus = pbpb->BPB_SecPerClus;
	gs_fat_struct.BytsPerSect  = pbpb->BPB_BytsPerSec;
	gs_fat_struct.RsvdSectCnt  = pbpb->BPB_RsvdSecCnt;
	
	if(pbpb->BPB_FATSz16 != 0)
		FATSz = pbpb->BPB_FATSz16;
	else {
		struct EXT_FAT32 *ext32;
		ext32 = (struct EXT_FAT32*)&buf[FAT_EXT_OFFSET];
		FATSz = ext32->BPB_FATSz32;
	}
	
	gs_fat_struct.FirstRootDirSect = pbpb->BPB_RsvdSecCnt + pbpb->BPB_NumFATs * FATSz;
	gs_fat_struct.RootDirSects     = ((pbpb->BPB_RootEntCnt * sizeof(struct DIRENTRY)) + (pbpb->BPB_BytsPerSec - 1)) / pbpb->BPB_BytsPerSec;
	gs_fat_struct.FirstDataSect    = gs_fat_struct.FirstRootDirSect + gs_fat_struct.RootDirSects;

    /* 判断文件系统的类型 */
    if(pbpb->BPB_TotSec16 != 0)
    	TotSec = pbpb->BPB_TotSec16;
    else
    	TotSec = pbpb->BPB_TotSec32;
    	
    CountofClusters = (TotSec - gs_fat_struct.FirstDataSect) / gs_fat_struct.SectsPerClus;
	if(CountofClusters < 4085) {			/* FAT12 */
		gs_fat_struct.FATType = FAT_TYPE_FAT12;	
	} else if(CountofClusters < 65525) {	/* FAT16 */
		gs_fat_struct.FATType = FAT_TYPE_FAT16;
	} else {								/* FAT32 */
		struct EXT_FAT32 *ext32;
		
		gs_fat_struct.FATType = FAT_TYPE_FAT32;		
		ext32 = (struct EXT_FAT32*)&buf[FAT_EXT_OFFSET];
		gs_fat_struct.RootDirClus = ext32->BPB_RootClus;
		gs_fat_struct.FirstRootDirSect = FirstSectOfClus(gs_fat_struct.RootDirClus);
	}
}

/*
================================================================================
  打开文件
参  数：pathname			文件路径及名称
		flag				打开文件的权限(只读，只写或者可读写)
返回值：>=0					文件描述符
		<0					错误信息
================================================================================
*/
int fat_open(const char *pathname, int flag)
{
	char *format83_pathname;
	int i;
	DWORD first_sector;
	struct FILE *fp = NULL;
	
	/* 获取空余的handles */
	for(i = 0; i < FAT_MAX_FILDES; i++) {
		if(!gs_handles[i].valid) {
			fp = &gs_handles[i];
			break;
		}
	}	
	if(fp == NULL)
		return -1;
	
	/* 将路径转化为8.3文件名格式 */
	format83_pathname = get_format83(pathname);
	if(format83_pathname == NULL)
		return -2;
	
	dprintf("format name=%s\n", format83_pathname);
	
	/* 定位文件 */
	first_sector = fat_locate(format83_pathname, fp);
	if(first_sector == -1)
		return -3;
	
	/* 初始化文件描述符信息 */	
	fp->valid = 1;
	fp->FirstSect = fp->CurrentSect = first_sector;
	fp->SectOffset = fp->Offset = 0;
	fp->FileFlag = flag;
	
	return i;
}

/*
================================================================================
  获取文件长度
参  数：fd					文件描述符
返回值：>=0					文件长度
		<0					错误信息
================================================================================
*/
int fat_getlen(int fd)
{
	struct FILE *fp;
	
	if(fd < 0 || fd >= FAT_MAX_FILDES)
		return -1;
		
	fp = &gs_handles[fd];
	
	return fp->Dir.DIR_FileSize;
}

/*
================================================================================
  关闭指定文件
参  数：fd				文件描述符
返回值：0				关闭成功
		<0				关闭失败
================================================================================
*/
int fat_close(int fd)
{
	struct FILE *fp;

	if(fd < 0 || fd >= FAT_MAX_FILDES)
		return -1;
	fp = &gs_handles[fd];

#if 0
	struct DIRENTRY *dir;
	struct FatDateTime tm;
	char buf[FAT_SECTOR_SIZE];
	
	if(fat_datetime(&tm) > 0) {
		fp->Dir.DIR_LstAccDate = fp->Dir.DIR_WrtDate = tm.Date;
		fp->Dir.DIR_WrtTime = tm.Time;
	
		fat_read_sect512(fp->DirSect, buf);
	
		dir = (struct DIRENTRY *)buf;
		dir += fp->DirIndex;
	
		memcpy(dir, &(fp->Dir), sizeof(struct DIRENTRY));
		fat_write_sect512(fp->DirSect);
	}
#endif
	
	gs_handles[fd].valid = 0;
	return 0;
}

/*
================================================================================
  指定文件的文件位置
参  数：fd				文件描述符
		offset			偏移量
		whence			偏移量的起始位置
返回值：>=0				当前文件位置从文件开始算起的便宜量
		<0				错误信息
================================================================================
*/
int  fat_lseek(int fd, unsigned int offset, int whence)
{
	int i;
	int cluster_index, sector_index, sector_offset;
	int free_bytes_per_cluster;
	DWORD cluster;
	struct FILE *fp;

	/* 参数检查 */
	if(fd < 0 || fd >= FAT_MAX_FILDES)
		return -1;
		
	fp = &gs_handles[fd];
	
	switch(whence) {
		case SEEK_SET:
			/* 如果偏移量超出文件长度 */
			if(offset >= fp->Dir.DIR_FileSize) {
				fp->Offset = offset;
				break;
			}
			
			/* 计算偏移量 */
			cluster_index = offset / (gs_fat_struct.BytsPerSect * gs_fat_struct.SectsPerClus);
			sector_index  = (offset / gs_fat_struct.BytsPerSect) % gs_fat_struct.SectsPerClus;
			sector_offset = offset % gs_fat_struct.BytsPerSect;
			cluster = SectNum2ClusNum(fp->FirstSect);
				
			if(cluster_index > 0) {		/* 偏移量超过1个簇的 */
				for(i = 0; i < cluster_index; i++)
					cluster = get_next_clus(cluster);

				fp->CurrentSect = FirstSectOfClus(cluster) + sector_index;
				fp->SectOffset  = sector_offset;
			} else {					/* 偏移量没有超过1个簇的 */
				fp->CurrentSect = fp->FirstSect + sector_index;
				fp->SectOffset  = sector_offset;
			}
			fp->Offset = offset;
						
			break;

		case SEEK_CUR:
			/* 如果偏移量超出文件长度 */
			if((offset + fp->Offset) >= fp->Dir.DIR_FileSize) {
				fp->Offset += offset;
				break;
			}
			
			sector_index = fp->CurrentSect - FirstSectOfClus(SectNum2ClusNum(fp->CurrentSect));
			free_bytes_per_cluster = (gs_fat_struct.SectsPerClus - 1 - sector_index) * gs_fat_struct.BytsPerSect +
				(gs_fat_struct.BytsPerSect - fp->SectOffset);
			if(offset <= free_bytes_per_cluster) {
				/* 定位的文件位置属于当前簇 */
				if(offset <= (gs_fat_struct.BytsPerSect - fp->SectOffset)) {		/* 定位的文件在当前扇区内 */
					sector_index = 0;
					sector_offset = fp->SectOffset + offset;
				} else {	/* 定位的文件超出当前扇区 */
					sector_index  = (offset - (gs_fat_struct.BytsPerSect - fp->SectOffset)) / gs_fat_struct.BytsPerSect + 1;
					sector_offset = (offset - (gs_fat_struct.BytsPerSect - fp->SectOffset)) % gs_fat_struct.BytsPerSect;
				}

				fp->CurrentSect += sector_index;
				fp->SectOffset   = sector_offset;
			} else {	/* 定位的文件位置超出当前簇 */
				/* 计算偏移量 */
				cluster_index = (offset - free_bytes_per_cluster) / (gs_fat_struct.BytsPerSect * gs_fat_struct.SectsPerClus) + 1;
				sector_index  = ((offset - free_bytes_per_cluster) / gs_fat_struct.BytsPerSect) % gs_fat_struct.SectsPerClus;
				sector_offset = (offset - free_bytes_per_cluster) % gs_fat_struct.BytsPerSect;
				cluster = SectNum2ClusNum(fp->CurrentSect);
				
				for(i = 0; i < cluster_index; i++)
					cluster = get_next_clus(cluster);

				fp->CurrentSect = FirstSectOfClus(cluster) + sector_index;
				fp->SectOffset  = sector_offset;
			}
			fp->Offset += offset;
			break;

		case SEEK_END:
			fp->Offset = fp->Dir.DIR_FileSize + offset;
			break;

		default:
			return -2;
	}
		
	return fp->Offset;
}

#ifdef FAT_READ
/*
================================================================================
  从指定的文件描述符中读取文件内容到指定的缓存中
参  数：fd				文件描述符
		buf				保存文件内容的缓存
		count			将读取的字节数
返回值：>=0				实际读取到的字节数
		<0				读取出错
================================================================================
*/
int fat_read(int fd, void *buf, unsigned int count)
{
	int read_bytes = 0;
	int i;
	int read_bytes_per_sector;
	DWORD cluster;
	struct FILE *fp;
	char tmpbuf[FAT_SECTOR_SIZE];
	
	/* 参数检查 */
	if(buf == NULL || count <= 0)
		return -1;
	if(fd < 0 || fd >= FAT_MAX_FILDES)
		return -2;
		
	fp = &gs_handles[fd];
	cluster = SectNum2ClusNum(fp->CurrentSect);
	
	/* 判断文件描述符标记 */
	if(!(fp->FileFlag & O_RDONLY) && !(fp->FileFlag & O_RDWR))
		return -3;
	
	/* 判断文件起始位置 */
	if((fp->Offset) >= (fp->Dir.DIR_FileSize)) {
		return 0;
	}

	/* 判断读取长度 */
	if(count > (fp->Dir.DIR_FileSize - fp->Offset))
		count = fp->Dir.DIR_FileSize - fp->Offset;
	
	/* 将数据写入簇缓存中 */
	while(1) {
		for(i = 0; i < gs_fat_struct.SectsPerClus; i++) {	/* 读取文件一簇的内容 */
			fat_read_sect512(fp->CurrentSect, tmpbuf);
			
			read_bytes_per_sector = (gs_fat_struct.BytsPerSect - fp->SectOffset) > (count - read_bytes)?
				(count - read_bytes): (gs_fat_struct.BytsPerSect - fp->SectOffset);
			memcpy(&(((char*)buf)[read_bytes]), &tmpbuf[fp->SectOffset], read_bytes_per_sector);
			read_bytes += read_bytes_per_sector;
			
			if(read_bytes >= count) {
				fp->SectOffset += read_bytes_per_sector;
				fp->Offset += read_bytes;
				return read_bytes;
			}
			
			fp->CurrentSect++;
			fp->SectOffset = 0;
		}
		
		cluster = get_next_clus(cluster);
		if( ((gs_fat_struct.FATType == FAT_TYPE_FAT12) && (cluster >= FAT12_EOC)) ||
			((gs_fat_struct.FATType == FAT_TYPE_FAT16) && (cluster >= FAT16_EOC)) ||
			((gs_fat_struct.FATType == FAT_TYPE_FAT32) && (cluster >= FAT32_EOC)) )
			break;
		
		fp->CurrentSect = FirstSectOfClus(cluster);
		fp->SectOffset = 0;
	}
	
	fp->Offset += read_bytes;	
	return read_bytes;	
}
#endif	/* FAT_READ */

#ifdef FAT_WRITE
int fat_write(int fildes);
#endif	/* FAT_WRITE */

#ifdef FAT_DIR

struct dirstream {
	long signature;		/* 用以鉴定该结构体是否是有效的，有效值为"lyst" */
#define DIRSIG	0xbfd3fc58

	long start_sect;		/* 文件夹的起始扇区号 */
	long curr_sect;		/* 当前所在的扇区  */
	long sect_off;		/* 当前扇区的偏移量(以文件项个数为单位) */
	long offset;			/* 当前的偏移量，以文件项的个数为单位 */
};

/*
================================================================================
  打开文件夹，返回文件夹流
参  数：  
			pathname		文件夹路径
返回值：
			NULL			路径错误，或者指定的不是文件夹
			DIR*			文件夹流
================================================================================
*/
DIR* opendir(const char *pathname)
{
	char *format83_pathname;
	char sect_buf[FAT_SECTOR_SIZE];
	long first_sect;
	struct FILE file;
	struct dirstream *dir_stream;
	struct DIRENTRY* pdir;
	
	if(pathname == NULL)
		return NULL;

	format83_pathname = get_format83(pathname);
	if(format83_pathname == NULL) {
		dprintf("[%s]@%d: format name failed\n", __FILE__, __LINE__);
		return NULL;
	}

	first_sect = fat_locate(format83_pathname, &file);
	if(first_sect < 0) {
		dprintf("[%s]@%d: can't locate file\n", __FILE__, __LINE__);
		return NULL;
	}

	fat_read_sect512(file.DirSect, sect_buf);
	pdir = (struct DIRENTRY*)sect_buf;
	pdir += file.DirIndex;
	if(!(pdir->DIR_Attr & ATTR_DIRECTORY)) {
		dprintf("[%s]@%d: it's not a directory\n", __FILE__, __LINE__);
		return NULL;
	}

	dir_stream = (struct dirstream*)malloc(sizeof(struct dirstream));
	dir_stream->signature  = DIRSIG;
	dir_stream->start_sect = dir_stream->curr_sect = first_sect;
	dir_stream->sect_off    = dir_stream->offset = 0;

	return (DIR*)dir_stream;
}

/*
================================================================================
  关闭文件夹流
参  数：  
			dir_stream		文件夹流
返回值：
			0				关闭成功
			<0				关闭错误
================================================================================
*/
int    closedir(DIR *dir_stream)
{
	struct dirstream *pdir_stream = (struct dirstream*)dir_stream;

	if((pdir_stream == NULL) || (pdir_stream->signature != DIRSIG)) {
		dprintf("[%s]@%d: invalid directory stream\n", __FILE__, __LINE__);
		return -1;
	}

	free(dir_stream);

	return 0;
}

static void format83_normal(char *dst, char* src)
{
	int count = 0;
	char *src_ptr = src;

	memset(dst, 0, 13);
	while((*src_ptr != 0x20) && (count != 8)) {
		dst[count++] = tolower(*src_ptr);
		src_ptr++;
	}

	if(src[8] == 0x20)
		return;

	dst[count++] = '.';
	src_ptr = &src[8];
	while((*src_ptr != 0x20) && (count != 11)) {
		dst[count++] = tolower(*src_ptr);
		src_ptr++;
	}
}

/*
================================================================================
 读取文件夹流中的文件信息
参  数：  
			dir_stream		文件夹流
返回值：
			NULL			读取失败
			struct dirent*		返回文件信息的指针
================================================================================
*/
struct dirent* readdir(DIR *dir_stream)
{
	static struct dirent dir_entry;	/* XXX 如果第二次调用此函数，之前的数据就会被清空，因此不允许重入 */
	char sect_buf[FAT_SECTOR_SIZE];
	struct DIRENTRY *pdir;
	int offset = 0;

	struct dirstream *pdir_stream = (struct dirstream*)dir_stream;

	if((pdir_stream == NULL) || (pdir_stream->signature != DIRSIG)) {
		dprintf("[%s]@%d: invalid directory stream\n", __FILE__, __LINE__);
		return NULL;
	}

	dprintf("[%s]@%d: begin to read dir, start_sect = %d, curr_setc = %d\n", __FILE__, __LINE__, pdir_stream->start_sect,
		pdir_stream->curr_sect);
	while(1) {
		dprintf("[%s]@%d: sect_off = %d\n", __FILE__, __LINE__, pdir_stream->sect_off);
		if(pdir_stream->sect_off >= (FAT_SECTOR_SIZE / sizeof(struct DIRENTRY))) {
			long clus_num;

			dprintf("[%s]@%d: go to end of sector\n", __FILE__, __LINE__);
			clus_num = get_next_clus(SectNum2ClusNum(pdir_stream->curr_sect));
			if ( ((gs_fat_struct.FATType == FAT_TYPE_FAT12) && (clus_num >= FAT12_EOC)) ||
				((gs_fat_struct.FATType == FAT_TYPE_FAT16) && (clus_num >= FAT16_EOC)) ||
				 ((gs_fat_struct.FATType == FAT_TYPE_FAT32) && (clus_num >= FAT32_EOC)) )
				break;

			pdir_stream->curr_sect = FirstSectOfClus(clus_num);
			pdir_stream->sect_off   = 0;
			offset = 0;
		}

		if(fat_read_sect512(pdir_stream->curr_sect, sect_buf) < 0) {
			dprintf("[%s]@%d: fat_read_sect512 failed\n", __FILE__, __LINE__);
			return NULL;
		}
		pdir = (struct DIRENTRY*)sect_buf;
		

		for(pdir += pdir_stream->sect_off; (char *)pdir < (char *)sect_buf + FAT_SECTOR_SIZE; pdir++) {
			//dprintf("[%s]@%d: name = %s\n", __FILE__, __LINE__, pdir->DIR_Name);
			pdir_stream->sect_off ++;
			if((pdir->DIR_Name[0] != SLOT_EMPTY) && (pdir->DIR_Name[0] != SLOT_DELETED)) {
				(pdir_stream->offset)++;
				
				dir_entry.offset = pdir_stream->offset;
				dir_entry.type      = pdir->DIR_Attr;
				format83_normal(dir_entry.name, pdir->DIR_Name);

				dprintf("[%s]@%d: find file, name = %s\n", __FILE__, __LINE__, dir_entry.name);				
				return &dir_entry;
			}
		}
	}

	return NULL;
}

void  rewinddir(DIR *dir_stream);
void  seekdir(DIR *dir_stream, long offset);
long  telldir(DIR *dir_stream);
#endif	/* FAT_DIR */

#endif
