#include "config.h"
#include "../drivers/drivers.h"

#ifdef CONFIG_DELAY

/* this should be approx 2 Bo*oMips to start (note initial shift), and will
 * still work even if initially too large, it will just take slightly longer */
unsigned long loops_per_tick = (1<<12);

/* This is the number of bits of precision for the loops_per_tick.  Each
 * bit takes on average 1.5/HZ seconds.  This (like the original) is a little
 * better than 1% */
#define LPS_PREC 8

static
volatile unsigned long ticks = 0; 

static
void delay_isr (unsigned int irq)
{
	++ticks;
}

static
void calibrate_delay(void)
{
        unsigned long tmp, loopbit;
        int lps_precision = LPS_PREC;

        loops_per_tick = (1<<12);
        
/*        printf("Calibrating delay loop... "); */

        while (loops_per_tick <<= 1) {
                /* wait for "start of" clock tick */
                tmp = ticks;
                while (tmp == ticks)
                        /* nothing */;
                /* Go .. */
                tmp = ticks;
                __delay(loops_per_tick);
                tmp = ticks - tmp;
                if (tmp)
                        break;
        }

	/* Do a binary approximation to get loops_per_tick set to equal one clock
	 * (up to lps_precision bits) */
        loops_per_tick >>= 1;
        loopbit = loops_per_tick;
        while ( lps_precision-- && (loopbit >>= 1) ) {
                loops_per_tick |= loopbit;
                tmp = ticks;
                while (tmp == ticks);
                tmp = ticks;
                __delay(loops_per_tick);
                if (ticks != tmp)   /* longer than 1 tick */
                        loops_per_tick &= ~loopbit;
        }

	/* Round the value and print it 
        printf("%lu.%02lu BogoMIPS\n",
                loops_per_tick/(500000/HZ),
                loops_per_tick/(5000/HZ) % 100); */
}

void init_delay (void)
{
    void (*oldisr)(unsigned int irq_nbr);
    
	TCFG0  = 0xff << 8; /*Prescaler = 255*/
	TCFG1  = 0x3 << 16; /*Divider = 16*/
	TCNTB4 = (PCLK / (255+1) / 16) / HZ;
	TCON = 0x6 << 20;	/* update TCNTB4 */
	TCON = 0x5 << 20;	/* timer on */

	oldisr = intr_setisr (INT_TIMER4, delay_isr);
	
	calibrate_delay();
	
	TCON = 0x0 << 20;	/* timer off */
	intr_setisr (INT_TIMER4, oldisr);
}

#endif/*CONFIG_DELAY*/