#include "config.h"
#include "drivers.h"

#ifdef CONFIG_IIC

#include  "lib.h"
#include  <stdio.h>


#define IIC_DATA_BUFFER_SIZE   0x20
#define WRDATA     (1)
#define POLLACK    (2)         //
#define RDDATA     (3)
#define SETRDADDR  (4)         //设置接收设备地址

static  unsigned char iic_data[IIC_DATA_BUFFER_SIZE];    //数据缓存区
static  volatile int iic_data_count;                     //流通数据次数
static  volatile int iic_status;                         //当前状态
static  volatile int iic_mode;                           //模式选择
static           int iic_data_pointer;                   //缓存区数组指数

static void iicPoll()
{
  int i;
	unsigned int _iicsts;
	
	_iicsts = IICSTAT ;

		switch(iic_mode)
		{
			case WRDATA:
				if((iic_data_count--) == 0)
					{
						IICSTAT = 0xd0;        //stop RX/TX
						IICCON  = 0xAf;        //resumes IIC operation
						for(i=0; i<400; i++)   //wait until stop condtion is in effect
						   ;
						break;
					}
			    
			    IICDS = iic_data[iic_data_pointer++];
			 
			    for(i=0; i<10; i++)
			       ;
			    
			    IICCON = 0xAf;                 //resumes the IIC operation
			    break;
			
			case POLLACK:
				iic_status = _iicsts;
				break;
				
			case RDDATA:
				if((iic_data_count--) == 0)
					{
						iic_data[iic_data_pointer++] = IICDS;
						
						IICSTAT = 0xd0;             //stop TX/RX
						IICCON  = 0xAf;             //resumes the IIC operation
						for(i=0; i<400; i++)        //wait until the stop condtion is in effect
						   ;
						break;
					}
				
				iic_data[iic_data_pointer++] = IICDS;
				
				//the last data has to be read with no ack
				if(iic_data_count == 0)
					IICCON = 0x2f;       //resumes IIC with no ack
				else
					IICCON = 0xAf;       //resumes IIC with ack
				break;
				
			case SETRDADDR:
				if((iic_data_count--) == 0)
					{
						break;
					}
				
				IICDS = iic_data[iic_data_pointer++];
				for(i=0; i<10; i++)
				   ;
				
				IICCON = 0xAf;          //resumes IIC operation 
				break;
			
			default:
				break;				
		}
}

void iic_write(unsigned int slaveAddress, unsigned int address, unsigned char data)
{
	int i;
	iic_mode = WRDATA;
	iic_data_count = 2;
	iic_data[0] =(unsigned char) address;
	iic_data[1] = data;
	iic_data_pointer = 0;
	
	IICSTAT = 0xf0;                     //主发送模式, 写时iic总线stop信号产生, TX/RX enable
	IICDS   = slaveAddress ;            //write the slave address to IICDS
	
	while(iic_data_count != -1)
	  if(IICCON & 0x10)                
		{
			iicPoll();
		}
	
		  
/*	iic_mode = POLLACK;               //XXX
    while(1)
	  {
	  	IICDS = (slaveAddress & 0xfe);
	  	iic_status = 0x100;             //to check if iic_status is changed
	  	IICSTAT    = 0xf0;              //master send mode, 
	  	IICCON     = 0xAf;              //resume the iic operation
	  	
	  	while(iic_status == 0x100)
	  	run_iicPoll();
	  	    
	  	if(!(iic_status & 0x1))     //when ACK is received
	   	//if(1)
	   	 break;                
	  }
*/	 

	IICSTAT = 0xd0;                 //stop TX/RX
	IICCON  = 0xAf;                 //resumes the IIC opeartion
	for(i=0; i<400; i++)            //wait until the stop condtion is in effect
	   ;
}

void iic_read(unsigned int slaveAddress, unsigned int address, unsigned char *data)
{
	iic_mode = SETRDADDR;
	iic_data_count = 1;
	iic_data_pointer = 0;
	iic_data[0] = (unsigned char)address;
	
	IICDS = slaveAddress ;
	IICSTAT = 0xf0;
	
	while(iic_data_count != -1)
    if(IICCON & 0x10)                
		{
			iicPoll();
		}  
	
	iic_mode = RDDATA;
  iic_data_pointer = 0;
	iic_data_count = 1;
	    
	IICDS = (slaveAddress & 0xfe);
	IICSTAT = 0xb0;                  //master receive mode 
  IICCON = 0xAf;                   //resume the IIC operation
	     
  while(iic_data_count != -1)
	  if(IICCON & 0x10)                
		{
			iicPoll();
		}
	    
	*data = iic_data[1];
}

#endif/*CONFIG_IIC*/
