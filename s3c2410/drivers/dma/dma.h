/*
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * 本文件定义S3C2410的DMA模块接口以及宏定义
 */

/*------------------------------------------------------------------------------
 * 项目名 ....: DMA
 * 文件名 ....: dma.h
 * 程序员 ....: 龙云
 * 指导者 ....: 洪明坚
 * 团 	 队 ....: 嵌入式开发小组
 * 组  	 织 ....: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * 时  	 间 ....: 2009/4/15
 */

#include "config.h"

#ifndef DMA_H
#define DMA_H

/*
================================================================================
  结构体定义
================================================================================
*/
/* DMA callback function prototype */
typedef void (*dma_callback_t)(void*, int, int);

/* DMA device structre */
typedef struct {
	dma_callback_t callback;
	unsigned long dst;
	unsigned long src;
	unsigned long ctl;
	unsigned long dst_ctl;
	unsigned long src_ctl;
} dma_device_t;

/*
================================================================================
  宏定义
================================================================================
*/
#define DMA_ON_AHB				 (0 << 1)
#define DMA_ON_APB				 (1 << 1)
#define DMA_ADDR_INC			 (0 << 0)
#define DMA_ADDR_FIX			 (1 << 0)

#define DCON_DEMAND        (0 << 31)   /* 0:Demand mode is selected */
#define DCON_HANDSHAKE     (1 << 31)   /* 1:Handshake mode is selected */

#define DCON_SYNC_PCLK     (0 << 30)   /* 0:DREQ and DACK are synchronized to PCLK (APB clock) */
#define DCON_SYNC_HCLK     (1 << 30)   /* 1:DREQ and DACK are synchronized to HCLK (AHP clock) */ 

#define DCON_BURST         (1 << 28)   /* burst */
#define DCON_UNIT          (0 << 28)   /* unit */

#define DCON_IRQ_ON        (1 << 29)
#define DCON_IRQ_OFF       (0 << 29)

#define DCON_WHOLE         (1 << 27)   /* 1:Whole service mode is selected  */
#define DCON_SINGLE        (0 << 27)   /* 0:Sigle service mode is selected */

#define DCON_SW_SEL        (0 << 23)   /* S/W request mode is selected and DMA is triggered by setting SW_TRIG bit of DMASKTRIG control register */
#define DCON_HW_SEL        (1 << 23)   /* DMA source selected by bit[26:24] triggers the DMA operation. */

#define DCON_RELOAD_ON     (1 << 22)   /* ON */
#define DCON_RELOAD_OFF    (1 << 22)   /* OFF */

#define DCON_BYTE          (0 << 20)   /* 00:BYTE */
#define DCON_HALFWORD      (1 << 20)   /* 01:HALFWORD */
#define DCON_WORD          (2 << 20)   /* 10:WORD */

#define DSTAT_BUSY         (1 << 20)
#define DSTAT_READY        (0 << 20)

#define DMASKTRIG_START    (0 << 2)
#define DMASKTRIG_STOP     (1 << 2)

#define DMASKTRIG_ON       (1 << 1)    /* 0:DMA channel is turned on */
#define DMASKTRIG_OFF      (0 << 1)

#define DMASKTRIG_HW_TRIG  (0 << 0)    /* It requests a DMA operation to this controller,When DMA operation starts,this bit is cleared automatically */
#define DMASKTRIG_SW_TRIG  (1 << 0)

#define BUF_ON_MEM				 (DMA_ON_AHB | DMA_ADDR_INC)
#define BUF_ON_APB				 (DMA_ON_APB | DMA_ADDR_FIX)

/*
================================================================================
  DMA模块接口声明
================================================================================
*/
/* DMA Interface */
void init_dma(void);
int  dma_request_channel(int channel, const char *device_id);
int  dma_queue_buffer(int channel, void* buf, int size, int count, int write);
int  dma_stop_channel(int channel);
int  dma_free_channel(int channel);
dma_device_t *dma_get_device(int channel, int write);

#endif	/* DMA_H */
