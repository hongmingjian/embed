/*
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * 本文件实现S3C2410 DMA模块的所有接口函数，DMA模块的内部实现
 * 
 * ==============================================================================
 * 本DMA模块依赖如下的函数(需在DMA模块外实现)：
 * strcmp
 * void *memset(void *s, char ch, unsigned n);
 * void *memcpy(void *dest, void *src, unsigned int count);
 * int memcmp(void *buf1, void *buf2, unsigned int count);
 * int fat_read_sect512(unsigned long logic_page, char *page_buf);
 * int fat_write_sect512(unsigned long logic_page, char *page_buf);
 * int printf(const char *format, ...);(在调试中使用，即定义了DEBUG_DMA)
 * ==============================================================================
 */

/*------------------------------------------------------------------------------
 * 项目名 ....: DMA
 * 文件名 ....: dma.c
 * 程序员 ....: 龙云
 * 指导者 ....: 洪明坚
 * 团  队 ....: 嵌入式开发小组
 * 组  织 ....: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * 时  间 ....: 2009/4/15
 */

#include "config.h"
#include "../drivers/drivers.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef CONFIG_DMA

/*
================================================================================
  调试定义
================================================================================
*/
#ifdef DEBUG_DMA
	#define dprintf	printf
#else
	#define dprintf
#endif

/*
================================================================================
  结构体定义
================================================================================
*/
/* DMA buffer struct */
typedef struct {
	int size;						/* item size */
	int count;                      /* number of items */
	void* dma_start;				/* starting DMA address */
	int write;					/* 1: buf to write , 0: buf to read  */
	struct dma_buf_t *next;		/* next buf to process */
} dma_buf_t;

/* DMA control register structure */
typedef struct {
	volatile unsigned long DISRC;
	volatile unsigned long DISRCC;
	volatile unsigned long DIDST;
	volatile unsigned long DIDSTC;
	volatile unsigned long DCON;
	volatile unsigned long DSTAT;
	volatile unsigned long DCSRC;
	volatile unsigned long DCDST;
	volatile unsigned long DMASKTRIG;
} dma_regs_t;

/* DMA channel structure */
typedef struct {
	int channel;
	unsigned int in_use;			/* Device is allocated */
	const char *device_id;			/* Device name */
	dma_buf_t *head;				/* where to insert buffers */
	dma_buf_t *tail;				/* where to remove buffers */
	dma_buf_t *curr;				/* buffer currently DMA'ed */
	unsigned long queue_count;	/* number of buffers in the queue */
	int active;					/* 1 if DMA is actually processing data */
	dma_regs_t *regs;			/* points to appropriate DMA registers */
	int irq;						/* IRQ used by the channel */
	dma_device_t write;			/* to write */
	dma_device_t read;			/* to read */
} s3c2410_dma_t;

/*
================================================================================
  宏定义
================================================================================
*/
#define S3C2410_DMA_CHANNELS 		4
#define S3C2410_DMA_CHAN_DEVS	5

/*
================================================================================
  全局数据定义
================================================================================
*/
static s3c2410_dma_t sg_dma_chan[S3C2410_DMA_CHANNELS];
static char* devices[][5] = {{"nXDREQ0", "UART0",  "SDI",    "Timer", "USB_EP1"},
                             {"nXDREQ1", "UART1",  "I2SSDI", "SPI",   "USB_EP2"},
                             {"I2SSDO",  "I2SSDI", "SDI",    "Timer", "USB_EP3"},
                             {"UART2",   "SDI",    "SPI",    "Timer", "USB_EP4"}};


/*
================================================================================
  代码区
================================================================================
*/
static void process_dma(s3c2410_dma_t *dma)
{
	dma_buf_t *buf, *next_buf;
	dma_regs_t *regs = dma->regs;
	int dsz, data_cnt;
	dma_device_t *device;

	buf = dma->head;

	if (buf && !(dma->active)) {		
		if(buf->write) {
			device = &dma->write;
			regs->DISRCC = device->src_ctl;
			regs->DISRC   = (unsigned long)(buf->dma_start);
			regs->DIDSTC = device->dst_ctl;
			regs->DIDST   = device->dst;
		} else {
			device = &dma->read;
			regs->DISRCC = device->src_ctl;
			regs->DISRC   = device->src;
			regs->DIDSTC = device->dst_ctl;
			regs->DIDST   = (unsigned long)(buf->dma_start);
		}

        switch(buf->size) {
        case 1:
            dsz = 0;
            break;
        case 2:
            dsz = 1;
            break;
        case 4:
            dsz = 2;
            break;
        }

		data_cnt = buf->count/( (device->ctl & DCON_BURST) ? 4 : 1 );
		regs->DCON = (device->ctl) | data_cnt | (dsz << 20);

		dma->curr = buf;
		next_buf = dma->head->next;
		dma->head = next_buf;
		if (!next_buf)
			dma->tail = NULL;	
		
		dma->active = 1;
		dma->queue_count--;
			
		if(((device->ctl) & DCON_HW_SEL))
			regs->DMASKTRIG = DMASKTRIG_START | DMASKTRIG_ON | DMASKTRIG_HW_TRIG;
		else
			regs->DMASKTRIG = DMASKTRIG_START | DMASKTRIG_ON | DMASKTRIG_SW_TRIG;
	}
}

static int dma_flush_all(int channel)
{
	s3c2410_dma_t *dma = &sg_dma_chan[channel];
	dma_buf_t *buf, *next_buf;
	unsigned int cpsr;

	if ((channel < 0) || (channel >= S3C2410_DMA_CHANNELS) || (!dma->in_use)) {
		dprintf("[%s]@%d: flush dma failed.\n", __FILE__, __LINE__);
		return -1;
	}

	save_flag_cli(cpsr);
	dma->regs->DMASKTRIG = DMASKTRIG_STOP;
	buf = dma->head;
	dma->head = dma->tail = dma->curr = NULL;
	dma->active = 0;
	dma->queue_count = 0;
	dma->in_use= 0;
	restore_flag(cpsr);
	while(buf) {
		next_buf = buf->next;
		free(buf);
		buf = next_buf;
	}

	return 0;
}

static inline void dma_done(s3c2410_dma_t *dma)
{
	dma_buf_t *buf = dma->curr;
	dma_callback_t callback;

	if (buf->write)
		callback = dma->write.callback;
	else
		callback = dma->read.callback;

	if (callback)
		callback(buf->dma_start, buf->size, buf->count);
	
	free(buf);
	dma->active = 0;
	process_dma(dma);
}

static void dma_irq_handler(unsigned int irq)
{
	int channel;
	s3c2410_dma_t *dma;
    
	channel = irq - INT_DMA0;	
	if(channel >= S3C2410_DMA_CHANNELS)
		return;	
	
	dma = &sg_dma_chan[channel];
	dma_done(dma);
}

void init_dma(void)
{
	int channel;
	
	for(channel = 0; channel < (S3C2410_DMA_CHANNELS); channel++) {
		sg_dma_chan[channel].regs     = (dma_regs_t*)(DMA_REGBASE + 0x40 * channel);
		sg_dma_chan[channel].irq        = INT_DMA0 + channel;
		sg_dma_chan[channel].channel = channel;
		sg_dma_chan[channel].in_use   = 0;
		
		intr_setisr(sg_dma_chan[channel].irq, dma_irq_handler);
	}
}

static int locate_dev(int channel, const char *device_id)
{
	int i;

	for(i = 0; i < __countof(devices[channel]); i++)
	 	if(strcmp(device_id, devices[channel][i]) == 0)
			break;
    
	if(i == __countof(devices[channel]))
		return -1;
		
    return i;
}

int  dma_request_channel(int channel, const char *device_id)
{
	unsigned int cpsr;
	s3c2410_dma_t *dma;
	int hwsrcsel;
	
	if(device_id == NULL) {
		dprintf("[%s]@%d: device_id is null.\n", __FILE__, __LINE__);
		return -1;
	}
	
	if((channel < 0) || (channel >= S3C2410_DMA_CHANNELS)) {
		dprintf("[%s]@%d: channel number is out of range.\n", __FILE__, __LINE__);
		return -2;
	}
	
	hwsrcsel = locate_dev(2, device_id);
	if(hwsrcsel < 0) {
		dprintf("[%s]@%d: unkown device_id.\n", __FILE__, __LINE__);
		return -3;
	}
	
	save_flag_cli(cpsr);
	dma = &sg_dma_chan[channel];
	
	if(dma->in_use) {
    	restore_flag(cpsr);
		dprintf("[%s]@%d: %d dma channel is busy.\n", __FILE__, __LINE__, channel);
		return -3;
	} else
		dma->in_use = 1;
	restore_flag(cpsr);
	
	dma->device_id = device_id;
	dma->head = dma->tail = dma->curr = NULL;
	dma->write.ctl = 
	dma->read.ctl  = (hwsrcsel << 24);
	dma->write.callback = 
	dma->read.callback  = NULL;
	dma->active = 0;
	
	return 0;
}

dma_device_t *dma_get_device(int channel, int write)
{
	s3c2410_dma_t *dma;

	if((channel < 0) || (channel >= S3C2410_DMA_CHANNELS) || (!sg_dma_chan[channel].in_use)) {
		dprintf("[%s]@%d: invalid channel or dma isn't in use.\n", __FILE__, __LINE__);
		return NULL;
	}

	dma = &sg_dma_chan[channel];
	
    if(write)    
        return &dma->write;
        
    return &dma->read;
}

int  dma_queue_buffer(int channel, void* data, int size, int count, int write)
{
	unsigned int cpsr;
	s3c2410_dma_t *dma;
	dma_buf_t *buf;

	if((channel < 0) || (channel >= S3C2410_DMA_CHANNELS) || (!sg_dma_chan[channel].in_use)) {
		dprintf("[%s]@%d: invalid channel or dma isn't in use.\n", __FILE__, __LINE__);
		return -1;
	}
	
	if((size != 1) && (size != 2) && (size != 4)) {
		dprintf("[%s]@%d: invalid buf->size.\n", __FILE__, __LINE__);
		return -3;
	}
	
	dma = &sg_dma_chan[channel];
	buf = malloc(sizeof(dma_buf_t));
	if(!buf) {
		dprintf("[%s]@%d: there's no enough buffer.\n", __FILE__, __LINE__);
		return -2;
	}
	
	buf->next = NULL;
	buf->dma_start = data;
	buf->size = size;
	buf->count = count;
	buf->write = write;
	
	save_flag_cli(cpsr);
	if(dma->tail)
		dma->tail->next = buf;
	else
		dma->head = buf;
	dma->tail = buf;
	buf->next = NULL;
	dma->queue_count++;
	process_dma(dma);	
	restore_flag(cpsr);
	
	return 0;
}

int  dma_stop_channel(int channel)
{
	s3c2410_dma_t *dma = &sg_dma_chan[channel];
	dma_buf_t *buf = dma->curr;
	dma_regs_t *regs = dma->regs;
	dma_callback_t callback;
	int cpsr;

	if (!dma->active)
		return 0;

	save_flag_cli(cpsr);
	regs->DMASKTRIG = DMASKTRIG_STOP; 

	if (buf->write)
		callback = dma->write.callback;
	else 
		callback = dma->read.callback;

	if(callback)
		callback(buf->dma_start, buf->size, buf->count);

	free(buf);
	dma->active = 0;

	restore_flag(cpsr);

	return 0;
}

int dma_free_channel(int channel)
{
	s3c2410_dma_t *dma;

	if((channel < 0) || (channel >= S3C2410_DMA_CHANNELS))
		return -1;

	dma = &sg_dma_chan[channel];
	if (!dma->in_use) {
		dprintf("[%s]@%d: the dma isn't in use, can't be free.\n", __FILE__, __LINE__);
		return -2;
	}

	if(dma_flush_all(channel) < 0) {
		dprintf("[%s]@%d: flush dma failed.\n", __FILE__, __LINE__);
		return -3;
	}

	dma->in_use = 0;
	return 0;
}


#endif /* CONFIG_DMA */
