#include "config.h"
#include "drivers.h"

#ifdef CONFIG_CH7004C

#include  "lib.h"
#include  <string.h>
#include  <stdio.h>

#define DISPLAY_CLKVAL (1<<8)   

#define MVAL           (13<<8)
#define MMODE          (0<<7)
#define PNRMODE_TFT    (3<<5)
#define LCD_EN         (1)
#define LCD_DIS        (0)
 
#define INVVCLK_FALL   (0<<10)
#define INVVLINE_INV   (1<<9)
#define INVVFRAME_INV  (1<<8)
#define INVVD_NORMAL   (0<<7)
#define INVVDEN_NORMAL (0<<6)
#define INVPWREN_NOR   (0<<5)
#define INVLEND_NOR    (0<<4)
#define PWREN_DIS      (0<<3)
#define PWREN_EN       (1<<3)
#define ENLEND_DIS     (0<<2)
#define BSWP_DIS       (0<<1)
#define BSWP_EN        (1<<1)
#define HWSWP_DIS      (0<<0)
#define HWSWP_EN       (1<<0)

/*support 640x480 only*/
#define VBPD		(32 << 24)
#define VFPD		(9  << 6 )
#define VSPW		(1  << 0 )
#define HBPD		(47 << 19)
#define HFPD		(15 << 0 )
#define HSPW		(95 << 0 )


int init_ch7004c()    
{
	GPEUP  =   (GPEUP & ~0xc000) | 0xc000;          //GPE15,,GPE14,,pull-up is disable
  GPECON =   (GPECON & ~0xf0000000) | 0xa0000000; //GPECON15:IICSDA, GPECON14:IICSCL 
	         
	
	IICCON =  (1<<7)|(0<<6)|(1<<5)|(0xf) ;
	IICADD =  0x9f;
	IICSTAT = 0x9f;           //default master receive mode , IICCLK = PCLK/16,  TX/RX enable
	                          //bus arbitration fail, slave address match to the IICADD
	                          //slave address is 00000000b,  ACK isn't received
	
	iic_write(0xec, 4,  0x20);    //设备ch7004c的地址为0xec，register：04H: RGBBP
	iic_write(0xec, 7,  0x8c);    //                         register: 07H: SAV7, SAV3, SAV2
	iic_write(0xec, 14, 0x1b);    //                         register: 0EH: SCART, Reset, PD1, PD0
	iic_write(0xec, 13, 0x3);     //                         register: 0DH: VSP, HSP
	
	return 0;
}

/***
**support 640x480 pixel mode only
**support 16 bpp and 24 bpp mode
**options means format565 or bpp24bl
**若是16bpp，则相关format565，options=0表示565格式，options=1表示5551格式
**若是24bpp，则相关bpp24bl，options=0表示LSB，options=1表示MSB
*/
int init_lcd(int bpp, int width, int hight, void  *fb, int options)
{
	  int bppmode;
	  int bpp24bl;
	  int format;
	  int framebuffer=(int)(fb);
	  int hozval = width - 1;
		int lineval, pagewidth, offsize, lcdbank, lcdbaseu, lcdbasel;
		
	  GPCUP  = 0xFFFFFFFF;    //GPCCON pull up funciton disable
	  GPCCON = 0xAAAAAAAA;    //VD0~~VD7, LCDVF2, LCDVF1, LCDVF0, VM, VFRAME, VLINE, VCLK, LEND
	  GPDUP =  0xFFFFFFFF;    //GPDCON pull up function disable
	  GPDCON = 0xAAAAAAAA;    //VD8~~VD23
	  
 		lineval = hight-1;
	  pagewidth = width*8/16;
    offsize = 0;
    lcdbank = framebuffer >> 22;
    lcdbaseu = (framebuffer & 0x1ffff) >> 1;
    lcdbasel = lcdbaseu + ( pagewidth + offsize ) * ( lineval + 1 );
	  
	  switch(bpp)
	  {
	  	case 16:
	  		   bppmode = 0x0C;
	           
	         switch(options)
	         {
	           case 0:
	    	     case 1:
	    	     	    format=options;
	    		        break;
	    		   default:
	    		   	    return -1;
	    		   	    
	    		  }
	          break;
	          		  
	    case 24:
	    	   bppmode = 0x0D;
	           
	         switch(options)
	         {
	         	  case 1:
	         	  case 0:
	         	  	   bpp24bl=options;
	         	  	   break;
	         	 default:
	         	 	     return -1;
	         	 	     
	         }
	         break;
	          
	     default:
	     	     return -1;
	     	     
	    }
	    
	   LCDCON1 = DISPLAY_CLKVAL|MMODE|PNRMODE_TFT|(bppmode<<1)|LCD_DIS;
	   LCDCON2 = VBPD|(lineval<<14)|VFPD|VSPW;
	   LCDCON3 = HBPD|(hozval<<8)|HFPD;
	   LCDCON4 = MVAL|HSPW;
	   LCDCON5 = (bpp24bl<<12)|((format<<11))|INVVLINE_INV|INVVFRAME_INV|BSWP_EN|HWSWP_EN;
	      
	  LCDINTMSK = (1<<1)|(1<<0);
	  
	  
	  LCDSADDR1 = (lcdbank << 21)|lcdbaseu;
	  
	  LCDSADDR2 = lcdbasel;
	  
	  LCDSADDR3 = (offsize<<11)|pagewidth;
	  
    LPCSEL&=(~7); //disable temp lpcsel
    
    LCDCON1=LCDCON1|LCD_EN;    // ENVID=ON
    
	  TPAL = 0;     //disable temp palette
	  
	  return 0;                  
}
#endif/*CONFIG_CH7004C*/
