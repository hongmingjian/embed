#include "config.h"
#include "drivers.h"

#ifdef CONFIG_UART

void init_uart( )
{
	GPHCON = 0x16faaa;		/* GPH2, GPH3 used as TXD0,RXD0 */
	GPHUP	 = 0x7ff;		/* GPH2, GPH3 disable pull up */

	__CONCAT(ULCON,  CONSOLE_PORT)	= 0x3;
	__CONCAT(UCON,   CONSOLE_PORT)	= 0x5;
	__CONCAT(UFCON,  CONSOLE_PORT)	= 0x0;
	__CONCAT(UMCON,  CONSOLE_PORT)	= 0x0;
	__CONCAT(UBRDIV, CONSOLE_PORT)	= PCLK / (CONSOLE_SPEED << 4) - 1;
}

int sio_ischar()
{
	return ( (__CONCAT(UTRSTAT, CONSOLE_PORT) & 1) );
}

int sio_oschar()
{
	return ( (__CONCAT(UTRSTAT, CONSOLE_PORT) & 4) );
}

char sio_getc( )
{
	while( !sio_ischar() )
		;
	return __CONCAT(URXH, CONSOLE_PORT);
}

void sio_putc(char c)
{
	while( !sio_oschar() )
		;
	__CONCAT(UTXH, CONSOLE_PORT) = c;
}
#endif
