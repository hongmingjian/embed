/* 
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * This file is used to define some macros and data packet format for usb host.
 */
 
/*------------------------------------------------------------------------------
 * project ....: USB Host driver
 * file .......: usb_desc.h
 * authors ....: Long Yun(lane312@126.com)
 * tutors  ....: Hong Mingjian
 * team .......: Driver Group(嵌入式驱动开发小组)
 * organization: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * creation ...: 1.3.2008
 */

#ifndef DESCRIPTOR_H
#define DESCRIPTOR_H

#include "type.h"
#include "ohci.h"
#include "queue.h"

/* USB PID type definitions */
#define USB_PID_OUT		0x87
#define USB_PID_IN		0x96
#define USB_PID_SOF		0xA5
#define USB_PID_SETUP	0xB4
#define USB_PID_DATA0	0xC3
#define USB_PID_DATA1	0xD2
#define USB_PID_ACK		0x4B
#define USB_PID_NAK		0x5A
#define USB_PID_STALL	0x78
#define USB_PID_PRE		0x3C

/* Standard request codes */
#define GET_STATUS			0
#define CLEAR_FEATURE		1
#define	SET_FEATURE			3
#define	SET_ADDRESS			5
#define	GET_DESCRIPTOR		6
#define	SET_DESCRIPTOR		7
#define	GET_CONFIGURATION	8
#define	SET_CONFIGURATION	9
#define	GET_INTERFACE		10
#define SET_INTERFACE		11
#define	SYNCH_FRAME			12

/* Descriptor types */
#define DESC_DEVICE			1
#define	DESC_CONFIGURATION	2
#define	DESC_STRING			3
#define	DESC_INTERFACE		4
#define	DESC_ENDPOINT		5

/* Feature selectors */
#define	DEVICE_REMOTE_WAKEUP	1
#define	ENDPOINT_HALT			0

#define USB_DEF_ADDR	0x00
#define USB_EP0			0x0
#define USB_SETUP_LEN	0x8

#define USB_MAX_INTERFACES	8
#define USB_MAX_ENDPOINTS	16
#define USB_MAX_DEVICES		32

/* Pipe information */
#define USB_PIPE_IN		1
#define USB_PIPE_OUT	0	

#define PIPE_TYPE_CTRL	0	/* control transfer */
#define PIPE_TYPE_ISOC	1	/* isochronous transfer */
#define PIPE_TYPE_BULK	2	/* bulk transfer */
#define PIPE_TYPE_INTR	3	/* interrupt transfer */

#define USB_PIPE_HALT	0
#define USB_PIPE_START	1

/* Standard device request packets */
struct usb_setup_pkt {
	uint8_t  bmRequestType;
	uint8_t  bRequest;
	uint16_t wValue;
	uint16_t wIndex;
	uint16_t wLength;
}__attribute__ ((packed));

struct usb_device_desc {
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint16_t bcdUSB;
	uint8_t  bDeviceClass;
	uint8_t  bDeviceSubClass;
	uint8_t  bDevicePortocol;
	uint8_t  bMaxPacketSize0;
	uint16_t idVendor;
	uint16_t idProduct;
	uint16_t bcdDevice;
	uint8_t  iManufacturer;
	uint8_t  iProduct;
	uint8_t  iSerialNumber;
	uint8_t  bNumConfigurations;
}__attribute__ ((packed));

struct usb_endpoint_desc {
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint8_t  bEndpointAddress;
	uint8_t  bmAttributes;
	uint16_t wMaxPacketSize;
	uint8_t  bInterval;
}__attribute__ ((packed));

struct usb_interface_desc {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint8_t bInterfaceNumber;
	uint8_t bAlternateSetting;
	uint8_t bNumEndpoints;
	uint8_t bInterfaceClass;
	uint8_t bInterfaceSubClass;
	uint8_t bInterfaceProtocol;
	uint8_t iInterface;
	struct usb_endpoint_desc *ep_desc;
}__attribute__ ((packed));

struct usb_config_desc {
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint16_t wTotalLength;
	uint8_t  bNumInterfaces;
	uint8_t  bConfigurationValue;
	uint8_t  iConfiguration;
	uint8_t  bmAttributes;
	uint8_t  MaxPower;
	struct usb_interface_desc *if_desc;
}__attribute__ ((packed));

struct usb_string_desc {
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint16_t wLANGID[1];
}__attribute__ ((packed));

/* Enum the transfer request status */
enum usb_req_status{
	REQ_UNINIT = 0,
	REQ_PENDING_REQUESTS,	
	REQ_IN_PROGRESS,
	REQ_NORMAL_COMPLETION,
	/* errors */
	REQ_CANCELLED,
	REQ_BAD_ADDR,
	REQ_BAD_EP,
	REQ_STALLED,
	REQ_ERROR,
	/* for device driver */
	REQ_UNKNOWN_DEVICE,

	REQ_ERROR_MAX		/* must be last */
};

/* Usb endpoint structure */
struct usb_endpoint {
    uint8_t  ep_type; 		/* 0-out, 1-in, 2-control */
    uint8_t  trans_type; 	/* 0-control, 1-isochronous, 2-bulk, 3-interrupt */
    uint8_t  dev_addr; 		/* device address */
    uint8_t  ep_nbr; 		/* endpoint number */
    uint8_t  lowspeed;		/* 0-high, 1-low */ 
    uint16_t ep_halted;		/* 0-normal, 1-halted */
    uint32_t interval; 		/* interval, 1 for isoc, 1-255 for interrupt */
    uint32_t max_pkt_size;	/* max packet size */
};

/* Hub port structure */
struct usb_hub_port {
	uint8_t port_num;
	uint32_t port_status;
	struct usb_device *device;
};

/* Hub structure */
struct usb_hub {
	int down_port_cnt;	/* count of downstream ports */
	uint32_t hub_status;
	struct usb_hub_port *up_port;
	struct usb_hub_port *down_ports;
};

/* Usb device structure */
struct usb_device {
	uint8_t dev_addr;		/* device address */
	uint8_t config_value;	/* config value of device */
  	uint8_t remote_wakeup;	/* 0-disabled, 1-enabled */
  	uint8_t self_powered;	/* 0-bus powered, 1-self powered */
  	uint8_t max_power;		/* the power bus should provide */
	int mf_index;			/* manufacturer index, 0 if no string desc for it */
	int prod_index;			/* product index, 0 if no string desc for it */
  	int serial_index;		/* serial number index, 0 if no string desc for it */
  	int  lowspeed;			/* 0 for high speed device, 1 for slow speed device */
  	int  max_pkt_size0;		/* ep0 max packet size */
  	int  config_count;		/* count of config */
  	sint16_t langid;		/* language ID for strings */
#define USBD_NOLANG	-1
  	struct usb_endpoint 	*usb_ep;		/* endpoints */
  	struct usb_device_desc	dev_desc;		/* device descriptor */
  	struct usb_config_desc	*config_desc;	/* configuration descriptors */
  	struct usb_hub			*hub;			/* only validate when this device is hub */
};

/* Usb IRP data block for the data buffer between usb driver and device driver */
struct irp_data_block {
	uint8_t *buffer;	/* data buffer */
	uint32_t act_len;	/* actual length transferred or returned */
	enum usb_req_status irp_status;	/* IRP's status */
};

/* Usb transfer request */
struct usb_req {
	uint8_t trans_dir;	/* transfer direction, 0-out, 1-in */
	uint8_t req_maxdi;	/* max delay interrupt */
	uint8_t req_shxferok;	/* short transfer ok */
	struct usb_setup_pkt pkt;	/* setup packet for control transfer */
	struct irp_data_block *idata;	/* irp data block */
};

/* The structure transferred between usb driver and host controller driver */
struct usb_req_block {
	struct usb_endpoint ep;
	struct usb_req req;
	SLIST_ENTRY(usb_req_block) entries;
};

/* Define one structure for usb driver in order to apply some memory */
struct usb_driver {
	uint8_t buffer[256];
	uint8_t version[8];		/* one string identify the version of usb */
	int tag;
	uint32_t dev_switch;	/* each bit for one device, 0-device on, 1-no device */
	uint32_t dev_count;		/* stands for the dev address of each one */
	struct irp_data_block inner_idata;	/* used for usb dirver inner IRP */
	struct usb_setup_pkt setup_pkt;
	struct usb_device device[32];
};

/* Define one structure to store the device driver address and some characters */
struct dev_driver {
	uint8_t dev_class;
	uint8_t dev_sub_class;
	uint16_t dev_vid;
	uint16_t dev_pid;
	uint32_t drv_addr;
	SLIST_ENTRY(dev_driver)	entries;
};

#endif
