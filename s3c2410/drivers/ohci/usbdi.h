/* 
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * This file is used to define the interface functions between usb driver and host
 * controller driver.
 */
 
/*------------------------------------------------------------------------------
 * project ....: USB Host driver
 * file .......: usbdi.h
 * authors ....: Long Yun(lane312@126.com)
 * tutors  ....: Hong Mingjian
 * team .......: Driver Group(嵌入式驱动开发小组)
 * organization: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * creation ...: 12.3.2008
 */

#ifndef USBDI_H
#define USBDI_H

#include "usb_desc.h"

void submit_ctrl_msg(struct usb_req_block *urb);
void submit_bulk_msg(struct usb_req_block *urb);
void submit_intr_msg(struct usb_req_block *urb);
void submit_isoc_msg(struct usb_req_block *urb);

/*******************************************************************************
 * The interface function from hub device in host to hub device in device 
 */
int roothub_reset_port(uint8_t port_num);
/**
 * This interface function can get the port number which is connected to one device,
 * or disconnected one device. The returned value contains such information:
 *  bit 0-7: port number
 *  bit 8: attach/unattach 0-attach one device, 1-unattach
 */
uint16_t roothub_get_connected_port();

#endif
