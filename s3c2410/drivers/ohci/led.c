#include "config.h"
#include "lib.h"

void led_on(unsigned int led)
{
	GPFDAT ^= led;
}

void led_off(unsigned int led)
{
	GPFDAT |= led;
}

void led_blink(unsigned int led, unsigned long freq)
{
	led_on(led);
	delay_ms(freq);
	led_off(led);
	delay_ms(freq);
}

void init_led()
{
  GPFCON &= 0xffff3cff;
  GPFCON |= 0x4100;
  GPFUP   = 0xff; /*disable pull up*/
  led_off(LED1 | LED2);
}
