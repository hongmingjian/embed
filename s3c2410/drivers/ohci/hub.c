/* 
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * This file implements all the functions of hub control.
 */
 
/*------------------------------------------------------------------------------
 * project ....: USB Host driver
 * file .......: usbd.c
 * authors ....: Long Yun(lane312@126.com)
 * tutors  ....: Hong Mingjian
 * team .......: Driver Group(嵌入式驱动开发小组)
 * organization: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * creation ...: 24.3.2008
 */

#include "hub.h"

/**
 * This function can disable the hub feature.
 * @param value can be C_HUB_LOCAL_POWER, or C_HUB_OVER_CURRENT, it means diable 
 * the relevant feature. 
 * You can get the result of this transfer event in idata->irp_status.
 */
void hub_clear_hub_feature(struct usb_device *dev, uint16_t value,
	struct irp_data_block *idata)
{
	struct usb_setup_pkt pkt;
	pkt.bRequestType = 0x20;
	pkt.bRequest = HUB_CLEAR_FEATURE;
	pkt.wValue = value;
	pkt.wIndex = 0x0000;
	pkt.wLength = 0x0000;
	
	if(value != C_HUB_OVER_CURRENT && value != C_HUB_LOCAL_POWER) {
		idata->irp_status = REQ_ERROR;
		return;
	}
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, 0), pkt, idata);
}

/**
 * This function can clear the port's feature. 
 * @param index specifies the port number and @param value specifies which feature
 * you want to clear. @param value should be as following:
 * 	PORT_ENABLE
 * 	PORT_SUSPEND
 * 	PORT_POWER
 * 	C_PORT_CONNECTION
 * 	C_PORT_RESET
 * 	C_PORT_ENABLE
 * 	C_PORT_SUSPEND
 * 	C_PORT_OVER_CURRENT.
 * If the specified port doesn't exist, then irp_status will be REQ_ERROR.
 */
void hub_clear_port_feature(struct usb_device *dev, uint16_t value, uint16_t index,
	struct irp_data_block *idata)
{
	struct usb_setup_pkt pkt;
	pkt.bRequestType = 0x23;
	pkt.bRequest = HUB_CLEAR_FEATURE;
	pkt.wValue = value;
	pkt.wIndex = index;
	pkt.wLength = 0x0000;	
	
	if(value != PORT_ENABLE && value != PORT_SUSPEND && value != PORT_POWER &&
		value != C_PORT_CONNECTION && value != C_PORT_RESET && value != C_PORT_ENABLE &&
		value != C_PORT_SUSPEND && value != C_PORT_OVER_CURRENT) {
		idata->irp_status = REQ_ERROR;
		return;
	}
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, 0), pkt, idata);
}

/**
 * This function can get the bus state value. The value contains following:
 * 	Bit 0: the value of D- signal
 * 	Bit 1: the value of D+ signal 
 * 	Bit 2-7: Reserved
 * The status of IRP would be REQ_STALL if index is an invalid port number.
 */
void 
hub_get_bus_state(struct usb_device *dev, uint16_t index,
	struct irp_data_block *idata)
{
	struct usb_setup_pkt pkt;
	pkt.bRequestType = 0xa3;
	pkt.bRequest = HUB_GET_STATE;
	pkt.wValue = 0x0000;
	pkt.wIndex = index;
	pkt.wLength = 0x0000;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, 0), pkt, idata);
}

/**
 * This function can get the hub descriptor.
 * @param desc_index specifies the index of descriptor.
 * @param length specifies the length you wonder to returned. 
 */
void 
hub_get_hub_desc(struct usb_device *dev, uint8_t desc_type, uint8_t desc_index,
	uint16_t length, struct irp_data_block *idata)
{
	struct usb_setup_pkt pkt;
	pkt.bRequestType = 0xa0;
	pkt.bRequest = HUB_GET_DESCRIPTOR;
	pkt.wValue = desc_type << 8 + desc_index;
	pkt.wIndex = 0x0000;
	pkt.wLength = length;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, 0), pkt, idata);
}

/**
 * Get the status of hub. The returned data is four bytes. And the structure is as
 * following:
 * 	Bit 0:  Local Power Source, 0-power supply good, 1-power supply lost
 *	Bit 1:  Over-current Indicator, 0-not exist, 1-exist
 *	Bit 2-15:  Reserved
 *	Bit 16: Local Power Status Change, 0-not changed, 1-changed
 *	Bit 17: Over-Current Indicator Change, 0-not changed, 1-changed
 *	Bit 18-31: Reserved 
 */
void 
hub_get_hub_status(struct usb_device *dev, struct irp_data_block *idata)
{
	struct usb_setup_pkt pkt;
	pkt.bRequestType = 0xa0;
	pkt.bRequest = HUB_GET_STATUS;
	pkt.wValue = 0x0000;
	pkt.wIndex = 0x0000;
	pkt.wLength = 0x4;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, 0), pkt, idata);
}

/**
 * This function can get the status of specified port. Returned data are as following:
 *  Bit 0:  Current Connect Status, 0-no device attached, 1-one device attached
 *	Bit 1:  Port Enabled/Disabled, 0-disabled, 1-enabled
 *  Bit 2:  Suspend, 0-not suspended, 1-suspended
 *	Bit 3:  Over-Current Indicator, 0-doesn't exist, 1-exist
 *	Bit 4:  Reset, 0-reset not asserted, 1-reset asserted
 *	Bit 5-7:   Reserved
 *	Bit 8:  Port Power, 0-in powered-off state, 1-not in powered-off state
 *	Bit 9:  Low Speed Device Attached: 0-full-speed, 1-low-speed
 *	Bit 10-15: Reserved
 *  Bit 16: Connect Status Change, 0-no change, 1-changed
 *	Bit 17: Port Enable/Disable Change, this bit set to one if port disabled
 *	Bit 18: Suspend Change, 0-no change, 1-resume complete
 *	Bit 19: Over-Current Indicator Change, 0-no change, 1-changed
 *	Bit 20: Reset Change, 0-no change, 1-reset complete
 *  Bit 21-31: Reserved
 */
void 
hub_get_port_status(struct usb_device *dev, uint16_t index, 
	struct irp_data_block *idata)
{
	struct usb_setup_pkt pkt;
	pkt.bRequestType = 0xa3;
	pkt.bRequest = HUB_GET_STATUS;
	pkt.wValue = 0x0000;
	pkt.wIndex = index;
	pkt.wLength = 0x4;
	
	if(index <= 0) {
		idata->irp_status = REQ_ERROR;
		return;	
	}
		
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, 0), pkt, idata);
}

/**
 * This function can set one new descriptor to the hub. 
 * @param desc_index speicify the index of new descriptor. And the new descriptor
 * stores in idata->buffer.
 */
void 
hub_set_hub_desc(struct usb_device *dev, uint8_t desc_type, uint8_t desc_index,
	uint16_t length, struct irp_data_block *idata)
{
	struct usb_setup_pkt pkt;
	pkt.bRequestType = 0x20;
	pkt.bRequest = HUB_GET_DESCRIPTOR;
	pkt.wValue = desc_type << 8 + desc_index;
	pkt.wIndex = 0x0000;
	pkt.wLength = length;
	
	if(desc_index <= 0) {
		idata->irp_status = REQ_ERROR;
		return;	
	}
		
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, 0), pkt, idata);
}

/**
 * This function can set the hub feature.
 * @param value can only be C_HUB_LOCAL_POWER or C_HUB_OVER_CURRENT.
 */
void 
hub_set_hub_feature(struct usb_device *dev, uint16_t value,
	struct irp_data_block *idata)
{
	struct usb_setup_pkt pkt;
	pkt.bRequestType = 0x20;
	pkt.bRequest = HUB_SET_FEATURE;
	pkt.wValue = Value;
	pkt.wIndex = 0x0000;
	pkt.wLength = 0x0000;
	
	if(value != C_HUB_LOCAL_POWER && value != C_HUB_OVER_CURRENT) {
		idata->irp_status = REQ_ERROR;
		return;	
	}
		
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, 0), pkt, idata);
}

/**
 * This function can clear the port feature.
 * @param index specifies the port number and @param value should be one of the 
 * following value:
 *  PORT_RESET
 *  PORT_SUSPEND
 *  PORT_POWER
 */		
void 
hub_set_port_feature(struct usb_device *dev, uint16_t value, uint16_t index,
	struct irp_data_block *idata)
{
	struct usb_setup_pkt pkt;
	pkt.bRequestType = 0x23;
	pkt.bRequest = HUB_SET_FEATURE;
	pkt.wValue = value;
	pkt.wIndex = index;
	pkt.wLength = 0x0000;
	
	if(value != PORT_RESET && value != PORT_SUSPEND && value != PORT_POWER) {
		idata->irp_status = REQ_ERROR;
		return;	
	}
		
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, 0), pkt, idata);	
}

/**
 * This function can initialize the hub device.
 * @return 0 if init ok, -1 if error.
 */
int hub_init(struct usb_device *dev)
{
	uint8_t buffer[256];
	struct usb_hub_desc	*hub_desc;
	struct irp_data_block idata;
	idata.buffer = buffer;
	idata.buf_len = 256;
	
	hub_get_hub_desc(dev, HUB_DESCRIPTOR, 0, 0xff, &idata);
	delay_ms(50);
	if(idata.irp_status == REQ_NORMAL_COMPLETION)
		*hub_desc = &idata;
		
	dev->hub->down_port_cnt = hub_desc->bNbrPorts;
	struct usb_hub_port hub_port[hub_desc->bNbrPorts];	
}
