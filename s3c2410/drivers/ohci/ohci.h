/* 
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * This file is used to define some registers and data structure for HCD.
 */
 
/*------------------------------------------------------------------------------
 * project ....: USB Host driver
 * file .......: usb_desc.h
 * authors ....: Du Jiangfeng  Long Yun(lane312@126.com)
 * tutors  ....: Hong Mingjian
 * team .......: Driver Group(嵌入式驱动开发小组)
 * organization: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * creation ...: 1.3.2008
 */
 
#ifndef _OHCI_DEF_H_
#define _OHCI_DEF_H_

#include "usbdi.h"
#include "type.h"

/************ OHCI registers ************/

/* Control and Status partition */
#define OHCI_REVISION			(0x00)
#define  OHCI_REV_LO(v)			((v) & 0xf)
#define  OHCI_REV_HI(v)			(((v) >> 4) & 0xf)
#define  OHCI_REV_LEGACY(v)		((v) & 0x100)
#define OHCI_CONTROL			(0x04)
#define  OHCI_CBSR_MASK			(0x3 << 0)
#define  OHCI_CBSR_1_1			(0x0 << 0)
#define  OHCI_CBSR_2_1			(0x1 << 0)
#define  OHCI_CBSR_3_1			(0x2 << 0)
#define  OHCI_CBSR_4_1			(0x3 << 0)
#define  OHCI_PLE				(0x1 << 2)
#define  OHCI_IE				(0x1 << 3)
#define  OHCI_CLE				(0x1 << 4)
#define  OHCI_BLE				(0x1 << 5)
#define  OHCI_HCFS_MASK			(0x3 << 6)
#define  OHCI_HCFS_RESET		(0x0 << 6)
#define  OHCI_HCFS_RESUME		(0x1 << 6)
#define  OHCI_HCFS_OPERA		(0x2 << 6)
#define  OHCI_HCFS_SUSPEND		(0x3 << 6)
#define  OHCI_IR				(0x1 << 8)
#define  OHCI_RWC				(0x1 << 9)
#define  OHCI_RWE				(0x1 << 10)
#define OHCI_COMMAND_STATUS		(0x08)
#define  OHCI_HCR				(0x1 << 0)
#define  OHCI_CLF				(0x1 << 1)
#define  OHCI_BLF				(0x1 << 2)
#define  OHCI_OCR				(0x1 << 3)
#define  OHCI_SOC_MASK			(0x3 << 16)
#define OHCI_INTERRUPT_STATUS	(0x0c)
#define OHCI_INTERRUPT_ENABLE	(0x10)
#define OHCI_INTERRUPT_DISABLE	(0x14)
#define  OHCI_SO				(0x1 << 0)
#define  OHCI_WDH				(0x1 << 1)
#define  OHCI_SF				(0x1 << 2)
#define  OHCI_RD				(0x1 << 3)
#define  OHCI_UE				(0x1 << 4)
#define  OHCI_FNO				(0x1 << 5)
#define  OHCI_RHSC				(0x1 << 6)
#define  OHCI_OC				(0x1 << 30)
#define  OHCI_MIE				(0x1 << 31)

/* Memory pointer partition */
#define OHCI_HCCA				(0x18)
#define OHCI_PERIOD_CURRENT_ED	(0x1c)
#define OHCI_CONTROL_HEAD_ED	(0x20)
#define OHCI_CONTROL_CURRENT_ED	(0x24)
#define OHCI_BULK_HEAD_ED		(0x28)
#define OHCI_BULK_CURRENT_ED	(0x2c)
#define OHCI_DONE_HEAD			(0x30)

/* Freame number partition */
#define OHCI_FM_INTERVAL		(0x34)
#define  OHCI_GET_IVAL(v)		((v) & 0x3fff)
#define  OHCI_GET_FSMPS(v)		(((v) >> 16) & 0x7fff)
#define  OHCI_FIT				(0x1 << 31)
#define OHCI_FM_REMAINING		(0x38)
#define OHCI_FM_NUMBER			(0x3c)
#define OHCI_PERIODIC_START		(0x40)
#define OHCI_LS_THRESHOLD		(0x44)

/* Root hub partition */
#define OHCI_RH_DESCRIPTOR_A	(0x48)
#define  OHCI_GET_NDP(v)		((v) & 0xff)
#define  OHCI_PSM				(0x1 << 8)
#define  OHCI_NPS				(0x1 << 9)
#define  OHCI_GET_POTPGT(v)		((v) >> 24)
#define OHCI_RH_DESCRIPTOR_B	(0x4c)
#define OHCI_RH_STATUS			(0x50)
#define  OHCI_LPS				(0x1 << 0)
#define  OHCI_OCI				(0x1 << 1)
#define  OHCI_DRWE				(0x1 << 15)
#define  OHCI_LPSC				(0x1 << 16)
#define  OHCI_OCIC				(0x1 << 17)
#define  OHCI_CRWE				(0x1 << 31)
#define OHCI_RH_PORT_STATUS1	(0x54)
#define OHCI_RH_PORT_STATUS2	(0x58)

#define OHCI_REGS_CNT			23

#define OHCI_ALL_LES (OHCI_PLE|OHCI_IE|OHCI_CLE|OHCI_BLE)
#define OHCI_ALL_INTRS (OHCI_SO|OHCI_WDH|OHCI_SF|OHCI_RD|OHCI_UE|\
						OHCI_FNO|OHCI_RHSC|OHCI_OC)
#define OHCI_NORMAL_INTRS (OHCI_SO|OHCI_WDH|OHCI_RD|OHCI_UE|OHCI_RHSC)
#define OHCI_FSMPS(i) (((i - 210) * 6 / 7) << 16)
#define OHCI_PERIODIC(i) ((i) * 9 / 10)

/************ OHCI HCCA descriptor ************/

typedef vuint32_t ohci_addr_t;

#define OHCI_INTRS_CNT 32

struct ohci_hcca {
	ohci_addr_t intrtable[OHCI_INTRS_CNT];
	uint16_t	fmnbr;
	uint16_t	pad1;
	ohci_addr_t donehead;
	uint8_t		rsvd[116];
} __attribute__ ((aligned(256)));

#define OHCI_HCCA_MASK 0xff
#define OHCI_HCCA_ALIGN 256

/************ OHCI endpoint descriptor ************/

struct ohci_ed {
	vuint32_t	ctrl;
#define OHCI_ED_FA_MASK		(0x3f)
#define OHCI_ED_GET_FA(v)	((v) & 0x3f)
#define OHCI_ED_SET_FA(v)	((v) & 0x3f)
#define OHCI_ED_EN_MASK		(0xf << 7)
#define OHCI_ED_GET_EN(v)	((v) >> 7 & 0xf)
#define OHCI_ED_SET_EN(v)	((v) << 7)
#define OHCI_ED_DI_MASK		(0x3 << 11) /* DIrection */
#define OHCI_ED_GET_DI(v)	((v)>>11 & 0x3)
#define  OHCI_ED_DI_TD		(0x0 << 11)
#define  OHCI_ED_DI_OUT		(0x1 << 11)
#define  OHCI_ED_DI_IN		(0x2 << 11)
#define OHCI_ED_SPEED		(0x1 << 13)
#define OHCI_ED_SKIP		(0x1 << 14)
#define OHCI_ED_FORMAT		(0x1 << 15)
#define OHCI_ED_MPS_MASK	(0x7ff << 16)
#define OHCI_ED_GET_MPS(v)	(((v) & OHCI_ED_MPS_MASK)>>16)
#define OHCI_ED_SET_MPS(v)	((v) << 16)
	ohci_addr_t tailp;
	ohci_addr_t headp;
#define OHCI_ED_HALTED		(0x1 << 0)
#define OHCI_ED_CARRY		(0x1 << 1)
	ohci_addr_t nexted;
} __attribute__ ((aligned(16)));

#define OHCI_ED_ALIGN	(16)
#define OHCI_ED_MASK	(0xf)

/************ OHCI transfer descriptor ***********/

/* General transfer descriptor */
struct ohci_gtd {
	vuint32_t ctrl;
#define OHCI_GTD_ROUNDING	(0x1 << 18)
#define OHCI_GTD_DP_MASK	(0x3 << 19)
#define  OHCI_GTD_SET_DP(v) ((v) << 19)
#define  OHCI_GTD_DP_SETUP	(0x0 << 19)
#define  OHCI_GTD_DP_OUT	(0x1 << 19)
#define  OHCI_GTD_DP_IN		(0x2 << 19)
#define OHCI_GTD_DI_MASK	(0x7 << 21) /* Delay Interrupt */
#define OHCI_GTD_SET_DI(v)	((v) << 21)
#define  OHCI_GTD_NO_DI		(0x7 << 21)
#define OHCI_GTD_T_MASK		(0x3 << 24)
#define  OHCI_GTD_T_CARRY 	(0x0 << 24)
#define  OHCI_GTD_T_DATA0	(0x2 << 24)
#define  OHCI_GTD_T_DATA1	(0x3 << 24)
#define OHCI_GTD_GET_EC(v)	(((v) >> 26) & 0x3)
#define OHCI_GTD_GET_CC(v)	((v) >> 28)
#define  OHCI_GTD_NO_CC		(0xf << 28)
	ohci_addr_t cbp;
	ohci_addr_t nextgtd;
	ohci_addr_t be;
} __attribute__ ((aligned(16)));

#define OHCI_GTD_ALIGN		16
#define OHCI_GTD_MASK		(0xf)

/* isochronous transfer descriptor */
#define OHCI_OFFSET_CNT 8
struct ohci_itd {
	vuint32_t 	ctrl;
#define OHCI_ITD_GET_SF(v)	((v) & 0xffff)
#define OHCI_ITD_SET_SF(v)	((v) & 0xffff)
#define OHCI_ITD_DI_MASK	(0x7 << 0x21)
#define OHCI_ITD_SET_DI(v)	((v) << 0x21)
#define OHCI_ITD_NO_DI		(0x7 << 0x21)
#define OHCI_ITD_GET_FC(v)	((((v) >> 24) & 7) + 1)
#define OHCI_ITD_SET_FC(v)	(((v) - 1) << 24)
#define OHCI_ITD_GET_CC(v)	((v) >> 28)
#define  OHCI_ITD_NO_CC		(0xf << 28)
	ohci_addr_t bp0;
#define OHCI_ITD_BP0_MASK	(0xfffff000)
	ohci_addr_t nextitd;
	ohci_addr_t be;
	vuint16_t	offset[OHCI_OFFSET_CNT];
#define psw 	offset
#define OHCI_ITD_OFFSET_MASK	(0xfff)
#define OHCI_ITD_PAGE_SELECT	(0x1 << 12)
#define OHCI_ITD_PSW_LENGTH(v)	((v) & 0xfff)
#define OHCI_ITD_PSW_GET_CC(v)	((v) >> 12)
} __attribute__ ((aligned(32)));

#define OHCI_ITD_ALIGN 32
#define OHCI_ITD_MASK 0x1f

/* Completion Code */
#define OHCI_CC_NO_ERROR				0
#define OHCI_CC_CRC						1
#define OHCI_CC_BIT_STUFFING			2
#define OHCI_CC_DATA_TOGGLE_MISMATCH	3
#define OHCI_CC_STALL					4
#define OHCI_CC_DEVICE_NOT_RESPONDING	5
#define OHCI_CC_PID_CHECK_FAILURE		6
#define OHCI_CC_UNEXPECTED_PID			7
#define OHCI_CC_DATA_OVERRUN			8
#define OHCI_CC_DATA_UNDERRUN			9
#define OHCI_CC_BUFFER_OVERRUN			12
#define OHCI_CC_BUFFER_UNDERRUN			13
#define OHCI_CC_NOT_ACCESSED			15

/********** OHCI soft descriptor ***********/

struct ohci_sgtd {
	struct ohci_gtd	 gtd;
	struct ohci_sed  *sed;
	struct usb_req	 *req;
	struct ohci_sgtd *next;
	ohci_addr_t		 physaddr;
	uint32_t		 status;
#define OHCI_TD_NOT_FILLED 0
#define OHCI_TD_FILLED 1
#define OHCI_TD_CANCELED 2
	uint32_t		transcnt;
};

struct ohci_sitd {
	struct ohci_itd  itd;
	struct ohci_sed  *sed;
	struct usb_req	 *req;
	struct ohci_sitd *next;
	ohci_addr_t		 physaddr;
	uint32_t		 status;
};

struct ohci_sed {
	struct ohci_ed		ed;
	struct usb_endpoint *ep;
	struct ohci_sed		*next;
	ohci_addr_t			physaddr; /* physical address of ed */
	uint32_t			rate;
	uint32_t			processing;
};

/************** OHCI soft controller **************/

#define OHCI_INTR_TREE_NO (OHCI_INTRS_CNT * 2 - 1)

struct ohci_softc {
	struct ohci_hcca	*hcca;
	struct ohci_sed		*ctrl_head;
	struct ohci_sed		*bulk_head;
	struct ohci_sed		*isoc_head;
	struct ohci_sed		*intr_tree[OHCI_INTR_TREE_NO];
	struct ohci_sed		*free_seds;
	struct ohci_sgtd	*free_sgtds;
	struct ohci_sitd	*free_sitds;
};

//#ifdef _DEBUG
struct ohci_softc sc;
//#endif /* _DEBUG */

/*************** OHCI interfaces *******************/
int ohci_init(struct ohci_softc *sc);

#endif /* _OHCI_DEF_H_ */
