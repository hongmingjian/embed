/* 
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * This file defines the functions of hub control and hub device driver. 
 * It contains the hub class-specific class, some hub defined macro and functions.
 */
 
/*------------------------------------------------------------------------------
 * project ....: USB Host driver
 * file .......: usbd.c
 * authors ....: Long Yun(lane312@126.com)
 * tutors  ....: Hong Mingjian
 * team .......: Driver Group(嵌入式驱动开发小组)
 * organization: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * creation ...: 24.3.2008
 */

#ifndef HUB_H
#define HUB_H

#include "usbd.h"

/* Hub class request codes */
#define HUB_GET_STATUS		0
#define HUB_CLEAR_FEATURE	1
#define HUB_GET_STATE		2
#define HUB_SET_FEATURE		3
#define HUB_GET_DESCRIPTOR	6
#define HUB_SET_DESCRIPTOR	7

/* Hub class feature selectors */
#define C_HUB_LOCAL_POWER		0
#define C_HUB_OVER_CURRENT		1
#define PORT_CONNECTION			0
#define PORT_ENABLE				1
#define PORT_SUSPEND			2
#define PORT_OVER_CURRENT		3
#define PORT_RESET				4
#define PORT_POWER				8
#define PORT_LOW_SPEED			9
#define C_PORT_CONNECTION		16
#define C_PORT_ENABLE			17
#define C_PORT_SUSPEND			18
#define C_PORT_OVER_CURRENT		19
#define	C_PORT_RESET			20

#define HUB_DESCRIPTOR		0x29

/* Usb hub descriptor */
struct usb_hub_desc {
	uint8_t bDescLength;
	uint8_t bDescriptorType;
	uint8_t bNbrPorts;
	uint16_t wHubCharacteristics;
	uint8_t bPwrOn2PwrGood;
	uint8_t bHubContrCurrent;
	uint32_t DeviceRemovable;
	uint32_t PortPwrCtrlMask;
}__attribute__ ((packed));


/* Hub class-specific request functions */
void hub_clear_hub_feature(struct usb_device *dev, uint16_t value,
	struct irp_data_block *idata);
void hub_clear_port_feature(struct usb_device *dev, uint16_t value, uint16_t index,
	struct irp_data_block *idata);
void hub_get_bus_state(struct usb_device *dev, uint16_t index,
	struct irp_data_block *idata);
void hub_get_hub_desc(struct usb_device *dev, uint8_t desc_type, uint8_t desc_index,
	uint16_t length, struct irp_data_block *idata);
void hub_get_hub_status(struct usb_device *dev, struct irp_data_block *idata);
void hub_get_port_status(struct usb_device *dev, uint16_t index,
	struct irp_data_block *idata);
void hub_set_hub_desc(struct usb_device *dev, uint8_t desc_type, uint8_t desc_index,
	uint16_t length, struct irp_data_block *idata);
void hub_set_hub_feature(struct usb_device *dev, uint16_t value,
	struct irp_data_block *idata);
void hub_set_port_feature(struct usb_device *dev, uint16_t value, uint16_t index,
	struct irp_data_block *idata);
	
void hub_init(struct usb_device *dev);

#endif
