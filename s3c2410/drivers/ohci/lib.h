#ifndef LIB_H
#define LIB_H

#include "frame.h"
#include "ohci.h"

extern void (*ivt[])(struct contextframe * cf, int irq);
void sti(), cli();

void delay_ms(register unsigned long ms);
void delay_us(register unsigned long us);

void init_uart();
int sio_ischar();
int sio_oschar();
char sio_getc();
void sio_putc(char);

void led_on(unsigned int led);
void led_off(unsigned int led);
void led_blink(unsigned int led, unsigned long freq);
void init_led();

#endif /* LIB_H */
