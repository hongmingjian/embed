#ifndef _TYPE_H_
#define _TYPE_H_

typedef void                void_t;

typedef signed char         sint8_t;
typedef unsigned char       uint8_t;
typedef signed short        sint16_t;
typedef unsigned short      uint16_t;
typedef signed long         sint32_t;
typedef unsigned long       uint32_t;

typedef volatile sint8_t    vsint8_t;
typedef volatile uint8_t    vuint8_t;
typedef volatile sint16_t   vsint16_t;
typedef volatile uint16_t   vuint16_t;
typedef volatile sint32_t   vsint32_t;
typedef volatile uint32_t   vuint32_t;

typedef uint32_t            count_t;
typedef uint32_t            size_t;


#endif /* _TYPE_H_ */
