/* 
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * This file defines all the interface between usb driver and device driver.
 * This file defines the functions, variables and pipes for usb driver. 
 * The variables should be defined here to ensure the enough memory for usb driver.
 * The functions are almost the interface of usb driver. The interfaces consist of 
 * device on/off function, control transfer, bulk transfer, interrupt transfer and
 * isochronous transfer functions. Pipe, the definition for transfer is also defined
 * here.
 */
 
/*------------------------------------------------------------------------------
 * project ....: USB Host driver
 * file .......: usbd.h
 * authors ....: Long Yun(lane312@126.com)
 * tutors  ....: Hong Mingjian
 * team .......: Driver Group(嵌入式驱动开发小组)
 * organization: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * creation ...: 1.3.2008
 */

#ifndef USBD_H
#define USBD_H

#include "usb_desc.h"
#include "queue.h"
#include "usbdi.h"

/*******************************************************************************
 * USB driver functions for initialize and device on/off 
 */
void usbh_boot();
int usbh_device_on(uint8_t port_nbr);
int usbh_device_off();

/*******************************************************************************
 * USB pipe is one information gather as following:
 * - device address:  bit 0-6 (7 bits) 
 * - endpoint number: bit 7-10 (4 bits)
 * - direction: bit 11-12 (2 bits, 0-Out, 1-In)
 * - transfer type: bit 16-17 (2 bits, 0-control, 1-isochronous,3-bulk, 4-interrupt)
 *	
 * This format is similar to the format of OHCI 1.0
 */
#define USBH_CREATE_PIPE(dev, ep) \
	(((dev)->dev_addr) | (ep << 7))
#define USBH_DEFAULT_PIPE	((USB_DEF_ADDR) | (USB_EP0 << 7) | (PIPE_TYPE_CTRL << 16))
/* Create various pipes, each returns one pipe */
#define USBH_SND_CTRL_PIPE(dev,ep)	((USB_PIPE_OUT << 11) | (PIPE_TYPE_CTRL << 16) | USBH_CREATE_PIPE(dev,ep))
#define USBH_RCV_CTRL_PIPE(dev,ep)	((USB_PIPE_IN  << 11) | (PIPE_TYPE_CTRL << 16) | USBH_CREATE_PIPE(dev,ep))
#define USBH_SND_ISOC_PIPE(dev,ep)	((USB_PIPE_OUT << 11) | (PIPE_TYPE_ISOC << 16) | USBH_CREATE_PIPE(dev,ep))
#define USBH_RCV_ISOC_PIPE(dev,ep)	((USB_PIPE_IN  << 11) | (PIPE_TYPE_ISOC << 16) | USBH_CREATE_PIPE(dev,ep))
#define USBH_SND_BULK_PIPE(dev,ep)	((USB_PIPE_OUT << 11) | (PIPE_TYPE_BULK << 16) | USBH_CREATE_PIPE(dev,ep))
#define USBH_RCV_BULK_PIPE(dev,ep)	((USB_PIPE_IN  << 11) | (PIPE_TYPE_BULK << 16) | USBH_CREATE_PIPE(dev,ep))
#define USBH_SND_INTR_PIPE(dev,ep)	((USB_PIPE_OUT << 11) | (PIPE_TYPE_INTR << 16) | USBH_CREATE_PIPE(dev,ep))
#define USBH_RCV_INTR_PIPE(dev,ep)	((USB_PIPE_IN  << 11) | (PIPE_TYPE_INTR << 16) | USBH_CREATE_PIPE(dev,ep))
#define USBH_SND_DEFCTRL_PIPE		((USB_PIPE_OUT << 11) | USBH_DEFAULT_PIPE)
#define USBH_RVC_DEFCTRL_PIPE		((USB_PIPE_IN  << 11) | USBH_DEFAULT_PIPE)
/* Pipe information */
#define USBH_PIPE_DEV_ADDR(pipe)		(pipe & 0x7f)			/* return device address */
#define USBH_PIPE_EP_NUM(pipe)			((pipe >> 7) & 0xf)		/* return endpoint number */
/* Endpoint control, return 0 if sucess, -1 if error */
#define	USBH_ENDPOINT_HALT(dev, ep)		(usbh_set_feature(dev, 2, 0, ep))
#define USBH_ENDPOINT_START(dev, ep)	(usbh_clear_feature(dev, 2, 0,ep))
/******************************************************************************* 
 * Standard device request functions.
 * These are the interfaces for device driver.
 */
void usbh_clear_feature(struct usb_device *dev, uint8_t request_type, uint16_t value,
	uint16_t index, struct irp_data_block *idata);
void usbh_get_config(struct usb_device *dev, struct irp_data_block *idata);
void usbh_get_desc(struct usb_device *dev, uint8_t desc_type, uint8_t desc_index,
	uint16_t index, uint16_t length, struct irp_data_block *idata);
void usbh_get_interface(struct usb_device *dev, uint16_t index, 
	struct irp_data_block *idata);
void usbh_get_status(struct usb_device *dev, uint8_t request_type, uint16_t index,
	struct irp_data_block *idata);

void usbh_set_address(struct usb_device *dev, uint16_t value,
	struct irp_data_block *idata);
void usbh_set_config(struct usb_device *dev, uint8_t value,
	struct irp_data_block *idata);
void usbh_set_desc(struct usb_device *dev, uint8_t desc_type, uint8_t desc_index,
	uint16_t index, uint16_t length, struct irp_data_block *idata);
void usbh_set_feature(struct usb_device *dev, uint16_t request_type, uint16_t value,
	uint16_t index, struct irp_data_block *idata);
void usbh_set_interface(struct usb_device *dev, uint16_t value, uint16_t index,
	struct irp_data_block *idata);
/* Only for isochronous transfer */	
int usbh_synch_frame(struct usb_device *dev, uint16_t index,
	struct irp_data_block *idata);

/* This function transfer setup packet to host controller driver */
void usbh_ctrl_msg(struct usb_device *dev, uint32_t pipe, struct usb_setup_pkt packet,
	struct irp_data_block *idata);

/*******************************************************************************
 * Bulk transfer interface 
 */
void usbh_bulk_msg(struct usb_device *dev, uint32_t pipe, 
	struct irp_data_block *idata);

/*******************************************************************************
 * Interrupt transfer interface 
 */
void usbh_intr_msg(struct usb_device *dev, uint32_t pipe, struct irp_data_block *idata,
	uint8_t interval);

/*******************************************************************************
 * Isochronous transfer interface
 */
void usbh_isoc_msg(struct usb_device *dev, uint32_t pipe, 
	struct irp_data_block *idata);
	
/*******************************************************************************
 * IRP manage functions
 */
void usbh_manage_irp();

/*******************************************************************************
 * This function is used for device driver to register on usb bus.
 * @param dev_vid is the idVendor of device descriptor
 * @param dev_pid is the idProduct of device descriptor
 * @param dev_class is the bDeviceClass of device descriptor
 * @param dev_sub_class is the bDeviceSubClass of device descriptor
 * @param driver_addr is the device driver function's address
 * @return 0 if register successful, -1 if driver already exists. 
 * Every time when the usb host boots, the device driver should first run this
 * function to register the driver. And the driver's format should as following:
 * void dev_driver(struct usb_device *).
 * The return type should be void and the parameter should be struct usb_device *
 */
int usbh_dev_reg(uint8_t dev_class, uint8_t dev_sub_class, uint16_t dev_vid,
	uint16_t dev_pid, uint32_t drv_addr);

#endif
