/*
 * timer.c: funcitons implement for timer
 */

#include "config.h"
#include "frame.h"
#include "lib.h"
#include "usbd.h"

#ifdef _DEBUG
#include <stdio.h>
#endif /* _DEBUG */

volatile unsigned ticks;

static void
timer_isr(struct contextframe *cf, int irq)
{
	usbh_manage_irp();
#ifdef _DEBUG
	
#endif /* _DEBUG */
}

void
init_timer(void)
{
	/*XXX*/
	TCFG0 = 0xff << 8;
	TCFG1 = 0x3 << 16;
	TCNTB4 = 6103;	
	TCON = 0x6 << 20; /*update TCNTBn*/	
	TCON = 0x5 << 20; /*kick it off*/
	
  ivt[INT_TIMER4] = timer_isr;
  CLEAR_PENDING(INT_TIMER4);
  INT_ENABLE(INT_TIMER4);
}
