/* 
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * This file is used to implementation the fucntion of HCD.
 */
 
/*------------------------------------------------------------------------------
 * project ....: USB Host driver
 * file .......: usb_desc.h
 * authors ....: Du Jiangfeng  Long Yun(lane312@126.com)
 * tutors  ....: Hong Mingjian
 * team .......: Driver Group(嵌入式驱动开发小组)
 * organization: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * creation ...: 1.3.2008
 */
 
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "ohci.h"

#ifdef _DEBUG
#define DPRINTF printf
#else
#define DPRINTF
#endif


struct ohci_sgtd *g_sgtd = NULL;

/******************************************************************************/

/* Entry bits for making up HCCA interrupt table */
static uint8_t entrybits[OHCI_INTRS_CNT] = {
	0x00, 0x10, 0x08, 0x18, 0x04, 0x14, 0x0c, 0x1c,
	0x02, 0x12, 0x0a, 0x1a, 0x06, 0x16, 0x0e, 0x1e,
	0x01, 0x11, 0x09, 0x19, 0x05, 0x15, 0x0d, 0x1d,
	0x03, 0x13, 0x0b, 0x1b, 0x07, 0x17, 0x0f, 0x1f 
};

/* Read and Write registers */
#define REG_READ4(x)		REGL(HC_REGBASE, (x))
#define REG_WRITE4(x, y)	REGL(HC_REGBASE, (x)) = (y)

/* Change between physical address and pointer */
#define ATOP(a, p) 	((p) (a))
#define PTOA(p)		((ohci_addr_t) (p))

#define MEM_SET(p, v, s) \
({ \
	int i = s; \
	while(i) \
		((char *) p)[--i] = v; \
})

/******************************************************************************/

static struct ohci_hcca *
ohci_alloc_hcca(struct ohci_softc *sc)
{
	struct ohci_hcca *hcca;
	hcca = (struct ohci_hcca *) 
		memalign(OHCI_HCCA_ALIGN, sizeof(struct ohci_hcca));

	if (hcca == NULL)
		return 0;

	MEM_SET(hcca, 0, sizeof(struct ohci_hcca));

	return (sc->hcca = hcca);
}

/******************************************************************************/

static struct ohci_sed *
ohci_alloc_sed(struct ohci_softc *sc)
{
	struct ohci_sed  *res;

	if (sc->free_seds) {
		res = sc->free_seds;
		sc->free_seds = res->next;
	} else {
		res = (struct ohci_sed *) 
			memalign(OHCI_ED_ALIGN, sizeof(struct ohci_sed));
	}

	if (res == NULL)
		return 0;
	
	MEM_SET(res, 0, sizeof(struct ohci_sed));

	return (res);
}

static void
ohci_free_sed(struct ohci_softc *sc, struct ohci_sed *sed)
{
	MEM_SET(sed, 0, sizeof(struct ohci_sed));
	
	sed->next = sc->free_seds;
	sc->free_seds = sed;
}

/******************************************************************************/

static struct ohci_sgtd *
ohci_alloc_sgtd(struct ohci_softc *sc)
{
	struct ohci_sgtd  *res;

	if (sc->free_sgtds) {
		res = sc->free_sgtds;
		sc->free_sgtds = res->next;
	} else {
		res = (struct ohci_sgtd *) 
			memalign(OHCI_GTD_ALIGN, sizeof(struct ohci_sgtd));
	}

	if (res == NULL)
		return 0;
	
	MEM_SET(res, 0, sizeof(struct ohci_sgtd));
	
	return (res);
}

static void
ohci_free_sgtd(struct ohci_softc *sc, struct ohci_sgtd *sgtd)
{
	MEM_SET(sgtd, 0, sizeof(struct ohci_sgtd));
	
	sgtd->next = sc->free_sgtds;
	sc->free_sgtds = sgtd;
}

/******************************************************************************/

static struct ohci_sitd *
ohci_alloc_sitd(struct ohci_softc *sc)
{
	struct ohci_sitd  *res;

	if (sc->free_sitds) {
		res = sc->free_sitds;
		sc->free_sitds = res->next;
	} else {
		res = (struct ohci_sitd *) 
			memalign(OHCI_ITD_ALIGN, sizeof(struct ohci_sitd));
	}

	if (res == NULL)
		return 0;
	
	return (res);
}

static void
ohci_free_sitd(struct ohci_softc *sc, struct ohci_sitd *sitd)
{
	MEM_SET(sitd, 0, sizeof(struct ohci_sitd));
	
	sitd->next = sc->free_sitds;
	sc->free_sitds = sitd;
}

/******************************************************************************/

#ifdef _DEBUG

static void
ohci_dump_control(void)
{
	vuint32_t r;
	char *hcfs[4] = {"RESET", "RESUME", "OPERATIONAL", "SUSPEND"};
	
	r = REG_READ4(OHCI_CONTROL);
	
	printf("<< OHCI_CONTROL >>\n"
		   "CBSR: %x : 1\n"
		   "LE: %s%s%s%s\n"
		   "HCFS: %s\n"
		   "IR: %x\n"
		   "RW: %s and%s Connected\n\n",
		   r & OHCI_CBSR_MASK,
		   (r & OHCI_PLE) ? "PLE " : "",
		   (r & OHCI_IE) ? "IE " : "",
		   (r & OHCI_CLE) ? "CLE " : "",
		   (r & OHCI_BLE) ? "BLE " : "",
		   hcfs[(r & OHCI_HCFS_MASK) >> 6],
		   (r & OHCI_IR) ? 1 : 0,
		   (r & OHCI_RWE) ? "Enable" : "Disable",
		   (r & OHCI_RWC) ? "" : " Not");

	delay_ms(100);
}

static void
ohci_dump_registers(void)
{
	int i;

	for (i = 0; i < OHCI_REGS_CNT; i++) {
		printf("0x%02x=0x%08x  ", i*4, REG_READ4(i*4));
		if (i % 2)
			printf("\n");
	}
	printf("\n");
	
//	ohci_dump_control();
	
	delay_ms(100);
}

static void
ohci_dump_sed(struct ohci_sed *sed)
{
	vuint32_t ctl;
	char *di[3] = {"From TD", "Out", "In"};
	
	ctl = sed->ed.ctrl;
	
	printf("$ ED Address: %p\n"
		   "$ ED ctrl: 0x%08x\n"
//		   "$ Device Address: %d\n"
//		   "$ Endpoint Number: %d\n"
//		   "$ Speed: %s Speed\n"
//		   "$ Direction: %s\n"
//		   "$ Skip: %s\n"
//		   "$ Format: %s\n"
//		   "$ Max Packet Size: %d\n"
		   "$ headp=0x%08x\n"
		   "$ tailp=0x%08x\n",
		   sed, ctl,
		   OHCI_ED_GET_FA(ctl),
//		   OHCI_ED_GET_EN(ctl),
//		   (ctl & OHCI_ED_SPEED) ? "Full" : "Low",
//		   di[OHCI_ED_GET_DI(ctl)],
//		   (ctl & OHCI_ED_SKIP) ? "Yes" : "No",
//		   (ctl & OHCI_ED_FORMAT) ? "Isochronous" : "General",
//		   OHCI_ED_GET_MPS(ctl),
		   sed->ed.headp, sed->ed.tailp);
}

static void
ohci_dump_gtd(struct ohci_gtd *td)
{
	printf("$ td_ctrl=0x%08x\n", td->ctrl);
	printf("$ td_next=0x%08x\n", td->nextgtd);
}

/*
static void
ohci_dump_hcca(struct ohci_hcca *hcca)
{
	printf("*** ohci_dump_hcca ***\n");
	printf("hcca_"
}
*/

#endif /* _DEBUG */

/******************************************************************************/

/*
 * Some codes are derived from FreeBSD website.
 * http://freebsd.active-venture.com/FreeBSD-srctree/newsrc/dev/usb/ohci.c.html
 *
 * Copyright (c) 1998 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * OHCI_init(): initialize ohci soft controller. 
 * Malloc memory resource, establish hcca interrupt table,
 * initialize ohci registers and root hub registers.
 */

int
ohci_init(struct ohci_softc *sc)
{
	int i;
	ohci_addr_t paddr;
	uint32_t s, ctl, ival, hcr, fm, per, rev;
	struct ohci_sed *sed, *psed;
	struct ohci_hcca *phcca;
	
	static struct ohci_sed static_sed;
	MEM_SET(&static_sed, 0, sizeof(struct ohci_sed));

/*	
	DPRINTF("ohci_init: lowlevel init\n");
	CLKSLOW |= (0x1<<7);
	UPLLCON = 0x78023;
	MISCCR &= ~(0x1<<3);
	MISCCR &= ~((0x1<<12)|(0x1<<13));
	CLKSLOW &= ~((0x1<<7)|(0x1<<5)|(0x1<<4));
	CLKCON |= (0x1<<6);
*/
	
#ifdef _DEBUG
	printf("<Debug> ohci_init: start\n");
#endif

	MEM_SET(sc, 0, sizeof(struct ohci_softc));
	
	/* Check the revision */
	rev = REG_READ4(OHCI_REVISION);
	printf("OHCI revision: %d.%d%s\n", OHCI_REV_HI(rev), OHCI_REV_LO(rev),
		OHCI_REV_LEGACY(rev) ? " Legacy Supported" : "");
	
	if (OHCI_REV_HI(rev) != 1 || OHCI_REV_LO(rev) != 0) {
		printf("<Err> Driver unsupported OHCI revision, init halted !!\n");
		return -1;
	}
	
	/* Allocate the HCCA area */
	phcca = ohci_alloc_hcca(sc);
	if (phcca == NULL) {
		printf("<Err> Alloc HCCA not access, init halted !!\n");
		return -2;
	}

#ifdef _DEBUG
	printf("<Debug> ohci_init: HCCA Address 0x%08x\n", phcca);
#endif
	
	/* Allocate dummy ED that starts the control list. */
	sed = ohci_alloc_sed(sc);
//	sed = &static_sed;
	if (sed == NULL) {
		printf("<Err> Alloc SED not access, init halted !!\n");
		return -2;
	}

	sed->ed.ctrl |= OHCI_ED_SKIP;
	sed->physaddr = PTOA(&sed->ed);
	sed->processing = 0;
	sc->ctrl_head = sed;

#ifdef _DEBUG
	printf("<Debug> ohci_init: Ctrl Head Address 0x%08x\n", 
		sc->ctrl_head->physaddr);
//	ohci_dump_sed(sed);
//	while (1);
#endif

	/* Allocate dummy ED that starts the bulk list. */
	sed = ohci_alloc_sed(sc);
	if (sed == NULL) {
		printf("<Err> Alloc SED not access, init halted !!\n");
		return -2;
	}

	sed->ed.ctrl |= OHCI_ED_SKIP;
	sed->physaddr = PTOA(&sed->ed);
	sed->processing = 0;
	sc->bulk_head = sed;

#ifdef _DEBUG
	printf("<Debug> ohci_init: Bulk Head Address 0x%08x\n", 
		sc->bulk_head->physaddr);
#endif

	/* Allocate dummy ED that starts the isochronous list. */
	sed = ohci_alloc_sed(sc);
	if (sed == NULL) {
		printf("<Err> Alloc SED not access, init halted !!\n");
		return -2;
	}

	sed->ed.ctrl |= OHCI_ED_SKIP;
	sed->physaddr = PTOA(&sed->ed);
	sed->processing = 0;
	sc->isoc_head = sed;

#ifdef _DEBUG
	printf("<Debug> ohci_init: Isoc Head Address 0x%08x\n", 
		sc->isoc_head->physaddr);
#endif

	/* Allocate all the dummy EDs that make up the interrupt tree */
	for (i = 0; i < OHCI_INTR_TREE_NO; i++) {
		sed = ohci_alloc_sed(sc);
		if (sed == NULL) {
			while (i--)
				ohci_free_sed(sc, sc->intr_tree[i]);
			printf("Make up interrupt tree failed, init halted !!\n");
			return -2;
		}

		sed->ed.ctrl |= OHCI_ED_SKIP;
		sed->physaddr = PTOA(&sed->ed);
		sed->processing = 0;
		sc->intr_tree[i] = sed;

		if (i != 0) {
			sc->intr_tree[i] = sed;
			psed = sc->intr_tree[i]->next = sc->intr_tree[(i-1)/2];
			sc->intr_tree[i]->ed.nexted = PTOA(psed->physaddr);
		} else {
			sc->intr_tree[i] = sed;
			psed = sc->intr_tree[i]->next = sc->isoc_head;
			sc->intr_tree[i]->ed.nexted = PTOA(psed->physaddr);
		}
	}
	
	/* Fill HCCA interrupt table */
	for (i = 0; i < OHCI_INTRS_CNT; i++)
//		sc->hcca->intrtable[entrybits[i]] = 
//			sc->intr_tree[OHCI_INTR_TREE_NO-OHCI_INTRS_CNT+i]->physaddr;
		sc->hcca->intrtable[i] = 0;

	/* Determain in what context we are running. */
	ctl = REG_READ4(OHCI_CONTROL);
	if (ctl & OHCI_IR) {
		/* SMM active, request change */
		printf("ohci_init: SMM active, owner change request...\n");
		s = REG_READ4(OHCI_COMMAND_STATUS);
		REG_WRITE4(OHCI_COMMAND_STATUS, s|OHCI_OCR);
		for (i = 0; i < 100 && (ctl & OHCI_IR); i++) {
			delay_ms(1);
			ctl = REG_READ4(OHCI_CONTROL);
		}
		if ((ctl & OHCI_IR) == 0) {
			printf("ohci_init: SMM does not repond, resetting...\n");
			REG_WRITE4(OHCI_CONTROL, OHCI_HCFS_RESET);
			/* Controller was cold started. */
			delay_ms(10); /* usb specification 1.1 (9.2.6.2) */
		}
	} else if ((ctl & OHCI_HCFS_MASK) != OHCI_HCFS_RESET) {
		/* BIOS started controller. */
		if ((ctl & OHCI_HCFS_MASK) != OHCI_HCFS_OPERA) {
			REG_WRITE4(OHCI_CONTROL, OHCI_HCFS_OPERA);
			delay_ms(10); /* usb specification 1.1 (9.2.6.2) */
		}
	} else {

#ifdef _DEBUG
		printf("ohci_init: cold started\n");
#endif
		delay_ms(10);
	}
	
	/*
	 * This reset should not be necessary according to the OHCI spec, 
	 * but without it some controllers do not start.
	 */
	REG_WRITE4(OHCI_CONTROL, OHCI_HCFS_RESET);
	delay_ms(10);
	
	/* We not own the host controller and the bus has been reset. */
	ival = OHCI_GET_IVAL(REG_READ4(OHCI_FM_INTERVAL));
	
	REG_WRITE4(OHCI_COMMAND_STATUS, OHCI_HCR); /* Reset HC */
	/* Nominal time for a reset is 10 us. */
	for (i = 0; i < 10; i++) {
		delay_us(1);
		hcr = REG_READ4(OHCI_COMMAND_STATUS) & OHCI_HCR;
		if (!hcr)
			break;
	}
	if (hcr) {
		printf("ohci_init: reset timeout\n");
		return -3;
	}
	
	/* The controller is now in SUSPEND state, we have 2ms to finish. */
	
	/* Set up HC registers. */
	REG_WRITE4(OHCI_HCCA, PTOA(sc->hcca));
	REG_WRITE4(OHCI_CONTROL_HEAD_ED, sc->ctrl_head->physaddr);
	REG_WRITE4(OHCI_BULK_HEAD_ED, sc->bulk_head->physaddr);
	/* disable all interrupts and then switch on all desired interrupts */
	REG_WRITE4(OHCI_INTERRUPT_DISABLE, OHCI_ALL_INTRS);
	REG_WRITE4(OHCI_INTERRUPT_ENABLE, OHCI_NORMAL_INTRS | OHCI_MIE);
	/* switch on desired function features */
	ctl = REG_READ4(OHCI_CONTROL);
	ctl &= ~(OHCI_CBSR_MASK | OHCI_ALL_LES | OHCI_HCFS_MASK | OHCI_IR);
//	ctl |= OHCI_ALL_LES | OHCI_CBSR_4_1 | OHCI_HCFS_OPERA;
	ctl |= OHCI_CLE | OHCI_CBSR_4_1 | OHCI_HCFS_OPERA;
	/* And finilly start it! */
	REG_WRITE4(OHCI_CONTROL, ctl);
	
	/*
	 * The controller is now OPERATIONAL. Set a some final
	 * registers that should be set earlier, but that the 
	 * controller ignores when in the SUSPEND state.
	 */
	fm = (REG_READ4(OHCI_FM_INTERVAL) & OHCI_FIT) ^ OHCI_FIT;
	fm |= OHCI_FSMPS(ival) | ival;
	REG_WRITE4(OHCI_FM_INTERVAL, fm);
	per = OHCI_PERIODIC(ival);
	REG_WRITE4(OHCI_PERIODIC_START, per);
	
	REG_WRITE4(OHCI_RH_STATUS, OHCI_LPSC); /* Enable prot power */

#undef _DEBUG
#ifdef _DEBUG
	printf("ohci_init: finished.\n");
#endif

#ifdef _DEBUG
	ohci_dump_registers();
#endif

	return 0;
}

#undef _DEBUG
#ifdef _DEBUG
void
test_ohci(struct ohci_softc *sc)
{
	int i = 1;
	vuint32_t is;

	printf("OHCI test begin...\n");

	while (1) {
		is = REG_READ4(OHCI_INTERRUPT_STATUS);
		if (is & OHCI_RHSC) {
			REG_WRITE4(OHCI_INTERRUPT_STATUS, OHCI_RHSC);
			printf("DEBUG: Device %s.\n", (i % 2) ? "connected" : "removed");
			i++;
		}
	}
}
#endif /* _DEBUG */


static struct ohci_sed *
ohci_add_ctrl_sed(struct ohci_softc *sc, struct usb_endpoint *ep)
{
	struct ohci_sed *sed;
	struct ohci_sgtd *sgtd;
//	static struct ohci_sed static_sed;
//	MEM_SET(&static_sed, 0, sizeof(struct ohci_sed));

	/* Initialize an endpoint descriptor for this endpoint */
	sed = ohci_alloc_sed(sc);
//	sed = &static_sed;
	if (sed == NULL) {
		printf("ERROR: Alloc ED not access.\n");
		return (0);
	}
	sed->ep = ep;
	sed->rate = ep->interval;
	sed->physaddr = PTOA(&sed->ed);	
	sed->ed.ctrl |= OHCI_ED_SET_FA(ep->dev_addr) | OHCI_ED_SET_EN(ep->ep_nbr);
	sed->ed.ctrl |= (ep->lowspeed ? OHCI_ED_SPEED : 0)
				 	| OHCI_ED_SET_MPS(ep->max_pkt_size);

	g_sgtd = sgtd = ohci_alloc_sgtd(sc);
	if (sgtd == NULL) {
		printf("ERROR: Alloc TD not access.\n");
		return 0;
	}
	sgtd->sed = sed;
	sgtd->status = OHCI_TD_NOT_FILLED;
	sgtd->physaddr = PTOA(&sgtd->gtd);
	sed->ed.headp = sed->ed.tailp = sgtd->physaddr;
	
	/* Link this ED onto the HC and HCD list */
	sed->next = sc->ctrl_head->next;
	sed->ed.nexted = sc->ctrl_head->ed.nexted;
	sc->ctrl_head->next = sed;
	sc->ctrl_head->ed.nexted = sed->physaddr;
//	DPRINTF("ctrlhead.ed.next=0x%08x\n", sc->ctrl_head->ed.nexted);
//	DPRINTF("ohci_add_ctrl_sed: sed=%p\n", sed);
//	DPRINTF("ohci_add_ctrl_sed: headp=0x%08x, tailp=0x%08x\n", sed->ed.headp, sed->ed.tailp);
//	DPRINTF("OHCI_CONTROL_HEAD: 0x%08x -> 0x%08x\n", 
//		HC_CONTROL_HEAD_ED, ((struct ohci_ed *)HC_CONTROL_HEAD_ED)->nexted);
#ifdef _DEBUG
	printf("add sed:");
	ohci_dump_sed(sed);
#endif
	
//	while (1);
	return (sed);
}

static void
ohci_rmv_ctrl_sed(struct ohci_softc *sc, struct ohci_sed *sed)
{
	struct ohci_sed *p;
	struct ohci_sgtd *sgtd, *nsgtd;
	
	for (p = sc->ctrl_head; p->next != sed; p = p->next)
		;
	
	/* Prevent Host Controller from processing this ED */
	sed->ed.ctrl |= OHCI_ED_SKIP;
	
	/* Unlink this ED */
	p->next = sed->next;
	p->ed.nexted = sed->next->physaddr;

	/* Free the TDs linking onto this ED, if there is */
	if (sed->ed.headp) {
		sgtd = ATOP(sed->ed.headp, struct ohci_sgtd *);
		nsgtd = sgtd->next;
		while (sgtd) {
			ohci_free_sgtd(sc, sgtd);
			sgtd = nsgtd;
			nsgtd = (nsgtd == NULL) ? NULL : nsgtd->next;
		}
	}
	
	/* Free this ED */
	ohci_free_sed(sc, sed);
}

static int
ohci_queue_ctrl_std(struct ohci_softc *sc, struct ohci_sed *sed, struct usb_req *req)
{
	struct ohci_sgtd *sgtd, *fsgtd, *lsgtd;
	uint32_t cnt, remlen; /* transfer count and remaining length */
	char *cbp;
	
	if ((sed == NULL) || (sed->ep == NULL))
		return -1; /* ED has not add on the list */
	
	fsgtd = sgtd = ATOP(sed->ed.tailp, struct ohci_sgtd *);
	
	req->idata->irp_status = REQ_PENDING_REQUESTS;

	remlen = 8;
	req->idata->act_len = 0; /* report back bytes transferred so far */
	cbp = req->idata->buffer;
	
	/* Setup a TD for setup packet */
#ifdef _DEBUG
	DPRINTF("ohci_queue_ctrl_std: setup=%p\n", sgtd);
#endif
	sgtd->req = req;
	sgtd->sed = sed;
/*	sgtd->gtd.cbp = PTOA(((char *) &req->pkt)[0]);
	sgtd->gtd.be = PTOA(((char *) &req->pkt)[7]);*/
	sgtd->gtd.cbp = &req->pkt;
	sgtd->gtd.be = (&req->pkt) + 8;
//	printf("%x %x\n", sgtd->gtd.cbp, sgtd->gtd.be);
	sgtd->gtd.ctrl |= OHCI_GTD_DP_SETUP | OHCI_GTD_T_DATA0 
					| OHCI_GTD_ROUNDING | OHCI_GTD_NO_DI | OHCI_GTD_NO_CC;
	sgtd->next = ohci_alloc_sgtd(sc);
	sgtd->next->physaddr = sgtd->gtd.nextgtd = PTOA(&sgtd->next->gtd);
	sgtd->transcnt = 0;
	sgtd->status = OHCI_TD_FILLED;
	lsgtd = sgtd;
	sgtd = sgtd->next;

	HC_COMMAND_STATUS |= OHCI_CLF;
	
	/* Setup TD for data packets */
	if (remlen) {
#ifdef _DEBUG
		DPRINTF("ohci_queue_ctrl_std: data=%p\n", sgtd);
#endif
		sgtd->req = req;
		sgtd->sed = sed;
		
		sgtd->gtd.cbp = &cbp;
		sgtd->gtd.be = sgtd->gtd.cbp + 255;
		sgtd->transcnt = remlen;
		sgtd->status = OHCI_TD_FILLED;
		sgtd->gtd.ctrl = req->trans_dir ? OHCI_GTD_DP_IN : OHCI_GTD_DP_OUT;
		sgtd->gtd.ctrl |= OHCI_GTD_T_DATA1;
		if (/*remlen -= cnt &&*/ !req->req_shxferok)
			sgtd->gtd.ctrl |= OHCI_GTD_ROUNDING;
		sgtd->gtd.ctrl |= OHCI_GTD_NO_DI;
		sgtd->gtd.ctrl |= OHCI_GTD_NO_CC;
		sgtd->next = ohci_alloc_sgtd(sc);
		sgtd->next->physaddr = sgtd->gtd.nextgtd = PTOA(&sgtd->next->gtd);
		lsgtd = sgtd;
		sgtd = sgtd->next;
		sgtd->req->idata->irp_status = REQ_PENDING_REQUESTS;
	}
	HC_COMMAND_STATUS |= OHCI_CLF;
	
	/* Setup TD for status phase */
#ifdef _DEBUG
	DPRINTF("ohci_queue_ctrl_std: status=%p\n", sgtd);
#endif
	sgtd->req = req;
	sgtd->sed = sed;
	sgtd->gtd.cbp = 0;
	sgtd->gtd.be = 0;
	sgtd->transcnt = 0;
	sgtd->gtd.ctrl = req->trans_dir ? OHCI_GTD_DP_OUT : OHCI_GTD_DP_IN;
	sgtd->gtd.ctrl |= OHCI_GTD_T_DATA0;
	sgtd->gtd.ctrl |= OHCI_GTD_NO_DI;
	sgtd->gtd.ctrl |= OHCI_GTD_NO_CC;

	sgtd->next = ohci_alloc_sgtd(sc);
	sgtd->next->physaddr = sgtd->gtd.nextgtd = PTOA(&sgtd->next->gtd);
	lsgtd = sgtd;
	sgtd = sgtd->next;
	
	/* Setup interrupt delay */
	lsgtd->gtd.ctrl &= ~OHCI_GTD_DI_MASK;
	lsgtd->gtd.ctrl |= OHCI_GTD_SET_DI(min(6, req->req_maxdi));
	
	/* Set new tailp in ED */
	sgtd->req = NULL;
	sed->ed.tailp = sgtd->physaddr;
#ifdef _DEBUG
	DPRINTF("ohci_queue_ctrl_std: sed.headp=%p, sed.tailp=%p\n", sed->ed.headp, sed->ed.tailp);
#endif
	
	HC_COMMAND_STATUS |= OHCI_CLF;
//	REG_WRITE4(OHCI_COMMAND_STATUS, OHCI_CLF);
//	DPRINTF("OHCI_COMMAND_STATUS: 0x%08x\n", HC_COMMAND_STATUS);
	
	return 0;
}


static void
ohci_process_done_queue(struct ohci_softc *sc)
{
	struct ohci_sgtd *p, *revhead = NULL;
	struct ohci_sed *sed;
	struct usb_req *req;
	struct usb_endpoint *ep;
	int transcnt;
	uint32_t dh, cc;
	
	dh = sc->hcca->donehead;

	struct ohci_ed *ed;
	
	/*
	 * Reverse the queue passed from controller while 
	 * virtualizing addresses.
	 */
#ifdef _DEBUG
	DPRINTF("ohci_process_done_queue: donehead=0x%08x\n", dh);
//	ohci_dump_gtd((struct ohci_gtd *) dh);
//	ohci_dump_registers();
	DPRINTF("OHCI_CONTROL_HEAD: 0x%08x -> 0x%08x", 
		HC_CONTROL_HEAD_ED, ((struct ohci_ed *)HC_CONTROL_HEAD_ED)->nexted);
	ed = (struct ohci_ed *)((struct ohci_ed *)HC_CONTROL_HEAD_ED)->nexted;
	printf(" -> 0x%08x\n", ed->nexted);
	DPRINTF("OHCI_BULK_HEAD: 0x%08x -> 0x%08x\n", 
		HC_BULK_HEAD_ED, ((struct ohci_ed *)HC_BULK_HEAD_ED)->nexted);
	
	ohci_dump_sed((struct ohci_sed *)((struct ohci_ed *)HC_CONTROL_HEAD_ED)->nexted);
	DPRINTF("setup_td.ctrl = 0x%08x\n", g_sgtd->gtd.ctrl);
#endif

	if (dh == 0) {
		DPRINTF("ohci_process_done_queue: donehead=0\n");
		return;
	}
	
//	DPRINTF("ohci_process_done_queue: reserve done head\n");
	do {
		p = ATOP(dh, struct ohci_sgtd *);
		dh = p->gtd.nextgtd;
		p->gtd.nextgtd = PTOA(revhead);
		revhead = p;
	} while(dh);

	/* Process list that is now reordered to completion order */
	while (revhead != NULL) {
		p = revhead;
		revhead = ATOP(p->gtd.nextgtd, struct ohci_sgtd *);
		sed = p->sed;
		ep = sed->ep;
		req = p->req;

		if (ep == NULL) {
			/* ED of this TD has retired */
			ohci_free_sgtd(sc, p);
			if (req->idata->irp_status == REQ_CANCELLED
			|| req->idata->irp_status == REQ_PENDING_REQUESTS )
				req->idata->irp_status = REQ_CANCELLED;
			continue;
		}

		if (ep->trans_type != 1) {
			if (p->gtd.cbp) {
				p->transcnt =
					(((p->gtd.be ^ p->gtd.cbp) & 0xfffff000) ? 0x00001000 : 0) 
					+ (p->gtd.be & 0x00000fff) - (p->gtd.cbp & 0x00000fff) + 1;
			}
			if ((p->gtd.ctrl & OHCI_GTD_DP_MASK) != OHCI_GTD_DP_SETUP) {
				req->idata->act_len += p->transcnt;
			}
			if (OHCI_ITD_GET_CC(p->gtd.ctrl) == OHCI_CC_NO_ERROR) {
				/*
				 * TD completed without error. remove it from USB
				 * request list, if USB request list is now empty,
				 * then complete the request.
				 */
				if (&p->next->gtd == ATOP(p->sed->ed.tailp, struct ohci_gtd *)) {
					if (req->idata->irp_status != REQ_CANCELLED)
						req->idata->irp_status = REQ_NORMAL_COMPLETION;
					else
						req->idata->irp_status = REQ_CANCELLED;
				 	
				 	/* XXX */
					ohci_rmv_ctrl_sed(sc, sed);
					ohci_free_sgtd(sc, p);
					
					return;
				}

				req->idata->irp_status = REQ_IN_PROGRESS;
			} else {
				/*
				 * TD completed with an error, remove it and other TDs for
				 * same request.
				 */
				if (ep != NULL)
					ohci_rmv_ctrl_sed(sc, sed);
				
				cc = OHCI_GTD_GET_CC(p->gtd.ctrl);
				switch (cc) {
				case OHCI_CC_STALL:
					req->idata->irp_status = REQ_STALLED;
					break;
				case OHCI_CC_DEVICE_NOT_RESPONDING:
					req->idata->irp_status = REQ_BAD_ADDR;
					break;
				default:
					req->idata->irp_status = REQ_BAD_EP;
					break;
				}				
				ohci_free_sgtd(sc, p);
			}
		}
	}
}

static int
ohci_intr(struct ohci_softc *sc)
{
	uint32_t intrs, temp, temp1;
	
	/* Check interrupt type */
	if (sc->hcca->donehead) {
		intrs = OHCI_WDH; /* note interrupt processing required */
		if (sc->hcca->donehead & 0x1) { /* has other interrupt */
			intrs |= REG_READ4(OHCI_INTERRUPT_STATUS)
					& REG_READ4(OHCI_INTERRUPT_ENABLE);
		}
	} else {
		intrs = REG_READ4(OHCI_INTERRUPT_STATUS)
			   & REG_READ4(OHCI_INTERRUPT_ENABLE);
		if (!intrs) {
			DPRINTF("ohci_intr: no interrupt, donehead=%p\n", sc->hcca->donehead);
			return -1; /* no interrupt */
		}
	}

	/* Has intrs, prevent HC from doing it to us again until we are finished */
	REG_WRITE4(OHCI_INTERRUPT_ENABLE, OHCI_MIE);
	
	if (intrs & OHCI_SO) {
		/* XXX do SchedulingOverrun */
	}
	if (intrs & OHCI_UE) {
		/* XXX The controller is hung, reset */
//		ohci_reset(sc);
	}
	if (intrs & OHCI_RD) {
		/*
		 * Resume has been requested ty a device on USB.
		 * HCD must wait 20ms then put controller in the
		 * USB_OPERATIONAL state.
		 */
		intrs &= ~OHCI_RD;
		REG_WRITE4(OHCI_INTERRUPT_STATUS, OHCI_RD);
		
		/* XXX other job */
	}
	if (intrs & OHCI_WDH) {
		ohci_process_done_queue(sc);

		/* Done Queue processing complete, notify controller */
		sc->hcca->donehead = 0;
		REG_WRITE4(OHCI_INTERRUPT_STATUS, OHCI_WDH);
		intrs &= ~OHCI_WDH;
	}
	if (intrs & OHCI_RHSC) {
		/* XXX leave it has been */
	}
	if (intrs & ~OHCI_MIE)
		REG_WRITE4(OHCI_INTERRUPT_DISABLE, intrs);
	
	REG_WRITE4(OHCI_INTERRUPT_ENABLE, OHCI_MIE);
	return 0;
}

static int
ohci_ctrl_transfer(struct ohci_softc *sc, struct usb_req_block *urb)
{
	int res;
	
	struct ohci_sed *sed = ohci_add_ctrl_sed(sc, &urb->ep);
	if (!sed) {
#ifdef _DEBUG
		printf("ohci_ctrl_transfer: add ED not access\n");
#endif
		return (-1);
	}
	
	HC_CONTROL_CURRENT_ED = HC_CONTROL_HEAD_ED;
	printf("%x\n\n", HC_CONTROL_CURRENT_ED);
	
	res = ohci_queue_ctrl_std(sc, sed, &urb->req);
	if (res) {
#ifdef _DEBUG
		printf("ohci_ctrl_transfer: queue TD not access\n");
#endif
		return (-2);
	}
	
	delay_ms(10);
	ohci_intr(&sc);

//#ifdef _DEBUG
//	ohci_dump_registers();
//#endif

	return (0);
}

static struct ohci_sed *
ohci_search_sed(struct ohci_sed *head, struct usb_endpoint *ep)
{
	struct ohci_sed *p;
	
	for (p = head; p; p = p->next)
		if (OHCI_ED_GET_FA(p->ed.ctrl) == ep->dev_addr
			|| OHCI_ED_GET_EN(p->ed.ctrl) == ep->ep_nbr)
			break;
	
	return p;
}

void
submit_ctrl_msg(struct usb_req_block *urb)
{
	int i;
	if (urb) {
		HC_CONTROL |= 1 << 4;	/* enable control list */
		ohci_ctrl_transfer(&sc, urb);
#ifdef _DEBUG
		printf("urb transfer begin: %p\n", urb);
#endif
	}
}

enum usb_req_status
usbd_cancel_xfer(struct usb_req_block *urb)
{
	struct ohci_sed *sed;
	
	switch (urb->ep.trans_type) {
	case 0:
		sed = ohci_search_sed(sc.ctrl_head, &urb->ep);
		ohci_rmv_ctrl_sed(&sc, sed);
		break;
	default:
		break;
	}
	
	return REQ_CANCELLED;
}

/*
enum usb_req_status
usbd_get_xfer_status(struct usb_req_block *urb)
{
	int i;
	uint32_t intrs;
	
	for (i = 0; i < 10; i++) {
		delay_ms(1);
		ohci_intr(&sc);
		if (urb->req.idata->irp_status != REQ_IN_PROGRESS)
			return urb->req.idata->irp_status;
	}
	
	return REQ_ERROR_MAX;
}
*/

uint16_t
roothub_get_connected_port(void)
{
	int i, ndp;
	uint16_t res;
	
	ndp = REG_READ4(OHCI_RH_DESCRIPTOR_A) & 0xff;
	
	for (i = 0; i < ndp; i++) {
		if (((REG_READ4(OHCI_RH_PORT_STATUS1 + i * 4) >> 16) & 0x1) &&
			(REG_READ4(OHCI_RH_PORT_STATUS1 + i * 4) & 0x1)) {
			res = i + 1;
			REG_WRITE4(OHCI_RH_PORT_STATUS1 + i * 4, 0x1 << 16);
		}
	}
	
	return res;	
}

int
roothub_reset_port(uint8_t port)
{
	int pot_pgt;
	
	if(REG_READ4(OHCI_RH_PORT_STATUS1 + (port - 1) * 4) & 0x1) {
		REG_WRITE4(OHCI_RH_PORT_STATUS1 + (port - 1) * 4, 0x1 << 4);
		pot_pgt = OHCI_GET_POTPGT(REG_READ4(OHCI_RH_DESCRIPTOR_A)) & 0xff;
		delay_ms(pot_pgt * 2);
		REG_WRITE4(OHCI_RH_DESCRIPTOR_B, 0x1 << 1);
		REG_WRITE4(OHCI_RH_PORT_STATUS1 + (port - 1) * 4, 0x1 << 16);
	}	
		
	return 0;
}
