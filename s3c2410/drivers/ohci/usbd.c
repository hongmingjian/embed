/* 
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * This file implements all the interface functions of four kind transfer type,
 * between the usb driver and device driver, and device on/off function as well. 
 * This file also implements some internal functions such as enumeration function,
 * initialize usb device function and so on.
 */
 
/*------------------------------------------------------------------------------
 * project ....: USB Host driver
 * file .......: usbd.c
 * authors ....: Long Yun(lane312@126.com)
 * tutors  ....: Hong Mingjian
 * team .......: Driver Group(嵌入式驱动开发小组)
 * organization: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * creation ...: 1.3.2008
 */

#include "usbd.h"
#include "config.h"
#include <stdlib.h>

/**
 * Create one usb driver structure for memory allocate.
 */
static struct usb_driver usbd;

/* Create the endpoint irp list */
SLIST_HEAD(devd_list_head, dev_driver) devd_head = SLIST_HEAD_INITIALIZER(devd_head);
LIST_HEAD(irp_list_head, usb_ep_irp) irp_head = LIST_HEAD_INITIALIZER(irp_head);

SLIST_HEAD(urb_list_head, usb_req_block);

/* Endpoint's IRP list */
struct usb_ep_irp {
	uint8_t dev_addr;
	uint8_t ep_nbr;
	struct urb_list_head *urb_list;
	LIST_ENTRY(usb_ep_irp) entries;
};



/* IRP add/del functions */
static void usbh_add_urb(uint8_t dev_addr, uint8_t ep_nbr, struct usb_req_block *new_urb);
static void usbh_del_urb(struct usb_ep_irp *irp);

static int usbh_xfer_ctrl(uint8_t dev_class, uint8_t dev_sub_class, 
	uint16_t dev_vid, uint16_t dev_pid, struct usb_device *dev);
static struct usb_req_block usbh_init_vital_urb(struct usb_device *dev,
	uint8_t request_type, uint8_t request, uint16_t value, uint16_t index,
	uint16_t length);
static void usbh_xfer_vital_urb(struct usb_req_block *urb);
static void usbh_init_device(struct usb_device *dev);

static enum usb_req_status usbh_enum(struct usb_device *dev, uint16_t dev_addr,
	uint8_t port_nbr);

static void usbh_del_device(struct usb_device *dev);
static void usbh_read_config(struct usb_device *dev, uint8_t *buffer);
static void usbh_copy_buffer(uint8_t *tag, uint8_t *src, int index, int length);
static void usbh_copy_buffer_at(uint8_t *tag, uint8_t *src, int tag_index,
	int src_index, int length);
	
/**
 * This function can send one usb request block directly to host controller driver,
 * without irp list. So you can use this function to execute one request immediately.
 */
static void 
usbh_xfer_vital_urb(struct usb_req_block *urb)
{
	submit_ctrl_msg(urb);
}	

/**
 * This function can initialize one usb request block which will be sent to HCD 
 * directly. @return struct usb_req_block type data.
 */
static struct usb_req_block 
usbh_init_vital_urb(struct usb_device *dev, uint8_t request_type, uint8_t request,
	uint16_t value, uint16_t index,	uint16_t length)
{
	struct usb_req_block urb;
	struct usb_req req;
	
	req.idata = &(usbd.inner_idata);
	req.trans_dir = 0;
	req.req_shxferok = 0;
	req.req_maxdi = 7;
	
	req.pkt.bmRequestType = request_type;
	req.pkt.bRequest = request;
	req.pkt.wValue = value;
	req.pkt.wIndex = index;
	req.pkt.wLength = length;
	
	urb.req = req;
	urb.ep = dev->usb_ep[0];
	
	return urb;
}

/**
 * This function transfers the control to device driver.
 * @param dev_vid is the idVendor of device descriptor.
 * @param dev_pid is the idProduct of device descriptor.
 * @param dev_class is the bDeviceClass of device descriptor.
 * @param dev_sub_class is the bDeviceSubClass of device descriptor.
 * @return -1 if if error. If success, there's no return or return 0. 
 */
static int usbh_xfer_ctrl(uint8_t dev_class, uint8_t dev_sub_class, 
	uint16_t dev_vid, uint16_t dev_pid, struct usb_device *dev)
{
	struct dev_driver *dev_drv;
	
	SLIST_FOREACH(dev_drv, &devd_head, entries) {
		if((dev_drv->dev_vid) == dev_vid && (dev_drv->dev_pid) == dev_pid &&
			(dev_drv->dev_class) == dev_class && 
			(dev_drv->dev_sub_class) == dev_sub_class) {
			void (* xfer_ctrl)(struct usb_device *) = (void *)(dev_drv->drv_addr);
			(* xfer_ctrl)(dev);
			return 0;
		} else if(!SLIST_NEXT(dev_drv, entries)) {
			return -1;
		}
	}
}

/** 
 * Initialize the device setting according to the selected device config. 
 * Return 0 if success and return -1 if error.
 */
static void
usbh_init_device(struct usb_device *dev)
{
	uint8_t config = dev->config_value;
	uint8_t ep_type;	/* endpoint type */
	int status;			/* device or endpoint status */
	int if_count;		/* count of interface  */
	int ep_count;		/* count of endpoints  */
	int ep_nbr;			/* number of endpoints */
	int pre_count;
	int i, j;
	struct usb_req_block urb;
	
	/* Initialize device config descriptor */
	urb = usbh_init_vital_urb(dev, 0x80, GET_DESCRIPTOR,
		(DESC_CONFIGURATION << 8) + config, 0x00, 0x01);
	usbh_xfer_vital_urb(&urb);
	delay_ms(50);
	if(usbd.inner_idata.irp_status == REQ_NORMAL_COMPLETION) {
		usbh_read_config(dev, usbd.inner_idata.buffer);
		if_count = dev->config_desc->bNumInterfaces;
	}
	
	/* Set device status */
	urb = usbh_init_vital_urb(dev, 0x80, GET_STATUS,
		0x00, 0x00, 0x2);
	usbh_xfer_vital_urb(&urb);
	if(usbd.inner_idata.irp_status == REQ_NORMAL_COMPLETION) {
		status = usbd.inner_idata.buffer[0];
		dev->self_powered = status & 1;
		dev->remote_wakeup = (status & 2) >> 1;
		dev->max_power = dev->config_desc->MaxPower;
	}
	
	/* Get the sum of endpoint count and define so much endpoint */
	ep_count = 0;
	for(i = 0; i < if_count; i++) {
		ep_count += dev->config_desc->if_desc[i].bNumEndpoints;
	}
	struct usb_endpoint *dev_ep = malloc(sizeof(struct usb_endpoint) * (ep_count + 1));
	
	/* Initialize each endpoint under its interface */
	pre_count = 0;
	for(i = 0; i < if_count; i++) {
		ep_count = dev->config_desc->if_desc[i].bNumEndpoints;
		for(j = 1; j <= ep_count; j++) {
			ep_type = (dev->config_desc->if_desc[i].ep_desc[j - 1].bEndpointAddress >> 7) & 1;
			ep_nbr = (dev->config_desc->if_desc[i].ep_desc[j - 1].bEndpointAddress) & 0xf;
			
			urb = usbh_init_vital_urb(dev, 0x80, GET_STATUS,
				0x00, 0x00, 0x2);
			usbh_xfer_vital_urb(&urb);
			if(usbd.inner_idata.irp_status == REQ_NORMAL_COMPLETION)
				status = usbd.inner_idata.buffer[0] & 1;
				
			dev_ep[j + pre_count].ep_type = ep_type;
			dev_ep[j + pre_count].trans_type = (dev->config_desc->if_desc[i].ep_desc[j - 1].bmAttributes) & 3;
			dev_ep[j + pre_count].dev_addr = dev->dev_addr;
			dev_ep[j + pre_count].ep_nbr = ep_nbr;
			dev_ep[j + pre_count].max_pkt_size = dev->config_desc->if_desc[i].ep_desc[j - 1].wMaxPacketSize;
			dev_ep[j + pre_count].ep_halted = status;
			dev_ep[j + pre_count].lowspeed = dev->lowspeed;
			dev_ep[j + pre_count].interval = dev->config_desc->if_desc[i].ep_desc[j - 1].bInterval;
		}
		pre_count += ep_count;
	}
	
	/* Endpoint 0 should be set seperately */
	dev_ep[0].ep_type = 2;
	dev_ep[0].trans_type = 0;
	dev_ep[0].ep_halted = 0;
	dev_ep[0].dev_addr = dev->dev_addr;
	dev_ep[0].ep_nbr = 0;
	dev_ep[0].max_pkt_size = dev->max_pkt_size0;
	dev_ep[0].lowspeed = dev->lowspeed;
	dev_ep[0].interval = 0;
	
	dev->usb_ep = dev_ep;
}

/** 
 * This function is the usb enumeration function.
 * Returned value please refer to enum usb_req_status.
 */
static enum usb_req_status
usbh_enum(struct usb_device *dev, uint16_t dev_addr, uint8_t port_nbr)
{
	uint8_t dev_class;		/* bDeviceClass in Device Desc */
	uint8_t dev_sub_class;	/* bDeviceSubClass in Device Desc */
	uint16_t dev_vid;		/* idVendor in Device Desc */
	uint16_t dev_pid;		/* idProduct in Device Desc */
	int result;	 		/* identify for the result of each standard request */
	int config_len;		/* length of configuration  */
	int config_count;	/* count of configurations  */
	int i, j;
	static struct usb_req_block urb;
	enum usb_req_status status = 0;
	struct usb_endpoint ep;
	
	usbd.tag = 1;
	
	roothub_reset_port(port_nbr);
	delay_ms(10);
	
	/* Init the device first */
	dev->dev_addr = 0;
	dev->lowspeed = 0;
	dev->max_pkt_size0 = 8;
	
	ep.ep_type = 2;
	ep.trans_type = 0;
	ep.dev_addr = dev->dev_addr;
	ep.ep_nbr = 0;
	ep.lowspeed = dev->lowspeed;
	ep.ep_halted = 0;
	ep.max_pkt_size = 8;
	
	dev->usb_ep = &ep;	
	
	/* Get device descriptor */
	urb = usbh_init_vital_urb(dev, 0x80, GET_DESCRIPTOR, (DESC_DEVICE << 8) + 0x00,
		0x00, 0x40);
	usbh_xfer_vital_urb(&urb);	
	delay_ms(50);
	
	printf("Returned data: ");
	
	int buf_i;
	for(buf_i = 0; buf_i < 18; buf_i++) {
		printf("%x ", usbd.inner_idata.buffer[i]);
	}
	printf("\n");
	
	if(usbd.inner_idata.irp_status == REQ_NORMAL_COMPLETION) {	
		dev->max_pkt_size0 = usbd.inner_idata.buffer[7];
		printf("We win!!!!hahaha\n");
	} else {
		printf("We lose!!!!Fuck\n");
		return usbd.inner_idata.irp_status;
	}		
#if 0	
	/* Set device address */
	urb = usbh_init_vital_urb(dev, 0x00, SET_ADDRESS, dev_addr, 0x00, 0x00);
	usbh_xfer_vital_urb(&urb);
	delay_ms(50);
	if(usbd.inner_idata.irp_status == REQ_NORMAL_COMPLETION) {
		dev->dev_addr = dev_addr;
	} else
		return usbd.inner_idata.irp_status;
	
	/* Get device descriptor */
	urb = usbh_init_vital_urb(dev, 0x80, GET_DESCRIPTOR, (DESC_DEVICE << 8) + 0x00,
		0x00, 0x12);
	usbh_xfer_vital_urb(&urb);
	delay_ms(50);
	if(usbd.inner_idata.irp_status == REQ_NORMAL_COMPLETION) {
		/* Read the infomation of device, the buffer length is result */
		dev_class = usbd.inner_idata.buffer[4];
		dev_sub_class = usbd.inner_idata.buffer[5];
		dev_vid = usbd.inner_idata.buffer[8];
		dev_pid = usbd.inner_idata.buffer[10];
		dev->mf_index = usbd.inner_idata.buffer[14];
		dev->prod_index = usbd.inner_idata.buffer[15];
		dev->serial_index = usbd.inner_idata.buffer[16];
		config_count = usbd.inner_idata.buffer[17];		
		dev->config_count = config_count;
		
		usbh_copy_buffer(&(dev->dev_desc), usbd.inner_idata.buffer, 0, 18);
	} else
		return -1;
	
	/* Get configurations */
	for(i = 1; i <= config_count; i++) {
		urb = usbh_init_vital_urb(dev, 0x80, GET_DESCRIPTOR, 
			(DESC_CONFIGURATION << 8) + i, 0x00, 0x09);
		usbh_xfer_vital_urb(&urb);
		delay_ms(50);
		if(usbd.inner_idata.irp_status == REQ_NORMAL_COMPLETION) {
			/* Read the infomation of device, the buffer length is result */
			if(i == 1)
				dev->max_power = usbd.inner_idata.buffer[8];
		} else 
			return -1;
	
		urb = usbh_init_vital_urb(dev, 0x80, GET_DESCRIPTOR,
			(DESC_CONFIGURATION << 8) + i, 0x00, 0x09);
		usbh_xfer_vital_urb(&urb);
		delay_ms(50);
		if(usbd.inner_idata.irp_status == REQ_NORMAL_COMPLETION);
		else
			return -1;
	}

	/* Get string descriptor */
	urb = usbh_init_vital_urb(dev, 0x80, GET_DESCRIPTOR,
		(DESC_STRING << 8) + 0x00, 0x00, 0xff);
	usbh_xfer_vital_urb(&urb);
	delay_ms(50);
	if(usbd.inner_idata.irp_status == REQ_NORMAL_COMPLETION) {
		/* Read the infomation of device, the buffer length is result */
		dev->langid = usbd.inner_idata.buffer[2] << 8 + usbd.inner_idata.buffer[3];
		
		if(dev->mf_index != 0) {
			urb = usbh_init_vital_urb(dev, 0x80, GET_DESCRIPTOR,
				(DESC_STRING << 8) + (dev->mf_index), 0x00, 0xff);
			usbh_xfer_vital_urb(&urb);
			if(usbd.inner_idata.irp_status != REQ_NORMAL_COMPLETION) {
				return usbd.inner_idata.irp_status;
			}
		}
		
		if(dev->prod_index != 0) {
			urb = usbh_init_vital_urb(dev, 0x80, GET_DESCRIPTOR,
				(DESC_STRING << 8) + (dev->prod_index), 0x00, 0xff);
			usbh_xfer_vital_urb(&urb);
			if(usbd.inner_idata.irp_status != REQ_NORMAL_COMPLETION) {
				return usbd.inner_idata.irp_status;
			}
		}
		
		if(dev->serial_index != 0) {
			urb = usbh_init_vital_urb(dev, 0x80, GET_DESCRIPTOR,
				(DESC_STRING << 8) + (dev->serial_index), 0x00, 0xff);
			usbh_xfer_vital_urb(&urb);
			if(usbd.inner_idata.irp_status != REQ_NORMAL_COMPLETION) {
				return usbd.inner_idata.irp_status;
			}
		}
	} else 
		dev->langid = USBD_NOLANG;

	/* Config the device */	
	urb = usbh_init_vital_urb(dev, 0x00, SET_CONFIGURATION,
		0x01, 0x00, 0x00);
	usbh_xfer_vital_urb(&urb);	/* XXXXXXX */
	delay_ms(50);
	if(usbd.inner_idata.irp_status != REQ_NORMAL_COMPLETION)
		return usbd.inner_idata.irp_status;
	dev->config_value = 1;

	/* Initialize the usb_device structure */
	usbh_init_device(dev);
	
	usbd.tag = 0;
	
	printf("Hahahahahaahahahahaha\n");
	
	/* First leave device driver alone for unit test */
	if(usbh_xfer_ctrl(dev_class, dev_sub_class, dev_vid, dev_pid, dev) == -1)
		return REQ_UNKNOWN_DEVICE;
#endif
}

/* 
 * This function is used when the device is off. 
 * It can clear all the data of usb_device structure.
 */
static void
usbh_del_device(struct usb_device *dev)
{
	int i, j;
	
	dev->dev_addr = 0;
	dev->lowspeed = 0;
	dev->mf_index = 0;
	dev->prod_index = 0;
	dev->serial_index = 0;
	dev->langid = 0;
	dev->max_pkt_size0 = 0;
	dev->config_value = 0;
	dev->config_count = 0;
	dev->self_powered = 0;
  	dev->max_power = 0;
	
	/* Clear endpoints */
	free(dev->usb_ep);
	
	/* Clear device descriptor */
	dev->dev_desc.bLength = 0;
	dev->dev_desc.bDescriptorType = 0;
	dev->dev_desc.bcdUSB = 0;
	dev->dev_desc.bDeviceClass = 0;
	dev->dev_desc.bDeviceSubClass = 0;
	dev->dev_desc.bDevicePortocol = 0;
	dev->dev_desc.bMaxPacketSize0 = 0;
	dev->dev_desc.idVendor = 0;
	dev->dev_desc.idProduct = 0;
	dev->dev_desc.bcdDevice = 0;
	dev->dev_desc.iManufacturer = 0;
	dev->dev_desc.iProduct = 0;
	dev->dev_desc.iSerialNumber = 0;
	dev->dev_desc.bNumConfigurations = 0;
	
	/* Clear config descriptor */
	free(dev->config_desc);
}

/* Read the config structure and init the config_desc in usb_device. */
static void 
usbh_read_config(struct usb_device *dev, uint8_t *buffer)
{
	int i;
	int if_count;	/* count of interfaces */
	int ep_count;	/* count of endpoints  */
	int length;
	
	if_count = buffer[4];
	ep_count = buffer[13];
	length = 9 + 9 * if_count + 7 * ep_count;
	
	struct usb_interface_desc *if_desc = malloc(sizeof(struct usb_interface_desc) 
		* if_count);
	for(i = 0; i < if_count; i++) {
		struct usb_endpoint_desc *ep_desc = malloc(sizeof(struct usb_endpoint_desc)
			* ep_count);
		ep_count = buffer[length + 4];
		length += 9 + 7 * ep_count;
		if_desc[i].ep_desc = ep_desc;
	}
	dev->config_desc->if_desc = if_desc;
	
	dev->config_desc = &buffer;
}

/* 
 * This function is used to copy the data in src buffer to tag buffer.
 * The data copied starts at src[index], with length as the data length.
 */
static void
usbh_copy_buffer(uint8_t *tag, uint8_t *src, int index, int length)
{
	int i;
	for(i = 0; i < length; i++) 
		tag[i] = src[i + index];
}

static void
usbh_copy_buffer_at(uint8_t *tag, uint8_t *src, int tag_index, int src_index, int length)
{
	int i;
	for(i = 0; i < length; i++)
		tag[i + tag_index] = src[i + src_index];
}

/**
 * This function adds one IRP to the irp list.
 * @return 0 if success, -1 if error.
 */
static void
usbh_add_urb(uint8_t dev_addr, uint8_t ep_nbr, struct usb_req_block *new_urb)
{
	struct urb_list_head *new_urb_head = malloc(sizeof(struct urb_list_head));
	struct usb_ep_irp *new_irp = malloc(sizeof(struct usb_ep_irp)), *irp;
	struct usb_req_block *urb;
	
	/* If no IRP for any endpoint, add new IRP to the endpoint irp list */
	if(LIST_EMPTY(&irp_head)) {
		SLIST_INSERT_HEAD(new_urb_head, new_urb, entries);
		new_irp->dev_addr = dev_addr;
		new_irp->ep_nbr = ep_nbr;
		new_irp->urb_list = new_urb_head;
		LIST_INSERT_HEAD(&irp_head, new_irp, entries);
		return;
	}
	
	/* Search all the list for one specific device's endpoint */
	LIST_FOREACH(irp, &irp_head, entries) {
		if(irp->dev_addr == dev_addr && irp->ep_nbr == ep_nbr) {
			
			/* If the specific endpoint IRP sub-list is empty, then add the irp->urb. */
			if(SLIST_EMPTY(irp->urb_list)) {				
				SLIST_INSERT_HEAD(irp->urb_list, new_urb, entries);
				return;
			} else {	/* If the sub-list is not empty, add urb at the end */
				SLIST_FOREACH(urb, irp->urb_list, entries) {
					if(!SLIST_NEXT(urb, entries)) {
						SLIST_INSERT_AFTER(urb, new_urb, entries);
						SLIST_NEXT(new_urb, entries) = NULL;
						return;
					}
				}
			}
		} else if(!LIST_NEXT(irp, entries)) { /* If no this endpoint's irp list */
			SLIST_INSERT_HEAD(new_urb_head, new_urb, entries);
			SLIST_NEXT(new_urb, entries) = NULL;
			
			new_irp->dev_addr = dev_addr;
			new_irp->ep_nbr = ep_nbr;
			new_irp->urb_list = new_urb_head;
			LIST_INSERT_AFTER(irp, new_irp, entries);
			return;
		}
	}
}

/**
 * Delete the first urb in specified item of irp list.
 * @param *irp is the pointer of specified irp.
 */
static void
usbh_del_urb(struct usb_ep_irp *irp)
{
	struct usb_req_block *urb = SLIST_FIRST(irp->urb_list);
	SLIST_REMOVE(irp->urb_list, urb, usb_req_block, entries);
	free(urb);
	
	return 0;	
}

/******************************************************************************* 
 * Initialize the usb driver 
 */
void
usbd_boot()
{
	/* Init ohci */
	ohci_init(&sc);
	
	/* Init usb driver */
	usbd.dev_count = 0;
	usbd.dev_switch = 0;
	usbd.inner_idata.buffer = usbd.buffer;
	
	usbd.version[0] = 'U';
	usbd.version[1] = 'S';
	usbd.version[2] = 'B';
	usbd.version[3] = ' ';
	usbd.version[4] = '1';
	usbd.version[5] = '.';
	usbd.version[6] = '1';
	
	usbd.tag = 0;
	
	LIST_INIT(&irp_head);
	SLIST_INIT(&devd_head);
}

/** 
 * These functions are used to invoke when the device is on/off. 
 * When device is on, usb host enum it. When off, retrieve its resource.
 * Return index of device if success, -1 if error. 
 */
int
usbh_device_on(uint8_t port_nbr)
{
	int i;
	/* Get the idle address for new address */
	for(i = 0; i < 32; i++) {
		if(((usbd.dev_switch >> i) & 1) == 0) {
			usbd.dev_switch |= 1 << i;
			usbd.dev_count = i;
		}
	}
	
	usbh_del_device(&usbd.device[usbd.dev_count]);
	switch(usbh_enum(&usbd.device[usbd.dev_count], usbd.dev_count, port_nbr)) {
		case REQ_PENDING_REQUESTS:
			printf("Request is pending\n");
			break;
		case REQ_UNKNOWN_DEVICE:
			printf("Unknown device\n");
			break;
	}
	
	return 0;
}

int
usbh_device_off()
{
	int i;
	struct usb_req_block urb;
	/* Get the device-off index */
	for(i = 0; i < 32; i++) {
		urb = usbh_init_vital_urb(&usbd.device[i], 0x80, GET_STATUS,
			0x00, 0x00, 0x2);
		usbh_xfer_vital_urb(&urb);
		if(usbd.inner_idata.irp_status == REQ_STALLED) {
			usbd.dev_count = i;
			usbd.dev_switch ^= 1 << i;
		}
	}
	
	usbh_del_device(&usbd.device[usbd.dev_count]);
	
	return 0;
}

/******************************************************************************* 
 * Standard device request functions. 
 * @param idata->irp_status is the tranfer status.
 * Parameter should be set as following:
 *
 * request_type			value			index
 * 		0		DEVICE_REMOTE_WAKEUP	  0
 *		2			ENDPOINT_HALT	 endpoint number
 */
void
usbh_clear_feature(struct usb_device *dev, uint8_t request_type, uint16_t value,
	uint16_t index, struct irp_data_block *idata)
{
	usbd.setup_pkt.bmRequestType = request_type;
	usbd.setup_pkt.bRequest = CLEAR_FEATURE;
	usbd.setup_pkt.wValue = value;
	usbd.setup_pkt.wIndex = index;
	usbd.setup_pkt.wLength = 0x0000;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
}

/** 
 * @param idata->irp_status is the transfer status.
 * @param idata->buffer[0] is the config value if @param idata.irp_status is 
 * REQ_NORMAL_COMPLETION.
 */
void
usbh_get_config(struct usb_device *dev, struct irp_data_block *idata)
{
	usbd.setup_pkt.bmRequestType = 0x80;
	usbd.setup_pkt.bRequest = GET_CONFIGURATION;
	usbd.setup_pkt.wValue = 0x0000;
	usbd.setup_pkt.wIndex = 0x0000;
	usbd.setup_pkt.wLength = 0x0001;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
}

/** 
 * @param idata->irp_status is the transfer status.
 * if @param idata->irp_status is REQ_NORMAL_COMPLETION, then @param idata->buffer
 * is the descriptor returned. @param idata->act_len is the length of buffer.
 */
void
usbh_get_desc(struct usb_device *dev, uint8_t desc_type, uint8_t desc_index,
	uint16_t index, uint16_t length, struct irp_data_block *idata)
{
	usbd.setup_pkt.bmRequestType = 0x80;
	usbd.setup_pkt.bRequest = GET_DESCRIPTOR;
	usbd.setup_pkt.wValue = (desc_type << 8) + desc_index;
	usbd.setup_pkt.wIndex = index;
	usbd.setup_pkt.wLength = length;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
}

/** 
 * @param idata->irp_status is the transfer status.
 * if @param idata->irp_status is REQ_NORMAL_COMPLETION, then @param idata->buffer
 * is the data returned. @param idata->act_len is one.
 */
void
usbh_get_interface(struct usb_device *dev, uint16_t index,
	struct irp_data_block *idata)
{	
	usbd.setup_pkt.bmRequestType = 0x81;
	usbd.setup_pkt.bRequest = GET_INTERFACE;
	usbd.setup_pkt.wValue = 0x0000;
	usbd.setup_pkt.wIndex = index;
	usbd.setup_pkt.wLength = 0x0001;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
}

/** 
 * @param idata->irp_status is the transfer status.
 * if @param idata->irp_status is REQ_NORMAL_COMPLETION, then @param idata->buffer
 * is the status returned. @param idata->act_len is two.
 * Parameters should be set as following:
 *
 * 		request_type		index			return data
 *			0x80			  0			   device status
 *			0x82		endpoint number	  endpoint status
 */
void
usbh_get_status(struct usb_device *dev, uint8_t request_type, uint16_t index,
	struct irp_data_block *idata)
{
	usbd.setup_pkt.bmRequestType = request_type;
	usbd.setup_pkt.bRequest = GET_STATUS;
	usbd.setup_pkt.wValue = 0x0000;
	usbd.setup_pkt.wIndex = index;
	usbd.setup_pkt.wLength = 0x0002;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
}

/** 
 * @param idata->irp_status is the transfer status.
 * if @param idata->irp_status is REQ_NORMAL_COMPLETION, then the device's address
 * is set successfully.
 */
void
usbh_set_address(struct usb_device *dev, uint16_t value,
	struct irp_data_block *idata)
{
	usbd.setup_pkt.bmRequestType = 0x00;
	usbd.setup_pkt.bRequest = SET_ADDRESS;
	usbd.setup_pkt.wValue = value;
	usbd.setup_pkt.wIndex = 0x0000;
	usbd.setup_pkt.wLength = 0x0000;
	
	usbh_ctrl_msg(dev, USBH_SND_DEFCTRL_PIPE, usbd.setup_pkt, idata);
}	

/** 
 * @param idata->irp_status is the transfer status.
 * if @param idata->irp_status is REQ_NORMAL_COMPLETION, then the device's config
 * is changed successfully.
 */
void
usbh_set_config(struct usb_device *dev, uint8_t value, struct irp_data_block *idata)
{
	usbd.setup_pkt.bmRequestType = 0x00;
	usbd.setup_pkt.bRequest = SET_CONFIGURATION;
	usbd.setup_pkt.wValue = value;
	usbd.setup_pkt.wIndex = 0x0000;
	usbd.setup_pkt.wLength = 0x0000;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
}	

/** 
 * @param idata->irp_status is the transfer status.
 * if @param idata->irp_status is REQ_NORMAL_COMPLETION, then the descriptor
 * is set successfully.
 */
void
usbh_set_desc(struct usb_device *dev, uint8_t desc_type,
 	uint8_t desc_index, uint16_t index, uint16_t length, struct irp_data_block *idata)
{
	usbd.setup_pkt.bmRequestType = 0x00;
	usbd.setup_pkt.bRequest = SET_DESCRIPTOR;
	usbd.setup_pkt.wValue = (desc_type << 8) + desc_index;
	usbd.setup_pkt.wIndex = index;
	usbd.setup_pkt.wLength = length;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
}	

/** 
 * @param idata->irp_status is the transfer status.
 * if @param idata->irp_status is REQ_NORMAL_COMPLETION, then the feature 
 * is set successfully.
 * Parameters should be set as following:
 *
 *	request_type		value			index
 *		 0		 DEVICE_REMOTE_WAKEUP	  0
 *		 2			ENDPOINT_HALT	 endpoint number
 */
void
usbh_set_feature(struct usb_device *dev, uint16_t request_type,	uint16_t value,
	uint16_t index, struct irp_data_block *idata)
{
	usbd.setup_pkt.bmRequestType = request_type;
	usbd.setup_pkt.bRequest = SET_FEATURE;
	usbd.setup_pkt.wValue = value;
	usbd.setup_pkt.wIndex = index;
	usbd.setup_pkt.wLength = 0x0000;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
}	

/**
 * @param idata->irp_status is the transfer status.
 * if @param idata->irp_status is REQ_NORMAL_COMPLETION, then the interface
 * is set successfully.
 */
void
usbh_set_interface(struct usb_device *dev, uint16_t value, uint16_t index,
	struct irp_data_block *idata)
{
	usbd.setup_pkt.bmRequestType = 0x01;
	usbd.setup_pkt.bRequest = SET_INTERFACE;
	usbd.setup_pkt.wValue = value;
	usbd.setup_pkt.wIndex = index;
	usbd.setup_pkt.wLength = 0x0000;

	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
}	

/**
 * @param idata->irp_status is the transfer status.
 * if @param idata->irp_status is REQ_NORMAL_COMPLETION, then the @param idata->buffer
 * stores the fram number and @param idata->act_len is two.
 * @return 0 if this required endpoint's transfer type is isochronous, -1 if endpoint's
 * transfer type is not isochronous.
 */
int
usbh_synch_frame(struct usb_device *dev, uint16_t index, struct irp_data_block *idata)
{
	/* If the specified endpoint is not isochronous transfer, return error */
	if((dev->usb_ep[index].trans_type) != 1)
		return -1;
	
	usbd.setup_pkt.bmRequestType = 0x82;
	usbd.setup_pkt.bRequest = SYNCH_FRAME;
	usbd.setup_pkt.wValue = 0x0000;
	usbd.setup_pkt.wIndex = index;
	usbd.setup_pkt.wLength = 0x0002;
	
	usbh_ctrl_msg(dev, USBH_SND_CTRL_PIPE(dev, USB_EP0), usbd.setup_pkt, idata);
	
	return 0;
}

/*
 * This function is to invoke host controller driver control transfer function. 
 * All the required information is stored in @param idata.
 */
void
usbh_ctrl_msg(struct usb_device *dev, uint32_t pipe, struct usb_setup_pkt packet,
	struct irp_data_block *idata)
{
	struct usb_req req;
	struct usb_req_block *urb = malloc(sizeof(struct usb_req_block));
	int i;
	
	req.idata = idata;
	req.trans_dir = (pipe >> 11) & 3;
	req.pkt = packet;
	
	urb->req = req;
	for(i = 0; ; i++) {
		if(USBH_PIPE_EP_NUM(pipe) == (dev->usb_ep + i)->ep_nbr) {
			urb->ep = *(dev->usb_ep + i);
			break;
		}
	}
	
	usbh_add_urb(dev->dev_addr, USBH_PIPE_EP_NUM(pipe), urb);
}

/*******************************************************************************
 * This function is used for bulk transfer. Return 0 if success(for OUT ep), return -1 if error.
 * If the function returns one positive number, then it's the length of IN ep's data.
 * Parameters len identify the data length, dir should be 1 or 2. 1 - out, 2- in.
 */
void
usbh_bulk_msg(struct usb_device *dev, uint32_t pipe,
	struct irp_data_block *idata)
{
	struct usb_req req;
	struct usb_req_block *urb = malloc(sizeof(struct usb_req_block));
	int i;
	
	req.idata = idata;
	req.trans_dir = (pipe >> 11) & 3;
	
	urb->req = req;
	for(i = 0; ; i++) {
		if(USBH_PIPE_EP_NUM(pipe) == (dev->usb_ep + i)->ep_nbr) {
			urb->ep = *(dev->usb_ep + i);
			break;
		}
	}
	
	usbh_add_urb(dev->dev_addr, USBH_PIPE_EP_NUM(pipe), urb);
}

/*******************************************************************************
 * This function is used for interrupt transfer. Return 0 if success(for OUT ep),
 * return -1 if error and return positive number for In ep's data length.
 * Parameters len is the length of buffer, dir is direction. 1 - out,
 * 2 - in. interval is the interval time of interrupt transfer.
 * As for high speed device, interval should be 1 - 255, low speed device should be
 * 10 - 255.
 */
void
usbh_intr_msg(struct usb_device *dev, uint32_t pipe, struct irp_data_block *idata,
	uint8_t interval)
{
	struct usb_req req;
	struct usb_req_block *urb = malloc(sizeof(struct usb_req_block));
	int i;
	
	req.idata = idata;
	req.trans_dir = (pipe >> 11) & 3;
	
	urb->req = req;
	for(i = 0; ; i++) {
		if(USBH_PIPE_EP_NUM(pipe) == (dev->usb_ep + i)->ep_nbr) {
			urb->ep = *(dev->usb_ep + i);
			break;
		}
	}
	
	usbh_add_urb(dev->dev_addr, USBH_PIPE_EP_NUM(pipe), urb);
}

/*******************************************************************************
 * This function is used for isochronous transfer. Return 0 if success(for OUT ep),
 * return -1 if error and return positive number for In ep's data length.
 * Parameters len is the length of buffer, dir is direction. 1 - out,
 * 2 - in. 
 */
void 
usbh_isoc_msg(struct usb_device *dev, uint32_t pipe, struct irp_data_block *idata)
{
	int i;
	struct usb_req req;
	struct usb_req_block *urb = malloc(sizeof(struct usb_req_block));
	
	req.idata = idata;
	req.trans_dir = (pipe >> 11) & 3;
	
	urb->req = req;
	for(i = 0; ; i++) {
		if(USBH_PIPE_EP_NUM(pipe) == (dev->usb_ep + i)->ep_nbr) {
			urb->ep = *(dev->usb_ep + i);
			break;
		}
	}
	
	usbh_add_urb(dev->dev_addr, USBH_PIPE_EP_NUM(pipe), urb);
}

/*******************************************************************************
 * These functions maintains one IRP to the current IRP list. IRP list is one 
 * list which is differenced by device and endpoint. That means each device's
 * endpoint will maintain one sub-list. And the sub-list consists of IRPs 
 * pointing to one same endpoint. Every first IRP in the sub-list specifies the 
 * IRP has been passed to HCD(host controller driver) for transfer through its
 * specific endpoint. When the IRP passed to HCD has been finished, the IRP will
 * be delete from the sub-list. If one endpoint return STALL error, then all the 
 * IRPs in this endpoint's sub-list will be cleared.
 */

/**
 * This function is used to manage the irp list.
 */
void
usbh_manage_irp()
{
	uint8_t dev_addr;
	uint8_t ep_nbr;
	int i;
	struct usb_ep_irp *irp = 0;
	struct urb_list_head *urb_list = 0;
	struct usb_req_block *urb = 0;
	
	if(usbd.tag == 1)
		return;
	
	/* Search every endpoint's IRP list and according to its status to take action */
	LIST_FOREACH(irp, &irp_head, entries) {
		dev_addr = irp->dev_addr;
		ep_nbr = irp->ep_nbr;		
		urb_list = irp->urb_list;
		
		urb = SLIST_FIRST(urb_list);
		if (!urb)
			continue;
				
		switch((urb->req.idata)->irp_status) {
			/* This request hasn't been send the HCD */
			case REQ_UNINIT:
				switch(urb->ep.trans_type) {
					case 0:
						submit_ctrl_msg(urb);
						break;
					case 1:
						//submit_isoc_msg(urb);
						break;
					case 2:
						//submit_bulk_msg(urb);
						break;
					case 3:
						//submit_intr_msg(urb);
						break;
				}
				break;
			
			/* This request has been transferred successfully */
			case REQ_NORMAL_COMPLETION:
				usbh_del_urb(irp);
				if(urb->ep.trans_type == 0) {
/*					for(i = 0; i < 8; i++) {
						if(usbd.device[i].dev_addr == dev_addr) {
							usbh_init_device(&(usbd.device[i]));
							break;
						}
					}*/
				}
				urb = SLIST_FIRST(irp->urb_list);
				if(!urb)
					break;

				switch(urb->ep.trans_type) {
					case 0:
						submit_ctrl_msg(urb);
						break;
					case 1:
						//submit_isoc_msg(urb);
						break;
					case 2:
						//submit_bulk_msg(urb);
						break;
					case 3:
						//submit_isoc_msg(urb);
						break;						
				}
				break;
			
			/* This request has been sent to HCD */
			case REQ_IN_PROGRESS: 
			case REQ_PENDING_REQUESTS:
				break;
			
			/* This request's endpoint has been stalled */
			case REQ_STALLED:
				free(irp->urb_list);		
				break;						
		} 
	}
}

/**
 * This function is used for registerring driver.
 */
int
usbh_dev_reg(uint8_t dev_class, uint8_t dev_sub_class, uint16_t dev_vid,
	uint16_t dev_pid, uint32_t drv_addr)
{
	struct dev_driver *reg_drv, *dev_drv;
	
	if(SLIST_EMPTY(&devd_head)) {
		reg_drv = malloc(sizeof(struct dev_driver));
		
		reg_drv->dev_vid = dev_vid;
		reg_drv->dev_pid = dev_pid;
		reg_drv->dev_class = dev_class;
		reg_drv->dev_sub_class = dev_sub_class;
		reg_drv->drv_addr = drv_addr;
		
		SLIST_INSERT_HEAD(&devd_head, reg_drv, entries);
		
		return 0;
	}
	
	SLIST_FOREACH(dev_drv, &devd_head, entries) {
		if(dev_drv->dev_vid == dev_vid && dev_drv->dev_pid == dev_pid &&
			dev_drv->dev_class == dev_class && 
			dev_drv->dev_sub_class == dev_sub_class)
			return -1;
		else if(!SLIST_NEXT(dev_drv, entries)) {
			reg_drv = malloc(sizeof(struct dev_driver));
		
			reg_drv->dev_vid = dev_vid;
			reg_drv->dev_pid = dev_pid;
			reg_drv->dev_class = dev_class;
			reg_drv->dev_sub_class = dev_sub_class;
			reg_drv->drv_addr = drv_addr;
			
			SLIST_INSERT_AFTER(dev_drv, reg_drv, entries);
			
			return 0;
		}
	}
}
