
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "lib.h"
#include "usbd.h"

//#define _DEBUG

void
int_usbh()
{
	int i = roothub_get_connected_port();
	vuint32_t is;

	/* Clear interrupt status */
	HC_INTERRUPT_STATUS = 1 << 6;
	
	if(REGL(HC_REGBASE, OHCI_RH_PORT_STATUS1 + (i - 1) * 4) & 1) {
		printf("Device is plused on port #%d\n", i);
		usbh_device_on(i);
	} else {
		printf("Device is removed on port #%d\n", i);
		usbh_device_off();
	}
}

int main()
{
	char *p;

	/* Test UART */
	printf("\nUART initialised successfully!\n");
	
	/* Test malloc */
	p = (char *)malloc(10*1024*1024);
	printf("malloc(10M) returns 0x%p\n", p);
	free(p);
	
	HC_INTERRUPT_ENABLE = OHCI_RHSC;
	ivt[INT_USBH] = int_usbh;
  	INT_CLEAR_PENDING(INT_USBH);
  	INT_ENABLE(INT_USBH);
	
	/* Test OHCI */
	delay_ms(1000);
	usbd_boot();
	
	init_led();
	
	while(1) {
		led_blink(LED1, 100);
		led_blink(LED2, 100);
	}

	return 0;
}

