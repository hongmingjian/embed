/* 
 * Copyright (C) 2008
 * 重庆大学行业信息化实验室
 *
 * This file is the u-disk driver. It's used to test the enumeration, control
 * and bulk transfer.
 */
 
/*------------------------------------------------------------------------------
 * project ....: USB Host driver
 * file .......: udisk.c
 * authors ....: Long Yun(lane312@126.com)
 * tutors  ....: Hong Mingjian
 * team .......: Driver Group(嵌入式驱动开发小组)
 * organization: cquCV(重庆大学行业信息化实验室 http://202.202.68.199)
 * creation ...: 1.3.2008
 */

#include "usbd.h"
#include "lib.h"
#include "config.h"

#define	BULK_RAM	0x32000000

/** 
 * This function lists the information and endpoints of specified device.
 * @param dev specifies the usb device wants to list.
 */
void devd_list_device(struct usb_device *dev)
{
	int i;
	
	printf("\n------------------------------------------------\n");
	printf("Device Address: 0x%x\n", dev->dev_addr);
	printf("Manufacturer: %s\n", dev->mf);
	printf("Product: %s\n", dev->prod);
	printf("Serial Number: %s\n", dev->serial);
	printf("Config Number: %d\n", dev->config_count);
	printf("Config Value: %s\n", dev->config_value);
	
	printf("\nEndpoint Number: 0\n");
	printf("Max Packet Size: %d Bytes\n", dev->max_pkt_size0);
	
	for(i = 0; i < USB_MAX_ENDPOINTS; i++) {
		if(dev->usb_ep[i].ep_nbr == 0)
			continue;
		else {
			printf("\nEndpoint Number: %d\n", i);
			printf("Status: %s\n", (dev->usb_ep[i].ep_status == 0)? "Started": "Halted");
			switch(dev->usb_ep[i].ep_type) {
				case 0:
					printf("Endpoint Type: Out\n");
					break;
				case 1:
					printf("Endpoint Type: In\n");
					break;
				case 2:
					printf("Endpoint Type: Control\n");
					break;
			}
			switch(dev->usb_ep[i].trans_type) {
				case 0:
					printf("Transfer Type: Control\n");
					break;
				case 1:
					printf("Transfer Type: Isochronous\n");
					break;
				case 2:
					printf("Transfer Type: Bulk\n");
					break;
				case 3:
					printf("Transfer Type: Interrupt\n");
					break;
			}
			printf("Max Packet Size: %d Bytes\n", dev->usb_ep[i].max_pkt_size);
		}
	}	
}

/**
 * This function is used to send command to device.
 * @param dev is the device wanted to send command.
 * @return 0 if success, -1 if error.
 */
int devd_snd_command(struct usb_device *dev)
{
	char c;
	char *where = (char *)BULK_RAM;
	int len, i;
	uint32_t pipe;
	struct irp_data_block idata, *idata_ptr = 0;
	
	printf("\n------------------------------------------------\n");
	printf("Which type of transfer you want:\n");
	printf("1-Control 2-Isochronous	3-Bulk 4-Interrupt\n");

	c = sio_getc();
	sio_putc(c);
	switch(c) {
		case '1':
			break;
		case '2':
			break;
		case '3':
			printf("\nWhich direction you want: 1-Out 2-In\n");
			
			c = sio_getc();
			sio_putc(c);
			switch(c) {
				case '1':
					printf("\nPlease enter one bulk endpoint number:\n");
					
					c = sio_getc();
					sio_putc(c);
					pipe = USBH_SND_BULK_PIPE(dev, (int)c);
					printf("\nPlease send the file through Xmodem\n");
					len = xmodem(where);
					if(len <= 0) {
						printf("!! Error receiving.\n");
						return -1;	
					}
					for(i = 0; i < 25; i++) {
						led_blink(LED1, 100);
						led_blink(LED2, 100);
					}
					idata.buffer = where;
					idata.buf_len = len;
					idata_ptr = &idata;
					
					printf("\n->bulk transfer\n");
					usbh_bulk_msg(dev, pipe, idata_ptr);
					if(i == -1) {
						printf("!!Transfer error.\n");
						return -1;
					}
					printf("<-bulk transfer\n");
					return 0;
					break;
				case '2':
/*					printf("\nPlease enter one bulk endpoint number:");
					
					c = sio_getc();
					sio_putc(c);
					pipe = USBH_RCV_BULK_PIPE(dev, (int)c);
					printf("\n->bulk transfer\n");
					i = usbh_bulk_msg(dev, pipe, where, 0xff, 2);
					if(i == -1) {
						printf("!!Transfer error.\n");
						return -1;
					}
					printf("<-bulk transfer\n");
					return 0;	*/					
					break;
			}
			break;
		case '4':
			break;
	}
}

