#include "config.h"
#include "drivers.h"

#ifdef CONFIG_XMODEM

#ifndef CONFIG_UART
#error	"Uart should be built in"
#endif

#define XMODEM_SOH 0x01
#define XMODEM_EOT 0x04
#define XMODEM_ACK 0x06
#define XMODEM_NAK 0x15
#define XMODEM_CAN 0x18
#define XMODEM_DATA_SIZE 128

struct frame
{
  char   blknum;
  char   blknumcompl;
  char   data[XMODEM_DATA_SIZE];
  char   cksum;
};

static char
cksum(struct frame *f)
{
  int i;
  int sum = 0;
  for(i = 0; i < XMODEM_DATA_SIZE; i++) {
    sum += f->data[i];		
  }
  return (sum & 0xff);	
}

static void 
get_frame(struct frame *rframe)
{	
  int i;
  rframe->blknum = sio_getc();
  rframe->blknumcompl = sio_getc();
  for(i=0; i < XMODEM_DATA_SIZE; i++) {
      rframe->data[i] = sio_getc();
    }
  rframe->cksum = sio_getc();
}	

int
xmodem(char *p)
{
  struct frame aframe;
  int i, len = 0;
  
  do {
		int loop = 900000; /*XXX*/
    sio_putc(XMODEM_NAK);
    
 		while(--loop && (!sio_ischar()))
			;
			
		if(loop)
			break;
			
  } while(1);
      
  while(1) {	
      switch(sio_getc()) {	
      case XMODEM_SOH:
				get_frame(&aframe);

				if((aframe.blknumcompl == 255 - aframe.blknum) && 
				   (cksum(&aframe) == aframe.cksum)) {
				    len += XMODEM_DATA_SIZE;
	    
				    for(i = 0; i < XMODEM_DATA_SIZE; i++)
				      *p++ = aframe.data[i];

				    sio_putc(XMODEM_ACK);
				} else {
				    sio_putc(XMODEM_NAK);
				}
				break;
				
      case XMODEM_EOT:
				sio_putc(XMODEM_ACK);
				return len;
	
      case XMODEM_CAN:
				return -1;				
      }  
  }	
}
#endif
