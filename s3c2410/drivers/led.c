#include "config.h"
#include "drivers.h"

#ifdef CONFIG_LED

void led_on(unsigned int led)
{
	GPFDAT ^= led;
}

void led_off(unsigned int led)
{
	GPFDAT |= led;
}

void init_led()
{
  GPFCON &= 0xffff3cff;
  GPFCON |= 0x4100;
  GPFUP   = 0xff; /*disable pull up*/
  led_off(LED1 | LED2);
}
#endif
