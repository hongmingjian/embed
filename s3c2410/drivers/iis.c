
#include "config.h"
#include "drivers.h"

#ifdef CONFIG_IIS

#define GPE0_I2SLRCK  (2 << 0)
#define GPE1_I2SSCLK  (2 << 2)
#define GPE2_CDCLK    (2 << 4)
#define GPE3_I2SSDI   (2 << 6)
#define GPE4_I2SSDO   (2 << 8)

#if IIS_CLOCK_FREQ == 256
#define IISMOD_MCLK	(0<<2)
#elif IIS_CLOCK_FREQ == 384
#define IISMOD_MCLK	(1<<2)
#else
#error "Unsupported sampling frequency!"
#endif

#define IISMOD_MASTER          (0 << 8)
#define IISMOD_SLAVE           (1 << 8)

#define IISMOD_RX              (1 << 6)
#define IISMOD_TX              (2 << 6)
#define IISMOD_RX_TX           (3 << 6)

#define IISCON_TX_DMA_DISABLE           (0 << 5)
#define IISCON_TX_DMA_ENABLE            (1 << 5)

#define IISCON_RX_DMA_DISABLE           (0 << 4)
#define IISCON_RX_DMA_ENABLE            (1 << 4)

#define IISCON_TX_CHN_NO_IDLE           (0 << 3)
#define IISCON_TX_CHN_IDLE              (1 << 3)

#define IISCON_RX_CHN_NO_IDLE           (0 << 2)
#define IISCON_RX_CHN_IDLE              (1 << 2)

#define IISCON_PSR_DISABLE          (0 << 1)
#define IISCON_PSR_ENABLE           (1 << 1)

#define IISMOD_SERIAL_FREQ_16fs         (0 << 0)
#define IISMOD_SERIAL_FREQ_32fs         (1 << 0)
#define IISMOD_SERIAL_FREQ_48fs         (2 << 0)

#define IISMOD_LEFT_LOW             (0 << 5)
#define IISMOD_LEFT_HIGH            (1 << 5)

#define IISMOD_SERIAL_FMT_IIS           (0 << 4)
#define IISMOD_SERIAL_FMT_MSBJ          (1 << 4)

#define IISMOD_SERIAL_CHN_8bit          (0 << 3)
#define IISMOD_SERIAL_CHN_16bit         (1 << 3)

#define IISFCON_TX_NO_DMA           (0 << 15)
#define IISFCON_TX_DMA              (1 << 15)

#define IISFCON_RX_NO_DMA           (0 << 14)
#define IISFCON_RX_DMA              (1 << 14)

#define IISFCON_TX_DISABLE          (0 << 13)
#define IISFCON_TX_ENABLE           (1 << 13)

#define IISFCON_RX_DISABLE          (0 << 12)
#define IISFCON_RX_ENABLE           (1 << 12)

int init_iis(int mode)
{	
	GPECON &= (~0x3FF);
	GPECON |= (GPE4_I2SSDO | GPE3_I2SSDI | GPE2_CDCLK | GPE1_I2SSCLK | GPE0_I2SLRCK);
	GPEUP = GPEUP | 0x1f;
		
	IISMOD  = IISMOD_MASTER | IISMOD_TX | IISMOD_LEFT_LOW  | IISMOD_SERIAL_FMT_IIS | IISMOD_MCLK | IISMOD_SERIAL_FREQ_32fs;
	IISCON  = IISCON_PSR_ENABLE | IISCON_RX_CHN_IDLE;
	IISFCON = IISFCON_TX_ENABLE;

#ifdef IIS_USE_DMA
	IISCON  |= IISCON_TX_DMA_ENABLE;	
	IISFCON |= IISFCON_TX_DMA;	
#endif
		
	if(mode) { /*record*/
		IISMOD  |= IISMOD_RX;
		IISCON  &= (~IISCON_RX_CHN_IDLE);
		IISFCON |= IISFCON_RX_ENABLE;	
#ifdef IIS_USE_DMA
		IISCON  |= IISCON_RX_DMA_ENABLE;				
		IISFCON |= IISFCON_RX_DMA;
#endif
	}	
	IISCON |= 0x1;

#ifdef IIS_USE_DMA
    {
        dma_device_t *device;
        
    	if((dma_request_channel(2, "I2SSDO") < 0) || 
    	    ((device = dma_get_device(2, 1)) == NULL)) {
    	    return -1;
    	}
    	
    	device->dst = (unsigned long)&IISFIFO;
    	device->dst_ctl = BUF_ON_APB;
  		device->src_ctl = BUF_ON_MEM;
    	device->ctl |= DCON_HANDSHAKE | DCON_SYNC_PCLK | DCON_UNIT | DCON_IRQ_ON | DCON_SINGLE | DCON_HW_SEL | DCON_RELOAD_OFF;
	}
#endif
    return 0;
}

void uninit_iis()
{
#ifdef IIS_USE_DMA
	dma_free_channel(2);
#endif
	IISCON &= ~0x1;
}

void iis_play(int nsamples, int samplerate, int channels, int bpc, void *data)
{
    static int oldsamplerate;
    
	if(bpc != 8 && bpc != 16)
		return;

	if(oldsamplerate != samplerate) {
    	int prescaler;
    	
		oldsamplerate = samplerate;
		
		/*XXX - Maybe a lookup table better?*/
		prescaler = __roundoff(1.0 * PCLK / (IIS_CLOCK_FREQ * samplerate)) - 1;
		 		        
		IISPSR = (prescaler << 5) | prescaler;	
	}

	IISMOD &= ~(1<<3);
	IISMOD |= ((bpc == 16)<<3);

#ifdef IIS_USE_DMA
	dma_queue_buffer(2, data, bpc / 8, nsamples * channels, 1);
#else
	#define WAV_PLAY(p)                  \
	do {                                 \
		int i;                           \
		for(i = 0; i < nsamples; i++) {  \
			while(IISCON & 0x80);        \
			IISFIFO = *((p)++);          \
			if(channels == 2)            \
				IISFIFO = *((p)++);      \
		}                                \
	} while(0)
		
	{
		short *sp = data;
		char *cp = data;
		if(bpc == 16)
			WAV_PLAY(sp);
		else
			WAV_PLAY(cp);
	}

#endif
}

#endif /*CONFIG_IIS*/
