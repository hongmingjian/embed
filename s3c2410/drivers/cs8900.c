
#include "config.h"
#include "drivers.h"

#ifdef CONFIG_CS8900

/*******************************/
/* CS8900 Registers And Values */
/*******************************/
/*using IO mode*/
#define RTdata      REGW(CS8900_BASE,0x00)
#define TxCMD       REGW(CS8900_BASE,0x04)
#define PTxLength   REGW(CS8900_BASE,0x06)
#define ISQ         REGW(CS8900_BASE,0x08)
#define PPP         REGW(CS8900_BASE,0x0a)
#define PPdata      REGW(CS8900_BASE,0x0c)

/*Transmit Configuration*/
#define PP_TxCFG                0x0106
#define TxCFG_Loss_of_CRSiE     (0x1 << 6)/*the AUI only*/
#define TxCFG_SQErroriE         (0x1 << 7)
#define TxCFG_TxOKiE            (0x1 << 8)
#define TxCFG_Out_of_windowiE   (0x1 << 9)
#define TxCFG_JabberiE          (0x1 << 10)
#define TxCFG_AnycolliE         (0x1 << 11)
#define TxCFG_T16colliE         (0x1 << 15)

/*Transmit event*/
#define PP_TxEvent              0x0128
#define TxEvent_Loss_of_CRS     (0x1 << 6)
#define TxEvent_SQEerror        (0x1 << 7)
#define TxEvent_TxOK            (0x1 << 8)
#define TxEvent_Out_of_window   (0x1 << 9)
#define TxEvent_Jabber          (0x1 << 10)
#define TxEvent_NumCollisions   (0xf << 11)
#define TxEvent_T16Collisions   (0x1 << 15)

/*Transmit Command Request*/
#define PP_TxCmd_Req            0x0144

/*Transmit Command Status*/
#define PP_TxCMD_Sta            0x0108
#define TxStart_After5          (0x0 << 6)
#define TxStart_After381        (0x1 << 6)
#define TxStart_After1021       (0x1 << 7)
#define TxStart_All             (0x3 << 6)
#define TxCMD_Force             (0x1 << 8)
#define TxCMD_Onecoll           (0x1 << 9)
#define TxCMD_InhibitCRC        (0x1 << 12)
#define TxCMD_TxpadDis          (0x1 << 13)

/*Bus Status*/
#define PP_BusST                0x0138
#define BusST_TxBidErr          (0x1 << 7)
#define BusST_Rdy4Tx_Now        (0x1 << 8)

/*Bus Control*/
#define PP_BusCTL               0x0116
#define BusCTL_ResetRxDMA       (0x1 << 6)
#define BusCTL_DMAextend        (0x1 << 8)
#define BusCTL_UseSA            (0x1 << 9)
#define BusCTL_MemoryE          (0x1 << 10)
#define BusCTL_DMABurst         (0x1 << 11)
#define BusCTL_IOCHRDYE         (0x1 << 12)
#define BusCTL_RxDMAsize        (0x1 << 13)
#define BusCTL_EnableIRQ        (0x1 << 15)

/*Buffer Configuration*/
#define PP_BufCFG               0x010A
#define BufCFG_SWint_X          (0x1 << 6)
#define BufCFG_RxDMAiE          (0x1 << 7)
#define BufCFG_Rdy4TxiE         (0x1 << 8)
#define BufCFG_TxUnderruniE     (0x1 << 9)
#define BufCFG_RxMissiE         (0x1 << 10)
#define BufCFG_Rx128iE          (0x1 << 11)
#define BufCFG_TxColOvfiE       (0x1 << 12)
#define BufCFG_MissOvfloiE      (0x1 << 13)
#define BufCFG_RxDestiE         (0x1 << 15)

/*Buffer event*/
#define PP_BufEvent             0x012C
#define BufEvent_SWint          (0x1 << 6)
#define BufEvent_RxDMAFrame     (0x1 << 7)
#define BufEvent_Rdy4Tx         (0x1 << 8)
#define BufEvent_TxUnderrun     (0x1 << 9)
#define BufEvent_RxMiss         (0x1 << 10)
#define BufEvent_Rx128          (0x1 << 11)
#define BufEvent_RxDest         (0x1 << 15)

/*Line Control*/
#define PP_LineCTL              0x0112
#define LineCTL_SerRxON         (0x1 << 6)
#define LineCTL_SerTxON         (0x1 << 7)
#define LineCTL_AUIonly         (0x1 << 8)
#define LineCTL_AutoAUI_10BT    (0x1 << 9)
#define LineCTL_ModBackoffE     (0x1 << 11)
#define LineCTL_PolarityDis     (0x1 << 12)
#define LineCTL_L2_partDefDis   (0x1 << 13)
#define LineCTL_LoRxSquelch     (0x1 << 14)

/*Receiver configuration*/
#define PP_RxCFG                0x0102
#define RxCFG_Skip_1            (0x1 << 6)
#define RxCFG_StreamE           (0x1 << 7)
#define RxCFG_RxOKiE            (0x1 << 8)
#define RxCFG_RxDMAonly         (0x1 << 9)
#define RxCFG_AutoRxDMAE        (0x1 << 10)
#define RxCFG_BufferCRC         (0x1 << 11)
#define RxCFG_CRCerroriE        (0x1 << 12)
#define RxCFG_RuntiE            (0x1 << 13)
#define RxCFG_ExtradataiE       (0x1 << 14)

/*Receiver Control*/
#define PP_RxCTL                        0x0104
#define RxCTL_IAHashA                   (0x1 << 6)
#define RxCTL_PromiscuousA      (0x1 << 7)
#define RxCTL_RxOKA                     (0x1 << 8)
#define RxCTL_MulticastA            (0x1 << 9)
#define RxCTL_IndividualA       (0x1 << 10)
#define RxCTL_BroadcastA            (0x1 << 11)
#define RxCTL_CRCerrorA             (0x1 << 12)
#define RxCTL_RuntA                     (0x1 << 13)
#define RxCTL_ExtradataA            (0x1 << 14)

/*Receive event*/
#define PP_RxEvent                      0x0124
#define RxEvent_IAHash              (0x1 << 6)
#define RxEvent_Dribbblebits    (0x1 << 7)
#define RxEvent_RxOK                    (0x1 << 8)
#define RxEvent_Hashed              (0x1 << 9)
#define RxEvent_IndividualAdr   (0x1 << 10)
#define RxEvent_Broadcast       (0x1 << 11)
#define RxEvent_CRCerror            (0x1 << 12)
#define RxEvent_Runt                    (0x1 << 13)
#define RxEvent_Extradata           (0x1 << 14)

/*Chip self status*/
#define PP_SelfSTAT                     0x0136
#define SelfST_T3VActive        (0x1 << 6)
#define SelfST_INITD            (0x1 << 7)
#define SelfST_SIBUSY           (0x1 << 8)
#define SelfST_EEPROMpresent    (0x1 << 9)
#define SelfST_EEPROMOK             (0x1 << 10)
#define SelfST_ELpresent        (0x1 << 11)
#define SelfST_EEsize           (0x1 << 12)

/*Self Control*/
#define PP_SelfCTL                      0x0114
#define SelfCTL_RESET                   (0x1 << 6)
#define SelfCTL_SWSuspend       (0x1 << 8)
#define SelfCTL_HWSleepE            (0x1 << 9)
#define SelfCTL_HWStandbyE      (0x1 << 10)
#define SelfCTL_HC0E            (0x1 << 12)
#define SelfCTL_HC1E            (0x1 << 13)
#define SelfCTL_HCB0            (0x1 << 14)
#define SelfCTL_HCB1            (0x1 << 15)

/* Receive event*/
#define PP_RER                          0x0124
#define RER_IAHash                      (0x1 << 6)
#define RER_Dribble                     (0x1 << 7)
#define RER_RxOK                    (0x1 << 8)
#define RER_Hashed                  (0x1 << 9)
#define RER_IA                              (0x1 << 10)
#define RER_Broadcast               (0x1 << 11)
#define RER_CRC                     (0x1 << 12)
#define RER_RUNT                    (0x1 << 13)
#define RER_EXTRA                   (0x1 << 14)

/*Interrupt Status Queue*/
#define PP_ISQ                          0x0120
#define ISQ_RxEvent                     0x04
#define ISQ_TxEvent                     0x08
#define ISQ_BufEvent                    0x0C
#define ISQ_RxMissEvent             0x10
#define ISQ_TxColEvent              0x12
#define ISQ_EventMask                   0x3F

/*Line Status*/
#define PP_LineST                       0x0134
#define LineST_LinkOK                   (0x1 << 7)
#define LineST_AUI                      (0x1 << 8)
#define LineST_10BT                     (0x1 << 9)
#define LineST_PolarityOK           (0x1 << 12)
#define LineST_CRS                      (0x1 << 14)

/*Test Control*/
#define PP_TestCTL                      0x0118
#define TestCTL_DisableLT           (0x1 << 7)
#define TestCTL_ENDECloop       (0x1 << 9)
#define TestCTL_AUIloop         (0x1 << 10)
#define TestCTL_DisBackoff      (0x1 << 11)
#define TestCTL_FDX             (0x1 << 15)

/*Interrupt Number*/
#define PP_IntRegNum                0x0022
#define IntRegNum_INTRQ0            0x0000/*choose this*/
#define IntRegNum_INTRQ1            0x0001
#define IntRegNum_INTRQ2            0x0002
#define IntRegNum_INTRQ3            0x0003

/*Product Identification Code*/
#define PP_ChipID                       0x0000
#define PP_ChipRev                      0x0002

/*I/O Base Address*/
#define PP_IOBASE                       0x0020

/*Receiver Miss Counter:The RxMISS counter (Bits 6 through F)
records the number of receive frames that are lost (missed)
due to the lack of available buffer space*/
#define PP_RxMiss                       0x0130

/*AUI Time Domain Reflectometer*/
#define PP_TDR                          0x013C

/*Transmit Packet Length*/
#define PP_TxLength                     0x0146
#define PP_RxLength                     0x0402

/*Logical Address Filter*/
#define PP_LAF                        0x0150

/*Individual Address*/
#define PP_IA                           0x0158

/*Transmit Collision Counter*/
#define PP_TxCOL                            0x0132

/*Receive Frame Byte Counter*/
#define FrameBCounter                   0x0050
/*******************************/
/* CS8900 Registers And Values */
/*******************************/

static unsigned short
get_reg (short  reg_Addr)
{
    PPP = reg_Addr;
    return (unsigned short) PPdata;
}

static void
put_reg (short reg_Addr, unsigned short val)
{
    PPP = reg_Addr;
    PPdata = val;
}

void
eth_out(struct eth_header *pkt, int pktlen)
{
    unsigned short * pShort;

    pShort = (unsigned short *)pkt;

    TxCMD = TxStart_All;
    PTxLength = pktlen;

    if(get_reg(PP_BusST) & BusST_TxBidErr)
        return;

    while(!(get_reg(PP_BusST) & BusST_Rdy4Tx_Now))
        ;

    for (; pktlen > 1; pktlen -= 2)
        RTdata = *pShort++;

    if(pktlen)
        RTdata = *(char *)pShort;
}

static void
cs8900_isr(int irq)
{
   static char pktbuf[ETH_MAX_PACKET_LENGTH];
   unsigned short isq;

   while(1) {
        isq = ISQ;
        if(isq == 0)
            break;
        switch(isq & 0x3f){/*low 6 bits*/
        case ISQ_RxEvent:
            if( isq & RxEvent_RxOK ) {
                unsigned short * pShort;
                unsigned short i, length;

                pShort = (unsigned short *)pktbuf;
                length = get_reg(PP_RxLength);

                for(i=length; i > 1; i -= 2)
                    *pShort++ = RTdata;

                if(i)
                    *(char *)pShort = (char)RTdata;

                eth_in(pktbuf, length);
            }
            break;
        }
    }
}

/*
* mode = 1, for promiscuous mode
*        0, for normal mode
*/
int
init_cs8900(struct eth_addr *addr, int mode)
{
    if (get_reg(PP_ChipID) != 0x630e)
        return -1;

    put_reg (PP_LineCTL,0);/*clear*/
    put_reg (PP_RxCTL,RxCTL_RxOKA |
                      (mode?RxCTL_PromiscuousA:(RxCTL_IndividualA | RxCTL_BroadcastA | RxCTL_MulticastA)));
    put_reg (PP_LineCTL,LineCTL_SerRxON | LineCTL_SerTxON | LineCTL_AutoAUI_10BT);

    put_reg (PP_TestCTL,TestCTL_DisableLT);/* cf p100 of manual*/

    put_reg (PP_RxCFG,  RxCFG_RxOKiE);

    put_reg (PP_IntRegNum, IntRegNum_INTRQ0);
    put_reg (PP_BusCTL, BusCTL_EnableIRQ);

    cs8900_set_addr(addr);

    GPGCON = (GPGCON & (~0xC)) | 8;
    GPGUP |= 0x2; /*Disable pull-up resistor*/

    EXTINT1 = ((EXTINT1 & (~(0x7<<4))) | (4<<4));/* Rising edge triggered */

    intr_setisr(CS8900_IRQ+32/*XXX*/, cs8900_isr);

    return 0;
}

void
cs8900_set_addr(struct eth_addr *p)
{
    unsigned short * pShort;
    int i;

    pShort = (unsigned short *)p;

    for(i = 0;
        i < ETH_ADDR_LENGTH;
        i += sizeof(unsigned short))/*write in 16_bits*/
    put_reg(PP_IA + i,*pShort++);
}

void
cs8900_get_addr(struct eth_addr *p)
{
    unsigned short *pShort;
    int i ;

    pShort = (unsigned short * )p;

    for(i = 0;
        i < ETH_ADDR_LENGTH;
        i += sizeof(unsigned short))/*write in 16_bits*/
    *pShort++ = get_reg(PP_IA + i);
}

#endif/*CONFIG_CS8900*/