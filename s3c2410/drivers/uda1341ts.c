
#include "config.h"
#include "drivers.h"

#ifdef CONFIG_UDA1341

#define L3CLOCK  (0x10)   /*(1<<4) GPB4 = L3CLOCK*/
#define L3DATA   (0x8)    /*(1<<3) GPB3 = L3DATA */
#define L3MODE   (0x4)    /*(1<<2) GPB2 = L3MODE */

#define GPB2_OUTPUT  (1 << 4) /* set output */
#define GPB3_OUTPUT  (1 << 6)
#define GPB4_OUTPUT  (1 << 8)
	 
#define L3MODE_LOW()  {GPBDAT &= ~(L3MODE);}
#define L3MODE_HIGH() {GPBDAT |= (L3MODE);}
#define L3DATA_LOW()  {GPBDAT &= ~(L3DATA);}
#define L3DATA_HIGH() {GPBDAT |= (L3DATA);}
#define L3CLOCK_LOW()  {GPBDAT &= ~(L3CLOCK);}
#define L3CLOCK_HIGH() {GPBDAT |= (L3CLOCK);}

/* UDA1341 Register bits */
#define UDA1341_ADDR		((1 << 2) + (1 << 4))  /*0x14*/
#define UDA1341_REG_DATA0		(UDA1341_ADDR + 0)
#define UDA1341_REG_STATUS		(UDA1341_ADDR + 2)

/* status control */
#define STAT0					(0x00)
#define STAT0_RST               (1 << 6)
#define STAT0_SC_MASK           (3 << 4)
#define STAT0_SC_512fs          (0 << 4)
#define STAT0_SC_384fs          (1 << 4)
#define STAT0_SC_256fs          (2 << 4)
#define STAT0_IF_MASK           (7 << 1)
#define STAT0_IF_I2S            (0 << 1)
#define STAT0_IF_LSB16          (1 << 1)
#define STAT0_IF_LSB18          (2 << 1)
#define STAT0_IF_LSB20          (3 << 1)
#define STAT0_IF_MSB            (4 << 1)
#define STAT0_IF_LSB16MSB       (5 << 1)
#define STAT0_IF_LSB18MSB       (6 << 1)
#define STAT0_IF_LSB20MSB       (7 << 1)
#define STAT0_DC_FILTER         (1 << 0)

#define STAT1					(0x80)
#define STAT1_DAC_GAIN          (1 << 6)        /* gain of DAC */
#define STAT1_ADC_GAIN          (1 << 5)        /* gain of ADC */
#define STAT1_ADC_POL           (1 << 4)        /* polarity of ADC */
#define STAT1_DAC_POL           (1 << 3)        /* polarity of DAC */
#define STAT1_DBL_SPD           (1 << 2)        /* double speed playback */
#define STAT1_ADC_ON            (1 << 1)        /* ADC powered */
#define STAT1_DAC_ON            (1 << 0)        /* DAC powered */

/* data0 direct control */
#define DATA0     				(0x00)
#define DATA0_VOLUME_MASK       (0x3f)
#define DATA0_VOLUME(x)         (x)

#define DATA1     				(0x40)
#define DATA1_BASS(x)           ((x) << 2)
#define DATA1_BASS_MASK         (15 << 2)
#define DATA1_TREBLE(x)         ((x))
#define DATA1_TREBLE_MASK       (3)

#define DATA2     				(0x80)
#define DATA2_PEAKAFTER         (0x1 << 5)
#define DATA2_DEEMP_NONE        (0x0 << 3)
#define DATA2_DEEMP_32KHz       (0x1 << 3)
#define DATA2_DEEMP_44KHz       (0x2 << 3)
#define DATA2_DEEMP_48KHz       (0x3 << 3)
#define DATA2_MUTE              (0x1 << 2)
#define DATA2_FILTER_FLAT       (0x0 << 0)
#define DATA2_FILTER_MIN        (0x1 << 0)
#define DATA2_FILTER_MAX        (0x3 << 0)
/* data0 extend control */
#define EXTADDR(n)              (0xc0 | (n))
#define EXTDATA(d)              (0xe0 | (d))

#define EXT0                    0
#define EXT0_CH1_GAIN(x)        (x)
#define EXT1                    1
#define EXT1_CH2_GAIN(x)        (x)
#define EXT2                    2
#define EXT2_MIC_GAIN_MASK      (7 << 2)
#define EXT2_MIC_GAIN(x)        ((x) << 2)
#define EXT2_MIXMODE_DOUBLEDIFF (0)
#define EXT2_MIXMODE_CH1        (1)
#define EXT2_MIXMODE_CH2        (2)
#define EXT2_MIXMODE_MIX        (3)
#define EXT4                    4
#define EXT4_AGC_ENABLE         (1 << 4)
#define EXT4_INPUT_GAIN_MASK    (3)
#define EXT4_INPUT_GAIN(x)      ((x) & 3)
#define EXT5                    5
#define EXT5_INPUT_GAIN(x)      ((x) >> 2)
#define EXT6                    6
#define EXT6_AGC_CONSTANT_MASK  (7 << 2)
#define EXT6_AGC_CONSTANT(x)    ((x) << 2)
#define EXT6_AGC_LEVEL_MASK     (3)
#define EXT6_AGC_LEVEL(x)       (x)

#define VOLUME_MAX   ((1 << 5) + (1 << 4) + (1 << 3) + (1 << 2) + (1 << 1) + (1 << 0))  /*0x3F*/
#define BASS_MAX     ((1 << 3) + (1 << 2) + (1 << 1) + (0 << 0))  /*0xE*/
#define TREBLE_MAX   ((1 << 1) + (1 << 0))     /*0x3*/

#if IIS_CLOCK_FREQ == 256
#define STAT0_SC  STAT0_SC_256fs
#elif IIS_CLOCK_FREQ == 384
#define STAT0_SC  STAT0_SC_384fs
#elif IIS_CLOCK_FREQ == 512
#define STAT0_SC  STAT0_SC_512fs
#else
#error "Unsupported sampling frequency!"
#endif

static int g_volume = 100, g_bass = 0, g_treble = 0, g_mute = 0;

static
void L3_write_addr(unsigned char data)
{
	int i,j;
	
	L3MODE_LOW();
	L3CLOCK_HIGH();
	
	for(j=0;j<4;j++)             /*L3 set up time > 190ns */
	     ;
	     
	for(i=0;i<8;i++)
	{
		if(data&0x1)
		{
				L3CLOCK_LOW();
				L3DATA_HIGH();
				for(j=0;j<4;j++)       /*L3 clock cycle time > 500ns */
				   ;
				L3CLOCK_HIGH();
				L3DATA_HIGH();
				for(j=0;j<4;j++)
				   ;
		}
		else
		{
			L3CLOCK_LOW();
			L3DATA_LOW();
			for(j=0;j<4;j++)
			   ;
			L3CLOCK_HIGH();
			L3DATA_LOW();
			for(j=0;j<4;j++)
			   ;
		}
		data >>= 1;			
	}
	L3CLOCK_HIGH();
	L3MODE_HIGH();
	
}

static
void L3_write_data(unsigned char data, int halt)
{
	int i,j;
	
	if(halt)
	{
		L3CLOCK_HIGH();
		for(j=0;j<4;j++)              /*tstp(L3) > 190ns */
		   ;
	}
	
	L3CLOCK_HIGH();
	L3MODE_HIGH();
	for(j=0;j<4;j++)               /*tsu(L3) > 190ns */
	    ;
	
	for(i=0;i<8;i++)
	{
		if(data & 0x1)
		{
			L3MODE_HIGH();
			L3CLOCK_LOW();
			L3DATA_HIGH();
			for(j=0;j<4;j++)
			   ;
			L3MODE_HIGH();
			L3CLOCK_HIGH();
			L3DATA_HIGH();
			for(j=0;j<4;j++)
			   ;
		}
		else
		{
			L3MODE_HIGH();
			L3CLOCK_LOW();
			L3DATA_LOW();
			for(j=0;j<4;j++)
			   ;
			L3MODE_HIGH();
			L3CLOCK_HIGH();
			L3DATA_LOW();
			for(j=0;j<4;j++)
			   ;
		}
		data >>= 1; 
	}
	L3CLOCK_HIGH();
	L3MODE_HIGH();
}

void set_volume(int val)
{
	if(val < 0)
		val = 0;
	if(val > 100)
		val = 100;
  
  g_volume = val;
  
	val = VOLUME_MAX - (val * VOLUME_MAX) / 100;

	L3_write_addr( UDA1341_REG_DATA0 );
	L3_write_data(val, 0);
}

void set_bass(int val)
{
	if(val < 0)
		val = 0;
	if(val > 100)
		val = 100;
		
  g_bass = val;
  
	val = BASS_MAX - (val * BASS_MAX) / 100;
	
	val = (1 << 6) + (val << 2) + (TREBLE_MAX - (g_treble * TREBLE_MAX) / 100);
	
	L3_write_addr( UDA1341_REG_DATA0 );
	L3_write_data(val,0);
	
}

void set_treble(int val)
{
	if(val < 0)
		val = 0;
	if(val > 100)
		val = 100;
		
	g_treble = val;
  
	val = TREBLE_MAX - (val * TREBLE_MAX) / 100;
	
	val = (1 << 6) + ((BASS_MAX - (g_bass * BASS_MAX) / 100) << 2) + val;

	L3_write_addr( UDA1341_REG_DATA0 );
	L3_write_data(val, 0);
}

void set_mute(int val)
{
  g_mute = (val != 0);
	val = (1 << 7) + (1 << 5) + (0 << 3) + (val ? (1 << 2) : 0) + 0;	

	L3_write_addr( UDA1341_REG_DATA0 );
	L3_write_data(val, 0);
}

void init_uda1341ts(int mode)
{
	/*
	 * L3MODE   GPB2
	 * L3DATA   GPB3
	 * L3CLOCK  GPB4
	*/
	GPBCON &= (~0x3F0);
	GPBCON |= (GPB4_OUTPUT | GPB3_OUTPUT | GPB2_OUTPUT);
	GPBUP = GPBUP | 0x1c;	
	
	L3MODE_HIGH();
	L3CLOCK_HIGH();
	
	/*0,1,10,000,0:reset,??fs,iis,no dcfilter*/
	L3_write_addr( UDA1341_REG_STATUS );
	L3_write_data( STAT0 | STAT0_RST | STAT0_SC | STAT0_IF_I2S, 0 );  
	/*0,0,10,000,0:no reset,??fs,iis,no dcfilter*/
	L3_write_addr( UDA1341_REG_STATUS );
	L3_write_data( STAT0 |             STAT0_SC | STAT0_IF_I2S, 0 );  
	
	/*1,0,0,0,0,0,01:OGS=0,IGS=0,ADC_NI,DAC_NI,single,AoffDon*/
	L3_write_addr( UDA1341_REG_STATUS );
	L3_write_data(STAT1 | STAT1_DAC_ON, 0);  
	
	set_volume(g_volume);
	set_bass(g_bass);
	set_treble(g_treble);
	set_mute(g_mute);
	
	if(mode) { /*record*/
		/*1,0,1,0,0,0,10 : OGS=0,IGS=1,ADC_NI,DAC_NI,single,AonDoff*/
		L3_write_addr( UDA1341_REG_STATUS );     /*STATUS (000101xx+10)*/
		L3_write_data( STAT1 | STAT1_ADC_GAIN | STAT1_ADC_ON , 0 );       

		/*11000,010  : DATA0, Extended addr(010) */
		L3_write_addr( UDA1341_REG_DATA0 );     /*DATA0 (000101xx+00)*/
		L3_write_data( EXTADDR(2), 0 );     
	}
}

#endif	/* CONFIG_UDA1341 */
