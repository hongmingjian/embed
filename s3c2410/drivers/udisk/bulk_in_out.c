/*
********************************************************************************
*
* Project ....: USB Disk
* File .......: usb_bulk_out.c
* Authors ....: ApolloHan
* Mail........: apollohan@gmail.com
* Date .......: 08.03.07
*
********************************************************************************
*/
#include "config.h"
#include "../drivers.h"

#ifdef CONFIG_UDISK

#include "usb_device.h"
#include "usb_bulk_only.h"


#define SET_EP1_IN_PKT_READY()          IN_CSR1_REG = ( (in_csr1 & ( ~EPIN_WR_BITS))\
 					               					      | EPIN_IN_PKT_READY)

#define SET_EP1_SEND_STALL()	          IN_CSR1_REG = ( (in_csr1 & ( ~EPIN_WR_BITS))\
				               				          | EPIN_SEND_STALL)

#define CLR_EP1_SENT_STALL()	          IN_CSR1_REG = ( (in_csr1 & ( ~EPIN_WR_BITS))\
				               					        & ( ~EPIN_SENT_STALL) )

#define FLUSH_EP1_FIFO()	       	      IN_CSR1_REG = ( (in_csr1 & ( ~EPIN_WR_BITS))\
				                                | EPIN_FIFO_FLUSH)

#define CLR_EP3_OUT_PKT_READY()         OUT_CSR1_REG = (out_csr3 & ( ~EPOUT_WR_BITS)\
																        & ( ~EPOUT_OUT_PKT_READY) )

#define SET_EP3_SEND_STALL()	          OUT_CSR1_REG = (out_csr3 & ( ~EPOUT_WR_BITS)\
																        | EPOUT_SEND_STALL )

#define CLR_EP3_SENT_STALL()	          OUT_CSR1_REG = (out_csr3 & ( ~EPOUT_WR_BITS)\
																        & ( ~EPOUT_SENT_STALL) )

#define FLUSH_EP3_FIFO() 	              OUT_CSR1_REG = (out_csr3 & ( ~EPOUT_WR_BITS)\
																        | EPOUT_FIFO_FLUSH )


#define ep1_send(x, y) do {                       \
    g_bufptr = x;                                 \
    g_buflen = y;                                 \
    if(g_buflen > EP1_PKT_SIZE) {                 \
        write_pkt_ep1(g_bufptr, EP1_PKT_SIZE);    \
 	   	g_bufptr += EP1_PKT_SIZE;                 \
   	    g_buflen -= EP1_PKT_SIZE;                 \
   	} else {                                      \
   	    write_pkt_ep1(g_bufptr, g_buflen);        \
   	    g_buflen = 0;                             \
   	}                                             \
  	SET_EP1_IN_PKT_READY();                       \
} while(0)

/*
********************************************************************************
* Variables
********************************************************************************
*/
static int   g_buflen = 0;
static char *g_bufptr;

static int	 g_cbw_pending = 0;

static unsigned int  g_lba_read;
static unsigned int  g_nlb_read = 0;

static void write_pkt_ep1( char *buf, int len)
{
  int i;

  for(i = 0; i < len; i++)
  {
    EP1_FIFO = ( char) buf[i];
  }
}

static void read_pkt_ep3( char *buf, int len)
{
  int i;

  for(i = 0; i < len; i++)
  {
      buf[i] = EP3_FIFO;
  }
}

static char  csw[13]=
{
	0x55,0x53,0x42,0x53,/*dCSWSignature*/
	0x00,0x00,0x00,0x00,/*dCSWTag*/
	0x00,0x00,0x00,0x00,/*dDataResiduce*/
	0x00                /*dCSWStatus*/
};
static void sendCSW(void)
{
	char in_csr1;

	INDEX_REG = 1;
	in_csr1 = IN_CSR1_REG;

	ep1_send(csw,sizeof(csw));

	g_cbw_pending = 0;
}

static void ufi_read_capacity(void)
{
	char in_csr1;

	char cap_data[8];
	
	*(long *)(&cap_data[0]) = htonl(NF_TOTAL_PAGES - NF_PAGES_RESERVED - 1);
	*(long *)(&cap_data[4]) = htonl(NF_BYTES_PER_PAGE);

	INDEX_REG = 1;

	in_csr1=IN_CSR1_REG;

	ep1_send(cap_data,sizeof(cap_data));
}

static void ufi_get_inquiry(void)
{
	char in_csr1;

	static char inquiry_data[] =
	{
		0x00,				/*Direct Access Device */
		0x80,				/*RMB, the device can be delete*/
		0x02,				/*ISO/ECMA/ANSI*/
		0x01,				/*Response Data Format*/
		0x1F,				/*Additional Length*/
		0x00,				/*Reserved*/
		0x00,				/*Reserved*/
		0x00,				/*Reserved*/
		'T','o','d','d','y','c','e','-',/*Vendor Information*/
		'U', 'S', 'B', '-', 'M', 'a', 's', 's', ' ','S', 't', 'o', 'r', 'a', 'g', 'e', /*Product Identification*/
		0x00,
		0x00,
		0x00,
		0x00
	};

	INDEX_REG = 1;
	in_csr1=IN_CSR1_REG;

	ep1_send(inquiry_data,sizeof(inquiry_data));
}

static void ufi_mode_sense(int pagecode)
{
	char in_csr1;

	char *p;
	int len;

    char mode_sense_all[] =
	{
		0x0b, 0x00,       											/*Mode Data Length*/
		0x00, 0x08, 0x00, 0x00,
		0x7d, 0, 0, 0, 0x02, 0x00
	};

	char mode_sense_tpp[] =
	{
		0xf0, 0x00,       											/*Mode Data Length*/
		05, 00, 00, 00, 00, 0x0b, 00, 00,
		00, 00, 0x24, 00, 00, 00, 00, 00
	};

	INDEX_REG=1;
	in_csr1=IN_CSR1_REG;

	switch(pagecode)
	{
    case SCSI_MSPGCD_RETALL:
        p = mode_sense_all;
        len = sizeof(mode_sense_all);
        break;
    case SCSI_MSPGCD_TPP:
        p = mode_sense_tpp;
        len = sizeof(mode_sense_tpp);
        break;
    default:
        return;
    }

    ep1_send(p,len);
}

static void ufi_read_format_capacity()
{
	char in_csr1;

	char fmt_cap_data[20];

	*(long *)(&fmt_cap_data[0 ]) = htonl(16);
	*(long *)(&fmt_cap_data[4 ]) = htonl(NF_TOTAL_PAGES - NF_PAGES_RESERVED);
	*(long *)(&fmt_cap_data[8 ]) = htonl(NF_BYTES_PER_PAGE);
	*(long *)(&fmt_cap_data[12]) = htonl(NF_TOTAL_PAGES - NF_PAGES_RESERVED);	
	*(long *)(&fmt_cap_data[16]) = htonl(NF_BYTES_PER_PAGE);
	
	fmt_cap_data[8 ] = 0x01;/* nformatted Media - Maximum formattable capacity for this cartridge*/
	fmt_cap_data[16] = 0;   /* reserved */	

	INDEX_REG=1;
	in_csr1=IN_CSR1_REG;

	ep1_send(fmt_cap_data,sizeof(fmt_cap_data));
}

static void ufi_read10(unsigned int lba,unsigned int nlb)
{
	char in_csr1;

	static char in_buf[NF_BYTES_PER_PAGE];
	
	INDEX_REG=1;
	in_csr1=IN_CSR1_REG;

	lba = lba + NF_PAGES_RESERVED;
	nf_read_page(lba, in_buf);

	ep1_send(in_buf,NF_BYTES_PER_PAGE);

	g_lba_read = lba + 1 - NF_PAGES_RESERVED;
	g_nlb_read = nlb - 1;
}

static void do_write10(char *buf, int len)
{
	char out_csr3;

	int fifoCnt;

	while(len > 0)
	{
		INDEX_REG=3;
		out_csr3=OUT_CSR1_REG;

		while (!(out_csr3 & EPOUT_OUT_PKT_READY))
		{
			out_csr3 = OUT_CSR1_REG;
		}

		fifoCnt=OUT_FIFO_CNT1_REG;

		read_pkt_ep3(buf, fifoCnt);
		len -= fifoCnt;
		buf += fifoCnt;

		CLR_EP3_OUT_PKT_READY();
  }
}

static void ufi_write10(unsigned int lba,unsigned int nlb)
{
	char *p;

	int i,preread = 1;
	unsigned int oblk, nblk;

	char readbuf[NF_BYTES_PER_BLOCK];
		
	lba  = lba + NF_PAGES_RESERVED;

	nblk = lba / NF_PAGES_PER_BLOCK;
	oblk = lba % NF_PAGES_PER_BLOCK;


	while (nlb--)
	{
		if(preread)
		{
			p = readbuf;
			for(i = 0;i < NF_PAGES_PER_BLOCK;i++)
			{
				nf_read_page(nblk * NF_PAGES_PER_BLOCK + i,p);
				p += NF_BYTES_PER_PAGE;
			}
			preread = 0;
		}

		p = &readbuf[oblk * NF_BYTES_PER_PAGE];
 		do_write10(p,NF_BYTES_PER_PAGE);

		if((0 == nlb) || (oblk == NF_PAGES_PER_BLOCK - 1))
		{
			nf_erase_blk(nblk * NF_PAGES_PER_BLOCK);
			p = readbuf;
			for(i = 0;i < NF_PAGES_PER_BLOCK;i++)
			{
				nf_write_page(nblk * NF_PAGES_PER_BLOCK + i, p);
				p += NF_BYTES_PER_PAGE;
			}
			preread =  1;
 		}

		oblk++;
		if(oblk == NF_PAGES_PER_BLOCK)
		{
			nblk++;
			oblk = 0;
		}
 	}
}

/*
********************************************************************************
* Description: Handle the transaction of EP1
* Arguments  : void
* Returns    : void
********************************************************************************
*/
void ep1_handler(void)
{
	char in_csr1;

	INDEX_REG=1;
	in_csr1=IN_CSR1_REG;

	if(in_csr1 & EPIN_SENT_STALL)
	{
		CLR_EP1_SENT_STALL();
 		return;
  	}

	if(g_buflen > 0)
	{
		ep1_send(g_bufptr, g_buflen);
		return;
	}

	if(g_nlb_read > 0)
	{
		ufi_read10(g_lba_read, g_nlb_read);
		return;
	}

	if(g_cbw_pending)
	{
		sendCSW();
    	return;
	}
}

/*
********************************************************************************
* Description: Handle the transaction of EP3
* Arguments  : void
* Returns    : void
********************************************************************************
*/
void ep3_handler(void)
{
	int fifoCnt;
    char out_csr3;

    char ep3_Buf[EP3_PKT_SIZE];

    char *cbw = &ep3_Buf[15];


	INDEX_REG=3;
    out_csr3=OUT_CSR1_REG;

    if(out_csr3 & EPOUT_SENT_STALL)
    {
    	CLR_EP3_SENT_STALL();
   		return;
    }

    if(out_csr3 & EPOUT_OUT_PKT_READY)
    {
    	fifoCnt=OUT_FIFO_CNT1_REG;
		read_pkt_ep3(ep3_Buf,fifoCnt);
		CLR_EP3_OUT_PKT_READY();

		INDEX_REG=3;
		out_csr3=OUT_CSR1_REG;

		memcpy(&csw[4],&ep3_Buf[4],4);

		g_cbw_pending = 1;

		switch(cbw[0])
		{
			case INQUIRY:
				ufi_get_inquiry();
				break;
			case MODE_SENSE:
				ufi_mode_sense(cbw[2]);
				break;
			case READ10:
				ufi_read10(cbw[5] | (cbw[4]<<8) | (cbw[3]<<16) | (cbw[2]<<24) , cbw[8] | (cbw[7]<<8));
				break;
			case READ_CAPACITY:
				ufi_read_capacity();
				break;
			case READ_FORMAT_CAPACITY:
				ufi_read_format_capacity();
				break;
			case WRITE10:
				ufi_write10(cbw[5] | (cbw[4]<<8) | (cbw[3]<<16) | (cbw[2]<<24) , cbw[8] | (cbw[7]<<8));
				/*FALL THRU*/
			case TEST_UNIT_READY:
			case VERIFY	:
			case MEDIUM_REMOVAL:
				sendCSW();
				break;
		}
	}
}

#endif /*CONFIG_UDISK*/