/*
********************************************************************************
*
* Project ....: USB Disk
* File .......: usb.h
* Authors ....: ApolloHan
* Mail........: apollohan@gmail.com
* Date .......: 08.03.07
*
********************************************************************************
*/



#ifndef __USB_H__
#define __USB_H__

/*
********************************************************************************
* Constants in USB1.1
********************************************************************************
*/

/*
 * Standard request type (direction)
 */ 
#define HOST_TO_DEVICE              0x00
#define DEVICE_TO_HOST              0x80    

/*
 * Standard request type (type)
 */ 
#define STANDARD_TYPE               0x00
#define CLASS_TYPE                  0x20
#define VENDOR_TYPE                 0x40
#define RESERVED_TYPE               0x60

/*
 * Standard request type (recipient)
 */ 
#define DEVICE_RECIPIENT            0x00
#define INTERFACE_RECIPIENT         0x01
#define ENDPOINT_RECIPIENT          0x02
#define OTHER_RECIPIENT             0x03

/*
 * Feature selectors
 */ 
#define DEVICE_REMOTE_WAKEUP        0x01
#define EP_STALL                    0x00

/*
 * Standard request codes
 */ 
#define GET_STATUS                  0x00
#define CLEAR_FEATURE               0x01
#define SET_FEATURE                 0x03
#define SET_ADDRESS                 0x05
#define GET_DESCRIPTOR              0x06
#define SET_DESCRIPTOR              0x07
#define GET_CONFIGURATION           0x08
#define SET_CONFIGURATION           0x09
#define GET_INTERFACE               0x0A
#define SET_INTERFACE               0x0B
#define SYNCH_FRAME                 0x0C

/*
 * Class-specific request codes
 */ 
#define GET_DEVICE_ID               0x00
#define GET_PORT_STATUS             0x01
#define SOFT_RESET                  0x02

/*
 * Descriptor types
 */
#define DEVICE_TYPE                 0x01
#define CONFIGURATION_TYPE          0x02
#define STRING_TYPE                 0x03
#define INTERFACE_TYPE              0x04
#define ENDPOINT_TYPE               0x05

/*
 * Configuration descriptor: bmAttributes
 */ 
#define CONF_ATTR_DEFAULT	        	0x80 
#define CONF_ATTR_REMOTE_WAKEUP     0x20                
#define CONF_ATTR_SELFPOWERED       0x40

/*
 * Endpoint descriptor
 */
#define EP_MODE_IN		              0x80 	
#define EP_MODE_OUT		              0x00

#define EP_ATTR_CONTROL		          0x00 
#define EP_ATTR_ISOCHRONOUS	        0x01 
#define EP_ATTR_BULK		          	0x02 
#define EP_ATTR_INTERRUPT	          0x03 

/*
 * String descriptor
 */
#define LANGID_US_L 		            0x09  
#define LANGID_US_H 		            0x04

/*
********************************************************************************
* Data Structure in USB1.1
********************************************************************************
*/

/*
 * USB Setup package
 */
struct USB_SETUP_DATA      /* USB设备请求 */
{
	 char bmRequestType;    		
	 char bRequest;         		
	 char bValueL;          		
	 char bValueH;          		
	 char bIndexL;        			
	 char bIndexH;          		
	 char bLengthL;         		
	 char bLengthH;         		
} __attribute__((packed));

/*
 * USB Device descriptor
 */
struct USB_DEVICE_DESCRIPTOR
{
	 char bLength;    		   
	 char bDescriptorType;  
	 char bcdUSBL;			     
	 char bcdUSBH;			     
	 char bDeviceClass;     
	 char bDeviceSubClass;  
	 char bDeviceProtocol;    
	 char bMaxPacketSize0;    
	 char idVendorL;		       
	 char idVendorH;		       
	 char idProductL;		     
	 char idProductH;		     
	 char bcdDeviceL;		     
	 char bcdDeviceH;		     
	 char iManufacturer;		   
	 char iProduct;		       
	 char iSerialNumber;		   
	 char bNumConfigurations;	
} __attribute__((packed));

/*
 * USB Configuration descriptor
 */
struct USB_CONFIGURATION_DESCRIPTOR
{
	 char bLength;    		      
	 char bDescriptorType;  	  
	 char wTotalLengthL;		   
	 char wTotalLengthH;
	 char bNumInterfaces;		 
	 char bConfigurationValue;
	 char iConfiguration;		 
	 char bmAttributes;	    	
	 char maxPower;          	
} __attribute__((packed));
    
/*
 * USB Interface descriptor
 */
struct USB_INTERFACE_DESCRIPTOR
{
	 char bLength;    		      
	 char bDescriptorType;     
	 char bInterfaceNumber;	  
	 char bAlternateSetting;	  
	 char bNumEndpoints;		    
	 char bInterfaceClass;		  
	 char bInterfaceSubClass;	
	 char bInterfaceProtocol;	
	 char iInterface;		      
} __attribute__((packed));

/*
 * USB Endpoint descriptor
 */
struct USB_ENDPOINT_DESCRIPTOR  /* XXX */
{
	 char bLength;    		      
	 char bDescriptorType;     
	 char bEndpointAddress;	  
	 char bmAttributes;		    
	 char wMaxPacketSizeL;  /*将2字节描述的值，拆分成高低两字节 */		  
	 char wMaxPacketSizeH;
	 char bInterval;		        
} __attribute__((packed));


/*
 * USB Configuration Set descriptor
 */
struct USB_CONFIGURATION_SET
{
	 char bConfigurationValue;	
} __attribute__((packed));

/*
 * USB Get Status descriptor
 */
struct USB_GET_STATUS
{
	 char bDevice;			        
	 char bInterface;		      
	 char bEndpoint0;		      
	 char bEndpoint1;		      
	 char bEndpoint3;		      
} __attribute__((packed));

/*
 * USB Interface Get descriptor
 */
struct USB_INTERFACE_GET
{
	 char bAlternateSetting;	  
} __attribute__((packed));

/*
********************************************************************************
* Size of Packet for Bulk Transfer
********************************************************************************
*/

#define BULK_PKT_SIZE            0x20
 
#endif /*__USB_H__*/ 

