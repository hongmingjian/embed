/*
********************************************************************************
*
* Project ....: USB Disk
* File .......: usb_device.h
* Authors ....: ApolloHan
* Mail........: apollohan@gmail.com
* Date .......: 08.03.07
*
********************************************************************************
*/
#ifndef  __USB_DEVICE_H__
#define  __USB_DEVICE_H__

/*
********************************************************************************
* Constants in S3C2410 for USB Device
********************************************************************************
*/

/*
 * Power Management register
 */
#define DISABLE_SUSPEND          0x00   
#define ENABLE_SUSPEND           0x01
#define SUSPEND_MODE		     		 0x02
#define MCU_RESUME               0x04
#define USB_RESET		         		 0x08

/*
 * MAXP register
 */
#define FIFO_SIZE_0              0x00  
#define FIFO_SIZE_8              0x01  
#define FIFO_SIZE_16             0x02  
#define FIFO_SIZE_32             0x04  
#define FIFO_SIZE_64             0x08  

/* 
 * EP0_CSR : mapped to IN_CSR1_REG
 */
#define EP0_OUT_PKT_READY        0x01  
#define EP0_IN_PKT_READY         0x02 
#define EP0_SENT_STALL           0x04         
#define EP0_DATA_END             0x08  
#define EP0_SETUP_END            0x10  
#define EP0_SEND_STALL           0x20  
#define EP0_SERVICED_OUT_PKT_RDY 0x40  
#define EP0_SERVICED_SETUP_END   0x80

/*
 * Clear the bits of EP0_SETUP_END and EP0_OUT_PKT_READY
 */
#define EP0_WR_BITS              0xc0 

/*
 * EP_INT_REG
 */ 
#define EP0_INT                	 0x01   
#define EP1_INT                  0x02   
#define EP2_INT                  0x04   
#define EP3_INT			         		 0x08   
#define EP4_INT			         		 0x10  

/*
 * USB_INT_REG
 */ 
#define SUSPEND_INT            	 0x01  
#define RESUME_INT               0x02  
#define RESET_INT                0x04  

/*
 * IN_CSR1_REG
 */
#define EPIN_IN_PKT_READY        0x01  
#define EPIN_UNDER_RUN		     	 0x04
#define EPIN_FIFO_FLUSH		     	 0x08
#define EPIN_SEND_STALL          0x10  
#define EPIN_SENT_STALL          0x20  
#define EPIN_CLR_DATA_TOGGLE     0x40	
#define EPIN_WR_BITS             0x49 
					                       
/*
 * IN_CSR2_REG
 */
#define EPIN_IN_DMA_INT_MASK	   0x10
#define EPIN_MODE_IN		       	 0x20
#define EPIN_MODE_OUT		       	 0x00
#define EPIN_ISO			       		 0x40
#define EPIN_BULK		           	 0x00
#define EPIN_AUTO_SET		       	 0x80

/*
 * OUT_CSR1_REG
 */
#define EPOUT_OUT_PKT_READY      0x01  
#define EPOUT_OVER_RUN		       0x04  
#define EPOUT_DATA_ERROR		 		 0x08  
#define EPOUT_FIFO_FLUSH		 		 0x10
#define EPOUT_SEND_STALL         0x20  
#define EPOUT_SENT_STALL         0x40
#define EPOUT_CLR_DATA_TOGGLE    0x80	
#define EPOUT_WR_BITS            0xB0
					
/*
 * OUT_CSR2_REG
 */
#define EPOUT_OUT_DMA_INT_MASK	 0x20
#define EPOUT_ISO		 	       		 0x40
#define EPOUT_BULK	 	           0x00
#define EPOUT_AUTO_CLR		       0x80

/*
 * USB DMA control register
 */
#define UDMA_IN_RUN_OB		       0x80
#define UDMA_IGNORE_TTC		       0x80
#define UDMA_DEMAND_MODE	       0x08
#define UDMA_OUT_RUN_OB		       0x04
#define UDMA_OUT_DMA_RUN	       0x04
#define UDMA_IN_DMA_RUN		       0x02
#define UDMA_DMA_MODE_EN	       0x01

/*
********************************************************************************
* The Packet Size of Endpoint 
********************************************************************************
*/

#define EP0_PKT_SIZE             0x08	
#define EP1_PKT_SIZE             0x20
#define EP3_PKT_SIZE             0x20

#endif/*__USB_DEVICE_H__*/

