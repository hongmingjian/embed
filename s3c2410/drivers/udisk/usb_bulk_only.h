/*
********************************************************************************
*
* Project ....: USB Disk
* File .......: usb_bulk_only.h
* Authors ....: ApolloHan
* Mail........: apollohan@gmail.com
* Date .......: 08.03.07
*
********************************************************************************
*/

#ifndef __BULK_ONLY_H__
#define __BULK_ONLY_H__

/*
********************************************************************************
* Constants in USB Massstorage for Bulk_only 
********************************************************************************
*/

#define GET_MAX_LUN          		0xFE

/*
********************************************************************************
* Constants in USB Massstorage for UFI Commands
********************************************************************************
*/

/*SCSI指令集中的几个指令*/

#define INQUIRY              		0x12
#define	MODE_SENSE					 		0x1A
#define	READ10							 		0x28
#define READ_CAPACITY 					0x25 
#define READ_FORMAT_CAPACITY 	  0x23 
#define TEST_UNIT_READY 				0x00 
#define VERIFY 									0x2F 
#define WRITE10 								0x2A 
#define MEDIUM_REMOVAL					0x1E

#define SCSI_MSPGCD_TPP		    	0x1C
#define SCSI_MSPGCD_RETALL			0x3F

#endif /* __BULK_ONLY_H__ */

