/*
********************************************************************************
*
* Project ....: USB Disk
* File .......: usb_device.c
* Authors ....: ApolloHan
* Mail........: apollohan@gmail.com
* Date .......: 08.03.07
*
********************************************************************************
*/
#include "config.h"
#include "../drivers.h"

#ifdef CONFIG_UDISK

#include "usb_device.h"

static void config_usbd(void)
{
	PWR_REG = DISABLE_SUSPEND;	                     
	
	/*
	 * Config EP0 for control transfer
	 */
	INDEX_REG = 0;	
	MAXP_REG = FIFO_SIZE_8;   	
    EP0_CSR = EP0_SERVICED_OUT_PKT_RDY | EP0_SERVICED_SETUP_END;	
  
  
	/*
	 * Config EP1 for bulk in transfer
	 */
	INDEX_REG   = 1;
	MAXP_REG    = FIFO_SIZE_32;	
    IN_CSR1_REG = EPIN_FIFO_FLUSH | EPIN_CLR_DATA_TOGGLE;	
    
    IN_CSR2_REG  = EPIN_MODE_IN | EPIN_IN_DMA_INT_MASK | EPIN_BULK; 
    OUT_CSR1_REG = EPOUT_CLR_DATA_TOGGLE;   	
    OUT_CSR2_REG = EPOUT_BULK | EPOUT_OUT_DMA_INT_MASK;
  
  
	/*
	 * Config EP3 for bulk out transfer
	 */
	INDEX_REG = 3;
	MAXP_REG = FIFO_SIZE_32;	
	IN_CSR1_REG = EPIN_FIFO_FLUSH | EPIN_CLR_DATA_TOGGLE|EPIN_BULK;

	IN_CSR2_REG = EPIN_MODE_OUT | EPIN_IN_DMA_INT_MASK;           
	OUT_CSR1_REG = EPOUT_CLR_DATA_TOGGLE;   	
	OUT_CSR2_REG = EPOUT_BULK | EPOUT_OUT_DMA_INT_MASK;   	


    /*
     *
     */
	EP_INT_REG = EP0_INT | EP1_INT | EP2_INT | EP3_INT | EP4_INT;
	USB_INT_REG = RESET_INT | SUSPEND_INT | RESUME_INT; 
  	  	
	EP_INT_EN_REG = EP0_INT | EP1_INT | EP3_INT;
	USB_INT_EN_REG = RESET_INT ;
}

static void usbd_isr(unsigned int irq)
{
	 char usbd_int_pnd;
	 char ep_int_pnd;
		
	usbd_int_pnd = USB_INT_REG;
	ep_int_pnd = EP_INT_REG;
	
	if(usbd_int_pnd & SUSPEND_INT)
	{
		USB_INT_REG = SUSPEND_INT;	
	}
	
	if(usbd_int_pnd & RESUME_INT)		
	{
		USB_INT_REG = RESUME_INT;
	}
	
	if(usbd_int_pnd & RESET_INT)	
	{
		config_usbd();
		USB_INT_REG = RESET_INT; 
	}
	
	if(ep_int_pnd & EP0_INT)		
	{
		EP_INT_REG = EP0_INT;  
		ep0_handler();     
	}
	else if(ep_int_pnd & EP1_INT)		
	{
		EP_INT_REG = EP1_INT;  
		ep1_handler();
	}
	else if(ep_int_pnd & EP3_INT)		
	{
		EP_INT_REG = EP3_INT;  
		ep3_handler();
	}
}

void init_udisk(void)
{
	UPLLCON = (U_MDIV << 12) | (U_PDIV << 4) | U_SDIV;
	
	config_usbd();
	
	intr_setisr(INT_USBD, usbd_isr);
}

#endif /*CONFIG_UDISK*/