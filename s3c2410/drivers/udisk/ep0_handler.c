/*
********************************************************************************
*
* Project ....: USB Disk
* File .......: usb_ep0.c
* Authors ....: ApolloHan
* Mail........: apollohan@gmail.com
* Date .......: 08.03.07
*
********************************************************************************
*/
#include "config.h"
#include "../drivers.h"

#ifdef CONFIG_UDISK

#include "usb.h"
#include "usb_device.h"
#include "usb_bulk_only.h"

#define CLR_EP0_OUT_PKT_RDY() 	        EP0_CSR = ( (ep0_csr & ( ~EP0_WR_BITS)) | \
								        								EP0_SERVICED_OUT_PKT_RDY )
						
#define CLR_EP0_OUTPKTRDY_DATAEND() 		EP0_CSR = ( (ep0_csr & ( ~EP0_WR_BITS)) | \
						                				    (EP0_SERVICED_OUT_PKT_RDY | EP0_DATA_END) )	
						 
#define SET_EP0_IN_PKT_RDY() 		        EP0_CSR = ( (ep0_csr & ( ~EP0_WR_BITS)) | \
					                		          (EP0_IN_PKT_READY) )
			
#define SET_EP0_INPKTRDY_DATAEND()      EP0_CSR = ( (ep0_csr & ( ~EP0_WR_BITS)) | \
					                			        (EP0_IN_PKT_READY|EP0_DATA_END) )	
			 
#define CLR_EP0_SETUP_END() 		        EP0_CSR = ( (ep0_csr & ( ~EP0_WR_BITS)) | \
		                						        (EP0_SERVICED_SETUP_END) )
			
#define CLR_EP0_SENT_STALL() 		        EP0_CSR = ( (ep0_csr & ( ~EP0_WR_BITS)) & \
					                			        (~EP0_SENT_STALL) )
			
#define FLUSH_EP0_FIFO() 		            do{int i;while(OUT_FIFO_CNT1_REG)i=EP0_FIFO;}while(0)

/*
 * USB USB Configuration descriptor which contain the configuration, interface and endpoint desciptor
 */
struct CONFIGURATION_DESCRIPTOR_ALL
{
	struct USB_CONFIGURATION_DESCRIPTOR config_desc; 	 /*configuration descriptor*/
	struct USB_INTERFACE_DESCRIPTOR     interface_desc;  /* interface descriptor*/
	struct USB_ENDPOINT_DESCRIPTOR      ep_in_desc;      /*bulk in endpoint descriptor*/
	struct USB_ENDPOINT_DESCRIPTOR      ep_out_desc;	 /*bulk out endpoint descriptor*/
}__attribute__((packed));

/*
********************************************************************************
* Variables                   
********************************************************************************
*/
static int				ep0State;

/*
********************************************************************************
* Macro Definition used in this file                  
********************************************************************************
*/
#define EP0_STATE_INIT 		 		  0x00 

#define EP0_STATE_GD_DEV_0	 		  0x10
#define EP0_STATE_GD_DEV_1 			  0x11  
#define EP0_STATE_GD_DEV_2 			  0x12 

#define EP0_STATE_GD_CFG_0	 		  0x20
#define EP0_STATE_GD_CFG_1 			  0x21
#define EP0_STATE_GD_CFG_2 			  0x22
#define EP0_STATE_GD_CFG_3 			  0x23
#define EP0_STATE_GD_CFG_4 			  0x24

#define EP0_STATE_GD_CFG_ONLY_0		0x30
#define EP0_STATE_GD_CFG_ONLY_1		0x31
#define EP0_STATE_GD_IF_ONLY_0 		0x32
#define EP0_STATE_GD_IF_ONLY_1 		0x34
#define EP0_STATE_GD_EP0_ONLY_0		0x34
#define EP0_STATE_GD_EP1_ONLY_0		0x35

#define EP0_CONFIG_GET				    0x40
#define EP0_INTERFACE_GET         0x41

#define MAX_LUN                   0x50

/*
********************************************************************************
* Data Structure for USB Enum 
* These Data Structure are defined in usb.h                   
********************************************************************************
*/
struct USB_DEVICE_DESCRIPTOR          device_desc = 
{
	.bLength              = 0x12,	
	.bDescriptorType      = DEVICE_TYPE,         
	.bcdUSBL              = 0x10,    
	.bcdUSBH              = 0x01, 
	.bDeviceClass         = 0x0,   /*0xFF*/       
	.bDeviceSubClass      = 0x0,          
	.bDeviceProtocol      = 0x0,          
	.bMaxPacketSize0      = 0x8, 
	
	/*三星的idVendor = 0x0839，idProductL = 0x000A*/
	/*ViVi中USB_DEVICE(0x5345, 0x1234)*/      
	.idVendorL            = 0x45,
	.idVendorH            = 0x53,
	.idProductL           = 0x34,
	.idProductH           = 0x12,
	
	.bcdDeviceL           = 0x00,
	.bcdDeviceH           = 0x01,
	
	.iManufacturer        = 0x0,  
	.iProduct             = 0x0,	
	.iSerialNumber        = 0x0,
	.bNumConfigurations   = 0x1,
    
};

struct CONFIGURATION_DESCRIPTOR_ALL   config_desc_all = 
{
	/*
	* Standard configuration descriptor
	*/
	.config_desc.bLength              = 0x9,    
	.config_desc.bDescriptorType      = CONFIGURATION_TYPE,         
	.config_desc.wTotalLengthL        = 0x20, 
	.config_desc.wTotalLengthH        = 0,
	.config_desc.bNumInterfaces       = 1,
	.config_desc.bConfigurationValue  = 1,  
	.config_desc.iConfiguration       = 0,
	.config_desc.bmAttributes         = CONF_ATTR_DEFAULT|CONF_ATTR_SELFPOWERED,/*0xc0*/  
	.config_desc.maxPower             = 25,   /*50*/      /*以2mA为单位,就是50mA*/     
	
	/*
	* Standard interface descriptor
	*/
	.interface_desc.bLength           = 0x9,    
	.interface_desc.bDescriptorType   = INTERFACE_TYPE,         
	.interface_desc.bInterfaceNumber  = 0x0,
	.interface_desc.bAlternateSetting = 0x0, 
	.interface_desc.bNumEndpoints     = 0x2,	
	.interface_desc.bInterfaceClass   = 0x08, 
	.interface_desc.bInterfaceSubClass= 0x06 /*0x06为SCSI传输命令集，0x04为UFI命令集*/,  
	.interface_desc.bInterfaceProtocol= 0x50,/*bulk 0nly 传输*/
	.interface_desc.iInterface        = 0x0,
	
	/*
	* Standard endpoint mode_in descriptor
	*/
	.ep_in_desc.bLength               = 0x7,    
	.ep_in_desc.bDescriptorType       = ENDPOINT_TYPE,         
	.ep_in_desc.bEndpointAddress      = 1|EP_MODE_IN,   
	.ep_in_desc.bmAttributes          = EP_ATTR_BULK,/*0x02,表示是一个bulk断点*/
	.ep_in_desc.wMaxPacketSizeL       = BULK_PKT_SIZE, 
	.ep_in_desc.wMaxPacketSizeH       = 0x0,
	.ep_in_desc.bInterval             = 0x0, /*表示主机多少时间查询一次 not used*/
	
	/*
	* Standard endpoint mode_out descriptor
	*/
	.ep_out_desc.bLength              = 0x7,    
	.ep_out_desc.bDescriptorType      = ENDPOINT_TYPE,         
	.ep_out_desc.bEndpointAddress     = 3|EP_MODE_OUT,   
	.ep_out_desc.bmAttributes         = EP_ATTR_BULK,
	.ep_out_desc.wMaxPacketSizeL      = BULK_PKT_SIZE, 
	.ep_out_desc.wMaxPacketSizeH      = 0x0,
	.ep_out_desc.bInterval            = 0x0, /*not used*/
};

static void  read_pkt_ep0(  char *buf, int len)
{
  int i;
    	
  for(i = 0; i < len; i++)
  {
    buf[i] = EP0_FIFO;	
  }
}

static void  write_pkt_ep0(  char *buf, int len)
{
  int i;
    	
  for(i = 0; i < len; i++)
  {
    EP0_FIFO = buf[i];	
  }
}

/*  
********************************************************************************                                      
* Description: Handle the transaction of EP0  
* Arguments  : void           
* Returns    : void                           
********************************************************************************
*/
void ep0_handler(void)
{
	int save;
	 char ep0_csr;
   char  logical_unit = 0x01;
	struct USB_SETUP_DATA setup_data;
	
	save = INDEX_REG ;

  INDEX_REG = 0;		
  ep0_csr = EP0_CSR;
  
  /*because ep0State==EP0_STATE_INIT when the DATAEND interrupt is issued.*/
  if(ep0_csr & EP0_SETUP_END)
  {   
  	/* Host may end GET_DESCRIPTOR operation without completing the IN data stage.*/
    /* If host does that, SETUP_END bit will be set.*/
    /* OUT_PKT_RDY has to be also cleared because status stage sets OUT_PKT_RDY to 1.*/
 
		CLR_EP0_SETUP_END();
		if(ep0_csr & EP0_OUT_PKT_READY) 
		{
			FLUSH_EP0_FIFO(); /*将缓冲区中的数据全部清除掉*/
	    /*I think this isn't needed because EP0 flush is done automatically.*/   
			CLR_EP0_OUT_PKT_RDY();
		}
		
		ep0State=EP0_STATE_INIT;	/*状态机回到初始态*/
		return;
	}	

 	/*I think that EP0_SENT_STALL will not be set to 1.*/
  if(ep0_csr & EP0_SENT_STALL)
  {   
  	CLR_EP0_SENT_STALL();
		if(ep0_csr & EP0_OUT_PKT_READY) 
		{	
			CLR_EP0_OUT_PKT_RDY();
		}							
		ep0State=EP0_STATE_INIT;	
		return;						
  }					
  
  if((ep0_csr & EP0_OUT_PKT_READY) && (ep0State == EP0_STATE_INIT))
  {	
		read_pkt_ep0(( char*)&setup_data,EP0_PKT_SIZE);	/*注此处直接就是获得建立阶段的数据包。其中的包头信息在硬件中已经处理过了*/
		
		switch(setup_data.bRequest)					/*关于这部分的内容，详见USB协议说明*/
		{
			case GET_DESCRIPTOR:
				switch(setup_data.bValueH)        
				{
					case DEVICE_TYPE:
						CLR_EP0_OUT_PKT_RDY();
	    			ep0State = EP0_STATE_GD_DEV_0;		/*在下面一个case中发送设备类型信息*/	        
	    			break;	
	    		case CONFIGURATION_TYPE:
	    			CLR_EP0_OUT_PKT_RDY();
 	    			if((setup_data.bLengthL+(setup_data.bLengthH<<8))>0x9)/*bLengthH should be used for bLength=0x209 at WIN2K.*/    	
	    				ep0State = EP0_STATE_GD_CFG_0;		/*for WIN98,WIN2K*/
          	else	    	    
  						ep0State = EP0_STATE_GD_CFG_ONLY_0;	/*for WIN2K*/
	    			break;
	    		case STRING_TYPE:
 	    			CLR_EP0_OUT_PKT_RDY();
	    			break;
					case INTERFACE_TYPE:
						CLR_EP0_OUT_PKT_RDY();
	    			break;
					case ENDPOINT_TYPE:	    	
						CLR_EP0_OUT_PKT_RDY();
	    			break;
					default:
						break;
				}	
				break;
			case SET_ADDRESS:	
				FUNC_ADDR_REG = setup_data.bValueL | 0x80;	/*更新USB地址*/
				CLR_EP0_OUTPKTRDY_DATAEND();				/*Because of no data control transfers.*/
        ep0State = EP0_STATE_INIT;
        break;
      case SET_CONFIGURATION:
      	CLR_EP0_OUTPKTRDY_DATAEND(); /*Because of no data control transfers.*/
      	ep0State = EP0_STATE_INIT;
				break;
    	case CLEAR_FEATURE:
    		CLR_EP0_OUTPKTRDY_DATAEND();
    	  ep0State = EP0_STATE_INIT;
    	  break;
    	case GET_CONFIGURATION:
        CLR_EP0_OUT_PKT_RDY();
	    	ep0State = EP0_CONFIG_GET;    	  	   
    	  break;
    	case GET_INTERFACE:
    		CLR_EP0_OUT_PKT_RDY();
	    	ep0State = EP0_INTERFACE_GET;
    	  break;
    	case GET_STATUS:
    	  CLR_EP0_OUT_PKT_RDY();
    	  break;
    	case SET_DESCRIPTOR: 
				CLR_EP0_OUTPKTRDY_DATAEND();
    	  ep0State = EP0_STATE_INIT;
    	  break;
			case SET_FEATURE:
    	  CLR_EP0_OUTPKTRDY_DATAEND();
    	  ep0State = EP0_STATE_INIT;
    	  break;
    	case SET_INTERFACE:
    	  CLR_EP0_OUTPKTRDY_DATAEND(); 
        ep0State = EP0_STATE_INIT;
    	  break;
    	case SYNCH_FRAME:
    	  ep0State = EP0_STATE_INIT;
    	  break;
			case GET_MAX_LUN:          /*存储设备的命令*/
				CLR_EP0_OUT_PKT_RDY(); 
    	  ep0State = MAX_LUN;
    	  break;		
    	default:
    		CLR_EP0_OUTPKTRDY_DATAEND(); /*Because of no data control transfers.*/
				ep0State = EP0_STATE_INIT;
				break;
    }
  } 
	
  switch(ep0State)
  {	
  	case EP0_STATE_INIT:
			break;
		case MAX_LUN:
	    write_pkt_ep0(( char *)&logical_unit,1);	 /*EP0_PKT_SIZE,只支持一个逻辑单元*/
      SET_EP0_INPKTRDY_DATAEND();
			ep0State=EP0_STATE_INIT;
      break;
		/*=== GET_DESCRIPTOR:DEVICE ===*/
  	case EP0_STATE_GD_DEV_0:
  		write_pkt_ep0(( char *)&device_desc+0,8); /*EP0_PKT_SIZE*/
      SET_EP0_IN_PKT_RDY();
      ep0State=EP0_STATE_GD_DEV_1;
      break;            
  	case EP0_STATE_GD_DEV_1:
  		write_pkt_ep0(( char *)&device_desc+8,8); 
      SET_EP0_IN_PKT_RDY();            
      ep0State=EP0_STATE_GD_DEV_2;
      break;
  	case EP0_STATE_GD_DEV_2:
      write_pkt_ep0(( char *)&device_desc+0x10,2);   /*8+8+2=0x12*/
      SET_EP0_INPKTRDY_DATAEND();
      ep0State=EP0_STATE_INIT;
      break;	
      /*=== GET_DESCRIPTOR:CONFIGURATION+INTERFACE+ENDPOINT0+ENDPOINT1 ===
      Windows98 gets these 4 descriptors all together by issuing only a request.
      Windows2000 gets each descriptor seperately.*/
  	case EP0_STATE_GD_CFG_0:
      write_pkt_ep0(( char *)&config_desc_all,8); /*EP0_PKT_SIZE*/
      SET_EP0_IN_PKT_RDY();
      ep0State=EP0_STATE_GD_CFG_1;
      break;
  	case EP0_STATE_GD_CFG_1:						
      write_pkt_ep0(( char *)&config_desc_all+8,8); 
      SET_EP0_IN_PKT_RDY();			
      ep0State=EP0_STATE_GD_CFG_2;	
      break;
    case EP0_STATE_GD_CFG_2:
      write_pkt_ep0(( char *)&config_desc_all+16,8); 
      SET_EP0_IN_PKT_RDY();
      ep0State=EP0_STATE_GD_CFG_3;
      break;
  	case EP0_STATE_GD_CFG_3:
      write_pkt_ep0(( char *)&config_desc_all+24,8); 
      SET_EP0_IN_PKT_RDY();
      ep0State=EP0_STATE_GD_CFG_4;            
      break;
  	case EP0_STATE_GD_CFG_4:
      /*zero length data packit*/
      SET_EP0_INPKTRDY_DATAEND();	/*表明数据阶段通讯结束*/	
      ep0State=EP0_STATE_INIT;            
      break;

		/*=== GET_DESCRIPTOR:CONFIGURATION ONLY===*/
  	case EP0_STATE_GD_CFG_ONLY_0:
      write_pkt_ep0(( char *)&config_desc_all+0,8); /*EP0_PKT_SIZE*/
      SET_EP0_IN_PKT_RDY();
      ep0State=EP0_STATE_GD_CFG_ONLY_1;
      break;
  
  	case EP0_STATE_GD_CFG_ONLY_1:
      write_pkt_ep0(( char *)&config_desc_all+8,1); 
      SET_EP0_INPKTRDY_DATAEND();
      ep0State=EP0_STATE_INIT;            
      break;

      /*=== GET_DESCRIPTOR:INTERFACE ONLY===*/
  	case EP0_STATE_GD_IF_ONLY_0:
      ep0State=EP0_STATE_GD_IF_ONLY_1;
      break;
 		default:	
   		break;
  }
  
  INDEX_REG=save ;			
}

#endif /*CONFIG_UDISK*/