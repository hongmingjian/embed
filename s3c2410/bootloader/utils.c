#include "config.h"

#ifdef CONFIG_UART
extern void sio_putc(char c);
void sio_puts(char *s)
{
  while(*s != 0) {
    sio_putc(*s); 
    s++;
  }
}
#endif

int
strncmp(const char *s1, const char *s2, unsigned int n)
{
	unsigned char u1, u2;

	while (n-- > 0)
	{
		u1 = (unsigned char) *s1++;
		u2 = (unsigned char) *s2++;
		if (u1 != u2)
			return u1 - u2;
		if (u1 == '\0')
			return 0;
	}
	return 0;
}

void *memcpy(void *dest, void *src, unsigned long len)
{
	unsigned long i;
	char *char_dest = (char *)dest;
	char *char_src  = (char *)src;
	
	for(i = 0; i < len; i++)
		*char_dest++ = *char_src++;
		
	return dest;
}
