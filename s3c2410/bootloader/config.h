#ifndef CONFIG_H
#define CONFIG_H

#include "board.h"


#define CONFIG_XMODEM
#define CONFIG_FAT
       #define FAT_READ




#ifdef CONFIG_XMODEM
#define CONFIG_UART
#define CONFIG_UUDECODE
#endif

#ifdef CONFIG_FAT
#define CONFIG_NF
	#ifdef FAT_READ
	#define NF_READ
	#endif
#endif

#ifdef CONFIG_NF
#define NF_PAGES_RESERVED	NF_PAGES_PER_BLOCK
#endif

#ifdef CONFIG_UART
#define CONSOLE_PORT	0
#define CONSOLE_SPEED	115200
#endif

#endif /*CONFIG_H*/
