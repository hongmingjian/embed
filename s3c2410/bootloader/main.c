#include "config.h"
#include "../drivers/drivers.h"

#ifdef CONFIG_FAT
	#ifdef FAT_READ
	extern int  load_file(const char *file_name, char *buffer);
	#endif
#endif

extern long evt[];

int main()
{
	int i, j, c, length;
	char *where = (char *)SDRAM_BASE;
	char *files[]={"UDISK   BIN", "SYSTEM  BIN"};

	init_uart();

	sio_puts("\r\n>> S-BOOT ("__DATE__" "__TIME__")\r\n");

menu:
	sio_puts(">> Menu:\r\n");

#ifdef CONFIG_XMODEM
	sio_puts(">>   1. Xmodem\r\n");
	c = '1';
#endif
#ifdef CONFIG_FAT
	init_nf();
	sio_puts(">>   2. udisk.bin\r\n");
	sio_puts(">>   3. system.bin [*]\r\n");
	c = '3';
#endif

	sio_puts(">> Choose(?):\b\b\b");

	for(i = 5; i; i--) {/* ~5 seconds */
		sio_putc('0' + i);
		sio_putc('\b');

		for(j = 800000; j; j--) {
			if(sio_ischar())
				goto out;
		}
	}
out:
	sio_putc('0'+i);
	sio_puts("):");

	if(sio_ischar())
	    c = sio_getc();

	switch(c) {
#ifdef CONFIG_FAT
		case '2':
		case '3':
		    sio_putc(c);
			sio_puts("\r\n");
			
			c -= '2';

            length = load_file(files[c], where);
			if( length < 0) {
				sio_puts("!! Error loading \"");
				sio_puts(files[c]);
				sio_puts("\".\r\n");
				goto menu;
			}
			break;
#endif

#ifdef CONFIG_XMODEM
		case '1':
			sio_puts("1\r\n");

			sio_puts("\r\n>> Please send an uuencoded program...\r\n");
			length = xmodem(where);
			if(length < 0) {
				sio_puts("!! Error receiving.\r\n");
				goto menu;
			}

			length = uudecode(where);
			if(length < 0) {
				sio_puts("!! Error uudecoding.\r\n");
				goto menu;
			}
			break;
#endif
		default:
			sio_puts("Bad input\r\n");
			goto menu;
	}

	/*fixup ivt*/
	for(i = 0; i < 8/*XXX*/; i++)
		evt[i] += (long)where;

	/*wait UART*/
	for(i = 0; i < 1000000; i++);

	((void (*)(void))where)();

	sio_puts("!! System failed.\r\n");

	return 0;
}
