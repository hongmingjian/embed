#include "config.h"
#include "../drivers/drivers.h"

#ifdef CONFIG_FAT
/*
================================================================================
  外部函数声明
================================================================================
*/
extern void *memcpy(void *dest, void *src, unsigned long len);
extern int   strncmp(const char *s1, const char *s2, unsigned int n);


/*
================================================================================
  数据类型定义
================================================================================
*/
#ifndef BYTE
typedef unsigned char BYTE;
#endif

#ifndef WORD
typedef unsigned short WORD;
#endif

#ifndef DWORD
typedef unsigned int DWORD;
#endif


/*
================================================================================
  文件系统结构体定义
================================================================================
*/

/**
 * BPB structure definition
 */
struct BPB {
	WORD    BPB_BytsPerSec; 			/* bytes per sector */
    BYTE    BPB_SecPerClus; 			/* sectors per cluster */
    WORD    BPB_RsvdSecCnt;  			/* number of reserved sectors */
    BYTE    BPB_NumFATs;        			/* number of FATs */
    WORD    BPB_RootEntCnt; 			/* number of root directory entries */
    WORD    BPB_TotSec16;     			/* total number of sectors */
    BYTE    BPB_Media;       			/* media descriptor */
    WORD    BPB_FATSz16;     			/* number of sectors per FAT */
    WORD    BPB_SecPerTrk; 			/* sectors per track */
    WORD    BPB_NumHeads;       			/* number of heads */
    DWORD   BPB_HiddSec;  			/* # of hidden sectors */
    DWORD   BPB_TotSec32 ; 			/* # of sectors if BPB_TotSec16 == 0 */

    /*The fields below only make sense for FAT32*/
    DWORD   BPB_FATSz32;
    WORD    BPB_ExtFlags;
    WORD    BPB_FSVer;
    DWORD   BPB_RootClus;
    WORD    BPB_FSInfo;
    WORD    BPB_BkBootSec;
    BYTE    BPB_Reserved[12];
}__attribute__ ((packed));

#define BPB_OFFSET		11

/**
 * FAT directory entry structure definition
 */
struct DIRENTRY
{
    BYTE        DIR_Name[11];      		/* filename, blank filled */
 #define SLOT_EMPTY      0x00           /* slot has never been used */
 #define SLOT_E5         0x05           /* the real value is 0xe5 */
 #define SLOT_DELETED    0xe5           /* file in this slot deleted */
 #define SLOT_IS_DIR     0x2e           /* this is directory */

    BYTE        DIR_Attr;   	        /* file attributes */
 #define ATTR_READ_ONLY  0x01           /* file is readonly */
 #define ATTR_HIDDEN     0x02           /* file is hidden */
 #define ATTR_SYSTEM     0x04           /* file is a system file */
 #define ATTR_VOLUME_ID  0x08           /* entry is a volume label */
 #define ATTR_DIRECTORY  0x10           /* entry is a directory name */
 #define ATTR_ARCHIVE    0x20           /* file is new or modified */
 #define ATTR_LONG_FILENAME \
                         (ATTR_READ_ONLY|ATTR_HIDDEN|ATTR_SYSTEM|ATTR_VOLUME_ID)       /* this is a long filename entry */

    BYTE        DIR_NTRes;    		    /* NT VFAT lower case flags */
 #define LCASE_BASE      0x08           /* filename base in lower case */
 #define LCASE_EXT       0x10           /* filename extension in lower case */

    BYTE        DIR_CrtTimeTenth;   	/* hundredth of seconds in CTime */
    WORD        DIR_CrtTime;     		/* create time */
    WORD        DIR_CrtDate;     		/* create date */
    WORD        DIR_LstAccDate;     	/* access date */
    WORD        DIR_FstClusHI;    		/* high bytes of cluster number */
    WORD        DIR_WrtTime;     		/* last update time */
    WORD        DIR_WrtDate;     		/* last update date */
    WORD        DIR_FstClusLO; 		    /* starting cluster of file */
    DWORD       DIR_FileSize;     		/* size of file in bytes */
}__attribute__ ((packed)) ;

/*
================================================================================
  系统相关宏定义
================================================================================
*/
#define FAT12_EOC       0x00000ff8UL /* FAT12文件系统中FAT表的EOC标记 */
#define FAT16_EOC       0x0000fff8UL /* FAT16文件系统中FAT表的EOC标记 */
#define FAT32_EOC		0x0ffffff8UL /* FAT32文件系统中FAT表的EOC标记 */

#define FirstSectorofCluster(N) ((((N) - 2) * SectorsPerCluster) + FirstDataSector)
#define NF_READ_SECTOR(sec, buf) do{nf_read_page(sec + NF_PAGES_RESERVED, buf);}while(0)

static
DWORD NextCluster(DWORD RsvdSecCnt, DWORD BytesPerSector, DWORD cluster, DWORD FATType)
{
    char tmpbuf[NF_BYTES_PER_PAGE+NF_BYTES_PER_PAGE];
    DWORD FATOffset, ThisFATSecNum, ThisFATEntOffset;

    if(FATType == 12)
        FATOffset = cluster + (cluster >> 1);
    else if (FATType == 16)
        FATOffset = cluster << 1;
    else/* if (FATType == 32)*/
        FATOffset = cluster << 2;

    ThisFATSecNum = RsvdSecCnt + (FATOffset / BytesPerSector);
    ThisFATEntOffset = FATOffset % BytesPerSector;

    NF_READ_SECTOR(ThisFATSecNum, tmpbuf);

    if(FATType == 12) {
        DWORD N = cluster;
        if(ThisFATEntOffset == (BytesPerSector - 1)) {
            /* This cluster access spans a sector boundary in the FAT      */
            /* There are a number of strategies to handling this. The      */
            /* easiest is to always load FAT sectors into memory           */
            /* in pairs if the volume is FAT12 (if you want to load        */
            /* FAT sector N, you also load FAT sector N+1 immediately      */
            /* following it in memory unless sector N is the last FAT      */
            /* sector). It is assumed that this is the strategy used here  */
            /* which makes this if test for a sector boundary span         */
            /* unnecessary.                                                */
            NF_READ_SECTOR(ThisFATSecNum+1, tmpbuf+BytesPerSector);
        }

       cluster = *((WORD *) &tmpbuf[ThisFATEntOffset]);
       if(N & 0x0001)
          cluster = cluster >> 4;	/* Cluster number is ODD */
       else
          cluster = cluster & 0x0FFF;	/* Cluster number is EVEN */
    } else if(FATType == 16)
        cluster = *((WORD *) &tmpbuf[ThisFATEntOffset]);
    else/* if (FATType == 32)*/
        cluster = (*((DWORD *) &tmpbuf[ThisFATEntOffset])) & 0x0FFFFFFF;
        
    return cluster;
}
/*
================================================================================
  从nand flash文件系统中载入指定文件
参  数：file_name		文件名
		pbuf			将文件载入的缓存
返回值：>=0				成功载入字节数
		-1				未找到指定文件
================================================================================
*/
#ifdef FAT_READ
int load_file(const char *file_name, char *pbuf)
{
    DWORD i, FATType;
    struct DIRENTRY *dir;
    char *obuf = pbuf, tmpbuf[NF_BYTES_PER_PAGE];
    struct BPB *pbpb = (struct BPB *)(tmpbuf+BPB_OFFSET);
	DWORD cluster, FileSize;
    DWORD RootDirSectors, FirstDataSector, FirstRootDirSector,
          SectorsPerCluster, BytesPerSector, RsvdSecCnt;

    /*Initialisation*/
    {
        DWORD FATSz;

        NF_READ_SECTOR(0,  tmpbuf);

        RsvdSecCnt        = pbpb->BPB_RsvdSecCnt;
        SectorsPerCluster = pbpb->BPB_SecPerClus;
        BytesPerSector    = pbpb->BPB_BytsPerSec;

        if(pbpb->BPB_FATSz16 != 0)
            FATSz = pbpb->BPB_FATSz16;
        else
            FATSz = pbpb->BPB_FATSz32;

    	FirstRootDirSector = pbpb->BPB_RsvdSecCnt + pbpb->BPB_NumFATs * FATSz;
        RootDirSectors     = ((pbpb->BPB_RootEntCnt * sizeof(struct DIRENTRY)) + (pbpb->BPB_BytsPerSec - 1)) / pbpb->BPB_BytsPerSec;

        FirstDataSector    = FirstRootDirSector + RootDirSectors;
    }

    /*Determinte FAT type (12, 16 or 32)*/
    {
        DWORD TotSec, CountofClusters;

        if(pbpb->BPB_TotSec16 != 0)
            TotSec = pbpb->BPB_TotSec16;
        else
            TotSec = pbpb->BPB_TotSec32;

        CountofClusters = (TotSec - FirstDataSector) / pbpb->BPB_SecPerClus;

        if(CountofClusters < 4085) {
            /* Volume is FAT12 */
            FATType = 12;
        } else if(CountofClusters < 65525) {
            /* Volume is FAT16 */
            FATType = 16;
        } else {
            /* Volume is FAT32 */
            FATType = 32;
            cluster = pbpb->BPB_RootClus;
            RootDirSectors = SectorsPerCluster;
        }
    }

    /*Find the specified file*/
next:
    if(FATType == 32) {
        if(cluster >= FAT32_EOC)
            goto notfound;
        FirstRootDirSector = FirstSectorofCluster(cluster);
    }

	for(i = 0; i < RootDirSectors; i++) {
	    NF_READ_SECTOR(FirstRootDirSector+i, tmpbuf);

	    dir = (struct DIRENTRY *)tmpbuf;
	    while((char *)dir < tmpbuf+BytesPerSector) {
	        if(strncmp((char*)(dir->DIR_Name), file_name, sizeof(dir->DIR_Name)) == 0)
	            goto found;
	        dir++;
	    }
	}

    if(FATType == 32) {
        cluster = NextCluster(RsvdSecCnt, BytesPerSector, cluster, FATType);
        goto next;
    }

notfound:    
	return -1;

found:
    cluster = dir->DIR_FstClusLO;
    if(FATType == 32)
        cluster |= (dir->DIR_FstClusHI << 16);

    FileSize = dir->DIR_FileSize;

    /*Do actual reading*/
    while(1) {

        if( ((FATType == 12) && (cluster>=FAT12_EOC)) ||
            ((FATType == 16) && (cluster>=FAT16_EOC)) ||
            ((FATType == 32) && (cluster>=FAT32_EOC)) )
            break;

 		for(i = 0; (i < SectorsPerCluster) && (FileSize > 0); i++) {
 		    if(FileSize < BytesPerSector) {
     		    NF_READ_SECTOR(FirstSectorofCluster(cluster)+i, tmpbuf);
     		    memcpy(pbuf, tmpbuf, FileSize);
     		    pbuf += FileSize;
     		    FileSize = 0;
 		    } else {
     		    NF_READ_SECTOR(FirstSectorofCluster(cluster)+i, pbuf);
     		    pbuf += BytesPerSector;
     		    FileSize -= BytesPerSector;
 		    }
		}

        /*Get next cluster*/
        cluster = NextCluster(RsvdSecCnt, BytesPerSector, cluster, FATType);
    }

    /*Return bytes read*/
	return pbuf-obuf;
}
#endif /*FAT_READ*/
#endif /*CONFIG_FAT*/
