#ifndef FRAME_H
#define FRAME_H


#ifndef __ASSEMBLY__

#include <machine/ansi.h>

/* From NetBSD */
struct irqframe {
        unsigned int if_spsr;
        unsigned int if_r0;
        unsigned int if_r1;
        unsigned int if_r2;
		unsigned int if_r3;
		unsigned int if_r4;
		unsigned int if_r5;
		unsigned int if_r6;
		unsigned int if_r7;
		unsigned int if_r8;
		unsigned int if_r9;
		unsigned int if_r10;
		unsigned int if_r11;
		unsigned int if_r12;
		unsigned int if_usr_sp;
		unsigned int if_usr_lr;
		unsigned int if_svc_sp;
		unsigned int if_svc_lr;
		unsigned int if_pc;
};

/*
#define IF_SPSR		__offsetof(struct irqframe, if_spsr)
#define IF_R0		__offsetof(struct irqframe, if_r0)
#define IF_R1		__offsetof(struct irqframe, if_r1)
#define IF_R2		__offsetof(struct irqframe, if_r2)
#define IF_R3		__offsetof(struct irqframe, if_r3)
#define IF_R4		__offsetof(struct irqframe, if_r4)
#define IF_R5		__offsetof(struct irqframe, if_r5)
#define IF_R6		__offsetof(struct irqframe, if_r6)
#define IF_R7		__offsetof(struct irqframe, if_r7)
#define IF_R8		__offsetof(struct irqframe, if_r8)
#define IF_R9		__offsetof(struct irqframe, if_r9)
#define IF_R10		__offsetof(struct irqframe, if_r10)
#define IF_R11		__offsetof(struct irqframe, if_r11)
#define IF_R12		__offsetof(struct irqframe, if_r12)
#define IF_USR_SP	__offsetof(struct irqframe, if_usr_sp)
#define IF_USR_LR	__offsetof(struct irqframe, if_usr_lr)
#define IF_SVC_SP	__offsetof(struct irqframe, if_svc_sp)
#define IF_SVC_LR	__offsetof(struct irqframe, if_svc_lr)
#define IF_PC		__offsetof(struct irqframe, if_pc)
*/
#endif /*__ASSEMBLY__*/

#define IF_SPSR		0
#define IF_R0		4
#define IF_R1		8
#define IF_R2		12
#define IF_R3		16
#define IF_R4		20
#define IF_R5		24
#define IF_R6		28
#define IF_R7		32
#define IF_R8		36
#define IF_R9		40
#define IF_R10		44
#define IF_R11		48
#define IF_R12		52
#define IF_USR_SP	56
#define IF_USR_LR	60
#define IF_SVC_SP	64
#define IF_SVC_LR	68
#define IF_PC		72

#define IRQFRAME_SIZE	76

#define	IRQ_R0		0
#define	IRQ_SPSR	4
#define	IRQ_LR		8

#define PUSHFRAMEINSVC					\
	sub		lr, lr, #4;					\
	sub		r13, r13, #12;				\
	str		lr, [r13, #IRQ_LR];			\
	mrs		lr, spsr;					\
	str		lr, [r13, #IRQ_SPSR];		\
	str		r0, [r13, #IRQ_R0];			\
	mov		r0, r13;					\
	add		r13, r13, #12;				\
	mrs		r14, cpsr;					\
	bic		r14, r14, #PSR_MODE_MASK;	\
	orr		r14, r14, #PSR_MODE_SVC;	\
	msr		cpsr_cxsf, r14;				\
	sub		sp, sp, #IRQFRAME_SIZE;		\
	str		lr, [sp, #IF_SVC_LR];		\
	ldr		lr, [r0, #IRQ_LR];			\
	str		lr, [sp, #IF_PC];			\
	ldr		r14, [r0, #IRQ_SPSR];		\
	str		r14, [sp, #IF_SPSR];		\
	ldr		r0, [r0, #IRQ_R0];			\
	add		r14, sp, #4;				\
	stmia	r14!, {r0-r12};				\
@	stmia	r14!, {r13, r14}^;			\
@	str		sp, [sp, #IF_SVC_SP]

#define PULLFRAME						\
	ldmia	sp!, {r0};					\
	msr		spsr, r0;					\
	ldmia	sp!, {r0-r12};				\
	add		sp, sp, #(3*4);				\
	ldmia	sp!, {lr, pc}^

#endif /*FRAME_H*/
