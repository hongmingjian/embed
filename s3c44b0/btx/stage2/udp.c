#include "lib.h"

struct pseudohdr {
	IpAddr	saddr;
	IpAddr	daddr;
	char	useless;
	unsigned char protocol;
	unsigned short length;
} __attribute__ ((packed));

static short
udp_cksum(struct udp_header *puh, IpAddr src, IpAddr dst)
{
	unsigned short	  *p;
	unsigned long      sum;
	struct pseudohdr	ph;
	int	len;

	sum = 0;
	
	/*
	 * sum up the pseudo header
	 */
	memcpy(ph.saddr, src, sizeof(IpAddr));
	memcpy(ph.daddr, dst, sizeof(IpAddr));
	ph.useless = 0;
	ph.protocol = IP_PROTO_UDP;
	ph.length   = puh->udp_len;

	len = sizeof(struct pseudohdr);
	p = (unsigned short *)&ph;
	while(len > 1) {
		sum += *p++;
		len -= 2;
	}

	/*
	 * sum up the UDP header and data
	 */ 
	len = ntohs(puh->udp_len);
	p = (unsigned short *)puh;
	while(len > 1) {
		sum += *p++;
		len -= 2;
	}

	if(len)
		sum += *(unsigned char *)p;

	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);

	return ~sum;
}

struct udp_conn {
	int 			state;
	IpAddr			laddr;
	unsigned short	lport;
	IpAddr			raddr;
	unsigned short	rport;
	void (*func)(int, char *, int, void *);
	void 			*arg;
};
#define UDP_MAX_CONN	10

#define UDP_CONN_CLOSED	0
#define UDP_CONN_OPEN	1

struct udp_conn udp_conn[UDP_MAX_CONN] = {{0, {0, 0, 0, 0}, 0, {0, 0, 0, 0}, 0, 0, 0}, };

int udp_open(IpAddr laddr, unsigned short lport, 
			 IpAddr raddr, unsigned short rport, 
			 void (*func)(int, char *, int, void *), 
			 void *arg)
{
	int i;
	unsigned s;

	s = splhigh();

	for(i = 0 ; i < UDP_MAX_CONN; i++) 
		if((udp_conn[i].state == UDP_CONN_OPEN) && 
		   (!memcmp(udp_conn[i].laddr, laddr, sizeof(laddr))) &&
		   (udp_conn[i].lport == lport) &&
		   (!memcmp(udp_conn[i].raddr, raddr, sizeof(raddr))) &&
		   (udp_conn[i].rport == rport)) {
		   		splx(s);
				printf("udp_open: PORT ALREADY OPENED!\n");
				return (-1);
			}

	for(i = 0; i < UDP_MAX_CONN; i++) {
		if(udp_conn[i].state != UDP_CONN_OPEN)
			break;
	}

	if(i == UDP_MAX_CONN) {
		splx(s);
		printf("udp_open: PORTS ARE USED UP!\n");
		return (-1);
	}

	memcpy(udp_conn[i].laddr, laddr, sizeof(laddr));
	udp_conn[i].lport = lport;
	memcpy(udp_conn[i].raddr, raddr, sizeof(raddr));
	udp_conn[i].rport = rport;
	udp_conn[i].func  = func;
	udp_conn[i].arg   = arg;
	udp_conn[i].state = UDP_CONN_OPEN;

	splx(s);

	return (i);
}

void
udp_close(int i)
{
	unsigned s;

	s = splhigh();

	udp_conn[i].state = UDP_CONN_CLOSED;
	
	splx(s);
}

void
udp_in(struct ip_header *pih)
{
	int i;
	struct udp_header *puh;
	unsigned s;

	puh = (struct udp_header *)pih->ip_data;

	if(puh->udp_cksum && udp_cksum(puh, pih->ip_src, pih->ip_dst)) {
		printf("udp_in: CHECKSUM ERROR!\n");
		return;
	}

	s = splhigh();

	/*
	 * De-multiplex
	 */
	for(i = 0; i < UDP_MAX_CONN; i++)
		if((udp_conn[i].state == UDP_CONN_OPEN) &&
		   (!memcmp(udp_conn[i].laddr, pih->ip_dst, sizeof(pih->ip_dst))) &&
		   (udp_conn[i].lport == ntohs(puh->udp_dst)) &&
		   (!memcmp(udp_conn[i].raddr, pih->ip_src, sizeof(pih->ip_src)))) { 
//		   (udp_conn[i].rport == ntohs(puh->udp_src))) {

			splx(s);

			udp_conn[i].rport = ntohs(puh->udp_src);		/*XXX*/

			udp_conn[i].func(i, puh->udp_data, 
							 ntohs(puh->udp_len) - UDP_HEADER_LENGTH, 
							 udp_conn[i].arg);
			return;

		   }

	splx(s);

	/*
	 * default to port unreachable
	 */
	{
		char icmp_data0[4] = {0, 0, 0, 0};
		icmp_out(pih->ip_src, 
				 ICMP_TYPE_UR, 
				 ICMP_UR_CODE_PORT, 
				 icmp_data0,
				 (char *)pih, 
				 IP_HLEN(pih) + min(64, 
				                    ntohs(pih->ip_len) - IP_HLEN(pih)));

	}

	return;
}

void
udp_out(int conn, char *data, short datalen, int do_cksum)
{
	struct nbuf *np;
	struct udp_header *puh;
	int total, pad;

	if(udp_conn[conn].state != UDP_CONN_OPEN) {
		printf("udp_out: INVALID CONNECTION!\n");
		return;
	}

	pad = (datalen & 0x1) ? 1 : 0;

	total = ETH_HEADER_LENGTH + 				/*frame header*/
	        (IP_MINHLEN<<2) + 					/*ip header*/
			UDP_HEADER_LENGTH + 				/*udp header*/
			 (datalen + pad);					/*data*/

	if(total < ETH_MIN_PACKET_LENGTH) {
		pad += (ETH_MIN_PACKET_LENGTH - total);
		total = ETH_MIN_PACKET_LENGTH;
	}

	np = nbuf_alloc(total);
	if(np == NULL) {
		printf("udp_out: NO MEMORY!\n");
		return;
	}

	puh = (struct udp_header *)
	      nbuf_push(np, UDP_HEADER_LENGTH + datalen + pad);
	
	puh->udp_src = htons(udp_conn[conn].lport);
	puh->udp_dst = htons(udp_conn[conn].rport);
	puh->udp_len = htons(UDP_HEADER_LENGTH + datalen);
	puh->udp_cksum = 0;

	memcpy(puh->udp_data, data, datalen);

	if(do_cksum) {
		puh->udp_cksum = udp_cksum(puh, udp_conn[conn].laddr, udp_conn[conn].raddr);
	}
	
	ip_out(np, udp_conn[conn].raddr, UDP_HEADER_LENGTH + datalen, IP_PROTO_UDP);
}
