/*
 * as far as I know, "-nostdlib" is for gcc only, it should not be required
 * at all. Normally, you shouldn't need libgcc.a or libc.a for your
 * bootloader. The missing __gccmain cannot be solved with -nostdlib
 * anyway, this function call is added to the module containing main()
 * automatically. Either you add a stub function __gccmain(){}, or you
 * don't name your entry function main(). 
 */
__gccmain(){}

/****************************************************************************/
#include "lib.h"
//#include "linux.h"

int
main(void)
{
	mi_startup();

	spl0();

#if 0
	{
		char *cmdl[] = {"kernel", "root=/dev/ram0 console=ttyS0 init=/linuxrc"};
		IpAddr ip_svr = {192, 168, 0, 254};

		while(1) {
			tftpc_download(KERNEL_RAM_BASE, ip_svr, 69, "zImage");
			printf("main: KENREL DOWNLOADED!\n");

			tftpc_download(RAMDISK_RAM_BASE, ip_svr, 69, "romfs.bin");
			printf("main: ROMFS DOWNLOADED!\n");
		}

		boot_linux(2, cmdl);
	}
#endif

	return 0;
}
