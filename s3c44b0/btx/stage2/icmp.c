#include "lib.h"

void
icmp_in(struct ip_header *pih)
{
	struct icmp_header *pch;

	pch = (struct icmp_header *)pih->ip_data;
	
	/*
	 * De-multiplex
	 */
	switch(pch->icmp_type) {
	case ICMP_TYPE_ECHO_REPLY:
//		printf("Reply from %u.%u.%u.%u id=%u seq=%u\n", pih->ip_src[0],
		printf("Reply from %u.%u.%u.%u seq=%u\n", pih->ip_src[0],
	                                              pih->ip_src[1],
												  pih->ip_src[2],
												  pih->ip_src[3],
//													    ntohs(*((short *)(&(pch->icmp_data0[0])))),
												  ntohs(*((short *)(&(pch->icmp_data0[2])))));
		break;
	case ICMP_TYPE_ECHO_REQUEST:
		icmp_out(pih->ip_src, 
		         ICMP_TYPE_ECHO_REPLY, 
				 0, 
				 pch->icmp_data0,
				 pch->icmp_data, 
				 ntohs(pih->ip_len) - IP_HLEN(pih) - ICMP_HEADER_LENGTH);
		break;
	}
}

void
icmp_out(IpAddr dest, char type, char code, 
         char data0[], char *data, short datalen)
{
	struct nbuf *np;
	struct icmp_header *pch;	
	int total, pad;

	pad = (datalen & 0x1) ? 1 : 0;

	total = ETH_HEADER_LENGTH + 				/*frame header*/
	        (IP_MINHLEN<<2) + 					/*ip header*/
			ICMP_HEADER_LENGTH + 				/*icmp header*/
			(datalen + pad);					/*data*/

	if(total < ETH_MIN_PACKET_LENGTH) {
		pad += (ETH_MIN_PACKET_LENGTH - total);
		total = ETH_MIN_PACKET_LENGTH;
	}

	np = nbuf_alloc(total);
	if(np == NULL) {
		printf("icmp_out: NO MEMORY!\n");
		return;
	}

	pch = (struct icmp_header *)
	      nbuf_push(np, ICMP_HEADER_LENGTH + datalen + pad);

	pch->icmp_type = type;
	pch->icmp_code = code;
	memcpy(pch->icmp_data0, data0, 4);
	pch->icmp_cksum= 0;

	memcpy(pch->icmp_data, data, datalen);

	pch->icmp_cksum= cksum(pch, ICMP_HEADER_LENGTH + datalen);

	ip_out(np, dest, ICMP_HEADER_LENGTH + datalen, IP_PROTO_ICMP);
}
