#ifndef LIB_H
#define LIB_H 1

#include <sys/types.h>
#include <stdarg.h>
#include "btx.h"
#include "TLSF_malloc.h"

/*
 * Utilities
 */
void *memcpy(void *, const void *, unsigned long);
void *memset(void *, int, unsigned long);
int   memcmp(const void *, const void *, unsigned long);
short cksum(const void *, unsigned long);
short htons(short);
short ntohs(short);
long  htonl(long);
long  ntohl(long);

char *strcat(char *s, const char *append);
int   strcmp(const char *s1, const char *s2);
char *strcpy(char *to, const char *from);
unsigned strlen(const char *str);
unsigned long strlcpy(char *dest, const char *src, unsigned long len); 
int   isdigit(char c);
int   isxdigit(char c);
int   islower(char c);
int   isupper(char c);
int   isalpha(char c);
int   isalnum(char c);
int   isspace(char c);
int   atoi(const char *p);
int   htoi(char *ptr);
long  htol(char *ptr);
char toupper(char c);
char tolower(char c);
size_t strnlen(const char *str, size_t count);
int sprintf(char * buf, const char *fmt, ...);
int vsprintf(char *buf, const char *fmt, va_list args);

/*
 * IRQ
 */
void splz(void);
void sti(void);
void cli(void);
void atomic_set_bit(unsigned *p, unsigned x);
void atomic_clear_bit(unsigned *p, unsigned x);

void die(void);

extern          unsigned cpl;
extern volatile unsigned ipending;

extern          unsigned intr_mask[];
extern void            (*intr_handler[])(void);

/*
 * clock
 */
extern volatile int ticks;
void *timeout(int, void (*)(void *), void *);
void  untimeout(void *);
int   wait(int t, volatile int *p, int v);
int   waitnot(int t, volatile int *p, int v);

/*
 * Memory
 */
#define NUM_MEM_AREAS	8
struct memory_area {
	unsigned long start;
	unsigned long len;
	int used;
};
extern struct memory_area memory_map[];	
void get_memory_map(void);

/*
 * console
 */
int putchar(int);
int getchar(void);
int printf(const char *,...);

/*
 * nbuf
 */
#include <sys/queue.h>

struct nbuf {
	STAILQ_ENTRY(nbuf) next;
	char *data;
	int  size;
	char *ptr;
	int  len;
};

STAILQ_HEAD(nbufhead, nbuf);

struct nbuf *nbuf_alloc(int size);
char        *nbuf_push(struct nbuf *np, int count);
char        *nbuf_pull(struct nbuf *np, int count);
void         nbuf_free (struct nbuf *np);

/*
 * ne2k
 */
void ne2k_send(struct nbuf *np);

/*
 * Ethernet
 */
#define ETH_MIN_PACKET_LENGTH	60
#define ETH_MAX_PACKET_LENGTH	1514

#define ETH_ADDR_LENGTH			6
#define PRO_ADDR_LENGTH			4	
#define ETH_TYPE_ARP			0x0806
#define ETH_TYPE_IP				0x0800

typedef char EthAddr[ETH_ADDR_LENGTH];
struct eth_header {
	EthAddr eth_dhost;	
	EthAddr eth_shost;	
	short   eth_type;	
	char    eth_data[0];
} __attribute__ ((packed));
#define ETH_HEADER_LENGTH		14

void eth_in(struct nbuf *np);
void eth_out(struct nbuf *np, short type, EthAddr hw);

/*
 * Internet Protocol, version 4
 */
#define IP_ADDR_LENGTH	4
typedef char IpAddr[IP_ADDR_LENGTH];
extern IpAddr Self;

struct ip_header {
	char       ip_verlen;
	char       ip_tos;
	short      ip_len;
	short      ip_id;
	short      ip_fragoff;
	char       ip_ttl;
	char       ip_proto;
	short      ip_cksum;
	IpAddr     ip_src;
	IpAddr     ip_dst;
	char       ip_data[0];
} __attribute__ ((packed));

#define IP_PROTO_ICMP 1
#define IP_PROTO_UDP  17

#define IP_VERSION	4
#define IP_MINHLEN	5

#define IP_DEFAULT_TTL	64

#define IP_HLEN(pih)	(((pih)->ip_verlen & 0xf) << 2)

void ip_in(struct eth_header *peh);
void ip_out(struct nbuf *np, IpAddr dst, short datalen, char proto);

/*
 * ARP
 */
#define ARP_HARDWARE	1			/*hardware type*/
#define ARP_PROTOCOL	0x0800		/*protocol type*/
#define ARP_REQUEST		1			/*operation - request*/
#define ARP_REPLY		2			/*operation - reply*/

struct arp_header {
	unsigned short hardware_type;
	unsigned short protocol_type;
	unsigned char  hlen;
	unsigned char  plen;
	unsigned short operation;
	EthAddr        hardware_sender;
	IpAddr         protocol_sender;
	EthAddr        hardware_target;
	IpAddr         protocol_target;
} __attribute__ ((packed));
#define ARP_HEADER_LENGTH		28
#define ARP_PADDING_LENGTH		(ETH_MIN_PACKET_LENGTH-		\
								 ETH_HEADER_LENGTH-			\
								 ARP_HEADER_LENGTH)

void arp_in(struct eth_header *peh);
void arp_out(short op, EthAddr hw, IpAddr ip);

/**********************************************
 *             ARP cache managment            *
 *********************************************/
struct arp_entry {
	int      state;
	int      ttl;		/*seconds*/
	EthAddr  addr_hw;
	IpAddr   addr_ip;
};
#define ARP_DEFAULT_TTL		300		/*default ttl*/
#define ARP_ENTRY_FREE		0
#define ARP_ENTRY_RESOLVED	1
#define ARP_ENTRY_PENDING	2

#define ARP_CACHE_SIZE		50

struct arp_entry *arp_cache_search(IpAddr addr);
void              arp_cache_dump();

/*
 * ICMP
 */
struct icmp_header {
	char  icmp_type;
	char  icmp_code;
	short icmp_cksum;
	char  icmp_data0[4];
	char  icmp_data[0];
} __attribute__ ((packed));
#define ICMP_HEADER_LENGTH			8

#define ICMP_TYPE_ECHO_REPLY		0
#define ICMP_TYPE_ECHO_REQUEST		8

#define ICMP_TYPE_UR				3
	#define ICMP_UR_CODE_PORT		3

void icmp_in(struct ip_header *pih);
void icmp_out(IpAddr dest, char type, char code, 
			  char data0[], char *data, short datalen);

/*
 * UDP
 */
struct udp_header {
	unsigned short	udp_src;
	unsigned short	udp_dst;
	unsigned short	udp_len;
	unsigned short	udp_cksum;
	char			udp_data[0];
} __attribute__ ((packed));
#define UDP_HEADER_LENGTH	8

void udp_in(struct ip_header *pih);

int  udp_open(IpAddr laddr, unsigned short lport, 
			  IpAddr raddr, unsigned short rport, 
			  void (*func)(int, char *, int, void *), 
			  void *);
void udp_close(int i);

void udp_out(int conn, char *data, short datalen, int do_cksum);

#endif /*LIB_H*/
