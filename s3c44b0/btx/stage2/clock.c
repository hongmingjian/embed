#include "btx.h"
#include "lib.h"

struct callout {
	SLIST_ENTRY(callout) next;
	int ticks;
	void (*func)(void *);
	void *arg;
};	

volatile int ticks = 0;

static
volatile SLIST_HEAD(callout_list, callout) 
	callout_list = SLIST_HEAD_INITIALIZER(callout_list);

void
swi_clock(void)
{
	unsigned s;
	struct callout *np;
	void (*func)(void *);
	void *arg;

	s = splhigh();

	np = SLIST_FIRST(&callout_list);
	if(np) {
		--np->ticks;
		while(np && np->ticks <= 0) {
			SLIST_REMOVE_HEAD(&callout_list, next);
			func = np->func;
			arg  = np->arg;

			free(np);

			splx(s);
			func(arg);
			s = splhigh();
			np = SLIST_FIRST(&callout_list);
		}
	}
	splx(s);
}

static void
clock_intr(void)
{
	ticks++;
	if(!SLIST_EMPTY(&callout_list))
		setsoftclock();
}


void *
timeout(int ticks, void (*func)(void *), void *arg)
{
	struct callout *cp, *p, *q;
	unsigned s;

	if(!func)
		return NULL;

	if(ticks <= 0) {
		func(arg);
		return NULL;
	}
	
	cp = malloc(sizeof(struct callout));
	if(cp == NULL) {
		printf("add_callout: NO MEMORY!\n");
		return NULL;	
	}
	
	cp->ticks	= ticks;
	cp->func	= func;
	cp->arg		= arg;

	s = splhigh();

	if(SLIST_EMPTY(&callout_list)) {
		SLIST_INSERT_HEAD(&callout_list, cp, next);
	} else {
		if(cp->ticks < SLIST_FIRST(&callout_list)->ticks) {
			SLIST_FIRST(&callout_list)->ticks -= cp->ticks;
			SLIST_INSERT_HEAD(&callout_list, cp, next);
		} else {
			q = SLIST_FIRST(&callout_list);
			p = SLIST_NEXT(q, next);
			while(p) {
				cp->ticks -= q->ticks;	
				if(cp->ticks < p->ticks) {
					p->ticks -= cp->ticks;
					break;
				}

				q = p;
				p = SLIST_NEXT(q, next);
			}
			if(!p)
				cp->ticks -= q->ticks;	
			SLIST_INSERT_AFTER(q, cp, next);
		}
	}
	splx(s);

	return cp;
}

void
untimeout(void *cookie)
{
	unsigned s;

	if(cookie == NULL)
		return;

	s = splhigh();
		SLIST_REMOVE(&callout_list, (struct callout *)cookie, callout, next);
	splx(s);

	free(cookie);
}

int
wait(int t, volatile int *p, int v)
{
	int old;

	old = ticks;

	while((ticks - old < t) && 
		  ((p == NULL) ||
		   (*p != v)))
		;

	return ((p == NULL) || 
	        (*p != v));
}

int
waitnot(int t, volatile int *p, int v)
{
	int old;

	old = ticks;

	while((ticks - old < t) && 
		  ((p == NULL) || 
		   (*p == v)))
		;

	return ((p == NULL) || 
	        (*p == v));
}

static void
clock_init(void *data)
{
	intr_mask[__CONCAT(INT_TIMER, IRQ_TIMER)] = INT_MASK | SWI_MASK;
	intr_handler[__CONCAT(INT_TIMER, IRQ_TIMER)] = &clock_intr;

	TCFG0	= 0x001d0000;						/*prescaler 2 = 29*/
	TCFG1	= 0x00000000;						/*divider value = 2*/
	__CONCAT(TCNTB, IRQ_TIMER) = 0x00002bf2;	/*MCLK = 67.5M, output = 100Hz*/
	TCON	=  0x06000000;
	TCON	=  0x05000000;

	CLEAR_PEND_INT(__CONCAT(INT_TIMER, IRQ_TIMER));
	INT_ENABLE(__CONCAT(INT_TIMER, IRQ_TIMER));
}

#include "stage2.h"
SYSINIT(clock, SI_SUB_CLOCKS, SI_ORDER_FIRST, clock_init, 0)
