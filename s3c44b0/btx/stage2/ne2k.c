#include "ne2k.h"
#include "lib.h"

static struct nbufhead	inq = STAILQ_HEAD_INITIALIZER(inq); 
static void				ne2k_init(void *data);
static void				ne2k_intr(void);
static unsigned char	ne2k_recv();
static void				ne2k_xmit(struct nbuf *np);

/*
 * NIC interrupt handler
 */
static void
ne2k_intr(void)
{
	unsigned char isr;

	NE2K_CR = NE2K_CR_PAGE_0 | NE2K_CR_STA;

	if((isr = NE2K_ISR) != 0) {

		/*
		 * ACK to NIC ASAP
		 */
		NE2K_ISR = isr;

		/*
		 * frame has arrived
		 */
		if(isr & NE2K_ISR_PRX) {
			ne2k_recv();
			setsoftnet();
		}

		if(isr & NE2K_ISR_OVW) {
			printf("ne2k_intr: OVW\n");
		}

		if(isr & NE2K_ISR_RXE) {
			printf("ne2k_intr: RXE\n");
		}
	}
}

/*
 * software interrupt handler
 */
void
swi_net(void)
{
	struct nbuf *np;
	unsigned s;

	s = splhigh();

	while(!STAILQ_EMPTY(&inq)) {
		np = STAILQ_FIRST(&inq);
		STAILQ_REMOVE_HEAD(&inq, next);

		splx(s);

		eth_in(np);

		s = splhigh();

		nbuf_free(np);
	}

	splx(s);
}

void
ne2k_send(struct nbuf *np)
{
	ne2k_xmit(np);
	nbuf_free(np);
}

static unsigned char
ne2k_recv()
{
	struct nbuf *np;
	struct ne2k_header nh;
	unsigned short *p;
	unsigned char cp;
	short half, pad;

	NE2K_CR		= NE2K_CR_PAGE_0 | NE2K_CR_STA;

	NE2K_RSAR0	= 0;
	NE2K_RSAR1	= NE2K_BNRY;
	NE2K_RBCR0	= sizeof(struct ne2k_header);
	NE2K_RBCR1	= 0;

	NE2K_CR		= NE2K_CR_RD0 | NE2K_CR_STA;

	p = (unsigned short *)&nh;
	half = sizeof(struct ne2k_header) >> 1;
	while(half--) {
		*p++ = inportw(NE2K_RDMA);
	}

	nh.count -= sizeof(struct ne2k_header);

	if((nh.count > ETH_MAX_PACKET_LENGTH) ||
	   (nh.count < ETH_MIN_PACKET_LENGTH)) {
	   	goto out;
	}

	pad = nh.count & 0x1;

	np = nbuf_alloc(nh.count + pad);
	if(np == NULL) {
		printf("ne2k_recv: NO MEMORY!\n");
		goto out;
	}

	STAILQ_INSERT_TAIL(&inq, np, next);

	NE2K_RSAR0	= sizeof(struct ne2k_header);
	NE2K_RSAR1	= NE2K_BNRY;
	NE2K_RBCR0	= nh.count & 0xff;
	NE2K_RBCR1	= nh.count >> 8;

	NE2K_CR		= NE2K_CR_RD0 | NE2K_CR_STA;

	p = (unsigned short *)nbuf_push(np, nh.count + pad);
	half = nh.count >> 1;
	while(half--) {
		*p++ = inportw(NE2K_RDMA);
	}

	if(pad)
		*(unsigned char *)p = inportb(NE2K_RDMA);

out:
	NE2K_CR		= NE2K_CR_PAGE_1 | NE2K_CR_STA;

	cp 			= NE2K_CURR;

	NE2K_CR		= NE2K_CR_PAGE_0 | NE2K_CR_STA;
	NE2K_BNRY	= nh.next;

	return cp;
}

static void
ne2k_xmit(struct nbuf *np)
{
	int half;
	unsigned short *p;
	int maxwait = 200;
	unsigned s;

	if(np == NULL)
		return;

	if((np->len < ETH_MIN_PACKET_LENGTH) ||
	   (np->len > ETH_MAX_PACKET_LENGTH)) {
		printf("ne2k_send: BAD PACKET(%u)!\n", np->len);
		return;
	}

	s = splhigh();

	NE2K_CR		= NE2K_CR_PAGE_0 | NE2K_CR_RD2 | NE2K_CR_STA;

	NE2K_RSAR0	= 0x00;
	NE2K_RSAR1	= NE2K_SEND_START;
	NE2K_RBCR0	= np->len & 0xff;
	NE2K_RBCR1	= np->len >> 8;
	NE2K_CR		= NE2K_CR_PAGE_0 | NE2K_CR_RD1 | NE2K_CR_STA;

	p = (unsigned short *)np->ptr;
	half = np->len >> 1;
	while(half--) {
		outportw(NE2K_RDMA, *p++);
	}

	if(np->len & 0x1)
		outportb(NE2K_RDMA, *(unsigned char *)p);

	while(((NE2K_ISR & NE2K_ISR_RDC) != NE2K_ISR_RDC) && --maxwait)
		;

	if(!maxwait) {
		splx(s);			/*give the interrupt a chance*/

		s = splhigh();
		ne2k_init(NULL);
		splx(s);
		return;
	}

	NE2K_ISR = NE2K_ISR_RDC;
	
	/*
	 * Remote DMA should complete. Command the NIC to start
	 * transmitting.
	 */
	NE2K_TPSR	= NE2K_SEND_START;
	NE2K_TBCR0  = np->len & 0xff;
	NE2K_TBCR1  = np->len >> 8;
	NE2K_CR		= NE2K_CR_PAGE_0 | NE2K_CR_TXP | NE2K_CR_STA;

	splx(s);
}

static void
ne2k_init(void *data)
{
	struct nbuf *np;
	int n = 5000;
	char c;

	c = NE2K_RST;
	NE2K_RST = c;
	NE2K_ISR = 0xff;

	while(--n)
		;

	NE2K_CR		= NE2K_CR_PAGE_0 | NE2K_CR_STP;

	/*data configuration register*/
	NE2K_DCR	= NE2K_DCR_FT1 | NE2K_DCR_WTS | NE2K_DCR_LS;

	/*byte counts of remote DMA*/
	NE2K_RBCR0	= 0x0;
	NE2K_RBCR1	= 0x0;

	NE2K_RCR	= NE2K_RCR_MON;

	/*start page address of the packet to the transmitted*/
	NE2K_TPSR	= NE2K_SEND_START;

	NE2K_TCR	= NE2K_TCR_LB0;

	/*start page address of the receive buffer ring*/
	NE2K_PSTART	= NE2K_RECV_START;

	/*pointer to the last receive buffer page the host has read*/
	NE2K_BNRY	= NE2K_RECV_START;

	/*stop page address of the receive buffer ring*/
	NE2K_PSTOP	= NE2K_RECV_STOP;

	NE2K_ISR	= 0xff;

	/*
	 * should be enough
	 */
	NE2K_IMR	= NE2K_IMR_RXE | NE2K_IMR_PRX | NE2K_IMR_OVW; 
	// | NE2K_IMR_TXE | NE2K_IMR_PTX;

	NE2K_CR		= NE2K_CR_PAGE_1 | NE2K_CR_STP;

	NE2K_PAR0	= NE2K_MAC0;
	NE2K_PAR1	= NE2K_MAC1;
	NE2K_PAR2	= NE2K_MAC2;
	NE2K_PAR3	= NE2K_MAC3;
	NE2K_PAR4	= NE2K_MAC4;
	NE2K_PAR5	= NE2K_MAC5;

	NE2K_CURR	= NE2K_RECV_START;

	NE2K_CR		= NE2K_CR_PAGE_0 | NE2K_CR_STP;

	NE2K_RCR	= NE2K_RCR_AB | NE2K_RCR_AM; //	| NE2K_RCR_PRO;

	NE2K_TCR	= 0;

	NE2K_CR		= NE2K_CR_RD2 | NE2K_CR_STA;

	/*
	 * Now prepare to handle the interrupts from NIC
	 */
	intr_mask[__CONCAT(INT_EINT, IRQ_NE2K)] = SWI_NET_MASK;
	intr_handler[__CONCAT(INT_EINT, IRQ_NE2K)] = &ne2k_intr;

	/*
	 * kick it off
	 */
	CLEAR_PEND_INT(__CONCAT(INT_EINT, IRQ_NE2K));
	INT_ENABLE(__CONCAT(INT_EINT, IRQ_NE2K));
}

#include "stage2.h"
SYSINIT(ne2k, SI_SUB_DRIVERS, SI_ORDER_FIRST, ne2k_init, NULL)
