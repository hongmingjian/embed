#include "lib.h"

#if 0
int init_memory_pool(max_fli, sli, block_size, ptr)
	max_fli  : TLSF  can  calculate  a optimal  fli  from a  given
               block_size (using max_fli = 0), however since user can add new free blocks
	           to the TLSF structure through the add_new_block() function a max_fli can be defined.
	           Possible values are:
	           0 -> the function calculates the optimal max_fli.
	           10 -> 1024 Kbytes * 4
		   ..
	           30 -> 1 Gbytes * 4

	sli  : Second Level Index. 1 < sli < 5
		The biggest, the less fragmentation.
		3 or 4 are fair numbers.
	block_size : size of the initial memory pool (in Kb).
	ptr  : Pointer to the memory pool.
#endif

extern void *_end __asm__("_end");

static void
malloc_init(void *data)
{
	init_memory_pool(0, 4, HEAP_SIZE>>10, (char *)&_end);
	associate_buffer(&_end);
}

#include "stage2.h"
SYSINIT(malloc, SI_SUB_KMEM, SI_ORDER_FIRST, malloc_init, 0)
