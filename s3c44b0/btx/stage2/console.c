#include "lib.h"

inline static int
sio_ischar()
{
	return __CONCAT(UTRSTAT, CONSOLE_PORT) & 0x1; 
}

inline static int
sio_getc()
{
	while( !sio_ischar() )
		;

	return __CONCAT(URXH, CONSOLE_PORT);
}

inline static int
sio_oschar()
{
	return __CONCAT(UTRSTAT, CONSOLE_PORT) & 0x2;
}

inline static void
sio_putc(char c)
{
	while( !sio_oschar() )
		;
	__CONCAT(UTXH, CONSOLE_PORT) = c;
}

static unsigned int
roundiv(unsigned int x, unsigned int y)
{
	unsigned int q, r, halfy;

	halfy = (y >> 1);

	q = x / y;
	r = x % y;
	
	return (r < halfy) ? q : (q + 1);
}

static int
xputc(int c)
{
	sio_putc(c);
    return c;
}

static int
xgetc(void)
{
	return sio_getc();
}

int
puts(const char *s)
{
	while(*s)
		putchar(*s++);
	putchar('\n');
	return 1;
}

int
putchar(int c)
{
    if (c == '\n')
	xputc('\r');
    return xputc(c);
}

int
printf(const char *fmt,...)
{
	char buf[1024];
	va_list args;
	int i, j;

	va_start(args, fmt);
	i=vsprintf(buf,fmt,args);
	va_end(args);

	for(j = 0; j < i; j++)
		putchar(buf[j]);

	return i;
}


int
getchar(void)
{
    int c;

    c = xgetc();
    if (c == '\r')
	c = '\n';
    return c;
}

/*
void
getstr(char *str, int size)
{
    char *s;
    int c;

    s = str;
    do {
	switch (c = getchar()) {
	case 0:
	    break;
	case '\b':
	case '\177':
	    if (s > str) {
		s--;
		putchar('\b');
		putchar(' ');
	    } else
		c = 0;
	    break;
	case '\n':
	    *s = 0;
	    break;
	default:
	    if (s - str < size - 1)
		*s++ = c;
	}
	if (c)
	    putchar(c);
    } while (c != '\n');
}
*/

static void
sio_init(void *data)
{
	__CONCAT(ULCON, CONSOLE_PORT)	= 0x3;
	__CONCAT(UCON, CONSOLE_PORT)	= 0x5;
	__CONCAT(UFCON, CONSOLE_PORT)	= 0x0;
	__CONCAT(UMCON, CONSOLE_PORT)	= 0x3;
	__CONCAT(UBRDIV, CONSOLE_PORT)	= roundiv(MCLK, CONSOLE_SPEED<<4) - 1;
}

#include "stage2.h"
SYSINIT(console, SI_SUB_CONSOLE, SI_ORDER_FIRST, sio_init, 0)
