#include "lib.h"

struct memory_area memory_map[NUM_MEM_AREAS];	

void get_memory_map(void)
{
	int i;
	for(i = 0; i < NUM_MEM_AREAS; i++)
		memory_map[i].used = 0;

	memory_map[6].used	= 1;
	memory_map[6].start	= SDRAM_START;
	memory_map[6].len	= SDRAM_END - SDRAM_START;
}
