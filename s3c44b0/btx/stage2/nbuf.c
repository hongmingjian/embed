#include "lib.h"

struct nbuf *
nbuf_alloc(int size)
{
	struct nbuf *np;
	unsigned s;

	if(size <= 0) {
		printf("nbuf_alloc: BAD SIZE!\n");
		return NULL;
	}

	s = splhigh();

	np = (struct nbuf *)malloc(sizeof(struct nbuf));
	if(np == NULL) {
		splx(s);
		return NULL;
	}

	np->data = (char *)malloc(size);
	if(np->data == NULL) {
		free(np);
		splx(s);
		return NULL;
	}

	splx(s);

	np->size = size;
	np->ptr  = np->data + np->size;
	np->len  = 0;

	memset(np->data, 0, np->size);

	return np;
}

char *
nbuf_push(struct nbuf *np, int count)
{
	unsigned s;
	s = splhigh();

	np->len += count;
	np->ptr -= count;

	splx(s);

	return np->ptr;
}

char *
nbuf_pull(struct nbuf *np, int count)
{
	unsigned s;
	s = splhigh();

	np->len -= count;
	np->ptr += count;

	splx(s);

	return np->ptr;
}

void
nbuf_free(struct nbuf *np)
{
	unsigned s;
	s = splhigh();

	free(np->data);
	free(np);
	
	splx(s);
}
