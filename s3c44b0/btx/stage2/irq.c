#include "lib.h"

unsigned intr_mask[INT_NR] = {0, };
void   (*intr_handler[INT_NR])(void) = {NULL, };

#if 0
	long cpsr;
	__asm__ __volatile__("mrs r1, cpsr\n"
	                     "str r1, %0"
						:"=m"(cpsr)
	                    :/* no inputs */
						:"r1"
						);
#endif

#define DO_SETBITS(name, var, bits) \
void name(void)						\
{									\
	atomic_set_bit(var, bits);		\
}

DO_SETBITS(setsoftclock, &ipending, SWI_CLOCK_PENDING)
DO_SETBITS(setsoftnet,   &ipending, SWI_NET_PENDING)

#define	GENSPL(NAME, OP, MODIFIER)		\
unsigned NAME(void)						\
{										\
	unsigned x;							\
										\
	x = cpl;							\
	cpl OP MODIFIER;					\
	return (x);							\
}

void
spl0(void)
{
	cpl = 0;
	if (ipending)
		splz();
}

void
splx(unsigned ipl)
{
	cpl = ipl;
	if (ipending & ~ipl)
		splz();
}

unsigned 
splq(unsigned mask)
{ 
	unsigned tmp = cpl;
	cpl |= mask;
	return (tmp);
}

GENSPL(splclock,	 =,	INT_MASK | SWI_MASK)
GENSPL(splhigh,		 =,	INT_MASK | SWI_MASK)
/*imp - Interface Message Processor, i.e., NIC*/
//GENSPL(splimp,		|=,	SWI_NET_MASK)// | (1<<__CONCAT(INT_EINT, IRQ_NE2K)))
GENSPL(splnet,		|=,	SWI_NET_MASK)
GENSPL(splsoftclock, =,	SWI_CLOCK_MASK)
