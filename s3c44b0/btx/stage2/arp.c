#include "lib.h"

static void              arp_cache_del(struct arp_entry *pae);
static struct arp_entry *arp_cache_find(IpAddr ip);
static struct arp_entry *arp_cache_add(IpAddr ip, EthAddr hw);


/*
 *  Input an ARP packets
 */
void
arp_in(struct eth_header *peh)
{
	struct arp_header *pah;

	pah = (struct arp_header *)peh->eth_data;

	/*
	 * de-multiplex
	 */
	switch(ntohs(pah->operation)) {
	case ARP_REQUEST:
		if(!memcmp(pah->protocol_target, 
		           Self, 
				   sizeof(IpAddr))) {
			arp_out(ARP_REPLY, 
			        pah->hardware_sender, pah->protocol_sender);
		}
		/*
		 * fall thru
		 */
	case ARP_REPLY:
		arp_cache_add(pah->protocol_sender, pah->hardware_sender);			
		break;
	}

	/*
	 * The packet has reached the end of lifetime.
	 */
}

/*
 *  Ouput an ARP packets
 */
void
arp_out(short op, EthAddr hw, IpAddr ip)
{
	struct nbuf *np;
	struct arp_header *pah;

	np = nbuf_alloc(ETH_MIN_PACKET_LENGTH);
	if(np == NULL) {
		printf("arp_out: NO MEMORY!\n");
		return;
	}

	/*
	 * push the ARP header
	 */
	pah = (struct arp_header *)nbuf_push(np, ARP_HEADER_LENGTH + ARP_PADDING_LENGTH);

	pah->hardware_type = htons(ARP_HARDWARE);
	pah->protocol_type = htons(ARP_PROTOCOL);

	pah->hlen = ETH_ADDR_LENGTH;
	pah->plen = PRO_ADDR_LENGTH;
	
	pah->operation = htons(op);

	pah->hardware_sender[0] = NE2K_MAC0;
	pah->hardware_sender[1] = NE2K_MAC1;
	pah->hardware_sender[2] = NE2K_MAC2;
	pah->hardware_sender[3] = NE2K_MAC3;
	pah->hardware_sender[4] = NE2K_MAC4;
	pah->hardware_sender[5] = NE2K_MAC5;

	pah->protocol_sender[0] = Self[0];
	pah->protocol_sender[1] = Self[1];
	pah->protocol_sender[2] = Self[2];
	pah->protocol_sender[3] = Self[3];

	memcpy(pah->hardware_target, hw, ETH_ADDR_LENGTH);
	memcpy(pah->protocol_target, ip, PRO_ADDR_LENGTH);

	eth_out(np, ETH_TYPE_ARP, hw);
}

/**********************************************
 *             ARP cache managment            *
 *********************************************/
struct arp_entry arp_cache[ARP_CACHE_SIZE] = {{0, },};

void
arp_cache_dump()
{
	int i; 
	unsigned s;

	printf("\n  Internet address\t\tPhysical address\tTTL\n");

	s = splhigh();

	for(i = 0 ;i < ARP_CACHE_SIZE; i++) {
		if(arp_cache[i].state != ARP_ENTRY_RESOLVED)
			continue;
		printf("  %u.%u.%u.%u\t\t\t%x:%x:%x:%x:%x:%x\t\t%u\n", 
		       arp_cache[i].addr_ip[0],
               arp_cache[i].addr_ip[1],		  
               arp_cache[i].addr_ip[2],		  
               arp_cache[i].addr_ip[3],		  
               arp_cache[i].addr_hw[0],		  
               arp_cache[i].addr_hw[1],		  
               arp_cache[i].addr_hw[2],		  
               arp_cache[i].addr_hw[3],		  
               arp_cache[i].addr_hw[4],		  
               arp_cache[i].addr_hw[5],
			   arp_cache[i].ttl);
	}

	splx(s);
}

static void
arp_cache_del(struct arp_entry *pae)
{
	unsigned s;
	s = splhigh();

	pae->state = ARP_ENTRY_FREE;
	
	splx(s);
}

static struct arp_entry *
arp_cache_find(IpAddr ip)
{
	int i;
	unsigned s;

	s = splhigh();

	for(i = 0; i < ARP_CACHE_SIZE; i++) {
		if(arp_cache[i].state == ARP_ENTRY_FREE)
			continue;
		if(!memcmp(arp_cache[i].addr_ip, 
		           ip, 
				   sizeof(IpAddr))) {
			splx(s);
			return &arp_cache[i];
		}
	}

	splx(s);
	return NULL;
}

static struct arp_entry *
arp_cache_add(IpAddr ip, EthAddr hw)
{
	int i;
	struct arp_entry *pae;

	unsigned s;

	pae = arp_cache_find(ip);

	s = splhigh();

	if(pae != NULL) {
		memcpy(pae->addr_hw, hw, sizeof(EthAddr));
		pae->ttl   = ARP_DEFAULT_TTL;
		pae->state = ARP_ENTRY_RESOLVED;

		splx(s);

		return pae;
	}
	
	for(i = 0; i < ARP_CACHE_SIZE; i++) {
		if(arp_cache[i].state == ARP_ENTRY_FREE) 
			break;
	}

	if(i == ARP_CACHE_SIZE) {
		printf("arp_cache_add: CACHE FULL!\n");
		splx(s);
		return NULL;
	}
	
	memcpy(arp_cache[i].addr_hw, hw, sizeof(EthAddr));
	memcpy(arp_cache[i].addr_ip, ip, sizeof(IpAddr));
	arp_cache[i].ttl   = ARP_DEFAULT_TTL;
	arp_cache[i].state = ARP_ENTRY_RESOLVED;

	splx(s);

	return &arp_cache[i];
}

struct arp_entry *
arp_cache_search(IpAddr ip)
{
	int i;
	struct arp_entry *pae;
	unsigned s;

	EthAddr brc = {0xff, 0xff, 0xff,
	               0xff, 0xff, 0xff};

	pae = arp_cache_find(ip);
	if(pae != NULL) {
		return pae;
	}

	s = splhigh();

	for(i = 0; i < ARP_CACHE_SIZE; i++) {
		if(arp_cache[i].state == ARP_ENTRY_FREE) 
			break;
	}

	if(i == ARP_CACHE_SIZE) {
		printf("arp_cache_search: CACHE FULL!\n");
		splx(s);
		return NULL;
	}

	memset(arp_cache[i].addr_hw, 0, sizeof(EthAddr));
	memcpy(arp_cache[i].addr_ip, ip, sizeof(IpAddr));
	arp_cache[i].ttl   = 5;
	arp_cache[i].state = ARP_ENTRY_PENDING;

	splx(s);

	arp_out(ARP_REQUEST, brc, ip);

	return &arp_cache[i];
}

void
arp_tick(void *data)
{
	int i;
	unsigned s;

	s = splhigh();

	for(i = 0 ;i < ARP_CACHE_SIZE; i++) {
		if(arp_cache[i].state == ARP_ENTRY_FREE)
			continue;
		if(--arp_cache[i].ttl <= 0)
			arp_cache_del(&arp_cache[i]);
	}

	splx(s);

	timeout(HZ, &arp_tick, NULL);	
}

void
arp_init(void *data)
{
	int i;
	for(i = 0 ;i < ARP_CACHE_SIZE; i++)
		arp_cache[i].state = ARP_ENTRY_FREE;

	timeout(HZ, &arp_tick, NULL);	
}

#include "stage2.h"
SYSINIT(arp, SI_SUB_DRIVERS, SI_ORDER_ANY, arp_init, NULL)
