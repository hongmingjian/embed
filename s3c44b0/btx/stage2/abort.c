#include "lib.h"
#include "frame.h"

void
abort(struct irqframe *pf)
{
	printf("**************************ABORT**************************\n");
	printf(	"r0=0x%08x r1=0x%08x r2=0x%08x r3=0x%08x\n"
			"r4=0x%08x r5=0x%08x r6=0x%08x r7=0x%08x\n"
			"r8=0x%08x r9=0x%08x r10=0x%08x r11=0x%08x r12=0x%08x\n"
			"sp=0x%08x lr=0x%08x pc=0x%08x spsr=0x%08x\n", 
			pf->if_r0, pf->if_r1, pf->if_r2, pf->if_r3,
			pf->if_r4, pf->if_r5, pf->if_r6, pf->if_r7,
			pf->if_r8, pf->if_r9, pf->if_r10, pf->if_r11, pf->if_r12,
			pf->if_svc_sp, pf->if_svc_lr, pf->if_pc, pf->if_spsr);
	printf("cpl=0x%08x ipending=0x%08x\n", cpl, ipending);
}
