#include "lib.h"

IpAddr Self = {192, 168, 0, 238};

/*
 *  Input an IP packets
 */
void
ip_in(struct eth_header *peh)
{
	struct ip_header *pih;

	pih = (struct ip_header *)peh->eth_data;

	if(cksum(pih, IP_HLEN(pih))) {
		printf("ip_in: CHECKSUM ERROR!\n");
		return;
	}

	/*
	 * de-multiplex
	 */
	switch(pih->ip_proto) {
	case IP_PROTO_ICMP:
		icmp_in(pih);
		break;
	case IP_PROTO_UDP:
		udp_in(pih);
		break;
	}
}

void
ip_out(struct nbuf *np, IpAddr dest, short datalen, char proto)
{
	static short ip_packet_id = 0;

	struct ip_header *pih;

	pih = (struct ip_header *)nbuf_push(np, IP_MINHLEN<<2);

	pih->ip_verlen = (IP_VERSION << 4) | IP_MINHLEN;
	pih->ip_tos    = 0;
	pih->ip_len    = htons(datalen + IP_HLEN(pih));
	pih->ip_id     = htons(ip_packet_id++);
	pih->ip_fragoff= 0;
	pih->ip_ttl    = IP_DEFAULT_TTL;
	pih->ip_proto  = proto;
	
	memcpy(pih->ip_src, Self, sizeof(IpAddr));
	memcpy(pih->ip_dst, dest, sizeof(IpAddr));

	pih->ip_cksum  = 0;
	pih->ip_cksum  = cksum(pih, IP_HLEN(pih));

	{
		EthAddr brc = {0xff, 0xff, 0xff,
	   	               0xff, 0xff, 0xff};
		eth_out(np, ETH_TYPE_IP, brc);
	}
}
