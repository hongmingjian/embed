#include "lib.h"

void
eth_in(struct nbuf *np)
{
	struct eth_header *peh;

	peh = (struct eth_header *)nbuf_push(np, 0);

	/*
	 * de-multiplex
	 */
	switch(ntohs(peh->eth_type)) {
	case ETH_TYPE_ARP:
		arp_in(peh);
		break;
	case ETH_TYPE_IP:
		ip_in(peh);
		break;
	}
}

void
eth_out(struct nbuf *np, short type, EthAddr hw)
{
	struct  eth_header *peh;

	/*
	 * push the link layer header
	 */
	peh = (struct eth_header *)nbuf_push(np, ETH_HEADER_LENGTH);

	/*
	 * fill the source address
	 */
	peh->eth_shost[0] = NE2K_MAC0;
	peh->eth_shost[1] = NE2K_MAC1;
	peh->eth_shost[2] = NE2K_MAC2;
	peh->eth_shost[3] = NE2K_MAC3;
	peh->eth_shost[4] = NE2K_MAC4;
	peh->eth_shost[5] = NE2K_MAC5;

	/*
	 * and destination address
	 */
	switch(type) {
	case ETH_TYPE_ARP:
		memcpy(peh->eth_dhost, hw, sizeof(EthAddr));
		break;
	case ETH_TYPE_IP:
		{
			struct  arp_entry *pae;
			struct  ip_header *pih;

			pih = (struct ip_header *)(peh->eth_data);

			pae = arp_cache_search(pih->ip_dst);

			/*XXX*/
#if 1
			if((pae == NULL) || 
			   (pae->state != ARP_ENTRY_RESOLVED)) {
#else
			if(wait(10, &pae->state, ARP_ENTRY_RESOLVED)) {
#endif

				printf("eth_out: ARP TIMEOUT!\n");

				nbuf_free(np);
				return;
			}

			memcpy(peh->eth_dhost, pae->addr_hw, sizeof(EthAddr));
		}
		break;
	}

	/*
	 * frame type
	 */
	peh->eth_type     = htons(type);

	ne2k_send(np);
}
