#ifndef LINUX_H
#define LINUX_H

#define KERNEL_RAM_BASE		(0x0c300000)

/* the position of the kernel boot parameters */
#define BOOT_PARAMS			(0x0c000100)

#define RAMDISK_RAM_BASE	(0x0c400000)
#define RAMDISK_FLASH_LEN	(4096 * 1024)

/* the size (in kbytes) to which the compressed ramdisk expands */
#define RAMDISK_SIZE		(4 * 1024)

#define ARCH_NUMBER			178

#endif /*LINUX_H*/
