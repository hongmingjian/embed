#include "lib.h"

struct tftp_header {
	short tftp_opcode;
	char  tftp_data[0];
} __attribute__ ((packed));

#define TFTP_RRQ	1
#define TFTP_WRQ	2
#define TFTP_DATA	3
#define TFTP_ACK	4
#define TFTP_ERROR	5

#define TFTP_BLK_LENGTH		512

static void
tftpc_out(int conn, short opcode, char *data, int datalen)
{
	struct tftp_header *pth;

	pth = (struct tftp_header *)malloc(sizeof(struct tftp_header) + 
									   datalen);

	if(pth == NULL) {
		printf("tftpc_out: NO MEMORY!\n");
		return;
	}

	pth->tftp_opcode = htons(opcode);

	memcpy(pth->tftp_data, data, datalen);

	udp_out(conn, (char *)pth, 
			sizeof(struct tftp_header) + datalen, 1);

	free(pth);
}

struct tftpc_arg {
	volatile int	state;
	char			*addr;
};
#define TFTPC_STATE_STOP	0
#define TFTPC_STATE_START	1
#define TFTPC_STATE_DONE	5

static void
tftpc_in(int conn, char *data, int datalen, void *arg)
{
	static short wanted = 1;
	static int   size = 0;

	struct tftp_header *pth = (struct tftp_header *)data;
	struct tftpc_arg *pta = (struct tftpc_arg *)arg;

	switch(ntohs(pth->tftp_opcode)) {
	case TFTP_RRQ:
		break;
	case TFTP_WRQ:
		break;
	case TFTP_DATA:
		datalen -= 4;		/* skip opcode and block # */

		if(ntohs(*(short *)pth->tftp_data) == 1) {
			size = 0;
			pta->state = TFTPC_STATE_START;
			wanted     = 1;
		}

		if(ntohs(*(short *)pth->tftp_data) == wanted) {

			memcpy(pta->addr + size, pth->tftp_data + 2, datalen);

			wanted++;
			size += datalen;

			if(datalen < TFTP_BLK_LENGTH) {
				pta->state = TFTPC_STATE_DONE;
				printf("tftpc_in: %u received.\n", size);
			}
		}

		/*ACK the block just received*/
		tftpc_out(conn, TFTP_ACK, pth->tftp_data, 2);
		break;
	case TFTP_ACK:
		break;
	case TFTP_ERROR:
		printf("tftpc_in: %s(%u)\n", pth->tftp_data + 2, 
					 				 ntohs(*(short *)pth->tftp_data));
		break;
	default:
		printf("tftpc_in: BAD TFTP PACKET!\n");
		break;
	}
}

int
tftpc_download(char *addr, IpAddr tftp_svr_addr, unsigned short tftp_svr_port, 
               char file[])
{
	int				conn;
	IpAddr			tftp_clt_addr;
	unsigned short	tftp_clt_port;
	char           *pos, *req;

	struct tftpc_arg	arg;

	arg.state = TFTPC_STATE_STOP;
	arg.addr  = addr;

	memcpy(tftp_clt_addr, Self, sizeof(Self));

	pos = req = (char *)malloc(strlen(file) + 7);
	if(req == NULL) {
		printf("tftpc_download: NO MEMORY!\n");
		return (-1);
	}

	strcpy(pos, file);
	pos += strlen(file);
	*pos++ = 0;
	strcpy(pos, "octet");

	do {
		tftp_clt_port = (ticks & 0xffff);

		conn = udp_open(tftp_clt_addr, tftp_clt_port, 
						tftp_svr_addr, tftp_svr_port, 
						tftpc_in, (void *)&arg);

		if(conn >= 0) {
			tftpc_out(conn, TFTP_RRQ, req, strlen(file) + 7);

			if(!waitnot(100, &arg.state, TFTPC_STATE_STOP))
				break;
		
			udp_close(conn);
		}
	} while(1);

	free(req);

	while(arg.state != TFTPC_STATE_DONE)
		;

	udp_close(conn);

	return 0;
}
