
#define DEC(c)  (((c) - ' ') & 077)

static int
strncmp(const char * cs,const char * ct,int count)
{
	register signed char __res = 0;

	while (count) {
		if ((__res = *cs - *ct++) != 0 || !*cs++)
			break;
		count--;
	}

	return __res;
}

static void
convert(char *r, char *w, int n)
{
	char ch1, ch2, ch3;
	
	ch1 = DEC(r[0]) << 2 | DEC(r[1]) >> 4;
	ch2 = DEC(r[1]) << 4 | DEC(r[2]) >> 2;
	ch3 = DEC(r[2]) << 6 | DEC(r[3]);
	
	if(n >= 1) 
		*w++ = ch1;
	if(n >= 2)
		*w++ = ch2;
	if(n >= 3)
		*w++ = ch3;
}

static int
dec_line(char *r, char *w, int len)
{

	int expected, ret, n;
	int i;

	ret = n = DEC(*r); 

   	expected = ((n + 2) / 3) << 2;

	for(i = len - 1; i <= expected; i++)		/*XXX*/
		r[i] = ' ';

	r++;

	while(n > 0){
				
		convert(r, w, n);	
		
		r += 4;
		w += 3;
		
		n -= 3;
	}

	return ret;
}

int 
uudecode(char *buf, int len)
{		
	int count;
	char *rp, *wp, *lp;

	if(len <= 0)
		return len;

	lp = rp = wp = buf;

	if(strncmp(buf, "end", 3) == 0)
		return wp - buf;

	if(strncmp(buf, "begin ", 6) == 0){
		while(*rp++ != '\n')
			;			
		lp = rp;
	}		
 
	while(rp - buf < len) {			/*XXX*/

		while(*lp++ != '\n')
					;	

		count = dec_line(rp, wp, lp - rp);
			
		if(count <= 0)
			break;

		rp = lp;		
		wp += count;
	}	
	return wp - buf;	
}

#if 0

#include <stdio.h>
#include <sys/mman.h>

main(int argc, char *argv[])
{	
	FILE *in, *out;

	if(argc < 3) {
		printf("Usage: %s infile outfile", argv[0]);
		exit(1);
	}

	if((in = fopen(argv[1], "r")) == NULL){
		printf("opening the file failed");	
		return 1;
	}
	if((out = fopen(argv[2], "wb")) == NULL){
		printf("opening the file failed");
		return 1;
	}	

	{
		char *p;
		long size;
		fseek(in, 0, SEEK_END);
		size = ftell(in);	
		p = mmap(NULL, size + 4, PROT_READ | PROT_WRITE, MAP_PRIVATE, fileno(in), 0);
		if(p == NULL)
			goto out;

		size = uudecode(p, size);

		if((size > 0) && fwrite(p, 1, size, out) != size)
			printf("writing file job failed");
	}
 out:
	fclose(in);			
	fclose(out);
	return 0;
}
#endif
