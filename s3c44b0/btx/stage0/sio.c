#include <stdarg.h>
#include <sys/types.h>
#include "btx.h"

int
sio_ischar()
{
	
	return __CONCAT(UTRSTAT, CONSOLE_PORT) & 0x1; 
}

int
timed_sio_ischar(int loop)
{
	while(--loop && (!sio_ischar()))
		;
	return loop;
}

int
sio_getc()
{
	while( !sio_ischar() )
		;

	return __CONCAT(URXH, CONSOLE_PORT);
}

int
sio_oschar()
{
	return __CONCAT(UTRSTAT, CONSOLE_PORT) & 0x2;
}

void
sio_putc(char c)
{
	while( !sio_oschar() )
		;
	__CONCAT(UTXH, CONSOLE_PORT) = c;
}

static unsigned int
div(unsigned int x, unsigned int y, unsigned int *r)
{
	unsigned int q;
	
	for(q = 0; x >= y; x -= y, q++)
		;

	if(r)
		*r = x;

	return q;
}

#if 0
static unsigned int
mod(unsigned int x, unsigned int y)
{
	unsigned int r;

	div(x, y, &r);
	return r;
}
#endif

static unsigned int
roundiv(unsigned int x, unsigned int y)
{
	unsigned int q, r, halfy;

	halfy = (y >> 1);

	q = div(x, y, &r);	
	
	return (r < halfy) ? q : (q + 1);
}


static int
xgetc(void)
{
	return sio_getc();
}

int
getchar(void)
{
    int c;

    c = xgetc();
    if (c == '\r')
		c = '\n';
    return c;
}

static int
xputc(int c)
{
	sio_putc(c);
    return c;
}

int
putchar(int c)
{
    if (c == '\n')
		xputc('\r');
    return xputc(c);
}

int
printf(const char *fmt,...)
{
    static const char digits[16] = "0123456789abcdef";
	va_list ap;
	char buf[10];
	char *s;
	int c;
	unsigned int b, u;
	unsigned int q, r;

	va_start(ap, fmt);
	while ((c = *fmt++)) {
		if (c == '%') {
			c = *fmt++;
			switch (c) {
			case 'c':
				putchar(va_arg(ap, int));
				continue;
			case 's':
				for (s = va_arg(ap, char *); *s; s++)
					putchar(*s);
				continue;
			case 'u':
			case 'x':
				b = c == 'u' ? 10U : 16U;
				u = va_arg(ap, unsigned);
				s = buf;
				do {
					q = div(u, b, &r);
					*s++ = digits[r];
				} while (u = q);
				while (--s >= buf)
					putchar(*s);
				continue;
			}
		}
		putchar(c);
	}
	va_end(ap);
	return 0;
}

void
sio_init(void)
{
	__CONCAT(ULCON, CONSOLE_PORT)	= 0x3;
	__CONCAT(UCON, CONSOLE_PORT)	= 0x5;
	__CONCAT(UFCON, CONSOLE_PORT)	= 0x0;
	__CONCAT(UMCON, CONSOLE_PORT)	= 0x3;
	__CONCAT(UBRDIV, CONSOLE_PORT)	= roundiv(MCLK, CONSOLE_SPEED<<4) - 1;
}
