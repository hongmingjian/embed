int
xdownload(char *where)
{
	sio_init();

	printf("** Copyright (C) 2005 Hong MingJian (HMJ)\n");
	printf("** All rights reserved.\n\n");

	printf("** Build at %s %s\n\n", __DATE__, __TIME__);

	printf("Please send the uuencoded program...\n");

	if(uudecode(where, xmodem(where)) <= 0) {
		printf("Failed to download or uudecode the program.\n");
		printf("Please reset the board.\n");
		return 1;
	}

	return 0;
}
