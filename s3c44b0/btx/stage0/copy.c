static void *
memcpy(void *dst, const void *src, unsigned long len)
{
	char *p1, *p2;
	p1 = (char *)dst;
	p2 = (char *)src;

	while(len--)
		*p1++ = *p2++;

	return dst;
}

int
xdownload(char *where)
{
	memcpy(where, (void *)0x400, 63 << 10);		/*XXX*/
	return 0;
}
