#define XMODEM_SOH 0x01
#define XMODEM_EOT 0x04
#define XMODEM_ACK 0x06
#define XMODEM_NAK 0x15
#define XMODEM_CAN 0x18
#define XMODEM_DATA_SIZE 128

struct frame
{
  char   blk_num;
  char   blk_com_num;
  char   data[XMODEM_DATA_SIZE];
  char   cksum;
};

static char
cksum(struct frame *f)
{
  int i;
  int sum = 0;
  for(i = 0; i < XMODEM_DATA_SIZE; i++) {
    sum += f->data[i];		
  }
  return (sum & 0xff);	
}

static int 
get_frame(struct frame *rframe)
{
  int i;

  rframe->blk_num = sio_getc();
  rframe->blk_com_num = sio_getc();

  if(rframe->blk_num != 255 - (rframe->blk_com_num & 255))
    return -1;

  for(i = 0; i < XMODEM_DATA_SIZE; i++) {
      rframe->data[i] = sio_getc();
  }
  rframe->cksum = sio_getc();

  return 0;
}	

int
xmodem(char *p)
{
  struct frame aframe;
  int i, len = 0;
  int loop = 300000;	/* ~3 seconds */
  
  while(1) {

    sio_putc(XMODEM_NAK);

    if(timed_sio_ischar(loop))
      break;
  }
  
  while(1) {	
      switch(sio_getc()) {	
      case XMODEM_EOT:
		sio_putc(XMODEM_ACK);
		return len;
	
      case XMODEM_CAN:
		sio_getc();
		sio_getc();
	
		return -1;
	
      case XMODEM_SOH:
		if((get_frame(&aframe) == 0) && 
		(cksum(&aframe) == aframe.cksum)) {
		    len += XMODEM_DATA_SIZE;
	    
		    /*
		     * copy memory
		     */
		    for(i = 0; i < XMODEM_DATA_SIZE; i++)
		      *p++ = aframe.data[i];
		    
		    /*
		     * send the ACK
		     */
		    sio_putc(XMODEM_ACK);
		} else {
		    sio_putc(XMODEM_NAK);
		}
		break;
      }  
  }	
}

