#ifndef GLOBAL_H
#define GLOBAL_H

#ifndef __ASSEMBLY__
#define	REGL(base, offset)	(*(volatile unsigned int *)(base+offset))
#define	REGW(base, offset)	(*(volatile unsigned short *)(base+offset))
#define	REGB(base, offset)	(*(volatile unsigned char *)(base+offset))
#else
#define	REGL(base, offset)	(base+offset)
#define	REGW(base, offset)	(base+offset)
#define	REGB(base, offset)	(base+offset)
#endif /* __ASSEMBLY__ */

#define outportb(p,d)		((*(volatile char *)(p))=(d))
#define outportw(p,d)		((*(volatile unsigned short *)(p))=(d))
#define outport(p,d)		((*(volatile unsigned long *)(p))=(d))

#define inportb(p)			(*(volatile char *)(p))
#define inportw(p)			(*(volatile unsigned short *)(p))
#define inport(p)			(*(volatile unsigned long *)(p))

#define __roundoff(x)		((int)(x+0.5))
#define __countof(x)		(sizeof(x)/sizeof((x)[0]))
#define __CONCAT1(x,y)		x ## y
#define __CONCAT(x,y)		__CONCAT1(x,y)
#define min(x,y)			(((x)>(y))?(y):(x))
#define max(x,y)			(((x)<(y))?(y):(x))

#define BCD2HEX(n)			(((n)>>4)*10+((n)&0x0f))

#endif /* GLOBAL_H */
