/*
 * upnet.h: UP-NETARM3000 specific defines
 */

#ifndef UPNET_H
#define UPNET_H

#include "s3c44b0x.h"

/*external clock frequency*/
#define	XTAL0		6000000

/*Master(?) clock frequency*/
#define	MCLK		67500000

/*divisors - calculated by pllset*/
#define MDIV		0x52
#define PDIV		0x2
#define SDIV		0x1

/*
	BANK0  BIOS NOR FLASH
	BANK1  USB D12
	BANK2  16M NAND FLASH
	BANK3  UNUSED
	BANK4  UNUSED
	BANK5  RTL8019
	BANK6  SDRAM
	BANK7  UNUSED
 */
#define SDRAM_START	0xc000000
#define SDRAM_END	0xc800000

#ifdef __BIG_ENDIAN
#define ENDIAN	1
#else
#define ENDIAN	0
#endif /*__BIG_ENDIAN*/

/*Bank 0 parameter*/
#define B0_DW	0x1

#define B0_Tacs 0x0
#define B0_Tcos 0x0
#define B0_Tacc	0x6
#define B0_Toch	0x0
#define B0_Tcah	0x0
#define B0_Tpac	0x0
#define B0_PMC	0x0

/*Bank 1 parameter*/
#define B1_DW	0x0
#define B1_WS	0x0
#define B1_ST	0x0

#define B1_Tacs	0x3
#define B1_Tcos	0x3
#define B1_Tacc	0x7
#define B1_Toch	0x3
#define B1_Tcah	0x3
#define B1_Tpac	0x3
#define B1_PMC	0x0

/*Bank 2 parameter*/
#define B2_DW	0x0
#define B2_WS	0x0
#define B2_ST	0x0

#define B2_Tacs	0x3
#define B2_Tcos	0x3
#define B2_Tacc	0x7
#define B2_Toch	0x3
#define B2_Tcah	0x3
#define B2_Tpac	0x3
#define B2_PMC	0x0

/*Bank 3 parameter*/
#define B3_DW	0x0
#define B3_WS	0x0
#define B3_ST	0x0

#define B3_Tacs	0x3
#define B3_Tcos	0x3
#define B3_Tacc	0x7
#define B3_Toch	0x3
#define B3_Tcah	0x3
#define B3_Tpac	0x3
#define B3_PMC	0x0

/*Bank 4 parameter*/
#define B4_DW	0x1
#define B4_WS	0x0
#define B4_ST	0x0

#define B4_Tacs	0x3
#define B4_Tcos	0x3
#define B4_Tacc	0x7
#define B4_Toch	0x3
#define B4_Tcah	0x3
#define B4_Tpac	0x3
#define B4_PMC	0x0

/*Bank 5 parameter*/
#define B5_DW	0x1
#define B5_WS	0x0
#define B5_ST	0x0

#define B5_Tacs	0x3
#define B5_Tcos	0x3
#define B5_Tacc	0x7
#define B5_Toch	0x3
#define B5_Tcah	0x3
#define B5_Tpac	0x3
#define B5_PMC	0x0

/*Bank 6/7*/
#define SCLKEN	0x1
#define BK76MAP	0x0

/*Bank 6 parameter*/
#define B6_DW	0x1
#define B6_WS	0x0
#define B6_ST	0x0

#define B6_MT   MT_SDRAM

#if B6_MT==MT_ROM
#define B6_Tacs	0x3
#define B6_Tcos	0x3
#define B6_Tacc	0x7
#define B6_Toch	0x3
#define B6_Tcah	0x3
#define B6_Tpac	0x3
#define B6_PMC	0x0
#elif B6_MT==MT_FPDRAM
#define B6_Trcd	0x1
#define B6_Tcas	0x1
#define B6_Tcp	0x1
#define B6_CAN	0x2
#elif B6_MT==MT_EDODRAM
#define B6_Trcd	0x1
#define B6_Tcas	0x1
#define B6_Tcp	0x1
#define B6_CAN	0x2
#elif B6_MT==MT_SDRAM
#define B6_Trcd	0x0
#define B6_SCAN	0x0
#endif /*B6_MT*/

#define B6_WBL	0x0
#define B6_TM	0x0
#define B6_CL	0x2
#define B6_BT	0x0
#define B6_BL	0x0


/*Bank 7 parameter*/
#define B7_DW	0x1
#define B7_WS	0x0
#define B7_ST	0x0

#define B7_MT   MT_SDRAM

#if B7_MT==MT_ROM
#define B7_Tacs	0x3
#define B7_Tcos	0x3
#define B7_Tacc	0x7
#define B7_Toch	0x3
#define B7_Tcah	0x3
#define B7_Tpac	0x3
#define B7_PMC	0x0
#elif B7_MT==MT_FPDRAM
#define B7_Trcd	0x1
#define B7_Tcas	0x1
#define B7_Tcp	0x1
#define B7_CAN	0x2
#elif B7_MT==MT_EDODRAM
#define B7_Trcd	0x1
#define B7_Tcas	0x1
#define B7_Tcp	0x1
#define B7_CAN	0x2
#elif B7_MT==MT_SDRAM
#define B7_Trcd	0x0
#define B7_SCAN	0x0
#endif /*B7_MT*/

#define B7_WBL	0x0
#define B7_TM	0x0
#define B7_CL	0x2
#define B7_BT	0x0
#define B7_BL	0x0

/*REFRESH parameter*/
#define REFEN   0x1
#define TREFMD  0x0
#define Trp		0x1
#define Trc     0x1
#define Tchr    0x2
/*
 * REFCNT=2048+1-(MCLK/1MHz)*(refresh period)
 */
#define REFCNT  0x591

/*
 * peripherals
 */
#define NE2K_REGBASE	0x0a000600
#define NE2K_SHIFT(x)	((x)<<1)

#define IRQ_USB		0		/* USB - INT_EINT0 */
#define IRQ_NE2K	1		/* RTL8019 - INT_EINT1 */

#endif /*UPNET_H*/
