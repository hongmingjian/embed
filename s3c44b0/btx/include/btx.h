#ifndef BTX_H
#define BTX_H	1

#include "up-net.h"

#define CONSOLE_PORT	0
#define CONSOLE_SPEED	115200

#define STAGE2_END		SDRAM_END
#define STAGE2_START	(SDRAM_START+0x8)

#define HEAP_SIZE		(640 * 1024)
#define STACK_SIZE		0x1000

#define STACK_SVC		STAGE2_END
#define STACK_FIQ		(STACK_SVC-STACK_SIZE)
#define STACK_IRQ		(STACK_FIQ-STACK_SIZE)
#define STACK_ABT		(STACK_IRQ-STACK_SIZE)
#define STACK_UND		(STACK_ABT-STACK_SIZE)
#define STACK_USR		(STACK_UND-STACK_SIZE)

#define IRQ_TIMER	  	5
#define HZ				100

/*
 * This address should work.
 */
#define	NE2K_MAC0		0x00
#define	NE2K_MAC1		0x05
#define	NE2K_MAC2		0x5d
#define	NE2K_MAC3		0xe3
#define	NE2K_MAC4		0x2c
#define	NE2K_MAC5		0xfc

/*
 * software interrupts
 */
#define SWI_CLOCK			(INT_NR+5)
#define SWI_NET				(INT_NR+4)

#define SWI_NR				6

#define SWI_CLOCK_PENDING	(1 << SWI_CLOCK)	
#define SWI_NET_PENDING		(1 << SWI_NET)	

#define SWI_CLOCK_MASK		SWI_CLOCK_PENDING
#define SWI_NET_MASK		(SWI_NET_PENDING | SWI_CLOCK_MASK)
#define SWI_MASK			(~INT_MASK)

#endif /*BTX_H*/
