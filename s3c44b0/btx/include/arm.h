#ifndef ARM_H
#define ARM_H

/*********************/
/* PSR Registers     */
/*********************/

#define PSR_MODE_USR  0x10
#define PSR_MODE_FIQ  0x11
#define PSR_MODE_IRQ  0x12
#define PSR_MODE_SVC  0x13
#define PSR_MODE_ABT  0x17
#define PSR_MODE_UND  0x1b
#define PSR_MODE_SYS  0x1f
#define PSR_MODE_MASK 0x1f

#define PSR_N		  0x80000000
#define PSR_Z		  0x40000000
#define PSR_C		  0x20000000
#define PSR_V		  0x10000000
#define PSR_I		  0x80
#define PSR_F		  0x40
#define PSR_T		  0x20

#endif /*ARM_H*/
