/*
 * Copyright (C) 2005 Hong MingJian (HMJ)
 * All rights reserved.
 */
#ifndef NE2K_H
#define NE2K_H 1

#include "global.h"
#include "up-net.h"

#ifndef NE2K_SHIFT
#define NE2K_SHIFT(x)	(x)
#endif /*NE2K_SHIFT*/

#define	NE2K_CR			REGB(NE2K_REGBASE,NE2K_SHIFT(0x00))

#define NE2K_RDMA		    (NE2K_REGBASE+NE2K_SHIFT(0x10))

#define	NE2K_RST		REGB(NE2K_REGBASE,NE2K_SHIFT(0x1f))

/*
 * Page 0 - Read
 */
#define	NE2K_CLDA0		REGB(NE2K_REGBASE,NE2K_SHIFT(0x01))
#define	NE2K_CLDA1		REGB(NE2K_REGBASE,NE2K_SHIFT(0x02))
#define	NE2K_BNRY		REGB(NE2K_REGBASE,NE2K_SHIFT(0x03))
#define	NE2K_TSR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x04))
#define	NE2K_NCR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x05))
#define	NE2K_FIFO		REGB(NE2K_REGBASE,NE2K_SHIFT(0x06))
#define	NE2K_ISR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x07))
#define	NE2K_CRDA0		REGB(NE2K_REGBASE,NE2K_SHIFT(0x08))
#define	NE2K_CRDA1		REGB(NE2K_REGBASE,NE2K_SHIFT(0x09))
#define	NE2K_8019ID0	REGB(NE2K_REGBASE,NE2K_SHIFT(0x0a))
#define	NE2K_8019ID1	REGB(NE2K_REGBASE,NE2K_SHIFT(0x0b))
#define	NE2K_RSR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0c))
#define	NE2K_CNTR0		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0d))
#define	NE2K_CNTR1		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0e))
#define	NE2K_CNTR2		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0f))

/*
 * Page 0 - Write
 */
#define	NE2K_PSTART		REGB(NE2K_REGBASE,NE2K_SHIFT(0x01))
#define	NE2K_PSTOP		REGB(NE2K_REGBASE,NE2K_SHIFT(0x02))
//#define	NE2K_BNRY		REGB(NE2K_REGBASE,NE2K_SHIFT(0x03))
#define	NE2K_TPSR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x04))
#define	NE2K_TBCR0		REGB(NE2K_REGBASE,NE2K_SHIFT(0x05))
#define	NE2K_TBCR1		REGB(NE2K_REGBASE,NE2K_SHIFT(0x06))
//#define	NE2K_ISR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x07))
#define	NE2K_RSAR0		REGB(NE2K_REGBASE,NE2K_SHIFT(0x08))
#define	NE2K_RSAR1		REGB(NE2K_REGBASE,NE2K_SHIFT(0x09))
#define	NE2K_RBCR0		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0a))
#define	NE2K_RBCR1		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0b))
#define	NE2K_RCR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0c))
#define	NE2K_TCR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0d))
#define	NE2K_DCR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0e))
#define	NE2K_IMR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0f))


/*
 * Page 1 - R/W
 */
#define	NE2K_PAR0		REGB(NE2K_REGBASE,NE2K_SHIFT(0x01))
#define	NE2K_PAR1		REGB(NE2K_REGBASE,NE2K_SHIFT(0x02))
#define	NE2K_PAR2		REGB(NE2K_REGBASE,NE2K_SHIFT(0x03))
#define	NE2K_PAR3		REGB(NE2K_REGBASE,NE2K_SHIFT(0x04))
#define	NE2K_PAR4		REGB(NE2K_REGBASE,NE2K_SHIFT(0x05))
#define	NE2K_PAR5		REGB(NE2K_REGBASE,NE2K_SHIFT(0x06))
#define	NE2K_CURR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x07))
#define	NE2K_MAR0		REGB(NE2K_REGBASE,NE2K_SHIFT(0x08))
#define	NE2K_MAR1		REGB(NE2K_REGBASE,NE2K_SHIFT(0x09))
#define	NE2K_MAR2		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0a))
#define	NE2K_MAR3		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0b))
#define	NE2K_MAR4		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0c))
#define	NE2K_MAR5		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0d))
#define	NE2K_MAR6		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0e))
#define	NE2K_MAR7		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0f))

/*
 * Page 2
 */
#define	NE2K_PSTART		REGB(NE2K_REGBASE,NE2K_SHIFT(0x01))
#define	NE2K_PSTOP		REGB(NE2K_REGBASE,NE2K_SHIFT(0x02))









//#define	NE2K_RCR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0c))
//#define	NE2K_TCR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0d))
//#define	NE2K_DCR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0e))
//#define	NE2K_IMR		REGB(NE2K_REGBASE,NE2K_SHIFT(0x0f))

/*
 * ne2k header followed by the ethernet packet
 */
struct ne2k_header {
	unsigned char rsr;
	unsigned char next;
	short count;
};


/*
 * Copyright (C) 1993, David Greenman. This software may be used, modified,
 *   copied, distributed, and sold, in both source and binary form provided
 *   that the above copyright and these terms are retained. Under no
 *   circumstances is the author responsible for the proper functioning
 *   of this software, nor does the author assume any responsibility
 *   for damages incurred with its use.
 *
 */

/*
 *		Command Register (CR) definitions
 */

/*
 * STP: SToP. Software reset command. Takes the controller offline. No
 *	packets will be received or transmitted. Any reception or
 *	transmission in progress will continue to completion before
 *	entering reset state. To exit this state, the STP bit must
 *	reset and the STA bit must be set. The software reset has
 *	executed only when indicated by the RST bit in the ISR being
 *	set.
 */
#define NE2K_CR_STP	0x01

/*
 * STA: STArt. This bit is used to activate the NIC after either power-up,
 *	or when the NIC has been put in reset mode by software command
 *	or error.
 */
#define NE2K_CR_STA	0x02

/*
 * TXP: Transmit Packet. This bit must be set to indicate transmission of
 *	a packet. TXP is internally reset either after the transmission is
 *	completed or aborted. This bit should be set only after the Transmit
 *	Byte Count and Transmit Page Start register have been programmed.
 */
#define NE2K_CR_TXP	0x04

/*
 * RD0, RD1, RD2: Remote DMA Command. These three bits control the operation
 *	of the remote DMA channel. RD2 can be set to abort any remote DMA
 *	command in progress. The Remote Byte Count registers should be cleared
 *	when a remote DMA has been aborted. The Remote Start Addresses are not
 *	restored to the starting address if the remote DMA is aborted.
 *
 *	RD2 RD1 RD0	function
 *	 0   0   0	not allowed
 *	 0   0   1	remote read
 *	 0   1   0	remote write
 *	 0   1   1	send packet
 *	 1   X   X	abort
 */
#define NE2K_CR_RD0	0x08
#define NE2K_CR_RD1	0x10
#define NE2K_CR_RD2	0x20

/*
 * PS0, PS1: Page Select. The two bits select which register set or 'page' to
 *	access.
 *
 *	PS1 PS0		page
 *	 0   0		0
 *	 0   1		1
 *	 1   0		2
 *	 1   1		reserved
 */
#define NE2K_CR_PS0	0x40
#define NE2K_CR_PS1	0x80
/* bit encoded aliases */
#define NE2K_CR_PAGE_0	0x00 /* (for consistency) */
#define NE2K_CR_PAGE_1	0x40
#define NE2K_CR_PAGE_2	0x80

/*
 *		Interrupt Status Register (ISR) definitions
 */

/*
 * PRX: Packet Received. Indicates packet received with no errors.
 */
#define NE2K_ISR_PRX	0x01

/*
 * PTX: Packet Transmitted. Indicates packet transmitted with no errors.
 */
#define NE2K_ISR_PTX	0x02

/*
 * RXE: Receive Error. Indicates that a packet was received with one or more
 *	the following errors: CRC error, frame alignment error, FIFO overrun,
 *	missed packet.
 */
#define NE2K_ISR_RXE	0x04

/*
 * TXE: Transmission Error. Indicates that an attempt to transmit a packet
 *	resulted in one or more of the following errors: excessive
 *	collisions, FIFO underrun.
 */
#define NE2K_ISR_TXE	0x08

/*
 * OVW: OverWrite. Indicates a receive ring-buffer overrun. Incoming network
 *	would exceed (has exceeded?) the boundary pointer, resulting in data
 *	that was previously received and not yet read from the buffer to be
 *	overwritten.
 */
#define NE2K_ISR_OVW	0x10

/*
 * CNT: Counter Overflow. Set when the MSB of one or more of the Network Talley
 *	Counters has been set.
 */
#define NE2K_ISR_CNT	0x20

/*
 * RDC: Remote Data Complete. Indicates that a Remote DMA operation has completed.
 */
#define NE2K_ISR_RDC	0x40

/*
 * RST: Reset status. Set when the NIC enters the reset state and cleared when a
 *	Start Command is issued to the CR. This bit is also set when a receive
 *	ring-buffer overrun (OverWrite) occurs and is cleared when one or more
 *	packets have been removed from the ring. This is a read-only bit.
 */
#define NE2K_ISR_RST	0x80

/*
 *		Interrupt Mask Register (IMR) definitions
 */

/*
 * PRXE: Packet Received interrupt Enable. If set, a received packet will cause
 *	an interrupt.
 */
#define NE2K_IMR_PRX	0x01

/*
 * PTXE: Packet Transmit interrupt Enable. If set, an interrupt is generated when
 *	a packet transmission completes.
 */
#define NE2K_IMR_PTX	0x02

/*
 * RXEE: Receive Error interrupt Enable. If set, an interrupt will occur whenever a
 *	packet is received with an error.
 */
#define NE2K_IMR_RXE 0x04

/*
 * TXEE: Transmit Error interrupt Enable. If set, an interrupt will occur whenever
 *	a transmission results in an error.
 */
#define NE2K_IMR_TXE	0x08

/*
 * OVWE: OverWrite error interrupt Enable. If set, an interrupt is generated whenever
 *	the receive ring-buffer is overrun. i.e. when the boundary pointer is exceeded.
 */
#define NE2K_IMR_OVW	0x10

/*
 * CNTE: Counter overflow interrupt Enable. If set, an interrupt is generated whenever
 *	the MSB of one or more of the Network Statistics counters has been set.
 */
#define NE2K_IMR_CNT	0x20

/*
 * RDCE: Remote DMA Complete interrupt Enable. If set, an interrupt is generated
 *	when a remote DMA transfer has completed.
 */
#define NE2K_IMR_RDC	0x40

/*
 * bit 7 is unused/reserved
 */

/*
 *		Data Configuration Register (DCR) definitions
 */

/*
 * WTS: Word Transfer Select. WTS establishes byte or word transfers for
 *	both remote and local DMA transfers
 */
#define NE2K_DCR_WTS	0x01

/*
 * BOS: Byte Order Select. BOS sets the byte order for the host.
 *	Should be 0 for 80x86, and 1 for 68000 series processors
 */
#define NE2K_DCR_BOS	0x02

/*
 * LAS: Long Address Select. When LAS is 1, the contents of the remote
 *	DMA registers RSAR0 and RSAR1 are used to provide A16-A31
 */
#define NE2K_DCR_LAS	0x04

/*
 * LS: Loopback Select. When 0, loopback mode is selected. Bits D1 and D2
 *	of the TCR must also be programmed for loopback operation.
 *	When 1, normal operation is selected.
 */
#define NE2K_DCR_LS	0x08

/*
 * AR: Auto-initialize Remote. When 0, data must be removed from ring-buffer
 *	under program control. When 1, remote DMA is automatically initiated
 *	and the boundary pointer is automatically updated
 */
#define NE2K_DCR_AR	0x10

/*
 * FT0, FT1: Fifo Threshold select.
 *		FT1	FT0	Word-width	Byte-width
 *		 0	 0	1 word		2 bytes
 *		 0	 1	2 words		4 bytes
 *		 1	 0	4 words		8 bytes
 *		 1	 1	8 words		12 bytes
 *
 *	During transmission, the FIFO threshold indicates the number of bytes
 *	or words that the FIFO has filled from the local DMA before BREQ is
 *	asserted. The transmission threshold is 16 bytes minus the receiver
 *	threshold.
 */
#define NE2K_DCR_FT0	0x20
#define NE2K_DCR_FT1	0x40

/*
 * bit 7 (0x80) is unused/reserved
 */

/*
 *		Transmit Configuration Register (TCR) definitions
 */

/*
 * CRC: Inhibit CRC. If 0, CRC will be appended by the transmitter, if 0, CRC
 *	is not appended by the transmitter.
 */
#define NE2K_TCR_CRC	0x01

/*
 * LB0, LB1: Loopback control. These two bits set the type of loopback that is
 *	to be performed.
 *
 *	LB1 LB0		mode
 *	 0   0		0 - normal operation (DCR_LS = 0)
 *	 0   1		1 - internal loopback (DCR_LS = 0)
 *	 1   0		2 - external loopback (DCR_LS = 1)
 *	 1   1		3 - external loopback (DCR_LS = 0)
 */
#define NE2K_TCR_LB0	0x02
#define NE2K_TCR_LB1	0x04

/*
 * ATD: Auto Transmit Disable. Clear for normal operation. When set, allows
 *	another station to disable the NIC's transmitter by transmitting to
 *	a multicast address hashing to bit 62. Reception of a multicast address
 *	hashing to bit 63 enables the transmitter.
 */
#define NE2K_TCR_ATD	0x08

/*
 * OFST: Collision Offset enable. This bit when set modifies the backoff
 *	algorithm to allow prioritization of nodes.
 */
#define NE2K_TCR_OFST	0x10

/*
 * bits 5, 6, and 7 are unused/reserved
 */

/*
 *		Transmit Status Register (TSR) definitions
 */

/*
 * PTX: Packet Transmitted. Indicates successful transmission of packet.
 */
#define NE2K_TSR_PTX	0x01

/*
 * bit 1 (0x02) is unused/reserved
 */

/*
 * COL: Transmit Collided. Indicates that the transmission collided at least
 *	once with another station on the network.
 */
#define NE2K_TSR_COL	0x04

/*
 * ABT: Transmit aborted. Indicates that the transmission was aborted due to
 *	excessive collisions.
 */
#define NE2K_TSR_ABT	0x08

/*
 * CRS: Carrier Sense Lost. Indicates that carrier was lost during the
 *	transmission of the packet. (Transmission is not aborted because
 *	of a loss of carrier)
 */
#define NE2K_TSR_CRS	0x10

/*
 * FU: FIFO Underrun. Indicates that the NIC wasn't able to access bus/
 *	transmission memory before the FIFO emptied. Transmission of the
 *	packet was aborted.
 */
#define NE2K_TSR_FU	0x20

/*
 * CDH: CD Heartbeat. Indicates that the collision detection circuitry
 *	isn't working correctly during a collision heartbeat test.
 */
#define NE2K_TSR_CDH	0x40

/*
 * OWC: Out of Window Collision: Indicates that a collision occurred after
 *	a slot time (51.2us). The transmission is rescheduled just as in
 *	normal collisions.
 */
#define NE2K_TSR_OWC	0x80

/*
 *		Receiver Configuration Register (RCR) definitions
 */

/*
 * SEP: Save Errored Packets. If 0, error packets are discarded. If set to 1,
 *	packets with CRC and frame errors are not discarded.
 */
#define NE2K_RCR_SEP	0x01

/*
 * AR: Accept Runt packet. If 0, packet with less than 64 byte are discarded.
 *	If set to 1, packets with less than 64 byte are not discarded.
 */
#define NE2K_RCR_AR	0x02

/*
 * AB: Accept Broadcast. If set, packets sent to the broadcast address will be
 *	accepted.
 */
#define NE2K_RCR_AB	0x04

/*
 * AM: Accept Multicast. If set, packets sent to a multicast address are checked
 *	for a match in the hashing array. If clear, multicast packets are ignored.
 */
#define NE2K_RCR_AM	0x08

/*
 * PRO: Promiscuous Physical. If set, all packets with a physical addresses are
 *	accepted. If clear, a physical destination address must match this
 *	station's address. Note: for full promiscuous mode, RCR_AB and RCR_AM
 *	must also be set. In addition, the multicast hashing array must be set
 *	to all 1's so that all multicast addresses are accepted.
 */
#define NE2K_RCR_PRO	0x10

/*
 * MON: Monitor Mode. If set, packets will be checked for good CRC and framing,
 *	but are not stored in the ring-buffer. If clear, packets are stored (normal
 *	operation).
 */
#define NE2K_RCR_MON	0x20

/*
 * INTT: Interrupt Trigger Mode for AX88190.
 */
#define NE2K_RCR_INTT	0x40

/*
 * bit 7 is unused/reserved.
 */

/*
 *		Receiver Status Register (RSR) definitions
 */

/*
 * PRX: Packet Received without error.
 */
#define NE2K_RSR_PRX	0x01

/*
 * CRC: CRC error. Indicates that a packet has a CRC error. Also set for frame
 *	alignment errors.
 */
#define NE2K_RSR_CRC	0x02

/*
 * FAE: Frame Alignment Error. Indicates that the incoming packet did not end on
 *	a byte boundary and the CRC did not match at the last byte boundary.
 */
#define NE2K_RSR_FAE	0x04

/*
 * FO: FIFO Overrun. Indicates that the FIFO was not serviced (during local DMA)
 *	causing it to overrun. Reception of the packet is aborted.
 */
#define NE2K_RSR_FO	0x08

/*
 * MPA: Missed Packet. Indicates that the received packet couldn't be stored in
 *	the ring-buffer because of insufficient buffer space (exceeding the
 *	boundary pointer), or because the transfer to the ring-buffer was inhibited
 *	by RCR_MON - monitor mode.
 */
#define NE2K_RSR_MPA	0x10

/*
 * PHY: Physical address. If 0, the packet received was sent to a physical address.
 *	If 1, the packet was accepted because of a multicast/broadcast address
 *	match.
 */
#define NE2K_RSR_PHY	0x20

/*
 * DIS: Receiver Disabled. Set to indicate that the receiver has entered monitor
 *	mode. Cleared when the receiver exits monitor mode.
 */
#define NE2K_RSR_DIS	0x40

/*
 * DFR: Deferring. Set to indicate a 'jabber' condition. The CRS and COL inputs
 *	are active, and the transceiver has set the CD line as a result of the
 *	jabber.
 */
#define NE2K_RSR_DFR	0x80

/*
 * 				Common constants
 */
#define NE2K_PAGE_SIZE	256		/* Size of RAM pages in bytes */

/*
 * Copyright (C) 2005 Hong MingJian (HMJ)
 * All rights reserved.
 */
#define NE2K_RECV_START	0x4c
#define NE2K_RECV_STOP	0x80
#define NE2K_SEND_START	0x40

#endif /*NE2K_H*/
